#pragma once
/*
 *  SfMUpdateListener.h
 *  SfMToyExample
 *
 *  Created by Roy Shilkrot on 10/7/12.
 *
 */
#include <opencv2/core/core.hpp>
#include "../Interface/Visualization.h"
#include <iostream>
#include <string.h>


class SfMUpdateListener
{
public:
    virtual void update(std::vector<cv::Point3d> pcld,
                        std::vector<cv::Vec3b> pcldrgb,
                        std::vector<cv::Matx34d> cameras,
                        std::vector<int> cameras_indexes) = 0;
};


class VisualizerListener : public SfMUpdateListener {


public:
    void update(std::vector<cv::Point3d> pcld,
                std::vector<cv::Vec3b> pcldrgb,
                std::vector<cv::Matx34d> cameras,
                std::vector<int> cameras_indexes) {

        ShowClouds(pcld, pcldrgb/*, pcld_alternate, pcldrgb_alternate*/);
        //clearCameras();

        std::vector<cv::Matx34d> v = cameras;
        for(unsigned int i=0;i<v.size();i++) {
            std::stringstream ss; ss << "camera" << i;
            cv::Matx33f R;
            R(0,0)=v[i](0,0); R(0,1)=v[i](0,1); R(0,2)=v[i](0,2);
            R(1,0)=v[i](1,0); R(1,1)=v[i](1,1); R(1,2)=v[i](1,2);
            R(2,0)=v[i](2,0); R(2,1)=v[i](2,1); R(2,2)=v[i](2,2);

            /*R = cv::Matx33d(1,0,0,
                            0,1,0,
                            0,0,1);
//*/
            //std::cout << ss.str() << " " << i <<std::endl;
            //dir, right, up, pos, color,
            float scale = 1.0;
            float x = v[i](0,3) + 0.0;
            float y = v[i](1,3) + 0.0;
            float z = v[i](2,3) + 0.0;
            int indx = cameras_indexes[i];
            /*
            if(indx == 0)  visualizerShowCamera(R,cv::Vec3f(x,y,z),50 ,  0,  0,0.2,ss.str());
            if(indx == 1)  visualizerShowCamera(R,cv::Vec3f(x,y,z),100,  0,  0,0.2,ss.str());
            if(indx == 2)  visualizerShowCamera(R,cv::Vec3f(x,y,z),150,  0,  0,0.2,ss.str());
            if(indx == 3)  visualizerShowCamera(R,cv::Vec3f(x,y,z),200,  0,  0,0.2,ss.str());
            if(indx == 4)  visualizerShowCamera(R,cv::Vec3f(x,y,z),255,  0,  0,0.2,ss.str());
            if(indx == 5)  visualizerShowCamera(R,cv::Vec3f(x,y,z),  0, 50,  0,0.2,ss.str());
            if(indx == 6)  visualizerShowCamera(R,cv::Vec3f(x,y,z),  0,100,255,0.2,ss.str());
            if(indx == 7)  visualizerShowCamera(R,cv::Vec3f(x,y,z),  0,150,255,0.2,ss.str());
            if(indx == 8)  visualizerShowCamera(R,cv::Vec3f(x,y,z),  0,200,255,0.2,ss.str());
            if(indx == 9)  visualizerShowCamera(R,cv::Vec3f(x,y,z),  0,255,255,0.2,ss.str());
            if(indx == 10) visualizerShowCamera(R,cv::Vec3f(x,y,z),  0,  0, 50,0.2,ss.str());
            if(indx == 11) visualizerShowCamera(R,cv::Vec3f(x,y,z),  0,  0,100,0.2,ss.str());
            if(indx == 12) visualizerShowCamera(R,cv::Vec3f(x,y,z),  0,  0,150,0.2,ss.str());
            if(indx == 13) visualizerShowCamera(R,cv::Vec3f(x,y,z),  0,  0,200,0.2,ss.str());
            if(indx == 14) visualizerShowCamera(R,cv::Vec3f(x,y,z),  0,  0,255,0.2,ss.str());
            if(indx == 15) visualizerShowCamera(R,cv::Vec3f(x,y,z), 50, 50,  0,0.2,ss.str());
            if(indx == 16) visualizerShowCamera(R,cv::Vec3f(x,y,z),100, 50,  0,0.2,ss.str());
            if(indx == 17) visualizerShowCamera(R,cv::Vec3f(x,y,z),150, 50,  0,0.2,ss.str());
            if(indx == 18) visualizerShowCamera(R,cv::Vec3f(x,y,z),200, 50,  0,0.2,ss.str());
            if(indx == 19) visualizerShowCamera(R,cv::Vec3f(x,y,z),255, 50,  0,0.2,ss.str());
            if(indx == 20) visualizerShowCamera(R,cv::Vec3f(x,y,z), 50,100,  0,0.2,ss.str());
            if(indx == 21) visualizerShowCamera(R,cv::Vec3f(x,y,z),100,100,  0,0.2,ss.str());
            if(indx == 22) visualizerShowCamera(R,cv::Vec3f(x,y,z),150,100,  0,0.2,ss.str());
            if(indx == 23) visualizerShowCamera(R,cv::Vec3f(x,y,z),200,100,  0,0.2,ss.str());
            if(indx == 24) visualizerShowCamera(R,cv::Vec3f(x,y,z),255,100,  0,0.2,ss.str());
            if(indx > 24)  visualizerShowCamera(R,cv::Vec3f(x,y,z),150,150,150,0.2,ss.str());
            */
            visualizerShowCamera(R,cv::Vec3f(x,y,z),255,  0,  0,0.2,ss.str());
                               //  std::cout<< "COORD " << 2.0+v[i](0,3) << std::endl;

        }

        return ;
    }
};
