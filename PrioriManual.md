### Prioritized Features


To build prioritized models based on a structure from motion model run:

```
	./GeocodeUI p build <path_to_cloud(string)> <location(string)>
```

If the photographs contained by the original model have the associated GPS coordinates, a transformation matrix is computed for the prioritized models. This matrix is then used to compute the GPS coordinates of images to geocode. The GPS coordinates can be injected to each model photograph (using the standard exif GPS tags), or by composing a file named 'coordinates.txt' with the following structure. This file needs to be located within the model folder.

```
[Number of Images]
[Image1] [Latitude] [Longitude] [Altitude]
[Image2] [Latitude] [Longitude] [Altitude]
[Image3] [Latitude] [Longitude] [Altitude]
    .
    .
    .
[ImageX] [Latitude] [Longitude] [Altitude]
```
Images which the GPS is unknown must be filled with -200 -200 -200. This is a naive identifier to indicate which photographs aren't geocoded. Further implemenation versions will not require this identifier.

Building prioritized models will create and store two point cloud files (seed cloud and compressed cloud) along with their original model in the prioritized's database. 
If you added a model by mistake to the database, the following option removes models from the database:

```
       ./GeocodeUI p rem <model_id>
```

To check model_id's, run:

```
      ./GeocodeUI p models
```

The prototype is ready to geocode new photographs whenever enough models are added. All the photographs to geocode need to be within the same folder and that folder needs to contain a .txt with the list of images to pose:

```
[Image_name1]
[Image_name2]
[Image_name3]
      .
      .
      .
[Image_nameX]
``` 

Then you simply need to run:

```
	./GeocodeUI p pose <path_imgs_folder>
```

After the geocoding process is finished, it will be outputed 2 files: 'output_statistics.txt' containing overall information of geocoded images, and 'output_viz.txt' which can be used to visualize the geocoded images placed within the 3D models. To run the output visualization, run:

```
	./GeocodeUI g <path_to_outputviz>
```

The PCL visualizer will be opened and 3D models may be explored. To change to the next model just type a letter and press "Enter" within the terminal. To quit the visualizer press 'q'.

