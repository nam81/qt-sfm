#include <iostream>
#include <string.h>
#include <time.h>

#include "Interface/Visualization.h"
#include "Interface/SfMUpdateListener.h"
#include "Code/IO/IOCommon.h"
#include "Code/SynthViews/SynthMain.h"
#include "Code/IO/ConfigFile.h"
#include "Code/PrioriFeats/PrioriMain.h"


using namespace std;

//#define NO_VIZ

void ProcessFurtherOptions(int first_op, int argc, char* argv[], ConfigOptions configs){

    std::string op = std::string(argv[2]);

    if(op == "pose"){
        std::cout << " [Pose]" << std::endl;

        if(argc == 4){

            if(first_op == 0){
                GeolocalizeSynthetic(argv[3],configs);
            }else if(first_op == 1){
                GeolocalizePrioritized(argv[3],configs);
            }
        }else{
            std::cout << "[Pose Estimation] Wrong number of arguments" << std::endl;
            std::cout << "Example: ./Geocode s/p pose path_to_imgs" << std::endl;
            exit(0);
        }

        return;

    }else if(op == "build"){
        std::cout << " [Build]" << std::endl;

        if(argc == 5){
            if(first_op == 0){
                BuildSyntheticViews(std::string(argv[3]),1,std::string(argv[4]),configs);
            }else if(first_op == 1){
                BuildPrioritizedCloud(std::string(argv[3]),1,std::string(argv[4]),configs);
            }

        }else{
            std::cout << "[Cloud Compressor] Wrong number of arguments" << std::endl;
            std::cout << "Example: ./Geocode s/p build cloud_path(string) description(string)" << std::endl;
            exit(0);
        }

        return;

    }else if(op == "learn"){
        std::cout << " [Learn]" << std::endl;

        if(first_op == 1)
        {
            std::cout << "Prioritized Features doesn't use Vocabulary Tree " << std::endl;
            return;
        }

        AddViewsToVocTree(std::string(configs.synth_root),std::string(configs.vocab_learn_path), std::string(configs.vocab_data_path));
        return;

    }else if(op == "rem"){
        std::cout << " [Remove Model]" << std::endl;

        if(argc == 4){
            if(first_op == 0)
                RemoveSyntheticModel(std::string(argv[3]), std::string(configs.synth_root), std::string(configs.vocab_learn_path), std::string(configs.vocab_data_path));
            else if(first_op == 1)
                RemovePrioriModel(std::string(argv[3]), configs.priori_root);
        }else{
            std::cout << "[Remove Model] Wrong number of arguments" << std::endl;
            std::cout << "Example: ./Geocode s/p model_id(int)" << std::endl;
            std::cout << "   To check current model_ids, run ./Geocode (s/p) models" << std::endl;
        }

        return;

    }else if(op == "reset"){
        std::cout << " [Reset DB]" << std::endl;

        if(first_op == 0)
            ResetSynthGeoDB(std::string(configs.synth_root), std::string(configs.vocab_learn_path), std::string(configs.vocab_data_path));
        else if(first_op == 1)
            ResetPrioriGeoDB(std::string(configs.priori_root));

        return;

    }else if(op == "models"){
        std::cout << " [Models]" << std::endl;

        if(first_op == 0)
            PrintSynthModels(std::string(configs.synth_root), std::string(configs.vocab_learn_path), std::string(configs.vocab_data_path));
        else if(first_op == 1)
            PrintPrioriModels(std::string(configs.priori_root));

        return;
    }else{
        std::cout << "Unknown option --> " << argv[2] << std::endl;
    }

    return;
}

void ProcessArgV(int argc, char* argv[], ConfigOptions configs){


    if(argc == 0)
        return;


    switch(argv[1][0]){
    case 's':
    {
        std::cout << "[Synthetics] ";

        ProcessFurtherOptions(0, argc, argv, configs);
        break;
    }

    case 'p':
    {
        std::cout << "[Prioritized] ";

        ProcessFurtherOptions(1, argc, argv, configs);
        break;
    }
    case 'c':
    {
        std::cout << "[Synthetics] ";

        ProcessFurtherOptions(2, argc, argv, configs);
        break;
    }

    case 'g':
    {
        std::cout << "[Output Vizualization] " << std::endl;

        if(argc < 3){
            std::cerr << "Path to the visualization file not defined" << std::endl;
            exit(0);
        }

        std::map<std::string, std::vector<int> > posed_list;
        std::vector<std::string> posed_names;
        std::vector<cv::Matx34d> posed_P;

        if(!ReadVizOutput((std::string(argv[2])).c_str(), posed_list,posed_names,posed_P)){
            std::cout << "Can't read " << std::string(argv[2]) << std::endl;
            exit(0);
        }

#ifdef GEO_DEBUG
        std::cout << "[Successfully Posed " << posed_names.size() << " Images] " << std::endl;

        for(int i = 0; i < posed_names.size(); i++)
            std::cout << "   " << posed_names[i] << std::endl;
#endif

#ifndef NO_VIZ
        cv::Ptr<VisualizerListener> visualizerListener;
        visualizerListener = new VisualizerListener; //with ref-count

        RunVisualizationThread();

        int cc = 0, counter = posed_list.size();
        for(std::map<std::string, std::vector<int> >::iterator iter = posed_list.begin(); iter != posed_list.end(); iter++){

            std::map<int,cv::Matx34d> ppmats;
            std::vector<cv::Point3d> cloud;
            std::vector<cv::Vec3b> ccloud;
            LoadFromNVM(iter->first.c_str(),ppmats,cloud,ccloud,1);

            std::cout << "[ShowModel] Posed images for model " << iter->first << std::endl;

            std::vector<cv::Matx34d> cams;//=distance->getCameras();
            std::vector<int> cams_idx;// = distance->getCamerasIndexes();
            std::vector<int> model_to_img = iter->second;

            for(int i = 0; i < model_to_img.size(); i++){
                cams_idx.push_back(model_to_img[i]);
                cams.push_back(posed_P[model_to_img[i]]);
                std::cout << "   " << posed_names[model_to_img[i]] << std::endl;
            }

            visualizerListener->update(cloud,
                                       ccloud,
                                       cams,
                                       cams_idx);
#ifdef GEO_DEBUG
            std::cout << "Press a key for the next model..." << std::endl;
#endif

            if(cc + 1 < counter){
                char waitkey;
                std::cin >> waitkey;
            }
            cc++;
        }

        WaitForVisualizationThread();//*/
#endif
        break;
    }

    case 'v' :
    {
        std::cout << "[Cloud Vizualization] " << std::endl;

        std::vector<int> indexes;
        std::map<int, cv::Matx34d> pmats;
        std::vector<cv::Matx34d> cams;
        std::vector<cv::Point3d> pc;
        std::vector<cv::Vec3b> pcrgb;
        std:vector<cv::Point3f> pcl_normals;

        VisualizerListener *visualizerListener = new VisualizerListener; //with ref-count

        RunVisualizationThread();

        clock_t start_nvm = clock();


        if(!LoadFromNVM(argv[3],pmats,pc,pcrgb,atoi(argv[2]))){
            std::cout << "[Loader] Couldn't load nvm file (" << std::string(argv[3]) << ")" << std::endl;
            exit(0);
        }

        clock_t end_nvm = clock();

        std::cout << "[TIMER] Nvm loader: " << (double) (end_nvm - start_nvm) / CLOCKS_PER_SEC << std::endl;

        for(std::map<int, cv::Matx34d>::iterator iter = pmats.begin(); iter != pmats.end(); iter++){
            cams.push_back(iter->second);
            indexes.push_back(iter->first);

        }

        visualizerListener->update(pc,
                                   pcrgb,
                                   cams,
                                   indexes);

        WaitForVisualizationThread();

        break;
    }
    default:
    {
        std::cout << "Wrong arguments..." << std::endl;
        std::cout << "Exiting now!" << std::endl;
        break;
    }
    }
}


//---------------------------- Using command-line ----------------------------

int main(int argc, char* argv[]) {


#ifdef GEO_DEBUG
    std::cout << "[DEBUG MODE] On" << std::endl;
#else
    std::cout << "[DEBUG MODE] Off" << std::endl;
#endif

    ConfigOptions configs;
    ReadConfigFile1("../config.ini",configs);

    ProcessArgV(argc,argv,configs);

    return 0;

}
