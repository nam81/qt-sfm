# **Image Geocoding **
-----------------
## **Dependencies**

#### Needed by this project
- Opencv 2.4.6.1
- Eigen 3.2                                   --> eigen.tuxfamily.org/
- pcl 1.7 (Point Cloud Library)               --> pointclouds.org/‎
- vtk-5.8 (Visualization ToolKit)             --> apt-get install libvtk5.8-dev
- boost 1.48 or later                         --> apt-get install libboost1.49-dev
- SQLite

#### Optional
- Cuda Toolkit for SiftGPU and/or Multicore BA

#### Inside the project
- SiftGPU v-400                               --> http://cs.unc.edu/~ccwu/siftgpu/
- Multi-core Bundle Adjustment                --> http://grail.cs.washington.edu/projects/mcba/
- exiv2                                       --> http://www.exiv2.org/
- VocabTree2 by Noah Snavely                  --> https://github.com/snavely/VocabTree2
- Triangulation and Pose Estimation functions --> https://github.com/royshil/SfM-Toy-Library/tree/master/SfMToyLib

-----------------

## **How to make**

Some preparations need to be done in order to run GPU version of SiftGPU. Go to 3rdparty/siftgpu/ and ensure that the makefile variable CUDA_INSTALL_PATH has the correct path to your CUDA toolkit. Currently there is a known bug in which compiling Parrallel Bundle Adjustment GPU version with SiftGPU causes the prototype to crash when extracting features. Since the bundle adjustment is used to solve small local adjustments, using PBA on CPU still delivers instant processing. 

To begin the compilation go to 3rdparty/ and execute:
```
        sudo make
```
On the project root, execute:
```
        mkdir build && cd build/
        cmake ..
        make
```

#### Issues

1. There may be a problem with linking libsiftgpu.so and libpba.so to the project. If the project starts with an error, try moving these 2 libraries to the /usr/lib/ folder.
```
        sudo cp 3rdparty/siftgpu/bin/libsiftgpu.so /usr/lib/
        sudo cp 3rdparty/pba/bin/libpba.so /usr/lib/
```

-----------------

## **Geocoding photographs**

This prototype requires 3D structure from motion models built by VisualSFM (http://ccwu.me/vsfm/)

Available synthetic's options:

```
	./GeocodeUI s build <path_to_cloud(string)> <location(string)>
	./GeocodeUI s learn
	./GeocodeUI s pose <path_to_poselist(string)>
	./GeocodeUI s rem <model_id(int)>
	./GeocodeUI s models
```

Available prioritized options:

```
	./GeocodeUI p build <path_to_cloud(string)> <location(string)>
	./GeocodeUI p pose <path_to_poselist(string)>
	./GeocodeUI p rem <model_id(int)>
	./GeocodeUI p models
```

Available visualization options:

```
	./GeocodeUI g <path_to_output_viz(string)>
	./GeocodeUI v nvm_type(0/1) <path_to_nvm(string)>
```

Check the SynthManual.md and PrioriManual.md for details on the utilization of these options.

## **Configuration File**

The file config.ini contains variables to adjust both synthetic and prioritized features to compact models and pose estimate images. The following list describes each parameter available:

```
priori_root ../PrioriOutput/     (output folder for Prioritized Features)
synth_root ../SynthOutput/       (output folder for Synthetic Features)

-----------------------------[Vocabulary Tree Options]-------------------------------
vocab_learn ../Vocabularies/voclearn.db (Path to the trained Vocabulary Tree)
vocab_data vocdata.db                   (Path to the Vocabulary Tree)
vocab_branches 10                       (Number of branches of the voctree)
vocab_levels 5                          (Number of levels of the voctree)
vocab_type 0                            (Currently not being used)
top_documents 10                        (Top documents to retrieve when querying images)

-------------------------------[Priorized Features]----------------------------------

   [Cloud builder stuff]
Ks 5                                    (Minimum coverage for the seed cloud)
Kc 100                                   (Minimum coverage for the compressed cloud)
max_seed_points 2000                     (Maximum number of points for the seed cloud)
max_comp_points 0                       (Maximum number of points for the comp cloud)


    [Geolocalizor]
ratio_seed 0.5  (Distance ratio to filter matches when querying seed clouds)
ratio_comp 0.7  (Distance ratio to filter matches when querying compressed clouds)
max_search 6000 (Maximum number of points to match (Stoping criteria))
thrs_w     10.0  (Prioritizing factor to match query point to relevant points)

    [SiftGPU options for Prioritized]
The arguments for siftgpu_priori are: 
                    - not use/use CUDA (0/1)
                    - Starting octavae (-1/0)
                    - Soft limit for feature extraction (limits the number of features extracted)
                    - Number of orientation to compute (1/2/3/4)

siftgpu_priori 1 -1 -tc1 3200 1

-------------------------------Synthetic Features-------------------------------
   
   [Cloud builder stuff]
view_width 1025.0       (Synthetic Views image Width)
view_height 1025.0       (Synthetic Views image Height)
view_focal 1000.0       (Synthetic Focal Lenght)
view_fov 65.0           (Synthetic Field of View)
view_min_focal 0.1      (Frustrum Culling min view distance)
view_max_focal 3.0      (Frustrum Culling max view distance)
view_lean_angle 10.0     (Lean angle to adjust sythetic views direction)
min_per_view 150         (Min number of points to consider that a synthetic view A covers a view B)
view_strafe 2.0         (Distance between synthetic views placement)
extend_boundary 1.0     (Extend boundaries when compute the ground plane)

   [Geolocalizor]
max_feats_match 2500    (Maximum number of features to feature match)

   [SiftGPU options for Synthetics]

The arguments for siftgpu_synth are the same as prioritized features: 
                    - not use/use CUDA (0/1)
                    - Starting octavae (-1/0)
                    - Soft limit for feature extraction (limits the number 
of features extracted)
                    - Number of orientation to compute (1/2/3/4)

siftgpu_synth 1 -1 -tc1 1600 1

```
 
### Relevant parameters on the config.ini

For Prioritized Features model compressing:

- Ks and Kc - by adjusting these thresholds, models may be more or less compressed. Note that an high compression may imply the impossibility to pose new photographs


For Prioritized Features Geocoding:

- ratio_seed and ratio_comp - incresing these threshold may increase the number of matches per query photograph, but also may imply accepting too much outliers. Low values may negate too much matches to consider a pose estimation.


For Synthetic Views model compressing:

- view_lean_angle - increase this angle to avoid views looking to the ground plane
- min_per_view    - adjust this coverage threshold to adapt to models with high or low number of 3D points.
- view_strafe     - decrease the distance between views for a more agressive view placement (also increases computational time considerably).

For Synthetic Views Geocoding:

- top_documents - increasing the number of top documents retrieved from the vocabulary tree may increase the chances of matching a query photograph when there are many similar models compressed.

-----------------

## **Visualizer Keys**

- Rotate                             - left click
- Stafe                              - shift + mouse left click
- Rotate around the camera direction - ctrl + mouse left click
- Zoom in/out                        - mouse wheel or move right click up/down
- Quit                               - press 'q'
