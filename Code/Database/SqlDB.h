#pragma once
#include <sqlite3.h>
#include <vector>
#include <string>


class SqlDB{
protected:
    sqlite3 *db;


public:

    SqlDB(){}



    int openDB(const char* db_path);
    int closeDB();

    int createTable(const char *query);
    int queryTable(const char *query);
    int queryTable(const char *query, std::vector<std::string> &result);

    int destroyTable();

    int insertValue();
    int updateValue();
    int deleteValue();



};
