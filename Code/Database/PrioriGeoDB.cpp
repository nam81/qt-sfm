#include "PrioriGeoDB.h"

//----------------------------------------------- Create routines -----------------------------------------------
bool PrioriGeoDB::CreatePrioriTable(){
    int ret = sqldb.queryTable("CREATE TABLE IF NOT EXISTS PRIORI_MODELS(" \
                               "MODEL_ID INTEGER PRIMARY KEY AUTOINCREMENT," \
                               "MODEL_NAME TEXT," \
                               "MODEL_PATH TEXT," \
                               "MODEL_SEED TEXT," \
                               "MODEL_SEED_NPTS INTEGER," \
                               "MODEL_COMP_NPTS INTEGER," \
                               "MODEL_SEED_NVIEWS INTEGER," \
                               "MODEL_COMP_NVIEWS INTEGER," \
                               "MODEL_COMP TEXT," \
                               "LOCATION TEXT," \
                               "HAS_GPS INTEGER NOT NULL DEFAULT(0))");

    ret = sqldb.queryTable("CREATE TABLE IF NOT EXISTS PRIORI_PHOTOS(PHOTO_ID INTEGER PRIMARY KEY AUTOINCREMENT," \
                           "MODEL_ID INTEGER SECONDARY KEY," \
                           "PHOTO_PATH TEXT)");
}

//Database initialization and closing
bool PrioriGeoDB::InitializeDB(){

    int ret;

#ifdef GEO_DEBUG
    std::cout << "   Opening database from " + db_path << std::endl;
#endif
    if((ret = sqldb.openDB(db_path.c_str())) == -1){
        std::cout << "Something went wrong when initializing database" << std::endl;
        return true;
    }

    return true;
}

void PrioriGeoDB::closeDB(){
    sqldb.closeDB();
}

//----------------------------------------------- Insertion routines -----------------------------------------------
bool PrioriGeoDB::InsertPhotosPaths(std::string root_path, std::vector<std::string> photos_files, std::string model_id){
    std::string query_str;

    for(int i = 0; i < photos_files.size(); i ++){
        query_str = std::string("INSERT INTO PRIORI_PHOTOS (MODEL_ID, PHOTO_PATH) VALUES (") +
                "'" + model_id + "', " +
                "'" + root_path + photos_files[i] + "');";

        sqldb.queryTable(query_str.c_str());
    }

    return true;
}

bool PrioriGeoDB::InsertModel(std::string model_path,
                              std::string model_seed,
                              std::string npts_s,
                              std::string npts_c,
                              std::string nviews_s,
                              std::string nviews_c,
                              std::string model_comp,

                              std::string location,
                              std::string has_gps,
                              std::string &model_id){

    int slashindex = model_path.find_last_of("/");
    std::string model_root = model_path.substr(0,slashindex + 1);
    std::string model_name = model_path.substr(slashindex + 1, model_path.size() - 1);

    std::string query_str = std::string("INSERT INTO PRIORI_MODELS (MODEL_NAME, MODEL_PATH, MODEL_SEED, MODEL_SEED_NPTS, MODEL_COMP_NPTS, MODEL_SEED_NVIEWS, MODEL_COMP_NVIEWS, MODEL_COMP, LOCATION, HAS_GPS) VALUES (") +
            "'" + model_name + "', " +
            "'" + model_root + "', " +
            "'" + model_seed + "', " +
            "" + npts_s + ", " +
            "" + npts_c + ", " +
            "" + nviews_s + ", " +
            "" + nviews_c + ", " +
            "'" + model_comp + "', " +
            "'" + location + "', " +
            has_gps + ");";

    sqldb.queryTable(query_str.c_str());

    query_str = "SELECT last_insert_rowid()";
    std::vector<std::string> result;

    sqldb.queryTable(query_str.c_str(),result);

    model_id = result[0];

    return true;

}

//----------------------------------------------- Selection routines -----------------------------------------------
bool PrioriGeoDB::SelectModel(std::string model_id, std::vector<std::string> &result){
    std::string query_str = std::string("SELECT * from PRIORI_MODELS WHERE MODEL_ID = ") + model_id;

    sqldb.queryTable(query_str.c_str(), result);
    return true;

}

bool PrioriGeoDB::SelectAllModels(std::vector<std::string> &result){
    std::string query_str = std::string("SELECT MODEL_NAME, MODEL_PATH, MODEL_SEED,MODEL_SEED_NPTS,MODEL_COMP_NPTS, MODEL_SEED_NVIEWS,MODEL_COMP_NVIEWS, MODEL_COMP, LOCATION, HAS_GPS from PRIORI_MODELS");

    sqldb.queryTable(query_str.c_str(), result);
    return true;

}

bool PrioriGeoDB::SelectAllSeedModels(std::vector<std::string> &result){
    std::string query_str = std::string("SELECT MODEL_SEED from PRIORI_MODELS");

    sqldb.queryTable(query_str.c_str(), result);
    return true;

}

bool PrioriGeoDB::SelectModelsIdName(std::vector<std::string> &result){
    std::string query_str = std::string("SELECT MODEL_ID, MODEL_NAME from PRIORI_MODELS");

    sqldb.queryTable(query_str.c_str(), result);
    return true;

}

//----------------------------------------------- Update Routines -----------------------------------------------
bool PrioriGeoDB::UpdateModel(std::string model_id, std::string model_seed, std::string npts_s,std::string npts_c,std::string nviews_s,std::string nviews_c,std::string model_comp, std::string location, std::string has_gps){
    std::string query_str = std::string("UPDATE PRIORI_MODELS SET MODEL_SEED = '" +  model_seed + "'," \
                                        "MODEL_SEED_NPTS = " + npts_s + "," \
                                        "MODEL_COMP_NPTS = " + npts_c + "," \
                                        "MODEL_SEED_NVIEWS = " + nviews_s + "," \
                                        "MODEL_COMP_NVIEWS = " + nviews_c + "," \
                                        "MODEL_COMP = '" + model_comp + "'," \
                                        "LOCATION = '"   + location   + "'," \
                                        "HAS_GPS  = "   + has_gps    + " WHERE MODEL_ID = " \
                                        + model_id);

    sqldb.queryTable(query_str.c_str());

    return true;
}

//----------------------------------------------- Deletion routines -----------------------------------------------
bool PrioriGeoDB::DeleteModel(std::string model_id){



    std::string query_str = "DELETE from PRIORI_PHOTOS WHERE MODEL_ID = " + model_id + "";

    sqldb.queryTable((query_str.c_str()));

    query_str = "DELETE from PRIORI_MODELS WHERE MODEL_ID = " + model_id + "";
    sqldb.queryTable(query_str.c_str());

    return true;
}

//Reset database dropping all the tables
void PrioriGeoDB::ResetDatabase(){

    std::string query_str;
    query_str = "DROP TABLE IF EXISTS PRIORI_MODELS";
    sqldb.queryTable(query_str.c_str());

    return ;
}

//----------------------------------------------- Others -----------------------------------------------
//Count DB models
int PrioriGeoDB::CountDBModels(){
    std::string query_str = "SELECT COUNT(*) from PRIORI_MODELS";
    std::vector<std::string> result;
    sqldb.queryTable(query_str.c_str(),result);
    return atoi(result[0].c_str());
}

//Check model existence
bool PrioriGeoDB::CheckModelExistence(std::string model_path, std::string &model_id){
    int slashindex = model_path.find_last_of("/");
    std::string model_name = model_path.substr(slashindex + 1, model_path.size() - 1);

    std::string query_str = "SELECT * from PRIORI_MODELS WHERE MODEL_NAME = '" + model_name + "'";
    std::vector<std::string> result;
    sqldb.queryTable(query_str.c_str(),result);

    if(result.size() > 0){
        model_id = result[0];
        return true;
    }else{
        return false;
    }

}
