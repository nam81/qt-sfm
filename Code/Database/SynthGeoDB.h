#pragma once
#include "SqlDB.h"
#include <string>
#include "../IO/ConfigFile.h"
#include "../VocabTree/VocabTreeSFM.h"

class SynthGeoDB{

protected:
    SqlDB sqldb;
    VocabTreeSFM vocab_tree;

    std::string db_path;

public:
    SynthGeoDB(){}
    SynthGeoDB(std::string database_path, std::string v_r, std::string vocab_learn, std::string vocab_db){
        db_path = database_path;
        sqldb = SqlDB();

        vocab_tree = VocabTreeSFM(v_r,vocab_learn,vocab_db);
    }

    //Create routines
    bool CreateSynthTable();
    bool CreatePrioriTable();

    //Database initialization and closing
    bool InitializeDB();
    bool InitializeVocTree();
    void closeDB();

    //Insertion routines
    bool InsertPhotosPaths(std::string root_path, std::vector<std::string> photos_files, std::string model_id);
    bool InsertModel(std::string model_path, std::string location, std::string has_gps, std::string &model_id);
    bool InsertSynthView(std::string synth_path, std::string model_id);
    void AddImagesToDB();

    //Update routines
    bool UpdateViewVocFlag(std::vector<std::string> views_ids, std::vector<int> views_voc_indexes);

    //Selection routines
    bool SelectEntry(std::string id, std::vector<std::string> &result);
    bool SelectModel(std::string model_id, std::vector<std::string> &result);
    bool SelectModelsIdName(std::vector<std::string> &result);
    bool SelectSynthToAdd(std::vector<std::string> &result);
    bool SelectViewsIds(std::vector<std::string> &imgs_ids);
    bool SelectSynthToDel(std::string model_id, std::vector<int> &result);

    //Deletion routines
    bool DeleteModel(std::string model_id);
    bool DeleteModelViews(std::string model_id);
    void RmImagesFromDB(std::string model_id, bool remove_model);
    void ResetDatabase();

    //Reset Database
    bool DropAllTables();

    //Query Potential Matches
    void QueryPotentialMatches(std::vector<float> descriptors_to_query, int top_documents, std::vector<std::pair<int,double> > &vocab_matches);

    //Check model existence
    bool CheckModelExistence(std::string model_path, std::string &model_id);

    //Count database images
    int CountDBImgs();
    int CountVocViews();
};
