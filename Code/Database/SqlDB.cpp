#include "SqlDB.h"
#include <stdio.h>
#include <vector>
#include <string>
#include <iostream>

int SqlDB::openDB(const char* db_path){
    int ret;

    if (SQLITE_OK != (ret = sqlite3_initialize()))
    {
        printf("Failed to initialize library: %d\n", ret);
        return -1;
    }

    return sqlite3_open_v2(db_path, &db,SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE, NULL);
}

int SqlDB::closeDB(){
    return sqlite3_close(db);
}


int SqlDB::queryTable(const char *query){

    sqlite3_stmt *statement;
    if(sqlite3_prepare_v2(db, query, -1, &statement, 0) == SQLITE_OK)
    {
        sqlite3_step(statement);
        sqlite3_finalize(statement);
    }

    std::string error = sqlite3_errmsg(db);
    if(error != "not an error") std::cout << query << " " << error << std::endl;

    return 0;
}


int SqlDB::queryTable(const char *query, std::vector<std::string> &result){

    sqlite3_stmt *statement;
    //std::vector<std::vector<std::string> > results;

    if(sqlite3_prepare_v2(db, query, -1, &statement, 0) == SQLITE_OK)
    {
        int cols = sqlite3_column_count(statement);
            int ret = 0;
            while(true)
            {
                ret = sqlite3_step(statement);

                if(ret == SQLITE_ROW)
                {
                    for(int col = 0; col < cols; col++)
                    {
                        result.push_back((char*)sqlite3_column_text(statement, col));
                    }
                    //results.push_back(values);
                }
                else
                {
                    break;
                }
            }

            sqlite3_finalize(statement);
    }

    std::string error = sqlite3_errmsg(db);
    if(error != "not an error") std::cout << query << " " << error << std::endl;

    return 0;
}
