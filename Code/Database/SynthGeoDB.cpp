#include "SynthGeoDB.h"
#include "SqlDB.h"
#include <iostream>
#include <sstream>
#include <cstdlib>

//----------------------------------------------- Initialization and closing Routines -----------------------------------------------
bool SynthGeoDB::InitializeDB(){
    int ret;

#ifdef GEO_DEBUG
    std::cout << "   Opening database from " + db_path << std::endl;
#endif
    if((ret = sqldb.openDB(db_path.c_str())) == -1){
        std::cout << "Something went wrong when initializing database" << std::endl;
        return false;
    }

    return true;
}

bool SynthGeoDB::InitializeVocTree(){

    int n = CountVocViews();

    vocab_tree.LoadVocabTree(n);

}

void SynthGeoDB::closeDB(){
    sqldb.closeDB();
}

//----------------------------------------------- Creation Routines -----------------------------------------------

bool SynthGeoDB::CreateSynthTable(){


    int ret = sqldb.queryTable("CREATE TABLE IF NOT EXISTS SYNTH_VIEWS(ID INTEGER PRIMARY KEY AUTOINCREMENT," \
                               "MODEL_ID INTEGER SECONDARY KEY," \
                               "SYNTH_PATH TEXT," \
                               "VOC_IDX INTEGER NOT NULL DEFAULT(-1))");

    ret = sqldb.queryTable("CREATE TABLE IF NOT EXISTS SYNTH_MODELS(MODEL_ID INTEGER PRIMARY KEY AUTOINCREMENT," \
                           "MODEL_NAME TEXT," \
                           "MODEL_PATH TEXT," \
                           "LOCATION TEXT NOT NULL DEFAULT('Undefined')," \
                           "HAS_GPS INTEGER NOT NULL DEFAULT(0))");

    ret = sqldb.queryTable("CREATE TABLE IF NOT EXISTS SYNTH_PHOTOS(PHOTO_ID INTEGER PRIMARY KEY AUTOINCREMENT," \
                           "MODEL_ID INTEGER SECONDARY KEY," \
                           "PHOTO_PATH TEXT)");


    //if(ret)
        return true;
    /*else{
#ifdef GEO_DEBUG
        std::cout << "Couldn't create the desired table." << std::endl;
#endif
        return false;
    }*/
}


//----------------------------------------------- Insertion Routines -----------------------------------------------
bool SynthGeoDB::InsertPhotosPaths(std::string root_path, std::vector<std::string> photos_files, std::string model_id){

    std::string query_str;

    for(int i = 0; i < photos_files.size(); i ++){
        query_str = std::string("INSERT INTO SYNTH_PHOTOS (MODEL_ID, PHOTO_PATH) VALUES (") +
                "'" + model_id + "', " +
                "'" + root_path + photos_files[i] + "');";

        sqldb.queryTable(query_str.c_str());
    }

    return true;
}

bool SynthGeoDB::InsertModel(std::string model_path, std::string location, std::string has_gps, std::string &model_id){
    std::string model_name, model_root;

    int slashindex = model_path.find_last_of("/");
    model_root = model_path.substr(0,slashindex + 1);
    model_name = model_path.substr(slashindex + 1, model_path.size() - 1);



    std::string query_str = std::string("INSERT INTO SYNTH_MODELS (MODEL_NAME, MODEL_PATH, LOCATION, HAS_GPS) VALUES (") +
            "'" + model_name + "', " +
            "'" + model_root + "', " +
            "'" + location + "', " +
            "'" + has_gps + "');";

    sqldb.queryTable(query_str.c_str());

    query_str = "SELECT last_insert_rowid()";
    std::vector<std::string> result;

    sqldb.queryTable(query_str.c_str(),result);

    model_id = result[0];

    return true;
}

bool SynthGeoDB::InsertSynthView(std::string synth_path, std::string model_id){
    std::string query_str = std::string("INSERT INTO SYNTH_VIEWS (MODEL_ID, SYNTH_PATH, VOC_IDX) VALUES (") +
            "'" + model_id + "', " +
            "'" + synth_path + "', " +
            "-1);";

    sqldb.queryTable(query_str.c_str());
}


//----------------------------------------------- Selection Routines -----------------------------------------------
bool SynthGeoDB::SelectEntry(std::string id, std::vector<std::string> &result){
    //std::stringstream ss;
    //ss << id;

    std::string query_str = std::string("SELECT * from SYNTH_VIEWS WHERE VOC_IDX = ") + id;

    sqldb.queryTable(query_str.c_str(), result);
    return true;
    /*else{
#ifdef GEO_DEBUG
        std::cout << "Couldn't insert value" << std::endl;
#endif
        return false;
    }//*/
}

bool SynthGeoDB::SelectModel(std::string model_id, std::vector<std::string> &result){
    //std::stringstream ss;
    //ss << id;

    std::string query_str = std::string("SELECT * from SYNTH_MODELS WHERE MODEL_ID = ") + model_id;

    sqldb.queryTable(query_str.c_str(), result);
    return true;
    /*else{
#ifdef GEO_DEBUG
        std::cout << "Couldn't insert value" << std::endl;
#endif
        return false;
    }//*/
}


bool SynthGeoDB::SelectModelsIdName(std::vector<std::string> &result){
    std::string query_str = std::string("SELECT MODEL_ID, MODEL_NAME from SYNTH_MODELS");

    sqldb.queryTable(query_str.c_str(), result);
    return true;

}


bool SynthGeoDB::SelectSynthToAdd(std::vector<std::string> &result){


    std::string query_str = "SELECT SYNTH_PATH from SYNTH_VIEWS WHERE VOC_IDX = -1";

    sqldb.queryTable(query_str.c_str(), result);
    return true;
}

bool SynthGeoDB::SelectViewsIds(std::vector<std::string> &imgs_ids){
    std::string query_str = "SELECT ID from SYNTH_VIEWS";

    sqldb.queryTable(query_str.c_str(),imgs_ids);

    return true;
}

bool SynthGeoDB::SelectSynthToDel(std::string model_id, std::vector<int> &result){

    std::string query_str = "SELECT VOC_IDX from SYNTH_VIEWS WHERE MODEL_ID = " + model_id
            + " AND VOC_IDX > -1";

    std::vector<std::string> result_str;

    sqldb.queryTable(query_str.c_str(), result_str);

    for (int i = 0; i < result_str.size(); i++)
        result.push_back(atoi(result_str[i].c_str()));

    return true;
}

//----------------------------------------------- Update ROutines -----------------------------------------------
bool SynthGeoDB::UpdateViewVocFlag(std::vector<std::string> views_ids, std::vector<int> views_voc_indexes){

    for(int i = 0; i < views_ids.size(); i++){
        std::stringstream ss;
        ss << views_voc_indexes[i];
        std::string query_str = "UPDATE SYNTH_VIEWS SET VOC_IDX = " + ss.str() + " WHERE SYNTH_PATH = '" + views_ids[i] + "'" ;

        sqldb.queryTable(query_str.c_str());
    }

    return true;
}

//----------------------------------------------- Deletion Routines -----------------------------------------------
bool SynthGeoDB::DropAllTables(){

    std::string query_str = "DROP TABLE IF EXISTS SYNTH_VIEWS";

    sqldb.queryTable(query_str.c_str());

    query_str = "DROP TABLE IF EXISTS SYNTH_MODELS";
    sqldb.queryTable(query_str.c_str());

    query_str = "DROP TABLE IF EXISTS SYNTH_PHOTOS";
    sqldb.queryTable(query_str.c_str());

    return true;
}

bool SynthGeoDB::DeleteModel(std::string model_id){
    std::string query_str = "DELETE from SYNTH_VIEWS WHERE MODEL_ID = '" + model_id + "'";
    sqldb.queryTable(query_str.c_str());

    query_str = "DELETE from SYNTH_MODELS WHERE MODEL_ID = '" + model_id + "'";
    sqldb.queryTable(query_str.c_str());

    query_str = "DELETE from SYNTH_PHOTOS WHERE MODEL_ID = '" + model_id + "'";
    sqldb.queryTable(query_str.c_str());

    return true;
}

bool SynthGeoDB::DeleteModelViews(std::string model_id){
    std::string query_str = "DELETE from SYNTH_VIEWS WHERE MODEL_ID = '" + model_id + "'";
    sqldb.queryTable(query_str.c_str());

    return true;
}

//----------------------------------------------- Others -----------------------------------------------
bool SynthGeoDB::CheckModelExistence(std::string model_path, std::string &model_id){
    int slashindex = model_path.find_last_of("/");
    std::string model_name = model_path.substr(slashindex + 1, model_path.size() - 1);
    std::string query_str = "SELECT * from SYNTH_MODELS WHERE MODEL_NAME = '" + model_name + "'";
    std::vector<std::string> result;
    sqldb.queryTable(query_str.c_str(),result);

    if(result.size() > 0){
        model_id = result[0];
        return true;
    }else{
        return false;
    }

}

int SynthGeoDB::CountDBImgs(){
    std::string query_str = "SELECT COUNT(*) from SYNTH_VIEWS";
    std::vector<std::string> result;
    sqldb.queryTable(query_str.c_str(),result);

    return atoi(result[0].c_str());
}

int SynthGeoDB::CountVocViews(){
    std::string query_str = "SELECT COUNT(*) from SYNTH_VIEWS WHERE VOC_IDX > -1";
    std::vector<std::string> result;
    sqldb.queryTable(query_str.c_str(),result);

    return atoi(result[0].c_str());
}





//----------------------------------------------- VocTree Operations -----------------------------------------------
void SynthGeoDB::AddImagesToDB(/*std::string synth_root*/){

    std::cout << "[Vocabulary Tree Builder]" << std::endl;

    std::vector<std::string> imgs_to_build;

    //Select images which aren't in the voctree
    SelectSynthToAdd(imgs_to_build);

    //If the selected images are already in the voc tree, return
    if(imgs_to_build.size() == 0){

        closeDB();
#ifdef GEO_DEBUG
        std::cout << "The database views are already added to the vocabulary tree..." << std::endl;
#endif
        return;
    }

    //Read voctree and update it
    std::vector<int> voc_indexes;
    vocab_tree.BuildVocabDB(/*synth_root,*/imgs_to_build, voc_indexes);
    UpdateViewVocFlag(imgs_to_build, voc_indexes);
}

void SynthGeoDB::RmImagesFromDB(std::string model_id, bool remove_model){
    //InitializeDB();

    std::vector<int> result;
    SelectSynthToDel(model_id, result);

    if(result.size() == 0){

#ifdef GEO_DEBUG
        std::cout << "Views to delete weren't added to the voctree" << std::endl;
#endif
        //return;
    }else{
        vocab_tree.DeleteVocabImgs(result);
    }

    if(remove_model)
        DeleteModel(model_id);
    else
        DeleteModelViews(model_id);

}


void SynthGeoDB::ResetDatabase(){
    //InitializeDB();

    DropAllTables();

    vocab_tree.ResetVocabTree();

}

void SynthGeoDB::QueryPotentialMatches(std::vector<float> descriptors_to_query,
                                        int top_documents,
                                        std::vector<std::pair<int,double> > &vocab_matches){

    vocab_tree.QueryVocTree(descriptors_to_query,top_documents,vocab_matches);

#ifdef GEO_DEBUG
    vocab_tree.PrintScores(/*voclist,*/vocab_matches,top_documents);
#endif

}


