#pragma once
#include "SqlDB.h"
#include <string>
#include "../IO/ConfigFile.h"
#include "../VocabTree/VocabTreeSFM.h"

class PrioriGeoDB{

protected:
    SqlDB sqldb;
    VocabTreeSFM vocab_tree;

    std::string db_path;


public:
    PrioriGeoDB(){}
    PrioriGeoDB(std::string database_path){
        db_path = database_path;
        sqldb = SqlDB();
    }

    //Create routines
    bool CreatePrioriTable();

    //Database initialization and closing
    bool InitializeDB();
    void closeDB();

    //Insertion routines
    bool InsertPhotosPaths(std::string root_path, std::vector<std::string> photos_files, std::string model_id);
    bool InsertModel(std::string model_path,
                     std::string model_seed, std::string npts_s, std::string npts_c, std::string nviews_s, std::string nviews_c,
                     std::string model_comp,
                     std::string location,
                     std::string has_gps,
                     std::string &model_id);

    //Selection routines
    bool SelectModel(std::string model_id, std::vector<std::string> &result);
    bool SelectAllModels(std::vector<std::string> &result);
    bool SelectModelsIdName(std::vector<std::string> &result);
    bool SelectAllSeedModels(std::vector<std::string> &result);

    //Update routines
    bool UpdateModel(std::string model_id,
                     std::string model_seed,
                     std::string npts_s,
                     std::string npts_c,
                     std::string nviews_s,
                     std::string nviews_c,
                     std::string model_comp,
                     std::string location,
                     std::string has_gps);
    //Deletion routines
    bool DeleteModel(std::string model_id);

    //Reset database dropping all the tables
    void ResetDatabase();

    //Count database compressed models
    int CountDBModels();

    //Check existence of a model
    bool CheckModelExistence(std::string model_path, std::string &model_id);

};
