#pragma once

#include "../Common.h"
#include "../IO/ConfigFile.h"

//Synthetic Views Builder and add view to the database
void BuildSyntheticViews(std::string cloudpath, int type, std::string location_name, ConfigOptions options);

//Remove Synthetic Model from database including related information
void RemoveSyntheticModel(std::string model_id, std::string synth_root, std::string v_l, std::string v_db);

//Reset Synthetic Database
void ResetSynthGeoDB(std::string synth_root, std::string v_l, std::string v_db);

//Print models id's and name's
void PrintSynthModels(std::string synth_root, std::string v_l, std::string v_db);

//Add database views to the vocabulary tree
void AddViewsToVocTree(std::string synth_root, std::string v_l, std::string v_db);

//Synthetic Geolocalizer
void GeolocalizeSynthetic(const char* list_path, ConfigOptions options);
