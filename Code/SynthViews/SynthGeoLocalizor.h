#pragma once

#include "../Common.h"
#include "../FMatchers/SiftGPUMatcher.h"
#include "../IO/ConfigFile.h"
#include "../Geocoding/GeoOperations.h"
#include "../Database/SynthGeoDB.h"
#include "../IO/Statistics.h"

/**
 * Geolocalizer class for Synthetic Views. This class assumes that a couple
 * of Synthetic Views and a Vocabulary Tree is available.
 */

class SynthGeolocalizer {

protected:
    //Feature Processor
    cv::Ptr<AbstractFeatureMatcher> feature_matcher;

    int top_documents;      //Top documents to match (vocabulary tree)
    int max_feats_match;    //Maximum number of features to match

    std::string synth_root; //Path for the synthetics folder

    SynthGeoDB geodb;       //Geocoding database (sqlite + voctree)

public:
    SynthGeolocalizer(){}

    SynthGeolocalizer(std::string imgs_path, bool preemp, ConfigOptions options){
        feature_matcher = new SiftGPUMatcher(imgs_path, preemp, 200, 10, 1);

        max_feats_match = options.synthfeats.max_feats_match;

        top_documents = options.top_documents;

        synth_root = std::string(options.synth_root);

        geodb = SynthGeoDB(std::string(options.synth_root) + "geodatabase.db", std::string(options.synth_root), std::string(options.vocab_learn_path), std::string(options.vocab_data_path));
    }

    //Initialize feature extractor and matcher
    void InitializeSiftGPU(SynthOptions so);

    //Dealloc feature extractor and matcher
    bool UnloadSiftGPU();

    //Load Vocabulary Tree to support the geocoding process
    bool LoadVocTree();

    //Find the Projection matrix given 2D and 3D points and the camera matrix (K)
    bool SFMFindPose(std::vector<cv::Point2f> max_2d, std::vector<cv::Point3f> max_3d,  cv::Mat K , cv::Matx34d& Pout);

    //Match 2 descriptor vectors
    bool MatchQueryImages(std::vector<cv::KeyPoint> kps_query,
                          std::vector<float> dsc_query,
                          std::vector<cv::KeyPoint> kps_list,
                          std::vector<float> dsc_list,
                          std::vector<cv::Point3f> p3f_list,
                          std::vector<cv::Point2f> &max2d,
                          std::vector<cv::Point3f> &max3d);


    //Geolocate a given photograph (extraction, vocab matching, pose verification and gps retrieval)
    bool LocateImg(std::string base_path,
                   std::string img_name,
                   StatSynth &loc_statistics);
                   //cv::Matx34d &Pout, GPSCoords &gps, std::string &location, std::string &direction, std::string &model_name);
};
