#include "Visibility.h"

#include "../Common.h"
#include "AuxOperations.h"
#include "../IO/ConfigFile.h"

//#include <Eigen/Eigen>


//======================================= Frustrum Culling ===============================================
//Distance point<->plane
bool DistancePlane(cv::Point3f normal, cv::Point3f center, cv::Point3f p){

    float D = - (normal.dot(center));
    float sign = normal.dot(p) + D;

    if(sign >= 0.0){
        return true;
    }else{
        return false;
    }
}

//Computes frustrum culling
bool ComputeFrust( cv::Matx33f R, cv::Matx31f T, float width, float height, float fov, float min_focal, float max_focal, std::vector<cv::Point3f> &frust_normals, std::vector<cv::Point3f> &frust_points){

    Eigen::Matrix3f RR = Eigen::Matrix<float,3,3,Eigen::RowMajor>(R.val);
    const Eigen::Vector3f _t = Eigen::Vector3f(T.val);

    Eigen::Vector3f t = -RR.transpose() * _t ;

    //Eigen::Vector3f right_eig = (RR.row(0).normalized());
    Eigen::Vector3f up_eig = (-RR.row(1).normalized());
    Eigen::Vector3f d_eig = (RR.row(2).normalized());

    //View Frustrum Stuff
    float ratio = width / height;
    float angle = (fov * M_PI / 180.0) * 0.5;
    float tang = 2.0 * (float) (tan(angle));
    float farDist = max_focal, nearDist = min_focal;
    float Hfar = farDist * tang, Wfar = Hfar * ratio;
    float Hnear = nearDist * tang, Wnear = Hnear * ratio;


    //cv::Point3f u(0.0,1.0,0.0);
    cv::Point3f p(t(0), t(1), t(2));
    cv::Point3f d(d_eig(0), d_eig(1), d_eig(2));
    cv::Point3f Z = -d;

    cv::Point3f up(up_eig(0), up_eig(1), up_eig(2));
    cv::Point3f right = up.cross(Z);

    cv::Point3f Y = Z.cross(right); //(right_eig(0), right_eig(1), right_eig(2));

    //NormalizeVector(right.x,right.y,right.z);

    //cv::Point3f up = d.cross(right);

    cv::Point3f fc = p - Z * farDist;
    /*cv::Point3f ftl = fc + (up * (Hfar/2.0)) - (right * (Wfar/2.0));
    cv::Point3f ftr = fc + (up * (Hfar/2.0)) + (right * (Wfar/2.0));
    cv::Point3f fbl = fc - (up * (Hfar/2.0)) - (right * (Wfar/2.0));
    cv::Point3f fbr = fc - (up * (Hfar/2.0)) + (right * (Wfar/2.0));
*/
    cv::Point3f nc = p - Z * nearDist;

  /*  cv::Point3f ntl = nc + (up * (Hnear/2.0)) - (right * (Wnear/2.0));
    cv::Point3f ntr = nc + (up * (Hnear/2.0)) + (right * (Wnear/2.0));
    cv::Point3f nbl = nc - (up * (Hnear/2.0)) - (right * (Wnear/2.0));
    cv::Point3f nbr = nc - (up * (Hnear/2.0)) + (right * (Wnear/2.0));
*/

    cv::Point3f nnear = -Z;
    NormalizeVector(nnear.x,nnear.y,nnear.z);
    cv::Point3f npoint = nc;

    cv::Point3f nfar = Z;
    NormalizeVector(nfar.x,nfar.y,nfar.z);
    cv::Point3f fpoint = fc;

    cv::Point3f ntop = ((nc + Y * (Hnear)) - p);
    NormalizeVector(ntop.x,ntop.y,ntop.z);
    ntop = ntop.cross(right);
    cv::Point3f toppoint = nc + Y * (Hnear);

    cv::Point3f nbot = (nc - Y * (Hnear)) - p;
    NormalizeVector(nbot.x,nbot.y,nbot.z);
    nbot = right.cross(nbot);
    cv::Point3f botpoint = nc - Y * (Hnear);

    cv::Point3f nleft = (nc - right * (Wnear)) - p;
    NormalizeVector(nleft.x,nleft.y,nleft.z);
    nleft = nleft.cross(Y);
    cv::Point3f leftpoint = nc - right * (Wnear);

    cv::Point3f nright = (nc + right * (Wnear)) - p;
    NormalizeVector(nright.x,nright.y,nright.z);
    nright = Y.cross(nright);
    cv::Point3f rightpoint = nc + right * (Wnear);

    frust_normals.push_back(nnear);
    frust_normals.push_back(nfar);
    frust_normals.push_back(ntop);
    frust_normals.push_back(nbot);
    frust_normals.push_back(nleft);
    frust_normals.push_back(nright);

    frust_points.push_back(npoint);
    frust_points.push_back(fpoint);
    frust_points.push_back(toppoint);
    frust_points.push_back(botpoint);
    frust_points.push_back(leftpoint);
    frust_points.push_back(rightpoint);

}


//======================================= Visibility Operations ==========================================
//Check Point visibility by a synthetic view
bool CheckPointVisibility(float view_width,
                          float view_height,
                          float view_fov,
                          float view_min_focal,
                          float view_max_focal,
                          cv::Mat K,
                          cv::Matx34d view,
                          std::vector<cv::Point3f> points,
                          std::vector<cv::Point3f> angles,
                          std::vector<float> &scales,
                          std::vector<int>& ptf,
                          std::vector<cv::Point3f>& visib){

    cv::Matx31f T(view(0,3), view(1,3), view(2,3));

    cv::Mat_<double> R = (cv::Mat_<double>(3,3) << view(0,0), view(0,1), view(0,2),
                                                   view(1,0), view(1,1), view(1,2),
                                                   view(2,0), view(2,1), view(2,2));

    cv::Matx33f R_AUX(R);

    cv::Matx31f C = (- R_AUX.t()) * T;

    Eigen::Matrix3f RR = Eigen::Matrix<float,3,3,Eigen::RowMajor>(cv::Matx33f(R).val);
    Eigen::Vector3f dir_eig = (RR.row(2).normalized());
    //Norma diretion
    cv::Point3f dir(dir_eig(0), dir_eig(1),dir_eig(2) );
    //NormalizeVector(dir.x,dir.y,dir.z);
    double normDir = sqrt(pow(dir.x,2.0) + pow(dir.y,2.0) + pow(dir.z,2.0));

    std::vector<cv::Point3f> frust_normals, frust_points;
    cv::Matx33f RRR(R);

    ComputeFrust(RRR,T,view_width, view_height,view_fov, view_min_focal, view_max_focal, frust_normals, frust_points);

    for(int i = 0; i < points.size(); i++){
        bool frustrum_visible = true;

        for(int j = 0; j < 6; j++){
            if(!DistancePlane(frust_normals[j],frust_points[j], points[i])){
                frustrum_visible = false;
                break;
            }
        }

        //Frustrum Culling test
        if(!frustrum_visible){
           continue;
        }

        cv::Point3f ang = angles[i];

        cv::Point3f dist(points[i].x - C(0,0),
                         points[i].y - C(1,0),
                         points[i].z - C(2,0));


        float scale = scales[i] * K.at<double>(0,0) / sqrt(powf(dist.x,2.0) + powf(dist.y,2.0) + powf(dist.z,2.0));

        scales[i] = scale;

        NormalizeVector(ang.x,ang.y,ang.z);

        double normAng = sqrt(powf(ang.x,2.0) + pow(ang.y,2.0) + pow(ang.z,2.0));
        normDir *= normAng;

        double dotAng = ang.x * dir.x + ang.y * dir.y + ang.z * dir.z;

        double dif_ang_rad = dotAng / normDir;
        dif_ang_rad = acos(dif_ang_rad);

        double dif_angle = (dif_ang_rad * 180.0) / M_PI;


        double ang_thrs = view_fov / 2.0;
        if(dif_angle <= ang_thrs && dif_angle >= - ang_thrs && scale >= 1.0 ){
            visib.push_back(points[i]);
            ptf.push_back(i);
        }
    }

    //std::cout << "From " << points_before << " filtered " << points_before - cv::countNonZero(ptf) << std::endl;

    return true;
}



//========================================== Views Selection ==============================================
void ComputeViewCoverage(std::vector<std::map<int,int> > view_relationship,
                         std::map<int, int > points_per_view,
                         int min_per_view,
                         std::map< int, std::map<int,int> > &visibility_matrix){

    std::map<int, std::map<int, int> > visibility_coverage;

    //Counts how many points each view sees from other views
    for(int index = 0; index < view_relationship.size(); index++){
        for(std::map<int,int>::iterator iter_outter = view_relationship[index].begin(); iter_outter != view_relationship[index].end(); iter_outter++){
            for(std::map<int,int>::iterator iter_inner = view_relationship[index].begin(); iter_inner != view_relationship[index].end(); iter_inner++){
                if(iter_outter->first != iter_inner->first){
                    visibility_coverage[iter_outter->first][iter_inner->first] += 1;
                }
            }
        }
    }



    //Fills the visibiltiy matrix with 1 for each view which sees more than min_per_view points of other view
    for(std::map<int,std::map<int, int> >::iterator iter_outter = visibility_coverage.begin(); iter_outter != visibility_coverage.end(); iter_outter++){
        for(std::map<int,int>::iterator iter_inner = visibility_coverage[iter_outter->first].begin(); iter_inner != visibility_coverage[iter_outter->first].end(); iter_inner++){

            if(visibility_coverage[iter_outter->first][iter_inner->first] == points_per_view[iter_inner->first])
                visibility_matrix[iter_outter->first][iter_inner->first] = 1;
            else
                if(visibility_coverage[iter_outter->first][iter_inner->first] > min_per_view )
                    visibility_matrix[iter_outter->first][iter_inner->first] = 1;
        }
    }

    //Each view covers itself
    for(std::map<int,std::map<int,int> >::iterator map_iter_outter = visibility_coverage.begin(); map_iter_outter != visibility_coverage.end(); map_iter_outter++)
        visibility_matrix[map_iter_outter->first][map_iter_outter->first] = 1;

    std::cout << "Ended selection" << std::endl;

}



void SelectBestViews(std::map<int,std::map<int,int> > visibility_matrix, std::vector<int> &views_to_save){

    //Decide which views to
    bool zero_matrix = false;

    while(!zero_matrix){
        std::map<int,int> n_visible;

        for(std::map<int,std::map<int,int> >::iterator iter = visibility_matrix.begin(); iter != visibility_matrix.end(); iter++){
            for(std::map<int,int>::iterator iter_inner = visibility_matrix[iter->first].begin(); iter_inner != visibility_matrix[iter->first].end(); iter_inner++){
                n_visible[iter->first] += iter_inner->second;
            }
        }

        int most_visible = -1, visib_count = 0;
        for(std::map<int,int>::iterator iter = n_visible.begin(); iter != n_visible.end(); iter++){
            if(iter->second > visib_count){
                most_visible = iter->first;
                visib_count = iter->second;
            }
        }

        if(most_visible == -1){
            zero_matrix = true;
            continue;
        }

        std::map<int,int> chosen_row = visibility_matrix[most_visible];

        views_to_save.push_back(most_visible);

        for(std::map<int,std::map<int,int> >::iterator iter = visibility_matrix.begin(); iter != visibility_matrix.end(); iter++){
            //std::vector<int> row_x = visibility_matrix[iter->first];
            for(std::map<int,int>::iterator iter_inner = visibility_matrix[iter->first].begin(); iter_inner != visibility_matrix[iter->first].end(); iter_inner++){
                int val_sub = 0;

                if(chosen_row.find(iter_inner->first) != chosen_row.end())
                    val_sub = chosen_row[iter_inner->first];

                int val = visibility_matrix[iter->first][iter_inner->first] - val_sub;
                val = std::max(0, val);
                visibility_matrix[iter->first][iter_inner->first] = val;

            }
        }

    }
}
