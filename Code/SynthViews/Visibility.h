#pragma once

#include "../Common.h"

//Check Point visibility by a synthetic view
bool CheckPointVisibility(float view_width,
                          float view_height,
                          float view_fov,
                          float view_min_focal,
                          float view_max_focal,
                          cv::Mat K,
                          cv::Matx34d view,
                          std::vector<cv::Point3f> points,
                          std::vector<cv::Point3f> angles,
                          std::vector<float> &scales,
                          std::vector<int>& ptf,
                          std::vector<cv::Point3f>& visib);


//Selects the best views to save based on their visibility
void SelectBestViews(std::map<int,std::map<int,int> > visibility_matrix,
                     std::vector<int> &views_to_save);
//Compute View Coverage
void ComputeViewCoverage(std::vector<std::map<int,int> > view_relationship, std::map<int, int> points_per_view,
                         int min_per_view,
                         std::map< int, std::map<int,int> > &visibility_matrix);

