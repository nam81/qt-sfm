#include "SynthGeoLocalizor.h"
#include "../Projections/FindCameraMatrices.h"
#include "../IO/IOCommon.h"
#include "../IO/IOSynthetics.h"
#include "../IO/ExifReader.h"
#include "../Geocoding/GeoOperations.h"

void SynthGeolocalizer::InitializeSiftGPU(SynthOptions so){
    if(so.use_cuda){
#ifdef SIFTGPU_CUDA

#ifdef GEO_DEBUG
        std::cout << "[SiftGPU] Using cuda" << std::endl;
#endif
        feature_matcher->SetCuda("0");
#endif
    }

    feature_matcher->SetOptions(so.fo,so.tc_type,so.tc,so.ori_n);
    feature_matcher->CreateExtractorContext();
}

bool SynthGeolocalizer::UnloadSiftGPU(){
    feature_matcher->UnloadSift();
}

//Loads database + vocabulary tree
bool SynthGeolocalizer::LoadVocTree(){

    //Initialize database to get existing models
    if(!geodb.InitializeDB()){
        std::cerr << "Couldn't load database." << std::endl;
        exit(0);
    }

    //Sanity test
    int n = geodb.CountVocViews();

    if(n == 0){
        std::cout << "[ERROR] Vocabulary contains 0 added views...run './Geocode s learn' to add views" << std::endl;
        exit(0);
    }

    //Initialize vocabulary tree
    if(!geodb.InitializeVocTree()){
        std::cerr << "Couldn't load VocTree." << std::endl;
        exit(0);
    }


    return true;
}

//Matches query photograph with the retrived vocabulary documents
//Also computes the fundamental & homography to refine matches
bool SynthGeolocalizer::MatchQueryImages(std::vector<cv::KeyPoint> kps_query,
                                         std::vector<float> dsc_query,
                                         std::vector<cv::KeyPoint>  kps_list,
                                         std::vector<float>  dsc_list,
                                         std::vector<cv::Point3f>  p3f_list,
                                         std::vector<cv::Point2f> &max2d,
                                         std::vector<cv::Point3f> &max3d){

        //Match i,j. If didnt pass preemptive matching, remove this pair
        std::vector<cv::DMatch> matches_tmp;
        if(!feature_matcher->MatchFeatures(dsc_query,
                                           dsc_query.size(),
                                           dsc_list,
                                           dsc_list.size(),
                                           max_feats_match,
                                           &matches_tmp)){
#ifdef GEO_DEBUG
            std::cout << "Didn't pass test" << std::endl;
#endif
            //continue;
            return false;
        }

        //Prune Matches to remove bad ones
        if(matches_tmp.size() == 0)
            return false;

        /*FindFundamentalMat(kps_query,
                           kps_list,
                           matches_tmp,
                           0);

        if(matches_tmp.size() == 0)
            return false;

   //*/

        //Refine F matrix with LMES
        FindFundamentalMat(kps_query,
                           kps_list,
                           matches_tmp,
                           1);//*/


         //============================ Get homography ===================================
         int inliers = 0;

         /*FindHomography(images_info[older_view].keypoints,
                        images_info[working_view].keypoints,
                        filtered_matches,
                        inliers,
                        0);//*/

         if(matches_tmp.size() == 0)
             return false;

         FindHomography(kps_query,
                        kps_list,
                        matches_tmp,
                        inliers,
                        1);


    //*/

         if(inliers > 14){

             for(int j = 0; j < matches_tmp.size(); j++){
                 max2d.push_back(kps_query[matches_tmp[j].queryIdx].pt);
                 max3d.push_back(p3f_list[matches_tmp[j].trainIdx]);
             }
         }

    return true;
}

//Computes the P matrix if a pose is validated
bool SynthGeolocalizer::SFMFindPose(std::vector<cv::Point2f> max_2d, std::vector<cv::Point3f> max_3d,  cv::Mat K , cv::Matx34d &Pout) {

    cv::Mat_<double> t = (cv::Mat_<double>(1,3) << 0.0, 0.0,0.0);
    cv::Mat_<double> R = (cv::Mat_<double>(3,3) << 1.0, 0.0, 0.0,
                                                   0.0, 1.0, 0.0,
                                                   0.0, 0.0, 1.0);
    cv::Mat_<double> rvec(0.0,0.0,0.0);

    bool pose_estimated;
    cv::Mat distortion_coeff = cv::Mat_<double>::zeros(1,4);

#ifdef MULTI_CAMERA
    pose_estimated = FindPoseEstimation(rvec,t,R,images_info[i]->getK(), distortion_coeff,max_3d,max_2d);
#else
    pose_estimated = FindPoseEstimation(rvec,t,R,K, distortion_coeff,max_3d,max_2d);
#endif
    if(!pose_estimated){
#ifdef GEO_DEBUG
        std::cout << "failed to pose estimate"<< std::endl;
#endif
        return false;
    }

    //store estimated pose
    Pout = cv::Matx34d(R(0,0),R(0,1),R(0,2),t(0),
                       R(1,0),R(1,1),R(1,2),t(1),
                       R(2,0),R(2,1),R(2,2),t(2));

    return true;
}

//Geolocalizor main function
bool SynthGeolocalizer::LocateImg(std::string base_path,
                                  std::string img_name,
                                  StatSynth &loc_statistics){// cv::Matx34d &Pout, GPSCoords &gps, std::string &location, std::string &direction,std::string &model_name){


    GPSCoords gps;
    cv::Matx34d Pout;
    std::string location;
    std::string direction;
    std::string model_name;

    clock_t start_localization = clock();

    //--------------------------------- Camera Matrix Extraction -------------------------------------
    cv::Mat K = cv::Mat_<double>::zeros(3,3);

    if(!ExtractKmatrix(base_path + img_name, K))
        std::cout << "   [Exif] Using default camera focal length..." << std::endl;
    else
        std::cout << "   [Exif] Using exif camera focal length..." << std::endl;

#ifdef GEO_DEBUG
    std::cout << "    [Exif] Computed Camera Matrix : " << std::endl;
    std::cout << "    " << K << std::endl;

    clock_t end_c_matrix = clock() - start_localization;

    clock_t start_ext = clock();
#endif

    //-------------------------------- Keypoints and Descriptors Extraction ---------------------------------

    std::vector<cv::KeyPoint> kps_to_query;
    std::vector<float> descriptors_to_query;

    feature_matcher->ExtractFeatures(img_name,kps_to_query,descriptors_to_query);

#ifdef GEO_DEBUG
    clock_t end_ext = clock() - start_ext;

    std::cout << "[Extraction "<< img_name << "] Extract Features: " << (double) (end_ext) / CLOCKS_PER_SEC << std::endl;

    clock_t start_vocab = clock();
#endif

    //-------------------------------- Vocabulary Documents Retrieval ---------------------------------
    std::vector<std::pair<int, double> > vocab_matches;

    geodb.QueryPotentialMatches(descriptors_to_query, top_documents ,vocab_matches);

#ifdef GEO_DEBUG
    clock_t end_vocab = clock() - start_vocab;

    clock_t end_match = 0, end_pose = 0;
#endif
    clock_t end_load = 0;
    //-------------------------------- Iterate Documents ---------------------------------

    bool success = false;

    cv::Matx44d G;
    int check_gps;
    int attempt = 0;

    for(int i = 0; i < vocab_matches.size(); i++){
        int index = vocab_matches[i].first;

        attempt++;

        if(vocab_matches[i].second == 0.0){
#ifdef GEO_DEBUG
            std::cout << "   Scores are 0 from now on...no need to continue." << std::endl;
#endif
            break;
        }

        //------------------------------- Document Loading -------------------------------

//#ifdef GEO_DEBUG
        clock_t start_load = clock();
//#endif
        std::vector<std::string> synth_view_info;
        std::stringstream ss;

        ss << index;

        geodb.SelectEntry(ss.str(),synth_view_info);

        std::vector<cv::Point3f>  p3f;
        std::vector<cv::KeyPoint>  kps;
        std::vector<float>  dcs;
        G = cv::Matx44d(0,0,0,0,
                        0,0,0,0,
                        0,0,0,0,
                        0,0,0,0);

        Read3DDoc((synth_view_info[2]).c_str(), kps,dcs,p3f,G);

        end_load += clock() - start_load;

#ifdef GEO_DEBUG
        clock_t start_match = clock();
#endif
        //------------------------------- Feature Matching -------------------------------

        std::vector<cv::Point2f> max2D;
        std::vector<cv::Point3f> max3D;

        MatchQueryImages( kps_to_query, descriptors_to_query, kps, dcs, p3f, max2D, max3D);

#ifdef GEO_DEBUG
        end_match += clock() - start_match;
#endif

        if(int(max2D.size()) == 0){
#ifdef GEO_DEBUG
            std::cout << "Not enough matches(" << i << ") when posing with " << synth_view_info[2] << std::endl;
#endif
            continue;
        }

        //-------------------------------- Pose Estimation ---------------------------------
#ifdef GEO_DEBUG
        clock_t start_pose = clock();
#endif

        success = SFMFindPose(max2D,max3D,K,Pout);

#ifdef GEO_DEBUG
        end_pose += clock() - start_pose;//*/
#endif
        if(success){
            std::vector<std::string> model_info;
            geodb.SelectModel(synth_view_info[1],model_info);
            model_name = model_info[2] + model_info[1];
            location = model_info[3];
            check_gps = atoi(model_info[4].c_str());
            break;
        }
    }


    //--------------------------------- Compute GPS Position and Direction ------------------------------------
#ifdef GEO_DEBUG
    clock_t start_gps_calculus = clock();
#endif

    if(success){
        gps = GPSCoords();

        std::cout << "   Successfully pose estimated img: " << img_name << std::endl;
        std::cout << "   Location: " << location << std::endl;

        if(check_gps){
            ModelToGPS(Pout, G,gps,direction);
            std::cout << "        [Coords] Lat: " << std::setprecision(10) << gps.latitude << ", " << std::setprecision(10)  << gps.longitude << ", " << std::setprecision(10) << gps.altitude << std::endl;
            std::cout << "        [Direction] Pointed at " << direction << std::endl;
        }else{
            std::cout << "        [Coords] GPS coordinates aren't available for this model...yet." << std::endl;
        }
    }else{
        std::cout << "   Failed to pose estimate img: " << img_name << std::endl;
    }

#ifdef GEO_DEBUG
    clock_t end_gps_calculus = clock() - start_gps_calculus;
#endif


    clock_t end_localization;

#ifdef GEO_DEBUG
    end_localization = end_c_matrix + end_ext + end_vocab  + end_match + end_pose + end_gps_calculus;
#else
    end_localization = clock() - start_localization - end_load;
#endif
    //--------------------------------- Update Statistics ---------------------------------
    if(success){
        loc_statistics = StatSynth(img_name, end_localization, attempt, gps, location, direction, model_name, Pout);

#ifdef GEO_DEBUG
        loc_statistics.setTimes(end_c_matrix,end_ext,end_vocab,end_match,end_pose,end_gps_calculus);
#endif

    }else{
        loc_statistics = StatSynth(img_name, end_localization);
#ifdef GEO_DEBUG
        loc_statistics.setTimes(end_c_matrix,end_ext,end_vocab,end_match,end_pose,end_gps_calculus);
#endif
    }



    //-------------------------------------- Timings --------------------------------------
#ifdef GEO_DEBUG
    std::cout << "[Extraction ] Extract Features: " << (double) (end_ext) / CLOCKS_PER_SEC << std::endl;
    std::cout << "[Query Vocab] Query Features: " << (double) (end_vocab) / CLOCKS_PER_SEC << std::endl;
    std::cout << "[Load Synths] Load positive matches: " << (double) (end_load) / CLOCKS_PER_SEC << std::endl;
    std::cout << "[Matches    ] Missing Matches: " << (double) (end_match) / CLOCKS_PER_SEC << std::endl;
    std::cout << "[Pose       ] Pose: " << (double) (end_pose) / CLOCKS_PER_SEC << std::endl;
    std::cout << "[Total      ] Took " << (double) (end_ext + end_vocab + end_load + end_match +  end_pose + end_c_matrix + end_gps_calculus) / CLOCKS_PER_SEC << std::endl;
    std::cout << "[Total-2    ] Took " << (double) (end_vocab + end_load + end_match +  end_pose + end_c_matrix + end_gps_calculus) / CLOCKS_PER_SEC << std::endl;
    std::cout << "[Total-3    ] Took " << (double) (end_vocab + end_match +  end_pose + end_c_matrix + end_gps_calculus) / CLOCKS_PER_SEC << std::endl;
    std::cout << "[Total-All  ] Took " << (double) (end_localization) / CLOCKS_PER_SEC << std::endl;
#endif

    return success;
}
