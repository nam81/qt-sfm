#pragma once

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/video/tracking.hpp>
#include <eigen3/Eigen/Eigen>
#include <iostream>


//3D vector normalization
void NormalizeVector(float &x, float &y, float &z);

//Compute rotation matrix given a vector and an angle
void ComputeArbRotation(cv::Point3f vec, float ang, cv::Matx33d &R);

//Rotate around a given vector
void RotateAroundVector(cv::Point3f &vec1, cv::Point3f &vec2, float ang);

//Point projections and reprojections
cv::Matx41d project2Dto3D(cv::Point2f vector2d, cv::Mat K, cv::Matx34d P);
cv::Matx31d project3Dto2D(cv::Matx41d vector3d,  cv::Mat K, cv::Matx34d P);
cv::Matx41d projectAngle2D3D(double angle, double scale , cv::Point2f pt, cv::Mat K, cv::Matx34d P);
void reprojectPoints(cv::Matx34d P, cv::Mat Kmat, std::vector<cv::Point3f> visible, std::vector<cv::Point2f> &projected2D);


//Remove Out Of Boundary points
void RemoveOOB(float width, float height, std::vector<cv::Point2f> &projected_points, std::vector<int> &back_indexing);

//Evaluate Spatial distribution given a set of points and size of image
bool evaluateSpatDist(std::vector<cv::Point2f> spat_dist, int width, int height, int cluster_step, int thrs);

//Distance between a point and a plane
bool DistancePlane(cv::Point3f normal, cv::Point3f center, cv::Point3f p);

//Method to compute mean shift clustering (not finished)
void MeanShiftClustering(float h, std::vector<cv::Point2f>& points);

//Convert 3D coordinates to GPS
void convertToGPS(cv::Matx34d P, cv::Matx44d G, std::vector<double> &gps_coords);


