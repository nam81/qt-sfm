#pragma once
#include "../Common.h"
//#include <opencv2/calib3d/calib3d.hpp>
#include "../IO/ConfigFile.h"
#include "../Geocoding/GeoOperations.h"
#include "../Database/SynthGeoDB.h"


/**
 * Contains methods responsable for cloud pre-processing.
 * Computes Synthetic Views, Adds and removes Views from
 * sql database and vocabulary tree
 *
 */


class SynthCompressor{

protected:
    float view_width;        //Synthetic View width
    float view_height;       //Synthetic View height
    float view_focal;        //Synthetic View focal length
    float view_fov;          //Synthetic View field of view
    float view_lean_angle;   //Synthetic View lean angle (to adjust views to not look at the ground)
    float view_max_focal;    //Synthetic View max focal (distance measured in the 3D system)
    float view_min_focal;    //Synthetic View min focal (distance measured in the 3D system)
    float strafe_pos;        //Units between synthetic view placement
    int min_per_view;        //Minimum number of features to consider a good coverage
    float extend_boundary;   //Used to extend the ground plane in which views are placed. Useful to distance views from walls
                             //in some 3D models

    std::string synth_root;  //Synthetic Views root folder

    std::vector<std::string> tmp_files; //Names of the temporary synthetic views files
    int next_synth_id;       //Next syntehtic id

    std::vector<std::map<int,int> > view_relationship;
    std::map<int,int> points_per_view;

    std::vector<cv::Point3f> cloud_points;                 //Store cloud 3D points
    std::vector<std::vector<float> > cloud_descriptors;    //Store cloud descriptors
    std::vector<cv::Point3f> cloud_angles3D;               //Store 3D angles
    std::vector<float> cloud_scales;                       //Store cloud scales

    SynthGeoDB geodb;        //Sqlite + voctree database for managing generated views


public:

    //============================= Constructors ===================================
    SynthCompressor(std::string s_r, std::string v_l, std::string v_db){
        synth_root = s_r;

        geodb = SynthGeoDB(s_r + "geodatabase.db", s_r, v_l, v_db);
    }

    SynthCompressor(std::string synthetic_root, ConfigOptions so){
        view_width = so.synthfeats.view_width;
        view_height = so.synthfeats.view_height;
        view_focal = so.synthfeats.view_focal;
        view_fov = so.synthfeats.view_fov;
        view_lean_angle = so.synthfeats.view_lean_angle;
        view_max_focal = so.synthfeats.view_max_focal;
        view_min_focal = so.synthfeats.view_min_focal;
        strafe_pos = so.synthfeats.strafe_pos;
        min_per_view = so.synthfeats.min_per_view;
        extend_boundary = so.synthfeats.extend_boundary;

        synth_root = synthetic_root;

        tmp_files = std::vector<std::string>();
        next_synth_id = 0;

        geodb = SynthGeoDB(synthetic_root+ "geodatabase.db",
                           std::string(so.synth_root),
                           std::string(so.vocab_learn_path),
                           std::string(so.vocab_data_path));
    }


    //================================ Methods =====================================
    //Extrapoles angles and scales into the 3D referential
    //Also retrieves point cloud information
    void CompressAndExtrapolate(std::vector<cv::Mat> K,
                                std::map<int, cv::Matx34d> pmats,
                                std::vector<CloudPoint> cloud,
                                std::vector<ImageV*> images_info);

    //Processes the original views from the inputted 3D models
    void ConvertToSViews(std::map<int,cv::Matx34d> pmats,
                         std::vector<ImageV*> images_info);

    //Computes the ground plane, and places views uniformly on it.
    //It then select ideal views based on the original and generated views.
    void ProcessSyntheticViews(std::string location_name, std::string model_name,
                               std::vector<std::string> photos_names,
                               std::vector<cv::Mat> Kmats,
                               std::map<int, cv::Matx34d> pmats,
                               bool has_gps,
                               cv::Matx44d G);

    //Reads cloud and its views, saves original views info
    void ProcessOriginalViews(const char* nvm_path,
                              const char* img_path,
                              int nvm_type,
                              std::vector<std::string> &original_imgs,
                              std::vector<cv::Mat> &Kmats,
                              std::map<int,cv::Matx34d> &pmats,
                              bool &has_gps,
                              cv::Matx44d &G);

    //========================== Storage Methods ==============================
    //Add Generated Views into the Vocabulary Tree
    void AddToVoc();

    //Remove Synthetic Views
    void RemoveSyntheticViews(std::string model_id);

    //Resets both sqlite database and voctree
    void ResetSynthDatabase();

    //============================== Prints ===================================
    //Prints database models id's and name's
    void PrintModels();

};

