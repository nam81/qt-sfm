#pragma once

#include "../Common.h"

class GroundPlane{

private:
    //Basic plane info
    cv::Point3f plane_normal;
    cv::Point3f plane_point;

    //Boundaries
    cv::Point3f lower_bound;
    cv::Point3f higher_bound;

public:
    GroundPlane(){
        plane_normal = cv::Point3f(0,0,0);
        plane_point  = cv::Point3f(0,0,0);
        lower_bound  = cv::Point3f(0,0,0);
        higher_bound = cv::Point3f(0,0,0);
    }

    //Computes the position of a synthetic view
    void ComputeSynthPosition(float x, float z, int w, float lean_angle, cv::Matx34d& P);

    //Computes the ground plane
    void ComputeGroundPlane(std::vector<cv::Point3f> cloud, std::map<int,cv::Matx34d> cameras,float extend_boundary);

    int ComputeNumberViews(float strafe);

    cv::Point3f getHB(){return higher_bound;}

    cv::Point3f getLB(){return lower_bound;}
};


