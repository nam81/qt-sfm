#include "GroundPlane.h"
#include "AuxOperations.h"
#include "GroundPlane.h"

//Computes the position of a synthetic view
void GroundPlane::ComputeSynthPosition(float x, float z, int w, float lean_angle, cv::Matx34d& P){

    float D = - plane_normal.dot(plane_point);

    cv::Point3f planez = plane_normal.cross(cv::Point3f(1,0,0));
    cv::Point3f planex = planez.cross(plane_normal);
    cv::Point3f planey = plane_normal;
    float ang = (M_PI / 6.0) * (w + 1);



    RotateAroundVector(planex, planey, ang);
    RotateAroundVector(planez, planey, ang);

    RotateAroundVector(planey, planex, - (lean_angle * M_PI / 180.0));
    RotateAroundVector(planez, planex, - (lean_angle * M_PI / 180.0));


    //Default direction
    cv::Matx33d R(planex.x,planex.y,planex.z,
                  -planey.x,-planey.y, -planey.z,
                  planez.x,planez.y,planez.z);

    cv::Point3f move_up = 0.1 * plane_normal;

    float y = (- D - (x) * plane_normal.x - (z) * plane_normal.z) / plane_normal.y;

    cv::Matx31d C(x + move_up.x,
                  y + move_up.y,
                  z + move_up.z);


    cv::Matx31d T = - R * C;

    P = cv::Matx34d(R(0,0), R(0,1), R(0,2), T(0,0),
                    R(1,0), R(1,1), R(1,2), T(1,0),
                    R(2,0), R(2,1), R(2,2), T(2,0));

}

//Computes the ground plane
void GroundPlane::ComputeGroundPlane(std::vector<cv::Point3f> cloud, std::map<int,cv::Matx34d> cameras, float extend_boundary){

    std::vector<cv::Point3f> plane_points;
    int n_points = 0;

    cv::Matx31f ABC, sumRight(0,0,0);
    cv::Matx33f sumLeft(0,0,0,
                        0,0,0,
                        0,0,0);

    //Planar surface approximation ---> output: (A, B, D) from Ax + By + D = 1z
    for(std::map<int,cv::Matx34d>::iterator iter = cameras.begin() ; iter != cameras.end(); iter++){
        cv::Matx34f P = iter->second;
        cv::Matx33f R(P(0,0), P(0,1), P(0,2),
                      P(1,0), P(1,1), P(1,2),
                      P(2,0), P(2,1), P(2,2));

        cv::Matx31f T(P(0,3),
                      P(1,3),
                      P(2,3));

        cv::Matx31f C = (-R.t()) * T;

        //plane_points.push_back(cv::Point3f(C(0,0),C(1,0), C(2,0)));
        n_points++;


        sumLeft(0,0) += pow(C(0,0),2.0);
        sumLeft(0,1) += C(0,0) * C(2,0);
        sumLeft(0,2) += C(0,0);

        sumLeft(1,0) += C(0,0) * C(2,0);
        sumLeft(1,1) += pow(C(2,0),2.0);
        sumLeft(1,2) += C(2,0);

        sumLeft(2,0) += C(0,0);
        sumLeft(2,1) += C(2,0);
        sumLeft(2,2) += 1.0;

        sumRight(0,0) += C(0,0) * C(1,0);
        sumRight(1,0) += C(2,0) * C(1,0);
        sumRight(2,0) += C(1,0);

    }

    ABC = sumLeft.inv() * sumRight;

    plane_normal = cv::Point3f(ABC(0,0),
                       -1.0,
                       ABC(1,0)
                       //(-1.0)
                       );

    cv::Point3f vecx(1,0,0);
    float angle = acos(vecx.dot(plane_normal));

    if(angle < 0){
        plane_normal.x = -plane_normal.x;
        plane_normal.y = -plane_normal.y;
        plane_normal.z = -plane_normal.z;
    }


    NormalizeVector(plane_normal.x,plane_normal.y,plane_normal.z);


    //Point from plane: p.n = -D
    plane_point = cv::Point3f(0.0,
                      ABC(2,0),
                      0.0
                      );

    //Search Upper and lower bounds and center of cloud
    cv::Point3f cloud_center(0,0,0);
    cv::Point3f cloud_upper_bound(-9000,-9000,-9000), cloud_lower_bound(9000,9000,9000);
    for(int i = 0; i < cloud.size(); i++){
        cloud_center.x = cloud_center.x + cloud[i].x;
        cloud_center.y = cloud_center.y + cloud[i].y;
        cloud_center.z = cloud_center.z + cloud[i].z;

        if(cloud[i].x < cloud_lower_bound.x)
            cloud_lower_bound.x = cloud[i].x;
        if(cloud[i].y < cloud_lower_bound.y)
            cloud_lower_bound.y = cloud[i].y;
        if(cloud[i].z < cloud_lower_bound.z)
            cloud_lower_bound.z = cloud[i].z;

        if(cloud[i].x > cloud_upper_bound.x)
            cloud_upper_bound.x = cloud[i].x;
        if(cloud[i].y > cloud_upper_bound.y)
            cloud_upper_bound.y = cloud[i].y;
        if(cloud[i].z > cloud_upper_bound.z)
            cloud_upper_bound.z = cloud[i].z;

    }

    cloud_center.x /= cloud.size();
    cloud_center.y /= cloud.size();
    cloud_center.z /= cloud.size();


    //Streatch boundaries
    cloud_lower_bound.x -= extend_boundary;
    cloud_lower_bound.y -= extend_boundary;
    cloud_lower_bound.z -= extend_boundary;

    cloud_upper_bound.x += extend_boundary;
    cloud_upper_bound.y += extend_boundary;
    cloud_upper_bound.z += extend_boundary;

    //Project both boudaries and center to the computed plane
    cv::Point3f v = cloud_center - plane_point;
    float distance = v.dot(plane_normal);

    cloud_center = cloud_center - (distance * plane_normal);

    v = cloud_lower_bound - plane_point;
    distance = v.dot(plane_normal);

    lower_bound = cloud_lower_bound - distance * plane_normal;

    v = cloud_upper_bound - plane_point;
    distance = v.dot(plane_normal);

    higher_bound = cloud_upper_bound - distance * plane_normal;
}

//Computes the number of views to place based on the boundaries and strafe value
int GroundPlane::ComputeNumberViews(float strafe){

    return ((floor(abs(higher_bound.x - lower_bound.x)) + 1.0) / strafe) * ((floor(abs(higher_bound.z - lower_bound.z)) + 1.0) / strafe) * 12.0;

}
