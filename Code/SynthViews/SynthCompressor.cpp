#include "SynthCompressor.h"

#include <math.h>
#include "../IO/IOCommon.h"
#include "../IO/IOSynthetics.h"
#include "AuxOperations.h"
#include "../../Interface/SfMUpdateListener.h"
#include <cstdio>
#include "../IO/ConfigFile.h"
#include "../IO/ExifReader.h"
#include "GroundPlane.h"
#include "Visibility.h"


//======================================== Process initial information =====================================
void SynthCompressor::CompressAndExtrapolate(std::vector<cv::Mat> K,
                                     std::map<int, cv::Matx34d> pmats,
                                     std::vector<CloudPoint> cloud,
                                     std::vector<ImageV*> images_info){

    std::cout << "[Synth] Started angle extrapolation..." << std::endl;

    view_relationship = std::vector<std::map<int,int> >(cloud.size());

    for(int i = 0; i < cloud.size(); i++){

        CloudPoint cp = cloud[i];

        cv::Point3f mean(0,0,0);
        int div = 0;
        std::vector<float> desc_aux(128, 0);
        float scale_aux = 0;

        for(std::map<int,int>::iterator it = cp.imgpt_for_img.begin(); it != cp.imgpt_for_img.end(); it++){
                cv::KeyPoint kp = images_info[it->first]->getPt(it->second);

                cv::Matx33d R;
                cv::Matx31d T;
                R(0,0) = pmats[it->first](0,0);
                R(0,1) = pmats[it->first](0,1);
                R(0,2) = pmats[it->first](0,2);

                R(1,0) = pmats[it->first](1,0);
                R(1,1) = pmats[it->first](1,1);
                R(1,2) = pmats[it->first](1,2);

                R(2,0) = pmats[it->first](2,0);
                R(2,1) = pmats[it->first](2,1);
                R(2,2) = pmats[it->first](2,2);

                T(0,0) = pmats[it->first](0,3);
                T(1,0) = pmats[it->first](1,3);
                T(2,0) = pmats[it->first](2,3);
                cv::Matx31d C = (- R.t()) * T;

                //Angle computed from the center of the camera to point
                cv::Point3f dist(cp.pt.x - C(0,0),
                                 cp.pt.y - C(1,0),
                                 cp.pt.z - C(2,0));

                float mag_dist = sqrt(pow(dist.x ,2.0) + pow(dist.y,2.0) + pow(dist.z,2.0));


                mean = mean + dist;
                div++;

                for(int k = 0; k < 128; k++){
                    desc_aux[k] += images_info[it->first]->getDes(it->second * 128 + k);
                }

                scale_aux += kp.size * mag_dist / K[it->first].at<double>(0,0);
        }

        for(int j = 0; j < 128; j++){
            desc_aux[j] /= div;
        }

        //Save orientation for further processing
        cloud_points.push_back(cp.pt);
        cloud_descriptors.push_back(desc_aux);

        //points_to_cams.push_back(cp.imgpt_for_img);
        view_relationship[i] = cp.imgpt_for_img;

        mean.x /= div;
        mean.y /= div;
        mean.z /= div;

        scale_aux /= div;
        cloud_angles3D.push_back(mean);
        cloud_scales.push_back(scale_aux);
    }

    std::cout << "[Synth] Angle extrapolation finished!" << std::endl;

}


void SynthCompressor::ProcessOriginalViews(const char* nvm_path,
                                          const char* img_path,
                                          int nvm_type,
                                          std::vector<std::string> &original_imgs,
                                          std::vector<cv::Mat> &Kmats,
                                          std::map<int,cv::Matx34d> &pmats,
                                          bool &has_gps,
                                          cv::Matx44d &G){

    std::vector<CloudPoint> pcloud;
    std::vector<cv::Vec3b> pcloudRGB;

    //Reads cloud and its views
    if(!LoadFromNVM(nvm_path, original_imgs,pmats,Kmats,pcloud,pcloudRGB,nvm_type)){
        std::cout << "[ERROR] Cloud specified wasn't found " << std::string(nvm_path) << std::endl;
        exit(0);
    }

    //Reads associated 2D info
    std::vector<ImageV*> images_info;

    bool coords_set;
    std::map<std::string, GPSCoords> gps_coordinates;
    coords_set = ReadCoordsFile(std::string(img_path) + "coordinates.txt", gps_coordinates);

    //Load images sift
    for(int i = 0; i < original_imgs.size(); i++){
        ImageV* imgv = new ImageV();
        std::vector<cv::KeyPoint> keys;
        std::vector<float> descs;
        std::string spl = split_on_limiter(original_imgs[i],'.', false);
        if(nvm_type == 0)
            ReadSiftA((img_path + spl + "sift").c_str(), keys,descs);
        else
            ReadSiftVSFM((img_path + spl + "sift").c_str(), keys,descs);

        imgv->setKeypoints(keys);
        imgv->setDescriptors(descs);

        GPSCoords gps;
        if(!coords_set){
            ExtractGeotags(img_path + original_imgs[i], gps);
        }else{
            std::map<std::string,GPSCoords>::iterator iter = gps_coordinates.find(original_imgs[i]);
            if(iter == gps_coordinates.end()){
//#ifdef GEO_DEBUG
                std::cerr << "Image " + original_imgs[i] + " didn't match any name on coordinates.txt" << std::endl;
//#endif
                exit(0);
            }

            gps = iter->second;
        }

        imgv->setGPS(gps);

        images_info.push_back(imgv);

        original_imgs[i] = img_path + original_imgs[i];

        std::cout << "Read img: " << original_imgs[i] << " Pts: " << keys.size() << std::endl;
    }

    //Extrapolate 2D info into 3D | Compress descriptors
    CompressAndExtrapolate(Kmats,pmats,pcloud,images_info);

    //Compute the GPS matrix based on the original views
    has_gps = Compute3DtoGPSMat(images_info,pmats,G);

    //Generate synthetic view versions of original views | saves info to temporary files
    ConvertToSViews(pmats,images_info);
}

void SynthCompressor::ConvertToSViews(std::map<int,cv::Matx34d> pmats, std::vector<ImageV*> images_info){

    std::vector<std::vector<cv::KeyPoint> > kps(pmats.size());
    std::vector<std::vector<float> > descs(pmats.size());
    std::vector<std::vector<cv::Point3f> > p3ds(pmats.size());

    for(int i = 0; i < cloud_points.size(); i++){

        for(std::map<int,int>::iterator iter = view_relationship[i].begin(); iter != view_relationship[i].end(); iter++){
            int cam_index = iter->first;

            p3ds[cam_index].push_back(cloud_points[i]);
            kps[cam_index].push_back(images_info[cam_index]->getPt(iter->second));
            for(int j = 0; j < 128; j++)
                descs[cam_index].push_back(cloud_descriptors[i][j]);
        }
    }



    for(int i = 0; i < pmats.size(); i++){
        std::stringstream ss;
        points_per_view[next_synth_id] = kps[i].size();

        ss << synth_root << next_synth_id++ << ".tmp";
//#ifndef SYNTH_DEBUG
        tmp_files.push_back(ss.str());

//#endif
        SaveTMP(ss.str().c_str(),p3ds[i],kps[i],descs[i]);
    }

}


//========================================== Generate Synthetic Views =============================================
void SynthCompressor::ProcessSyntheticViews(std::string location_name,
                               std::string model_name,
                               std::vector<std::string> photos_names,
                               std::vector<cv::Mat> Kmats,
                               std::map<int, cv::Matx34d> pmats,
                               bool has_gps,
                               cv::Matx44d G){

    //Computes focallength for synthetic views
    float view_focal_length = 2.0 * tan((view_fov * M_PI / 180.0)/2.0) * view_width;

    //Initializes info based on the original views
    std::map<int, cv::Matx34d> synth_pmats(pmats);
    std::vector<CloudPoint> cloud;

    //Computes ground plane
    //cv::Point3f plane_point, plane_normal, plane_highest, plane_lowest;
    GroundPlane ground_plane;
    ground_plane.ComputeGroundPlane(cloud_points,pmats,extend_boundary);

    //Computes views to place
    int n_views_to_place = ground_plane.ComputeNumberViews(strafe_pos);

    std::cout << "   Placing about " << n_views_to_place << std::endl;
    std::cout << "[Synth] Started view filtering" << std::endl;

    int view = -1;
    cv::Point3f plane_highest = ground_plane.getHB();
    cv::Point3f plane_lowest = ground_plane.getLB();

    for(float x = plane_lowest.x; x < plane_highest.x ; x+= strafe_pos){
        std::cout << "   Ongoing processing: " << view << std::endl;
        for(float z = plane_lowest.z; z < plane_highest.z; z+= strafe_pos){
            for(int w = 0; w < 12; w++){
                view++;

                std::vector<int> points_to_filter;
                std::vector<cv::Point3f> visible;
                std::vector<cv::Point2f> projected2D;

                cv::Matx34d P;

                ground_plane.ComputeSynthPosition(x,z,w,view_lean_angle,P);

                //Re extrapolate scales to 2D again and check point visibility based on the frustum culling
                std::vector<float> scales_copy(cloud_scales);
                CheckPointVisibility(view_width,view_height,view_fov,view_min_focal,view_max_focal,Kmats[0],P,cloud_points,cloud_angles3D,scales_copy,points_to_filter,visible);

                //If the view doesn't sees points at all, discard it
                if(visible.size() == 0)
                    continue;

                //Build the camera matrix
                Kmats[0].at<double>(0,0) = view_focal_length;
                Kmats[0].at<double>(1,1) = view_focal_length;
                Kmats[0].at<double>(0,2) = view_width / 2.0;
                Kmats[0].at<double>(1,2) = view_height / 2.0;

                //reproject points from 3D into 2D
                reprojectPoints(P,Kmats[0],visible,projected2D);

                //Remove Out of Bound points
                RemoveOOB(view_width, view_height, projected2D, points_to_filter);

                //Evaluate View spatial distribution
                bool good_view = evaluateSpatDist(projected2D,view_width, view_height, 25, 0.3 * view_width);

                //Discard view if not a good view
                if(!good_view){
                    continue;
                }

                //Show resulting view
#if 0
                cv::Mat ooo = cv::Mat(HEIGHT,WIDTH, CV_8U,cv::Scalar(255,255,255));
                //std::vector<cv::KeyPoint> imgPoints = keypoints[0];

                cv::Mat reprojected; ooo.copyTo(reprojected);

                for(int ppt=0;ppt<projected2D.size();ppt++) {
                    //cv::circle(reprojected, imgPoints[ppt].pt, 2, cv::Scalar(255,0,0), CV_FILLED);
                    cv::circle(reprojected, projected2D[ppt], 2, cv::Scalar(0,255,0),CV_FILLED);
                }

                cv::imshow("__tmp", reprojected);
                cv::waitKey(0);
                cv::destroyWindow("__tmp");
#endif

                if(points_to_filter.size() < 30)
                    continue;

                std::vector<cv::KeyPoint> points_to_keep;
                std::vector<float> desc_to_keep;
                std::vector<cv::Point3f> cps_to_keep;

                //Remove all points selected
                for(int index = 0; index < projected2D.size(); index++){
                    cv::KeyPoint newkp;
                    newkp.pt = projected2D[index];
                    newkp.angle = 0;
                    newkp.size = scales_copy[points_to_filter[index]];

                    points_to_keep.push_back(newkp);

                    cps_to_keep.push_back(cloud_points[points_to_filter[index]]);

                    view_relationship[points_to_filter[index]].insert(std::make_pair(next_synth_id,0));

                    for(int desc_index = 0; desc_index < 128; desc_index++){
                        float d = cloud_descriptors[points_to_filter[index]][desc_index];
                        desc_to_keep.push_back(d);
                    }
                }

                points_per_view[next_synth_id] = points_to_keep.size();



                synth_pmats[next_synth_id] = P;

                //Save resulting view
                std::stringstream ss;
                ss << synth_root << next_synth_id<< ".tmp";

                next_synth_id++;
#ifndef SYNTH_DEBUG
                tmp_files.push_back(ss.str());
#endif



                SaveTMP(ss.str().c_str(),cps_to_keep,points_to_keep,desc_to_keep);
            }
        }
    }

    std::cout << "[Synth] Compute View coverage now" << std::endl;

    //Computes the view corage based on the original and generate views
    std::map< int, std::map<int,int> > visibility_matrix;
    ComputeViewCoverage(view_relationship,points_per_view,min_per_view,visibility_matrix);

    std::cout << "[Synth] View coverage done" << std::endl;

    std::cout << "[Synth] Started view selection" << std::endl;

    //Selects the best views based on how many views they cover
    std::vector<int> best_views;
    SelectBestViews(visibility_matrix,best_views);

    std::vector<int> camera_map;
    std::vector<std::string> names_aux;
    std::map<int,cv::Matx34d> pmats_aux;
    bool save_cloud = true;

    //Open Database to Update Synthetics Pool
    geodb.InitializeDB();
    geodb.CreateSynthTable();

    std::string check_gps;
    if(has_gps)
        check_gps = "1";
    else
        check_gps = "0";

    std::cout << "[Synth] Started adding model, views and images to database" << std::endl;

    //Insert new model or update existing
    std::string model_id;
    bool model_exists = false;
    if(!geodb.CheckModelExistence(model_name, model_id)){
        geodb.InsertModel(model_name,location_name,check_gps,model_id);
        geodb.InsertPhotosPaths("",photos_names,model_id);
    }else{
        geodb.RmImagesFromDB(model_id, false);

        model_exists = true;
    }

    int lastindex = model_name.find_last_of("/");
    std::string model_rawname = model_name.substr(lastindex + 1, model_name.size() - 1);

    //Begin Synthetic saving
    for(int most_visible = 0; most_visible < best_views.size(); most_visible++)
    {
        pmats_aux[most_visible] = synth_pmats[best_views[most_visible]];

        camera_map.push_back(most_visible);

        std::stringstream ss;
        ss << synth_root;ss << best_views[most_visible];ss << ".tmp";
        std::vector<cv::KeyPoint> keys;
        std::vector<float> descs;
        std::vector<cv::Point3f> cps;

        ReadTMP(ss.str().c_str(), cps,keys,descs);

        //Save resulting view
        std::stringstream ss1,ss2;
        ss1 << model_rawname << "_" << most_visible;ss1 << ".sift";
        ss2 << model_rawname << "_" << most_visible;ss2 << ".synt";

        names_aux.push_back(ss1.str());

        //if(keys.size() > 50)
            Save3DDoc((synth_root +  ss2.str()).c_str(), cps, keys,  descs, G);
        //else
        //    continue;
        //Update database
        //if(!model_exists)
            geodb.InsertSynthView(synth_root + ss2.str(),model_id);
        //else
        //    geodb.UpdateSynthView();
        //geodb.InsertEntry(synth_root +  ss2.str(),model_name,location_name,check_gps);

        std::cout << "   Saved view " << ss2.str() << " with " << cps.size() << " features." << std::endl;

        if(save_cloud){
            for(int i = 0; i < cps.size(); i++){
                CloudPoint cp;
                cp.pt = cps[i];
                cp.imgpt_for_img.insert(std::make_pair(most_visible,i));
                cloud.push_back(cp);
            }
        }
    }

    std::cout << "[Synth] Ended adding model, views and images to database" << std::endl;

    geodb.closeDB();

    std::cout << "[Synth] View selection done" << std::endl;

    //Save the compressed model for visualization purpose
    if(save_cloud){
        pmats = pmats_aux;

        //Save model
        std::vector<cv::Vec3b> pcloudRGB = std::vector<cv::Vec3b>(cloud.size(), cv::Vec3b(100,100,100));
        SaveToNVM((synth_root +"cloud_synth.nvm").c_str(), names_aux,pmats,Kmats[0],cloud,pcloudRGB);
    }


    //Clean tmp files
//#ifndef SYNTH_DEBUG
    std::cout << "[Synth] Cleaning up tmp files.." << std::endl;
    for(int i = 0; i < tmp_files.size(); i++){
        std::remove(tmp_files[i].c_str());
    }
//#endif

    std::cout << "[Synth] Synthetic process ended" << std::endl;
}




//=========================================== Database and Voc Operations =========================================
void SynthCompressor::RemoveSyntheticViews(std::string model_id){

    geodb.InitializeDB();
    std::cout << "   Started removing model " << model_id << std::endl;

    geodb.RmImagesFromDB(model_id,true);
    std::cout << "   Finished removing model " << std::endl;

}

void SynthCompressor::ResetSynthDatabase(){

    geodb.InitializeDB();

    std::cout << "   Started reseting database & vocabulary tree" << std::endl;
    geodb.ResetDatabase();
    std::cout << "   Finished reseting database & vocabulary tree" << std::endl;

}

void SynthCompressor::AddToVoc(){

    geodb.InitializeDB();
    geodb.AddImagesToDB();
}

void SynthCompressor::PrintModels(){
    geodb.InitializeDB();

    std::vector<std::string> models;
    geodb.SelectModelsIdName(models);

    for(int i = 0; i < models.size(); i+=2){

        std::cout << "  [ID]    [NAME]" << std::endl;
        std::cout << "    " << models[i] << "      " << models[i + 1] << std::endl;
    }

}


