﻿#include "SynthMain.h"
#include "../IO/IOCommon.h"
#include "../SynthViews/SynthCompressor.h"
#include "../IO/ExifReader.h"
#include "AuxOperations.h"
#include "../IO/ConfigFile.h"
#include "SynthGeoLocalizor.h"
#include "../IO/Statistics.h"

//Synthetic Views Builder and add view to the database
void BuildSyntheticViews(std::string cloudpath, int type, std::string location_name, ConfigOptions options){

    std::vector<std::string> /*imgnames,*/ original_imgs;
    std::map<int,cv::Matx34d> Pmats;
    std::vector<cv::Mat> Kmats;
    cv::Matx44d G;
    bool has_gps;

    std::cout << "[Synthetic Builder]" << std::endl;

    std::string imgpath = split_on_limiter(cloudpath, '/', false);

    SynthCompressor synthetics(std::string(options.synth_root), options);

    //Process original views
    synthetics.ProcessOriginalViews(cloudpath.c_str(),imgpath.c_str(),type, original_imgs, Kmats,Pmats,/*pcl,pcl_descriptors,pcl_scales,pcl_ori,/*p_to_p,*/has_gps,G);

    //Process synthetic views    
    synthetics.ProcessSyntheticViews(location_name,cloudpath, original_imgs, Kmats, Pmats,/*, pcl,pcl_descriptors,pcl_scales,pcl_ori,/*p_to_p,imgnames,*/has_gps, G);

}

//Remove Synthetic Model from database including related information
void RemoveSyntheticModel(std::string model_id, std::string synth_root, std::string v_l, std::string v_db){

    SynthCompressor synthetics(synth_root,v_l,v_db);

    synthetics.RemoveSyntheticViews(model_id);

}

//Reset Synthetic Database
void ResetSynthGeoDB(std::string synth_root, std::string v_l, std::string v_db){
    SynthCompressor synthetics(synth_root, v_l,v_db);

    synthetics.ResetSynthDatabase();

}

void PrintSynthModels(std::string synth_root, std::string v_l, std::string v_db){
    SynthCompressor synthetics(synth_root, v_l,v_db);
    synthetics.PrintModels();
}


//Add database views to the vocabulary tree
void AddViewsToVocTree(std::string synth_root, std::string v_l, std::string v_db){
    SynthCompressor synthetics(synth_root,v_l, v_db);
    synthetics.AddToVoc();
}

//Synthetic Geolocalizer
void GeolocalizeSynthetic(const char* list_path,
                          ConfigOptions options){

    //std::vector<std::string> posed_names;
    //std::vector<cv::Matx34d> posed_P;

    //Geolocalizer initialization
    std::string imgs_path = std::string(list_path);

    imgs_path = split_on_limiter(imgs_path, '/', false);

    SynthGeolocalizer geoloc(std::string(imgs_path), false, options);

    //SiftGPU initialization
    geoloc.InitializeSiftGPU(options.synthfeats);

    //Get photograph list provided
    std::vector<std::string> imgs_to_pose;
    if(!ReadImageList(list_path/*(std::string(imgs_path) + "pose_list.txt").c_str()*/, imgs_to_pose)){
        std::cerr << "[ERROR] pose_list.txt nout found" << std::endl;
        exit(0);
    }

    //Load vocabulary tree
    geoloc.LoadVocTree();

    //Initializes Output Statistics
    StatisticsSynth statistics = StatisticsSynth(options.synthfeats.use_cuda);

    int posed_index = 0;
    for(int i = 0; i < imgs_to_pose.size(); i++){

        StatSynth loc_statistics;
        if(geoloc.LocateImg(imgs_path,imgs_to_pose[i],loc_statistics)){//P,gps,location, direction, model_name)){

            posed_index++;
        }

        statistics.insertInfo(loc_statistics);

    }

//#ifdef GEO_DEBUG
    std::cout << "-----------------------------------------------------------------------" << std::endl <<std::endl;
    std::cout << "[Total Final] Posed " << imgs_to_pose.size() << " imgs (" << posed_index << " successful)" << std::endl;// << (double) time_end / CLOCKS_PER_SEC << " secs (+- " << ((double) time_end / CLOCKS_PER_SEC) / int(imgs_to_pose.size()) << "secs per img)" << std::endl;
//#endif

    //Saved Output File
    statistics.toFileStatistics(imgs_path);

    //Save positions to visualization file
    statistics.toFileViz(imgs_path);
    //SaveVizOutput((imgs_path + "output_viz.txt").c_str(), img_to_model,posed_names,posed_P);

    //Unload SiftGPU
    geoloc.UnloadSiftGPU();

    return ;
}



