#include "AuxOperations.h"
#include "../Geocoding/GeoOperations.h"
#include "opencv2/calib3d/calib3d.hpp"

//3D vector normalization
void NormalizeVector(float &x, float &y, float &z){

    float norm = sqrt( pow(x, 2.0) + pow(y, 2.0) + pow(z, 2.0));

    x /= norm;
    y /= norm;
    z /= norm;

}

//Compute rotation matrix given a vector and an angle
void ComputeArbRotation(cv::Point3f vec, float ang, cv::Matx33d& R){

    R = cv::Matx33d(cos(ang) + pow(vec.x,2.0) * (1-cos(ang)),
                    vec.x * vec.y * (1-cos(ang)) - vec.z * sin(ang),
                    vec.x * vec.z * (1-cos(ang)) + vec.y * sin(ang),

                    vec.y * vec.x * (1-cos(ang)) + vec.z * sin(ang),
                    cos(ang) + pow(vec.y,2.0) * (1-cos(ang)),
                    vec.y * vec.z * (1-cos(ang)) - vec.x * sin(ang),

                    vec.z * vec.x * (1-cos(ang)) - vec.y * sin(ang),
                    vec.z * vec.y * (1-cos(ang)) + vec.x * sin(ang),
                    cos(ang) + pow(vec.x,2.0) * (1-cos(ang)));
}

//Rotate around a given vector
void RotateAroundVector(cv::Point3f &vec1, cv::Point3f &vec2, float ang){
    float ux = vec2.x * vec1.x;
    float uy = vec2.y * vec1.x;
    float uz = vec2.z * vec1.x;
    float vx = vec2.x * vec1.y;
    float vy = vec2.y * vec1.y;
    float vz = vec2.z * vec1.y;
    float wx = vec2.x * vec1.z;
    float wy = vec2.y * vec1.z;
    float wz = vec2.z * vec1.z;
    float sa = sin(ang);
    float ca = cos(ang);

    vec1.x = vec2.x * (ux + vy + wz) + (vec1.x * (pow(vec2.y, 2.0) + pow(vec2.z, 2.0)) - vec2.x * (vy + wz)) * ca + (-wy + vz) * sa;
    vec1.y = vec2.y * (ux + vy + wz) + (vec1.y * (pow(vec2.x, 2.0) + pow(vec2.z, 2.0)) - vec2.y * (ux + wz)) * ca + (wx - uz) * sa;
    vec1.z = vec2.z * (ux + vy + wz) + (vec1.z * (pow(vec2.x, 2.0) + pow(vec2.y, 2.0)) - vec2.z * (ux + vy)) * ca + (-vx + uy) * sa;

}



//======================================= Point Projections =============================================
//Point projections and reprojections
cv::Matx41d project2Dto3D(cv::Point2f vector2d, cv::Mat K, cv::Matx34d P){

    cv::Matx44d K4x4(K.at<double>(0,0), K.at<double>(0,1), K.at<double>(0,2), 0.0,
                     K.at<double>(1,0), K.at<double>(1,1), K.at<double>(1,2), 0.0,
                     K.at<double>(2,0), K.at<double>(2,1), K.at<double>(2,2), 0.0,
                     0.0, 0.0, 0.0, 1.0);

    cv::Matx44d P4x4(P(0,0), P(0,1), P(0,2), P(0,3),
                     P(1,0), P(1,1), P(1,2), P(1,3),
                     P(2,0), P(2,1), P(2,2), P(2,3),
                     0.0, 0.0, 0.0, 1.0);

    cv::Matx44d KP4x4 = K4x4 * P4x4;
    cv::Matx44d KPinv = KP4x4.inv();
    cv::Matx41d alpha2d( vector2d.x, vector2d.y, 1.0, 1.0);


    cv::Matx41d vector3d =KPinv * alpha2d ;

    return vector3d;
}

cv::Matx31d project3Dto2D(cv::Matx41d vector3d,  cv::Mat K, cv::Matx34d P){
    //K = K * (1.0/ K(0,0));
    //K(2,2) = 1.0;

    cv::Matx34d K3x4(
                K.at<double>(0,0), K.at<double>(0,1), K.at<double>(0,2), 0.0,
                K.at<double>(1,0), K.at<double>(1,1), K.at<double>(1,2), 0.0,
                K.at<double>(2,0), K.at<double>(2,1), K.at<double>(2,2), 0.0);


    cv::Matx44d P4x4(
                P(0,0), P(0,1), P(0,2), P(0,3),
                P(1,0), P(1,1), P(1,2), P(1,3),
                P(2,0), P(2,1), P(2,2), P(2,3),
                0.0, 0.0, 0.0, 1.0);



    cv::Matx34d aux1 = K3x4 * P4x4;
    cv::Matx31d vector2d = aux1 * vector3d;

    return vector2d;
}

cv::Matx41d projectAngle2D3D(double angle, double scale , cv::Point2f pt, cv::Mat K, cv::Matx34d P){

    float degrees =  angle * 180.0 / 3.14;
    cv::Point2f angle2D;

    angle2D.x = cos(angle) * scale + pt.x;
    angle2D.y = sin(angle) * scale + pt.y;


    //angle2D.x *= scale;
    //angle2D.y *= scale;

    cv::Matx44d K4x4(K.at<double>(0,0), K.at<double>(0,1), K.at<double>(0,2), 0.0,
                     K.at<double>(1,0), K.at<double>(1,1), K.at<double>(1,2), 0.0,
                     K.at<double>(2,0), K.at<double>(2,1), K.at<double>(2,2), 0.0,
                     0.0, 0.0, 0.0, 1.0);

    cv::Matx44d P4x4(P(0,0), P(0,1), P(0,2), P(0,3),
                     P(1,0), P(1,1), P(1,2), P(1,3),
                     P(2,0), P(2,1), P(2,2), P(2,3),
                     0.0, 0.0, 0.0, 1.0);

    cv::Matx44d KP4x4 = K4x4 * P4x4;

    cv::Matx41d alpha2d( angle2D.x, angle2D.y, 1.0, 1.0);

    cv::Matx44d KPinv = KP4x4.inv();

    cv::Matx41d alpha = KPinv * alpha2d;



    cv::Matx34d K3x4(K.at<double>(0,0), K.at<double>(0,1), K.at<double>(0,2), 0.0,
                     K.at<double>(1,0), K.at<double>(1,1), K.at<double>(1,2), 0.0,
                     K.at<double>(2,0), K.at<double>(2,1), K.at<double>(2,2), 0.0);


    cv::Matx44d P4x42(P(0,0), P(0,1), P(0,2), P(0,3),
                      P(1,0), P(1,1), P(1,2), P(1,3),
                      P(2,0), P(2,1), P(2,2), P(2,3),
                      0.0, 0.0, 0.0, 1.0);



    cv::Matx34d aux1 = K3x4 * P4x42;
    cv::Matx31d vector2d = aux1 * alpha;
//*/
    int asd = 0;

    return  alpha;
}

void reprojectPoints(cv::Matx34d P, cv::Mat Kmat, std::vector<cv::Point3f> visible, std::vector<cv::Point2f> &projected2D){
    //Reproject points
    cv::Mat_<double> rvec(3,3);
    cv::Mat_<double> t(3,1);
    rvec.at<double>(0,0) = P(0,0);
    rvec.at<double>(0,1) = P(0,1);
    rvec.at<double>(0,2) = P(0,2);

    rvec.at<double>(1,0) = P(1,0);
    rvec.at<double>(1,1) = P(1,1);
    rvec.at<double>(1,2) = P(1,2);

    rvec.at<double>(2,0) = P(2,0);
    rvec.at<double>(2,1) = P(2,1);
    rvec.at<double>(2,2) = P(2,2);

    t.at<double>(0,0) = P(0,3);
    t.at<double>(1,0) = P(1,3);
    t.at<double>(2,0) = P(2,3);

    cv::Mat distortion_coeff = cv::Mat_<double>::zeros(1,4);
    cv::projectPoints(visible, rvec, t, Kmat, distortion_coeff, projected2D);
}


//======================================= Spatial Distribution ==========================================
//Evaluate Spatial Distribution
bool evaluateSpatDist(std::vector<cv::Point2f> spat_dist, int width, int height, int cluster_step, int thrs){

    int width_step_size = width / cluster_step;
    int height_step_size = height / cluster_step;

    int hist_center_w = width_step_size / 2;
    int hist_center_h = height_step_size / 2;

    float mean_w = 0;
    float mean_h = 0;

    int thrs_w = thrs / cluster_step, thrs_h = (thrs / cluster_step) * height / width;
    int total_points = 0;

    for(int i = 0; i < spat_dist.size(); i++){
        //if(points_to_filter[i] == 1){
            cv::Point2f pt = spat_dist[i];

            mean_w += pt.x / cluster_step;
            mean_h += pt.y / cluster_step;

            total_points++;
        //}
    }

    mean_w /= total_points;
    mean_h /= total_points;

    bool width_validation = (hist_center_w + thrs_w > mean_w) && (hist_center_w - thrs_w < mean_w);
    bool height_validation = (hist_center_h + thrs_h > mean_h) && (hist_center_h - thrs_h < mean_h);

    if(width_validation && height_validation)
        return true;
    else
        return false;
}

//Remove Out of Bound points
void RemoveOOB(float width, float height, std::vector<cv::Point2f> &projected_points, std::vector<int> &back_indexing){

    std::vector<cv::Point2f> projected_aux;
    std::vector<int> indexing_aux;
    for(int i = 0; i < projected_points.size(); i++){
        cv::Point2f p2f = projected_points[i];

        if(p2f.x < 0 || p2f.x > width || p2f.y > height || p2f.y < 0){
            continue;
        }

        projected_aux.push_back(projected_points[i]);
        indexing_aux.push_back(back_indexing[i]);
    }

    projected_points = projected_aux;
    back_indexing = indexing_aux;
}

//=========================================    MSC (Incomplete and not used) ==========================
void MeanShiftClustering(float h, std::vector<cv::Point2f>& points){

    std::map<std::pair<int,int>, std::vector<cv::Point2f> > clusterization;

    //For each point
    for(int i = 0; i < points.size(); i++){

        //Choose point
        cv::Point2f last_y = points[i];

        int t = 0;

        bool not_converged = true;

        //Save to NVM file
        while(not_converged && t < 100){

            cv::Point2f upper_exp(0,0);
            float under_exp = 0;

            float ins = 0;

            for(int j = 0; j < points.size(); j++){

                cv::Point2f to_norm = (last_y - points[j]);



                float ins_aux = - pow(sqrt(pow(to_norm.x,2.0) + pow(to_norm.y,2.0)), 2.0f) / pow(h,2.0);
                ins += ins_aux;

                ins_aux = exp(ins_aux);


                //float exp = - pow(norm(last_y - xj),2.0) / pow(h,2.0);

                upper_exp = upper_exp + points[j] * ins_aux;
                under_exp = under_exp + ins_aux;
            }

            upper_exp.x = upper_exp.x / under_exp;
            upper_exp.y = upper_exp.y / under_exp;


            cv::Point2f norm_aux = upper_exp - last_y;
            float norm = pow(sqrt(pow(norm_aux.x,2.0) + pow(norm_aux.y,2.0)),2.0);

            //Test convergence
            float converge = (1.0 / pow(h,2.0)) * (- ins) * norm;//upper_exp - last_y;

            if(converge < 1e-2 && converge > -1e-2)
                not_converged = false;
            else
                last_y = upper_exp;

            t++;

        }

        bool exists = false;


        if(clusterization.find(std::make_pair((int) last_y.x,(int) last_y.y)) != clusterization.end())
            clusterization.find(std::make_pair((int) last_y.x, (int) last_y.y))->second.push_back(last_y);
        else{
            std::vector<cv::Point2f> p_aux;
            p_aux.push_back(last_y);
            clusterization[std::make_pair((int) last_y.x,(int) last_y.y)] =  p_aux;
         }

        std::cout;
    }

    std::vector<cv::Point2f> ppp;
    for(std::map<std::pair<int,int> ,std::vector<cv::Point2f> >::iterator i = clusterization.begin(); i != clusterization.end(); ++i) {
        ppp.push_back(cv::Point2f(i->first.first, i->first.second));
    }

    points = ppp;
    std::cout << "Points: " << ppp.size() << std::endl;

}

//=========================================    GEO    ==========================================

//Convert 3D coordinates to GPS
void convertToGPS(cv::Matx34d P, cv::Matx44d G, GPSCoords &gps_coords){

    cv::Matx33d R;
    cv::Matx31d T;
    R(0,0) = P(0,0);
    R(0,1) = P(0,1);
    R(0,2) = P(0,2);

    R(1,0) = P(1,0);
    R(1,1) = P(1,1);
    R(1,2) = P(1,2);

    R(2,0) = P(2,0);
    R(2,1) = P(2,1);
    R(2,2) = P(2,2);

    T(0,0) = P(0,3);
    T(1,0) = P(1,3);
    T(2,0) = P(2,3);
    cv::Matx31d C = (- R.t()) * T;
    cv::Matx41d C2(C(0,0),
                   C(1,0),
                   C(2,0),
                   1.0);

    cv::Matx41d gps =  G * C2;//C2 * G.inv() ;
    gps_coords.latitude = gps(0,0) / gps(0,3);
    gps_coords.longitude = gps(0,1) / gps(0,3);
    gps_coords.altitude = gps(0,2) / gps(0,3);
}



