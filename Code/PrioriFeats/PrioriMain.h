#include "../IO/ConfigFile.h"
#include "../Common.h"


//Build Prioritized Features Models
void BuildPrioritizedCloud(std::string nvm_path,
                           int nvm_type,
                           std::string location,
                           ConfigOptions options);


//Geocode new Photographs with Priori Feats
void GeolocalizePrioritized(const char* list_path,
                            ConfigOptions options);


//Database operations
void RemovePrioriModel(std::string model_id,
                       std::string priori_root);

void ResetPrioriGeoDB(std::string priori_root);

void PrintPrioriModels(std::string priori_root);
