#pragma once

#include "../Common.h"
#include "../../3rdparty/VocabTree2/VocabLib/keys2.h"
#include "../../3rdparty/VocabTree2/lib/ann_1.1_char/include/ANN/ANN.h"
#include "../IO/ConfigFile.h"
#include "../Database/PrioriGeoDB.h"


class PrioriFeats{

protected:
    int Ks;
    int Kc;
    int max_seed_points;
    int max_comp_points;

    std::string priori_root;
    PrioriGeoDB priori_geodb;

public:
    PrioriFeats(std::string priori_r){
        Ks = 0;
        Kc = 0;
        max_seed_points = 0;
        max_comp_points = 0;
        priori_root = priori_r;
        priori_geodb = PrioriGeoDB(priori_r + "geodatabase.db");
    }

    PrioriFeats(ConfigOptions options){
        Ks = options.priorifeats.Ks;
        Kc = options.priorifeats.Kc;
        max_seed_points = options.priorifeats.max_seed_points;
        max_comp_points = options.priorifeats.max_comp_points;
        priori_root = std::string(options.priori_root);
        priori_geodb = PrioriGeoDB(priori_root + "geodatabase.db");
    }

    void BuildPrioritizedCloud(std::string model_name, std::string clouds, std::string cloudc, std::string location, std::string imgs_root, std::vector<std::string> imgs_names, std::vector<ImageV*> img_info, std::map<int,cv::Matx34d> Pmats, std::vector<CloudPoint> pcl);

    void GenerateCompressedModel(std::string filename, std::string belonging_model,
                                 std::vector<ImageV*> img_info,
                                 std::vector<CloudPoint> pcl,
                                 std::vector<int> counter_per_img,
                                 std::vector<std::vector<int> > visibility_matrix,
                                 cv::Matx44d G, int &npts, int &nviews);

    void GenerateSeedModel(std::string filename,
                           std::string belonging_model,
                           std::vector<ImageV*> img_info,
                           std::vector<CloudPoint> pcl,
                           std::vector<int> counter_per_img,
                           std::vector<std::vector<int> > visibility_matrix,
                           cv::Matx44d G, int &npts, int &nviews);

    void RemoveModel(std::string model_id);
    void ResetDB();
    void PrintModels();

};





