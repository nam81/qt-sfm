#pragma once
#include "../Common.h"

class PBundleAdjuster {
public:
    void adjustBundle(std::vector<cv::Point2f> p2D,
                      //cv::Mat& cam_matrix,
                      std::vector<cv::Point3f> p3D, cv::Mat Kmat,
                      cv::Matx34d &Pout);

    //int pba(int argc, char* argv[]);
private:
    int Count2DMeasurements(const std::vector<CloudPoint>& pointcloud);


};
