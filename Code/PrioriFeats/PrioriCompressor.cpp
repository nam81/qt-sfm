﻿#include "PrioriCompressor.h"
//#include "../Readers/IOCommon.h"
#include "../IO/IOPrioriFeats.h"
//#include "../../3rdparty/VocabTree2/lib/ann_1.1_char/include/ANN/ANN.h"
#include "../Geocoding/GeoOperations.h"
#include <sstream>

//#define Kc 100
//#define Ks 5
//#define MAX_SEED_POINTS 2000
//#define MAX_COMP_POINTS 0    //0 = infinite

bool cmpByVisibility(const std::pair<int,int> &a,const  std::pair<int,int> &b) {
    return a.second > b.second;
}

void ComputeGeoAxis2(std::vector<cv::Point3d> positions, std::vector<std::vector<double> > gps_coordinates, cv::Matx33d &geo_axis){

    geo_axis = cv::Matx33d(0.0,0.0,0.0,
                           0.0,0.0,0.0,
                           0.0,0.0,0.0);

    for(int i = 0; i < positions.size(); i++){
        for(int j = 0; j < gps_coordinates.size(); j++){
            geo_axis(0,0) = geo_axis(0,0) + positions[i].x * gps_coordinates[j][0];
            geo_axis(0,1) = geo_axis(0,1) + positions[i].x * gps_coordinates[j][1];
            geo_axis(0,2) = geo_axis(0,2) + positions[i].x * gps_coordinates[j][2];

            geo_axis(1,0) = geo_axis(1,0) + positions[i].x * gps_coordinates[j][0];
            geo_axis(1,1) = geo_axis(1,1) + positions[i].x * gps_coordinates[j][1];
            geo_axis(1,2) = geo_axis(1,2) + positions[i].x * gps_coordinates[j][2];

            geo_axis(2,0) = geo_axis(2,0) + positions[i].x * gps_coordinates[j][0];
            geo_axis(2,1) = geo_axis(2,1) + positions[i].x * gps_coordinates[j][1];
            geo_axis(2,2) = geo_axis(2,2) + positions[i].x * gps_coordinates[j][2];
        }
    }

    geo_axis(0,0) = geo_axis(0,0) / double(positions.size());
    geo_axis(0,1) = geo_axis(0,1) / double(positions.size());
    geo_axis(0,2) = geo_axis(0,2) / double(positions.size());

    geo_axis(1,0) = geo_axis(1,0) / double(positions.size());
    geo_axis(1,1) = geo_axis(1,1) / double(positions.size());
    geo_axis(1,2) = geo_axis(1,2) / double(positions.size());

    geo_axis(2,0) = geo_axis(2,0) / double(positions.size());
    geo_axis(2,1) = geo_axis(2,1) / double(positions.size());
    geo_axis(2,2) = geo_axis(2,2) / double(positions.size());

}

int findHighest(std::vector<std::pair<int,int> > elemvec){

   std::sort(elemvec.begin(), elemvec.end(),cmpByVisibility);

    return elemvec[0].first;
}

void visibilityCoverage(int n_views, int n_points, std::vector<std::vector<int> > &visibility_matrix_bp, std::vector<int> &points_per_view, int K, int upper_limit, std::map<int,int> &ptk){

    std::vector<std::vector<int> > visibility_matrix = visibility_matrix_bp;

    bool zero_matrix = false;
    int n_pt_kept = 0;
    std::vector<int> counter_per_view(n_views,K);
    std::map<int,int> coverage;


    //Insert any point which can't fill the condition of >= K visibility
    std::vector<int> imp_to_cover;
    for(int i = 0; i < n_views; i++){
        if(points_per_view[i] < K)
            imp_to_cover.push_back(i);
    }

    //Initialize visibility numbers
    std::vector<std::pair<int,int> > n_visible;// = std::vector<std::pair<int,int> >(n_points, std::pair<0);
    for(int x = 0; x < n_points; x++){
        n_visible.push_back(std::make_pair(x,0));
    }

    //Compute the visibility counter for each view
    for(int x = 0; x < n_points; x++){
        std::vector<int> row_x = visibility_matrix[x];
        for(int y = 0; y < n_views; y++){
            n_visible[x].second = n_visible[x].second + row_x[y];
        }
    }

    //Sort descreasing
    //std::sort(n_visible.begin(), n_visible.end(), cmpByVisibility);


    int covered_views = int(imp_to_cover.size());

    //While the rest of the views aren't fully covered
    //int index = 0;
    while(covered_views < n_views && !zero_matrix){

        int most_visible = findHighest(n_visible);//[index].first;
        int visib_count = n_visible[most_visible].second;


        //If there are no more points to
        if(most_visible == -1){
            zero_matrix = true;
            continue;
        }

        //Add point to keep
        ptk.insert(std::make_pair(most_visible, visib_count));
        n_pt_kept++;

        //If there is a threshold on the max number to keep
        if(upper_limit != 0 && n_pt_kept >= upper_limit)
            break;

        n_visible[most_visible].second = -1;

        //index++;

        //Update counter per views
        for(int v = 0; v < n_views; v++){
            if(visibility_matrix[most_visible][v] == 1){
                counter_per_view[v]--;

                visibility_matrix[most_visible][v] = 0;

                if(counter_per_view[v] == 0){
                    coverage.insert(std::make_pair(v,0));
                    covered_views++;

                    for(int p = 0; p < n_points; p++){
                        if(visibility_matrix[p][v] == 1){
                            n_visible[p].second = n_visible[p].second - 1;
                            visibility_matrix[p][v] = 0;
                        }
                    }
                }
            }
        }
    }

    int idx = 0;
    for(std::map<int,int>::iterator iter = ptk.begin(); iter != ptk.end(); iter++){
        //idx_to_comp_cloud.insert(std::make_pair(iter->, idx++));
        ptk[iter->first] = idx++;
    }
}

void visibilityCoverage(int n_views, int n_points, std::vector<std::vector<int> > visibility_matrix, std::vector<int> points_per_view, int K, int upper_limit,std::map<int,int> idx_to_comp_cl, std::map<int,int> &ptk){

    std::cout << "Entry <<<<<< " << std::endl;

    bool zero_matrix = false;
    int n_pt_kept = 0;
    std::vector<int> counter_per_view(n_views,K);
    std::map<int,int> coverage;


    //Insert any point which can't fill the condition of >= K visibility
    std::vector<int> imp_to_cover;
    for(int i = 0; i < n_views; i++){
        if(points_per_view[i] < K)
            imp_to_cover.push_back(i);
    }

    //Initialize visibility numbers
    std::vector<std::pair<int,int> > n_visible;// = std::vector<std::pair<int,int> >(n_points, std::pair<0);
    for(int x = 0; x < n_points; x++){
        n_visible.push_back(std::make_pair(x,0));
    }

    //Compute the visibility counter for each view
    for(int x = 0; x < n_points; x++){
        std::vector<int> row_x = visibility_matrix[x];
        for(int y = 0; y < n_views; y++){
            n_visible[x].second = n_visible[x].second + row_x[y];
        }
    }

    //Sort descreasing
    //std::sort(n_visible.begin(), n_visible.end(), cmpByVisibility);


    int covered_views = int(imp_to_cover.size());

    //While the rest of the views aren't fully covered
    while(covered_views < n_views && !zero_matrix){

        int most_visible = findHighest(n_visible);//[index].first;
        int visib_count = n_visible[most_visible].second;


        //If there are no more points to
        if(most_visible == -1){
            zero_matrix = true;
            continue;
        }

        //Add point to keep
        ptk.insert(std::make_pair(most_visible, visib_count));
        n_pt_kept++;

        //If there is a threshold on the max number to keep
        if(upper_limit != 0 && n_pt_kept >= upper_limit)
            break;

        n_visible[most_visible].second = -1;

        //index++;

        //Update counter per views
        for(int v = 0; v < n_views; v++){
            if(visibility_matrix[most_visible][v] == 1){
                counter_per_view[v]--;

                visibility_matrix[most_visible][v] = 0;

                if(counter_per_view[v] == 0){
                    coverage.insert(std::make_pair(v,0));
                    covered_views++;

                    for(int p = 0; p < n_points; p++){
                        if(visibility_matrix[p][v] == 1){
                            n_visible[p].second = n_visible[p].second - 1;
                            visibility_matrix[p][v] = 0;
                        }
                    }
                }
            }
        }
    }

    for(std::map<int,int>::iterator iter = ptk.begin(); iter != ptk.end(); iter++){
        ptk[iter->first] = idx_to_comp_cl.find(iter->first)->second;
    }
}


void PrioriFeats::GenerateCompressedModel(std::string filename,
                                          std::string belonging_model,
                                          std::vector<ImageV*> img_info,
                                          std::vector<CloudPoint> pcl,
                                          std::vector<int> counter_per_img,
                                          std::vector<std::vector<int> > visibility_matrix,
                                          cv::Matx44d G,
                                          int &npts,
                                          int &nviews){

    int n_images = int(img_info.size());
    int n_3Dpoints = int(pcl.size());
    std::map<int,int> compressed_points;


    std::cout << "[Comp] start" << std::endl;
    visibilityCoverage(n_images,n_3Dpoints,visibility_matrix, counter_per_img, Kc, max_comp_points, compressed_points);
    std::cout << "[Comp] end" << std::endl;

    SavePrioriCloud((priori_root + filename).c_str(),belonging_model, img_info, pcl, compressed_points,G);

    nviews = n_images;
    npts = compressed_points.size();
}

void PrioriFeats::GenerateSeedModel(std::string filename,
                                    std::string belonging_model,
                                    std::vector<ImageV*> img_info,
                                    std::vector<CloudPoint> pcl,
                                    std::vector<int> counter_per_img,
                                    std::vector<std::vector<int> > visibility_matrix,
                                    cv::Matx44d G,
                                    int &npts,
                                    int &nviews){

    int n_images = int(img_info.size());
    int n_3Dpoints = int(pcl.size());

    std::map<int,int> compressed_points;

    std::cout << "[Seed] start" << std::endl;
    visibilityCoverage(n_images,n_3Dpoints,visibility_matrix, counter_per_img, Ks, max_seed_points, compressed_points);
    std::cout << "[Seed] end" << std::endl;

    SavePrioriCloud((priori_root + filename).c_str(),belonging_model, img_info, pcl, compressed_points,G);

    nviews = n_images;
    npts = compressed_points.size();
}


// Compresses cloud while giving priority to features visibility
void PrioriFeats::BuildPrioritizedCloud(std::string model_name,
                                        std::string clouds,
                                        std::string cloudc,
                                        std::string location,
                                        std::string imgs_root,
                                        std::vector<std::string> imgs_names,
                                        std::vector<ImageV*> img_info,
                                        std::map<int,cv::Matx34d> Pmats,
                                        std::vector<CloudPoint> pcl){
    int n_images = int(img_info.size());
    int n_3Dpoints = int(pcl.size());
    cv::Matx44d G;

    bool check_gps;
    check_gps = Compute3DtoGPSMat(img_info,Pmats,G);

    std::vector<std::vector<int> > visibility_matrix;
    visibility_matrix.resize(n_3Dpoints , std::vector<int>(n_images, 0 ) );
    std::vector<int> counter_per_img(n_images, 0);

    for(int i = 0; i < n_3Dpoints; i++){

        std::map<int,int> img_mapping = pcl[i].imgpt_for_img;

        for(std::map<int,int>::iterator it = img_mapping.begin(); it != img_mapping.end(); it++){
            visibility_matrix[i][it->first]++;
            counter_per_img[it->first]++;
        }
    }

    std::cout << "[Priori] Started compression" << std::endl;

    //Gen seed cloud and save seed<->comp relation
    int seed_npts, comp_npts;
    int seed_nviews, comp_nviews;
    GenerateSeedModel(clouds, model_name, img_info, pcl, counter_per_img, visibility_matrix, G, seed_npts, seed_nviews);

    GenerateCompressedModel(cloudc, model_name, img_info, pcl, counter_per_img, visibility_matrix, G, comp_npts, comp_nviews);

    std::cout << "[Priori] Ended compression" << std::endl;


    std::cout << "[Priori] Adding models and images to database" << std::endl;
    std::string has_gps = "0";

    if(check_gps)
        has_gps = "1";

    std::string model_id;
    priori_geodb.InitializeDB();
    priori_geodb.CreatePrioriTable();


    std::stringstream ss1, ss2, ss3, ss4;

    ss1 << seed_npts;
    ss2 << seed_nviews;
    ss3 << comp_npts;
    ss4 << comp_nviews;

    if(!priori_geodb.CheckModelExistence(model_name,model_id)){
        priori_geodb.InsertModel(model_name,clouds,ss1.str(), ss3.str(), ss2.str(), ss4.str(), cloudc,location,has_gps,model_id);
        priori_geodb.InsertPhotosPaths(imgs_root,imgs_names,model_id);
    }else{
        priori_geodb.UpdateModel(model_id, clouds,ss1.str(), ss3.str(), ss2.str(), ss4.str(),  cloudc,location,has_gps);
    }

    std::cout << "[Priori] Finished adding" << std::endl;

}



void PrioriFeats::RemoveModel(std::string model_id){

    priori_geodb.InitializeDB();
    priori_geodb.DeleteModel(model_id);

}

void PrioriFeats::ResetDB(){

    priori_geodb.InitializeDB();
    priori_geodb.ResetDatabase();

}

void PrioriFeats::PrintModels(){

    priori_geodb.InitializeDB();

    std::vector<std::string> models;
    priori_geodb.SelectModelsIdName(models);

    for(int i = 0; i < models.size(); i+=2){

        std::cout << "  [ID]    [NAME]" << std::endl;
        std::cout << "    " << models[i] << "      " << models[i + 1] << std::endl;
    }

}
