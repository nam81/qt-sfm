#include "PrioriMain.h"
#include "../IO/IOCommon.h"
#include "../IO/ExifReader.h"
#include "../IO/ConfigFile.h"
#include "PrioriCompressor.h"
#include "PrioriGeoLocalizor.h"
#include "../IO/IOPrioriFeats.h"
#include "../IO/Statistics.h"



void BuildPrioritizedCloud(std::string nvm_path, int nvm_type, std::string location, ConfigOptions options){

    std::vector<CloudPoint> pcloud;
    std::vector<cv::Vec3b> pcloudRGB;
    std::vector<std::string> imgnames;
    std::map<int,cv::Matx34d> pmats;
    std::vector<ImageV*> images_info;
    std::vector<cv::Mat> K;

    std::cout << "[Prioritized Builder]" << std::endl;

    int lastindex = nvm_path.find_last_of("/");
    std::string imgs_path = nvm_path.substr(0, lastindex + 1);

    LoadFromNVM(nvm_path.c_str(), imgnames,pmats,K,pcloud,pcloudRGB,nvm_type);

    bool coords_set;
    std::map<std::string, GPSCoords> gps_coordinates;
    coords_set = ReadCoordsFile(std::string(imgs_path) + "coordinates.txt", gps_coordinates);

    //Load images sift
    for(int i = 0; i < imgnames.size(); i++){
        ImageV* imgv = new ImageV();
        std::vector<cv::KeyPoint> keys;
        std::vector<float> descs;
        std::string spl = split_on_limiter(imgnames[i],'.', false);
        if(nvm_type == 0)
            ReadSiftA((imgs_path + spl + "sift").c_str(), keys,descs);
        else
            ReadSiftVSFM((imgs_path + spl + "sift").c_str(), keys,descs);

        imgv->setKeypoints(keys);
        imgv->setDescriptors(descs);

        GPSCoords gps_tags;

        //ExtractGeotags(imgs_path + imgnames[i],gps_tags);

        if(!coords_set){
            ExtractGeotags(imgs_path + imgnames[i], gps_tags);
        }else{
            std::map<std::string,GPSCoords>::iterator iter = gps_coordinates.find(imgnames[i]);
            if(iter == gps_coordinates.end()){
//#ifdef GEO_DEBUG
                std::cerr << "Image " + imgnames[i] + " didn't match any name on coordinates.txt" << std::endl;
//#endif
                exit(0);
            }

            gps_tags = iter->second;
        }

        imgv->setGPS(gps_tags);
        images_info.push_back(imgv);

        std::cout << "   Read img: " << imgnames[i] << " Pts: " << keys.size() << std::endl;

    }

    lastindex = nvm_path.find_last_of("/");
    std::string model_rawname = nvm_path.substr(lastindex + 1, nvm_path.size() - 1);

    std::stringstream ss1,ss2;
    ss1 << std::string(options.priori_root) << std::string(model_rawname) << "_" << "seed";ss1 << ".nvm";
    ss2 << std::string(options.priori_root) << std::string(model_rawname) << "_" << "comp";ss2 << ".nvm";

    PrioriFeats priorifeats(options);

    priorifeats.BuildPrioritizedCloud(std::string(nvm_path), ss1.str(),ss2.str(),location,imgs_path, imgnames, images_info, pmats, pcloud);
}

void GeolocalizePrioritized(const char* list_path,
                            ConfigOptions options){


    //std::vector<std::string> posed_names;

    std::cout << "[Prioritized Pose Estimation]" << std::endl;

    std::string imgs_path = split_on_limiter(std::string(list_path),'/', false);

    PrioriGeoLocalizor geoloc(imgs_path, false, options);

    geoloc.InitializeDB();
    geoloc.LoadGlobalCloud();

    geoloc.InitializeSiftGPU(options.priorifeats);

    std::vector<std::string> imgs_to_pose;

    if(!ReadImageList(list_path, imgs_to_pose)){
        std::cout << "[ERROR] pose_list.txt nout found" << std::endl;
        exit(0);
    }

    //Initializes Output Statistics
    StatisticsPriori statistics = StatisticsPriori(options.synthfeats.use_cuda);

    clock_t time_start = clock() ;
    int posed_index = 0;
    for(int i = 0; i < imgs_to_pose.size(); i++){
        StatPriori loc_statistics;
        if(geoloc.LocateImg(imgs_path,imgs_to_pose[i], loc_statistics)){//imgs_to_pose[i],P,gps,direction,location, model_name)){
            posed_index++;
        }

        statistics.insertInfo(loc_statistics);
    }

    clock_t time_end = clock() - time_start;

    std::cout << "[Total ALL] " << (double) (time_end) / CLOCKS_PER_SEC << std::endl;

    std::cout << "[Total Posed] Posed " << imgs_to_pose.size() << " (" << posed_index/*posed_names.size()*/ << " successful)" << std::endl;

    //Save Output file
    statistics.toFileStatistics(imgs_path);

    //Save positions to visualization file
    statistics.toFileViz(imgs_path);

    geoloc.UnloadSiftGPU();

    return ;
}


void RemovePrioriModel(std::string model_id, std::string priori_root){
    PrioriFeats priorifeats(priori_root);

    std::cout << "   Removing model " << model_id << std::endl;
    priorifeats.RemoveModel(model_id);
    std::cout << "   Finished removing. " << std::endl;
}

void ResetPrioriGeoDB(std::string priori_root){
    PrioriFeats priorifeats(priori_root);

    std::cout << "   Reseting database now..." << std::endl;
    priorifeats.ResetDB();
    std::cout << "   Finished reset. " << std::endl;
}

void PrintPrioriModels(std::string priori_root){
    PrioriFeats priorifeats(priori_root);

    priorifeats.PrintModels();
}
