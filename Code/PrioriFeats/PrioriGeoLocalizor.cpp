#include "PrioriGeoLocalizor.h"
#include "../../3rdparty/VocabTree2/lib/ann_1.1_char/include/ANN/ANN.h"
#include "../Projections/FindCameraMatrices.h"
#include "../IO/IOCommon.h"
#include "../IO/IOPrioriFeats.h"
#include "../IO/ExifReader.h"
#include "../Geocoding/GeoOperations.h"
#include <string>

#define SEED_PRIORI_VAL 300

void PrioriGeoLocalizor::InitializeSiftGPU(PrioriOptions po){
    if(po.use_cuda){
#ifdef SIFTGPU_CUDA

#ifdef GEO_DEBUG
        std::cout << "[SiftGPU] Using cuda" << std::endl;
#endif
        feature_matcher->SetCuda("0");
#endif
    }

    feature_matcher->SetOptions(po.fo,po.tc_type,po.tc,po.ori_n);
    feature_matcher->CreateExtractorContext();
}

bool PrioriGeoLocalizor::UnloadSiftGPU(){
    feature_matcher->UnloadSift();
}

bool PrioriGeoLocalizor::InitializeDB(){

    geodb.InitializeDB();

    std::vector<std::string> result;

    geodb.SelectAllModels(result);

    if(result.size() == 0){
        std::cout << "No models were found in the database...can't continue the pose estimate process" << std::endl;
        exit(0);
    }

    for(int i = 0; i < result.size(); i+=10){
        bool check_gps;
        if(result[i+9].compare("1") == 0)
            check_gps = 1;
        else
            check_gps = 0;

        PrioriModel pm = PrioriModel(result[i + 1] + result[i + 0],
                result[i + 2],
                result[i+7],
                result[i+8],
                atoi(result[i+3].c_str()),
                atoi(result[i+4].c_str()),
                atoi(result[i+5].c_str()),
                atoi(result[i+6].c_str()),
                check_gps);

        model_list.push_back(pm);
    }

    //std::string("SELECT MODEL_NAME, MODEL_PATH, MODEL_SEED,MODEL_SEED_NPTS,MODEL_COMP_NPTS, MODEL_SEED_NVIEWS,MODEL_COMP_NVIEWS, MODEL_COMP, LOCATION, HAS_GPS from PRIORI_MODELS");


    return true;
}


void PrioriGeoLocalizor::LoadGlobalCloud(){

    int total_points_seed = 0;
    int total_points_comp = 0;
    seed_start_indexes.push_back(0);
    comp_start_indexes.push_back(0);
    for(int i = 0; i < model_list.size();i++){
        total_points_seed += model_list[i].n_pts_seed;
        total_points_comp += model_list[i].n_pts_comp;

        seed_start_indexes.push_back(total_points_seed);
        comp_start_indexes.push_back(total_points_comp);
    }

    std::vector<unsigned char> seed_descriptors;

    //Allocate storage for seed cloud information
    seed_cloud_points = std::vector<cv::Point3f>(total_points_seed);
    seed_descriptors = std::vector<unsigned char>(total_points_seed * 128);
    seed_visibility_matrix = std::vector<std::map<int,int> >(total_points_seed);
    seed_visibility_indexes = std::vector<std::pair<int,float> >(total_points_seed);

    comp_cloud_points = std::vector<cv::Point3f>(total_points_comp);
    comp_descriptors = std::vector<unsigned char>(total_points_comp * 128);
    comp_visibility_matrix = std::vector<std::map<int,int> >(total_points_comp);
    comp_visibility_indexes = std::vector<std::pair<int,float> >(total_points_comp);


    int last_cam_idx_seed = 0;
    int last_cam_idx_comp = 0;
    int last_point_idx_seed = 0;
    int last_point_idx_comp = 0;

    std::vector<cv::Matx44d > comp_gps_axis;


    for(int i = 0; i < model_list.size(); i++){
        LoadPrioriCloud(model_list[i].model_seed.c_str(),seed_cloud_points,seed_descriptors,seed_visibility_indexes,seed_visibility_matrix,seed_gps_axis,last_point_idx_seed, last_cam_idx_seed);
        LoadPrioriCloud(model_list[i].model_comp.c_str(), comp_cloud_points,comp_descriptors,comp_visibility_indexes,comp_visibility_matrix,comp_gps_axis, last_point_idx_comp, last_cam_idx_comp);
    }

    ann_1_1_char::ANNpointArray seed_ann_descs = ann_1_1_char::annAllocPts(seed_cloud_points.size(), 128);

    for(int i = 0; i < seed_cloud_points.size(); i++){
        for(int j = 0; j < 128; j++){
            seed_ann_descs[i][j] = seed_descriptors[i * 128 + j];
        }
    }

    seed_tree = new ann_1_1_char::ANNkd_tree(seed_ann_descs, seed_cloud_points.size(), 128, 4);

    //Increase the priority for seed points
    for(int i = 0; i < seed_visibility_indexes.size();i++)
        seed_visibility_indexes[i].second += SEED_PRIORI_VAL;

}

int PrioriGeoLocalizor::InferModelIDSeed(int index){

    for(int i = 0; i < seed_start_indexes.size(); i++){
        if(seed_start_indexes[i] > index){
            return i - 1;
        }
    }

    //Should not reach here
    return -1;
}

int PrioriGeoLocalizor::InferModelIDComp(int index){

    for(int i = 0; i < comp_start_indexes.size(); i++){
        if(comp_start_indexes[i] > index){
            return i - 1;
        }
    }

    //Should not reach here
    return -1;
}

void GetMaxVisib2(std::vector<std::pair<int,float> > visibility_indexes, int &index){

    float highest = -1, pos = -1;
    for(int i = 0; i < visibility_indexes.size(); i++){
        if(visibility_indexes[i].second > highest){
            pos = i;
            highest = visibility_indexes[i].second;
        }

    }

    index =  pos;
}

bool PrioriGeoLocalizor::FindPose(std::vector<cv::Point2f> &max_2d, std::vector<cv::Point3f> &max_3d,  cv::Mat K , cv::Matx34d &Pout) {
    cv::Mat_<double> t = (cv::Mat_<double>(1,3) << 0.0, 0.0,0.0);
    cv::Mat_<double> R = (cv::Mat_<double>(3,3) << 1.0, 0.0, 0.0,
                                                   0.0, 1.0, 0.0,
                                                   0.0, 0.0, 1.0);

    //std::cout << "-------------------------- " << imgs_names[to_pose] << " --------------------------\n";

    bool pose_estimated;
    cv::Mat distortion_coeff = cv::Mat_<double>::zeros(1,4);

#ifdef MULTI_CAMERA
    pose_estimated = FindPoseEstimationPriori(t,R,images_info[i]->getK(), distortion_coeff,max_3d,max_2d);
#else
    pose_estimated = FindPoseEstimationPriori(t,R,K, distortion_coeff,max_3d,max_2d);
#endif
    if(!pose_estimated){
#ifdef GEO_DEBUG
        std::cout << "failed to pose estimate"<< std::endl;
#endif
        return false;
    }

    //store estimated pose
    Pout = cv::Matx34d(R(0,0),R(0,1),R(0,2),t(0),
                       R(1,0),R(1,1),R(1,2),t(1),
                       R(2,0),R(2,1),R(2,2),t(2));

    return true;
}


bool PrioriGeoLocalizor::CoocurrenceSamplingComp(std::vector<cv::KeyPoint> query_points,
                                                 std::map<int, std::pair<int,cv::DMatch> > matches,
                                                 std::vector<cv::Point2f> &max2D,
                                                 std::vector<cv::Point3f> &max3D){

    std::map<int,int> coocurrence_set;

    std::map<int,std::map<int,int> > sets;

    int highest_idx =-1, highest_count = 0;

    for(std::map<int, std::pair<int,cv::DMatch> >::iterator it = matches.begin(); it != matches.end(); it++){

        int visib_idx = it->second.second.trainIdx;
        if(it->second.first == 0){
            if(highest_count < seed_visibility_indexes[visib_idx].second){//seed_visibility_matrix[visib_idx].size()){
                highest_count = seed_visibility_indexes[visib_idx].second;//seed_visibility_matrix[visib_idx].size();
                highest_idx = it->first;
            }

            sets[it->first] = seed_visibility_matrix[visib_idx];

        }else{
            if(highest_count < comp_visibility_indexes[visib_idx].second){//comp_visibility_matrix[visib_idx].size()){
                highest_count = comp_visibility_indexes[visib_idx].second;//comp_visibility_matrix[visib_idx].size();
                highest_idx = it->first;
            }

            sets[it->first] = comp_visibility_matrix[visib_idx];

        }
    }

    std::vector<int> choosen_matches;

    choosen_matches.push_back(highest_idx);

    coocurrence_set = sets[highest_idx];

    int P = highest_count;

    int Pr = 2000, set_n = 0;
    std::map<int,int> v;

    while(Pr > 3 && set_n < 30){

        int current_Pr = 0;
        //Select the match with highest contribution

        std::map<int,int> coocurrence_set_new;
        std::map<int, std::map<int,int> >::iterator iter_to_rem;

        for(std::map<int, std::map<int,int> >::iterator iter = sets.begin(); iter != sets.end(); iter++){
            std::map<int,int> current_set = iter->second;


            std::set_intersection (coocurrence_set.begin(), coocurrence_set.end(), current_set.begin(), current_set.end(), std::inserter(v,v.begin()));

            int new_Pr = v.size();

            if(new_Pr > current_Pr){
                //Update best match to add
                coocurrence_set_new = v;
                current_Pr = new_Pr;
                iter_to_rem = iter;
            }
            v.clear();

        }

        if(current_Pr == 0){
            Pr = current_Pr;
            continue;
        }
        //Update set vector & chosen matches
        set_n++;

        choosen_matches.push_back(iter_to_rem->first);

        coocurrence_set = coocurrence_set_new;
        Pr = current_Pr;
        sets.erase(iter_to_rem);
    }

    if(choosen_matches.size() > 12){

        for(int i = 0; i < choosen_matches.size(); i++){
            max2D.push_back(query_points[matches[choosen_matches[i]].second.queryIdx].pt);
            if(matches[choosen_matches[i]].first == 0)
                max3D.push_back(seed_cloud_points[matches[choosen_matches[i]].second.trainIdx]);
            else
                max3D.push_back(comp_cloud_points[matches[choosen_matches[i]].second.trainIdx]);
        }

        return true;
    }else{
        return false;
    }
}


//Receives the new matches and groups them by visibility relation
bool PrioriGeoLocalizor::CoocurrenceSamplingSeed(std::vector<cv::KeyPoint> query_points, std::map<int, cv::DMatch> matches, std::vector<cv::Point2f> &max2D, std::vector<cv::Point3f> &max3D){

    std::map<int,int> coocurrence_set;

    std::map<int,std::map<int,int> > sets;

    int highest_idx =-1, highest_count = 0;

    for(std::map<int,cv::DMatch>::iterator it = matches.begin(); it != matches.end(); it++){
        int visib_idx = it->second.trainIdx;

        if(highest_count < seed_visibility_indexes[visib_idx].second){

            highest_count = seed_visibility_indexes[visib_idx].second;
            highest_idx = it->first;
        }

        sets[it->first] = seed_visibility_matrix[visib_idx];
    }

    std::vector<int> choosen_matches;

    choosen_matches.push_back(highest_idx);

    coocurrence_set = sets[highest_idx];

    int P = highest_count;

    int Pr = 2000, set_n = 0;
    std::map<int,int> v;

    while(Pr > 1 && set_n < 30){

        int current_Pr = 0;
        //Select the match with highest contribution

        std::map<int,int> coocurrence_set_new;
        std::map<int, std::map<int,int> >::iterator iter_to_rem;

        for(std::map<int, std::map<int,int> >::iterator iter = sets.begin(); iter != sets.end(); iter++){
            std::map<int,int> current_set = iter->second;


            std::set_intersection (coocurrence_set.begin(), coocurrence_set.end(), current_set.begin(), current_set.end(), std::inserter(v,v.begin()));

            int new_Pr = v.size();

            if(new_Pr > current_Pr){
                //Update best match to add
                coocurrence_set_new = v;
                current_Pr = new_Pr;
                iter_to_rem = iter;
            }
            v.clear();

        }

        if(current_Pr == 0){
            Pr = current_Pr;
            continue;
        }
        //Update set vector & chosen matches
        set_n++;

        choosen_matches.push_back(iter_to_rem->first);

        coocurrence_set = coocurrence_set_new;
        Pr = current_Pr;
        sets.erase(iter_to_rem);
    }

    if(choosen_matches.size() > 12){

        for(int i = 0; i < choosen_matches.size(); i++){
            max2D.push_back(query_points[matches[choosen_matches[i]].queryIdx].pt);
            max3D.push_back(seed_cloud_points[matches[choosen_matches[i]].trainIdx]);
        }

        return true;
    }else{
        return false;
    }
}

bool PrioriGeoLocalizor::MatchGlobalComp(std::vector<cv::KeyPoint> query_keys,
                                         std::vector<float> query_descs,
                                         std::vector<std::pair<int,float> > seed_visibility_indexes_copy,
                                         std::map<int,std::map<int, cv::DMatch> > matches_by_model,
                                         cv::Mat K,
                                         StatPriori &loc_statistics,
                                         cv::Matx34d &Pout,
                                         int &posed_idx){

#ifdef GEO_DEBUG
    clock_t end_coocurrence = 0,end_pose = 0, end_pba = 0;
#endif

    std::map<int,int> needed_matches;
    std::map<int,std::map<int, std::pair<int,cv::DMatch> > > matches_by_model_comp;

    //Convert idx_seed to idx_comp
    for(std::map<int, std::map<int, cv::DMatch> >::iterator it = matches_by_model.begin(); it != matches_by_model.end(); it++){

        for(std::map<int, cv::DMatch>::iterator it_inner = matches_by_model[it->first].begin(); it_inner != matches_by_model[it->first].end(); it_inner++){
            matches_by_model_comp[it->first][it_inner->second.queryIdx] = std::make_pair(0,it_inner->second);
        }
    }

    std::vector<std::pair<int,float> > comp_visibility_indexes_copy = comp_visibility_indexes;

    //Update indexes for comp
    int idx_limit_seed = 1, idx_limit_comp  =1, limit_min = 0, limit_max = comp_start_indexes[idx_limit_comp];
    for(int i = 0; i < seed_visibility_indexes_copy.size(); i++){

        if(i == seed_start_indexes[idx_limit_seed])
        {
            idx_limit_seed++;
            limit_min = limit_max;
            limit_max = comp_start_indexes[++idx_limit_comp];
        }

        for(int j = limit_min; j < limit_max; j++)
        {
            if(seed_visibility_indexes_copy[i].first == comp_visibility_indexes_copy[j].first)
                comp_visibility_indexes_copy[j].second = seed_visibility_indexes_copy[i].second;
        }
    }

    //Convert descs to unsigned char
    ann_1_1_char::ANNpointArray descriptors_query = ann_1_1_char::annAllocPts(int(query_keys.size()), 128);
    for (int i = 0; i < query_keys.size(); i++) {
        for (int j = 0; j < 128; j++) {
            descriptors_query[i][j] = static_cast<unsigned char>(static_cast<unsigned int>(floor(query_descs[i * 128 + j] * 512.0 + 0.5)));
        }
    }

    //int n_points = comp_cloud_points.size();// int(cloud_points_comp.size());

    //clock_t start_load_ann2 = clock();
    /* Create a search tree for k2 */
    ann_1_1_char::ANNkd_tree *tree_compressed = new ann_1_1_char::ANNkd_tree(descriptors_query,int(query_keys.size()), 128, 4);
    //clock_t end_build_ann2 = clock() - start_load_ann2;

    //clock_t start_matching2 = clock();
    //std::map<int,cv::DMatch> matches_comp;
    ann_1_1_char::ANNpoint query = ann_1_1_char::annAllocPt(128);
    //query = ann_1_1_char::annAllocPt(128);
    int n_points_searched = 0, index = -1;

    while(n_points_searched < max_search && n_points_searched <  int(comp_cloud_points.size()) ){
        //int j;

        GetMaxVisib2(comp_visibility_indexes_copy,index);

        if(index == -1){
            std::cout << "[Warning] Index out of range" << std::endl;
            break;
        }

        for (int j = 0; j < 128; j++) {
            query[j] = comp_descriptors[index * 128 + j];
        }

        ann_1_1_char::ANNidx nn_idx[3];
        ann_1_1_char::ANNdist dist[3];

        tree_compressed->annkPriSearch(query, 3, nn_idx, dist, 0.0);

        double distance = sqrt(((double) dist[0]) / ((double) dist[2]));

        if (distance <= ratio_comp) {
            //int idx_comp = seed_visibility_indexes_copy[nn_idx[0]].first;
            int idx_model = InferModelIDComp(index);

            if(idx_model == -1){
                std::cout << "[Warning] Index out of range" << std::endl;
            }
            if(matches_by_model_comp[idx_model].find(nn_idx[0]) != matches_by_model_comp[idx_model].end() ){
                if(distance < matches_by_model_comp[idx_model][nn_idx[0]].second.distance){

                    int old_i = matches_by_model_comp[idx_model][nn_idx[0]].second.queryIdx;

                    matches_by_model_comp[idx_model].erase(matches_by_model_comp[idx_model].find(old_i));

                    matches_by_model_comp[idx_model][nn_idx[0]] = std::make_pair(1, cv::DMatch(nn_idx[0],index,comp_visibility_indexes_copy[index].first,distance));

                    for(std::map<int,int>::iterator iter_horizontal = comp_visibility_matrix[index].begin(); iter_horizontal != comp_visibility_matrix[index].end(); iter_horizontal++){

                        for(int k = comp_start_indexes[idx_model]; k < comp_start_indexes[idx_model + 1]; k++){
                            if(comp_visibility_matrix[k].find(iter_horizontal->first) != comp_visibility_matrix[k].end())
                                comp_visibility_indexes_copy[k].second = comp_visibility_indexes_copy[k].second + thrs_w / comp_visibility_indexes_copy[k].second;
                        }
                    }

                }
            }else{
                matches_by_model_comp[idx_model][nn_idx[0]] = std::make_pair(1,cv::DMatch(nn_idx[0],index,comp_visibility_indexes_copy[index].first, distance));

                for(std::map<int,int>::iterator iter_horizontal = comp_visibility_matrix[index].begin(); iter_horizontal != comp_visibility_matrix[index].end(); iter_horizontal++){
                    for(int k = comp_start_indexes[idx_model]; k < comp_start_indexes[idx_model + 1]; k++){
                        if(comp_visibility_matrix[k].find(iter_horizontal->first) != comp_visibility_matrix[k].end())
                            comp_visibility_indexes_copy[k].second = comp_visibility_indexes_copy[k].second + thrs_w / comp_visibility_indexes_copy[k].second;
                    }
                }
            }

            bool test_coocurrence = false;
            if(matches_by_model_comp[idx_model].size() >= thrs_w && needed_matches.find(idx_model) == needed_matches.end()){
                test_coocurrence = true;
            }else if(needed_matches.find(idx_model) != needed_matches.end()){
                test_coocurrence = true;
            }

            if(test_coocurrence){
                //Coocurrence sampling on matches relative to model_idx
                std::vector<cv::Point2f> max2D;
                std::vector<cv::Point3f> max3D;

#ifdef GEO_DEBUG
                clock_t start_coocurrence = clock();
#endif
                bool cooc_sample_valid = CoocurrenceSamplingComp(query_keys, matches_by_model_comp[idx_model], max2D, max3D);
#ifdef GEO_DEBUG
                end_coocurrence += clock() - start_coocurrence;
#endif

                if(cooc_sample_valid){

#ifdef GEO_DEBUG
                    clock_t start_pose = clock();
                    //Try to find a pose estimation with the sampled points
#endif

                    bool pose_valid = FindPose(max2D,max3D,K,Pout);

#ifdef GEO_DEBUG
                    end_pose += clock() - start_pose;
#endif

                    if(pose_valid){
#ifdef GEO_DEBUG
                        clock_t start_pba = clock();
#endif
                        //Local Bundle Adjustment to enhance the Projection matrix
                        pba.adjustBundle(max2D,max3D,K,Pout);

#ifdef GEO_DEBUG
                        end_pba += clock() - start_pba;

                        loc_statistics.setTP3P(end_pose);
                        loc_statistics.setTCooc(end_coocurrence);
                        loc_statistics.setTPBA(end_pba);
#endif

                        posed_idx = idx_model;
                        std::cout << "Posed on model " << idx_model << std::endl;
                        return true;
                    }else{
                        if(needed_matches.find(idx_model) != needed_matches.end()){
                            needed_matches[idx_model] += 10;
                        }else{
                            needed_matches[idx_model] = 20;
                        }
                    }

                }

            }
        }


        comp_visibility_indexes_copy[index].second = - std::numeric_limits<float>::infinity();

        n_points_searched++;
    }
    ann_1_1_char::annDeallocPts(descriptors_query);
    ann_1_1_char::annDeallocPt(query);

#ifdef GEO_DEBUG
    loc_statistics.setTP3P(end_pose);
    loc_statistics.setTCooc(end_coocurrence);
    loc_statistics.setTPBA(end_pba);
#endif

    return false;
}



bool PrioriGeoLocalizor::MatchGlobalSeed(std::vector<cv::KeyPoint> query_points,
                                          std::vector<float> query_descs,
                                         std::vector<std::pair<int,float> > &seed_visibility_indexes_copy,
                                         std::map<int, std::map<int, cv::DMatch> > &matches_by_model,
                                         //std::map<int, std::map<int, std::map<int,int> > > &matches_views_by_model,
                                         cv::Mat K,
                                         StatPriori &loc_statistics,
                                          cv::Matx34d &Pout,
                                          int &posed_idx){


#ifdef GEO_DEBUG
    clock_t end_coocurrence = 0,end_pose = 0, end_pba = 0;
#endif


    //int n_views = int(total_seed_views);
    int n_points = int(seed_cloud_points.size());

    /* Now do the search */
    ann_1_1_char::annMaxPtsVisit(max_search);
    ann_1_1_char::ANNpoint query = ann_1_1_char::annAllocPt(128);

    for (int i = 0; i < (int) query_descs.size() / 128; i++) {
        //int j;

        for (int j = 0; j < 128; j++) {
            query[j] = static_cast<unsigned char>(floor(query_descs[i * 128 + j] * 512.0 + 0.5));
        }

        ann_1_1_char::ANNidx nn_idx[3];
        ann_1_1_char::ANNdist dist[3];

        seed_tree->annkPriSearch(query, 3, nn_idx, dist, 0.0);

        double distance = sqrt(((double) dist[0]) / ((double) dist[1]));

        if (distance <= ratio_seed) {
            int idx_comp = nn_idx[0];
            int idx_model = InferModelIDSeed(nn_idx[0]);

            if(idx_model == -1){
                std::cout << "[Warning] Index out of range" << std::endl;
            }

            if(matches_by_model[idx_model].find(idx_comp) != matches_by_model[idx_model].end() ){
                 if(distance < matches_by_model[idx_model][idx_comp].distance){

                     matches_by_model[idx_model][idx_comp] = cv::DMatch(i,nn_idx[0],distance);

                     for(std::map<int,int>::iterator iter_horizontal = seed_visibility_matrix[nn_idx[0]].begin(); iter_horizontal != seed_visibility_matrix[nn_idx[0]].end(); iter_horizontal++){
                             for(int k = 0; k < n_points; k++){
                                 if(seed_visibility_matrix[k].find(iter_horizontal->first) != seed_visibility_matrix[k].end())
                                     seed_visibility_indexes_copy[k].second = seed_visibility_indexes_copy[k].second + thrs_w / seed_visibility_indexes_copy[k].second;
                             }
                     }
                 }
            }else{
                matches_by_model[idx_model][idx_comp] = cv::DMatch(i,nn_idx[0],distance);

                for(std::map<int,int>::iterator iter_horizontal = seed_visibility_matrix[nn_idx[0]].begin(); iter_horizontal != seed_visibility_matrix[nn_idx[0]].end(); iter_horizontal++){
                    for(int k = 0; k < n_points; k++){
                        if(seed_visibility_matrix[k].find(iter_horizontal->first) != seed_visibility_matrix[k].end())
                            seed_visibility_indexes_copy[k].second = seed_visibility_indexes_copy[k].second + thrs_w / seed_visibility_indexes_copy[k].second;
                    }
                }
            }

            if(matches_by_model[idx_model].size() > 12){
                //Coocurrence sampling on matches relative to model_idx

                std::vector<cv::Point2f> max2D;
                std::vector<cv::Point3f> max3D;

#ifdef GEO_DEBUG
                clock_t start_coocurrence = clock();
#endif

                bool cooc_sample_valid = CoocurrenceSamplingSeed(query_points, matches_by_model[idx_model], max2D, max3D);

#ifdef GEO_DEBUG
                end_coocurrence += clock() - start_coocurrence;

#endif

                if(cooc_sample_valid){

#ifdef GEO_DEBUG
                    clock_t start_pose = clock();
#endif
                    //Try to find a pose estimation with the sampled points

                    bool pose_valid = FindPose(max2D,max3D,K,Pout);

#ifdef GEO_DEBUG
                        end_pose += clock() - start_pose;
#endif

                    if(pose_valid){

#ifdef GEO_DEBUG
                        clock_t start_pba = clock();
#endif
                        //Bundle Adjustment to enhance the projection matrix
                        pba.adjustBundle(max2D,max3D,K,Pout);
#ifdef GEO_DEBUG
                        end_pba += clock() - start_pba;
#endif

                        posed_idx = idx_model;

#ifdef GEO_DEBUG
                        loc_statistics.setTCooc(end_coocurrence);
                        loc_statistics.setTP3P(end_pose);
                        loc_statistics.setTPBA(end_pba);
#endif
                        std::cout << "Posed on mode " << idx_model << std::endl;
                        return true;
                    }

                }

            }

        }

        seed_visibility_indexes_copy[nn_idx[0]].second = - std::numeric_limits<float>::infinity();

    }

#ifdef GEO_DEBUG
    loc_statistics.setTCooc(end_coocurrence);
    loc_statistics.setTP3P(end_pose);
    loc_statistics.setTPBA(end_pba);
#endif

    ann_1_1_char::annDeallocPt(query);

    return false;
}



//Queries extracted feature to the global seed cloud. Returns A pose or an updated list of visibility prioritization
//and the coocurrence sampling matches
bool PrioriGeoLocalizor::QueryPhotograph(std::vector<cv::KeyPoint> query_keys,
                                          std::vector<float> query_descs,
                                          cv::Mat K,
                                         StatPriori &loc_statistics){/*
                                          cv::Matx34d &Pout,
                                          GPSCoords &gps,
                                          std::string &direction,
                                          int &idx){*/


    int idx;
    cv::Matx34d Pout;
    std::vector<std::pair<int,float> > seed_visibility_indexes_copy = seed_visibility_indexes;
    std::map<int, std::map<int, cv::DMatch> > matches_by_model;

    bool posed = false;

    //---------------------------------- Search Seed Cloud ------------------------------
#ifdef GEO_DEBUG
    clock_t start_matching_seed = clock();
#endif

    posed = MatchGlobalSeed(query_keys,query_descs,seed_visibility_indexes_copy,matches_by_model,K,loc_statistics, Pout/* K,Pout,*/,idx);

#ifdef GEO_DEBUG
    clock_t end_matching_seed = clock() - start_matching_seed;

    //loc_statistics.setTANN(end_matching_seed - loc_statistics.getTCooc() - loc_statistics.getTP3P() - loc_statistics.getTPBA());
    std::cout <<"[Priori] Matching time " << (double) (end_matching_seed) / CLOCKS_PER_SEC << std::endl;
#endif

    //---------------------------------- Search Comp Cloud ------------------------------

#ifdef GEO_DEBUG
    clock_t start_matching_comp = 0, end_matching_comp = 0;
#endif
    if(!posed){
#ifdef GEO_DEBUG
        start_matching_comp = clock();
#endif

        posed = MatchGlobalComp(query_keys, query_descs, seed_visibility_indexes_copy, matches_by_model,K, loc_statistics, Pout, idx);// K, Pout, idx);

#ifdef GEO_DEBUG
        end_matching_comp = clock() - start_matching_comp;


        std::cout <<"[Priori] Matching time " << (double) (end_matching_comp) / CLOCKS_PER_SEC << std::endl;
#endif
    }

#ifdef GEO_DEBUG
    loc_statistics.setTANN(end_matching_comp + end_matching_seed - loc_statistics.getTCooc() - loc_statistics.getTP3P() - loc_statistics.getTPBA());
#endif

    //---------------------------------- Compute GPS -------------------------------------

#ifdef GEO_DEBUG
    clock_t start_compute_gps = 0, end_compute_gps = 0;
#endif


    if(posed){

        //Compute GPS coordinates if the model has them
        if(model_list[idx].has_gps){
#ifdef GEO_DEBUG
            start_compute_gps = clock();
#endif
            GPSCoords gps;
            std::string direction;
            ModelToGPS(Pout, seed_gps_axis[idx],gps, direction);

//#ifdef GEO_DEBUG
            loc_statistics.setGPS(gps);
            loc_statistics.setDirection(direction);
//#endif

            std::cout << "        [Coords] Lat: " << gps.latitude << ", " << gps.longitude << ", " << gps.altitude << std::endl;
            std::cout << "        [Direction] Pointed at " << direction << std::endl;

#ifdef GEO_DEBUG
            end_compute_gps = clock() - start_compute_gps;

            std::cout <<"[Priori] Compute GPS time " << (double) (end_compute_gps) / CLOCKS_PER_SEC << std::endl;
#endif
        }
#ifdef GEO_DEBUG
        loc_statistics.setTGPS(end_compute_gps);
#endif

//#ifdef GEO_DEBUG
        loc_statistics.setModelName(model_list[idx].model_path);
        loc_statistics.setLocation(model_list[idx].model_location);
        loc_statistics.setPmat(Pout);
//#endif

        return true;
    }else{
        return false;
    }
}



//Starting process to pose a new photograph. Performs a query to the global seed and comp to return a pose
bool PrioriGeoLocalizor::LocateImg(std::string imgs_path, std::string img_name, StatPriori &loc_statistics){

    std::cout << "[Priori] --------------------------------------------" << std::endl;
    std::cout << "   Posing image: " << imgs_path << img_name << std::endl;

    loc_statistics = StatPriori();

//#ifdef GEO_DEBUG
    clock_t start_priori = clock();
//#endif
    cv::Mat K = cv::Mat_<double>::zeros(3,3);;
    ExtractKmatrix(imgs_path + img_name, K);

#ifdef GEO_DEBUG
    clock_t end_compute_K = clock() - start_priori;
    clock_t start_ext = clock();
#endif

    std::vector<cv::KeyPoint> kps_to_query;
    std::vector<float> descriptors_to_query;
    feature_matcher->ExtractFeatures(img_name,kps_to_query,descriptors_to_query);

#ifdef GEO_DEBUG
    clock_t end_ext = clock() - start_ext;
    std::cout << "   [Extraction] => " << (double)( end_ext) / CLOCKS_PER_SEC << std::endl;

    loc_statistics.setTK(end_compute_K);
    loc_statistics.setTExt(end_ext);
#endif

    int idx;
    cv::Matx34d Pout;
    GPSCoords gps;
    std::string direction;

    clock_t total_time;

    if(QueryPhotograph(kps_to_query,descriptors_to_query,K, loc_statistics)){//,Pout, gps, direction,idx)){

#ifdef GEO_DEBUG
        total_time = loc_statistics.getTPBA() + loc_statistics.getTGPS() + loc_statistics.getTANN() + loc_statistics.getTCooc() + loc_statistics.getTP3P() + end_compute_K + end_ext;

        loc_statistics.setTTime(total_time);
#else
        total_time = clock() - start_priori;
        loc_statistics.setTTime(total_time);
#endif
        //loc_statistics = StatPriori(img_name, end_priori, gps, model_list[idx].model_location,direction,model_list[idx].model_path,Pout);

//#ifdef GEO_DEBUG
        loc_statistics.setName(img_name);
        loc_statistics.setSuccess(true);
//#endif

        std::cout << "   [Pose] Pose estimation was successful." << std::endl;

        //std::cout << "[Total pose] " << (double)(end_priori) / CLOCKS_PER_SEC << std::endl;
        return true;
    }else{
        //end_priori = clock() - start_priori;
#ifdef GEO_DEBUG
        total_time = loc_statistics.getTPBA() + loc_statistics.getTGPS() + loc_statistics.getTANN() + loc_statistics.getTCooc() + loc_statistics.getTP3P() + end_compute_K + end_ext;

        loc_statistics.setTTime(total_time);
#else
        total_time = clock() - start_priori;
        loc_statistics.setTTime(total_time);
#endif
        //loc_statistics = StatPriori(img_name, end_priori);
//#ifdef GEO_DEBUG
        loc_statistics.setSuccess(false);
        loc_statistics.setName(img_name);
//#endif


        //std::cout << "   [Pose] Pose estimation wasn't successful." << std::endl;
        //std::cout << "[Total pose] " << (double)(end_priori) / CLOCKS_PER_SEC << std::endl;

        return false;
    }
}

