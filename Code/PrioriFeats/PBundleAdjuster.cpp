#define HAVE_PBA

#include "PBundleAdjuster.h"
#include "../Common.h"
#include <iostream>
#include <iomanip>
#include <algorithm>
#include "../../3rdparty/pba/src/pba/pba.h"
#include "../../3rdparty/pba/src/pba/util.h"
#include <cstdlib>

//using namespace cv;
//using namespace std;

#ifndef HAVE_SSBA
#include <opencv2/contrib/contrib.hpp>
#endif

//#ifdef HAVE_SSBA
//#define V3DLIB_ENABLE_SUITESPARSE

//#include <Math/v3d_linear.h>
//#include <Base/v3d_vrmlio.h>
//#include <Geometry/v3d_metricbundle.h>

//using namespace V3D;


std::map<int,int> idxcamera2idx;
int distortion_type;
int fixed;

//PBA function

int pba(int argc,
        char* argv[],
        vector<CameraT>& camera_data,
        vector<Point3D>& point_data,
        vector<Point2D>& measurements,
        vector<int>& camidx,
        vector<int>& ptidx){

    //////////////////////////////////////////////////////
    char* input_filename  = argv[1];            //first argument must be filename
    char* driver_argument =  argv[argc - 1];    //last argument  for the driver

    /////////////////////////////////////////////////////////////////
    //CameraT, Point3D, Point2D are defined in src/pba/DataInterface.h
    //vector<CameraT>        camera_data;    //camera (input/ouput)
    //vector<Point3D>        point_data;     //3D point(iput/output)
    //vector<Point2D>        measurements;   //measurment/projection vector
    //vector<int>            camidx, ptidx;  //index of camera/point for each projection

    /////////////////////////////////////
    vector<string>         photo_names;        //from NVM file, not used in bundle adjustment
    vector<int>            point_color;        //from NVM file, not used in bundle adjustment

    /////////////////////////////////////////////////////////////
    ///load the data. You will need to replace the loader for your own data
    /*if(argc < 2 || ! LoadModelFile(input_filename, camera_data, point_data, measurements,
                        ptidx, camidx, photo_names, point_color))
    {
        std::cout << "==== Multicore Bundle Adjustment ---- Demo Driver Syntax ====\n"
#ifdef WIN32
            "driver(_x64)(_debug) "
#else
            "driver "
#endif
            "input [pba parameters][-out output_nvm][driver argument]\n"
            "	input:            file must be NVM or bundler format\n"
            "[driver argument] must be the last one. It can be one of the following\n"
            "	--noise:          add 5% random noise to all parameters\n"
            "	--float:          use CPU-float implementation instead of GPU\n"
            "	--double:         use CPU-double implementation instead of GPU\n"
            "[pba parameters]: -lmi <#>, -cgi <#>, -cgn <f>, -budget <#>...\n"
            "	-lmi <#>:         specify the number of LM iterations\n"
            "	-profile:         run profiling experiments and see the speeds\n"
            "	                  check documentation for more details.\n";
        return 0;
    }else//*/
    {
        //if(strstr(driver_argument, "--checkv")) ExamineVisiblity(input_filename);

        //remove file extension for conversion/saving purpose
        char* dotpos = ".nvm";// strrchr(input_filename, '.');
        //if(dotpos && strchr(dotpos, '/') == NULL && strchr(dotpos, '\\') == NULL) *dotpos = 0;

        //////////////////////////////////////////////////////////
        //use the last parameter for special purpose.
        string surfix = "-converted";
        if(strstr(driver_argument, "--fix_visibility"))
        {
            if(RemoveInvisiblePoints(camera_data, point_data, ptidx, camidx, measurements, photo_names, point_color))
                surfix = "-fixed";
        }else
        {
            if(strstr(driver_argument, "--noise") != NULL)     //add noise for experimentation
            {
                //AddNoise(camera_data, point_data, 0.05f);    //add 5% noise for experiments
                AddStableNoise(camera_data, point_data, ptidx, camidx, 0.05f);
                surfix = "-noised";
            }
        }

        //file format conversion for experimentation
        if(strstr(driver_argument, "--convert_to_nvm"))
            SaveNVM(    (string(input_filename) + surfix + ".nvm").c_str(), camera_data,
                        point_data, measurements, ptidx, camidx, photo_names, point_color);
        if(strstr(driver_argument, "--convert_to_bm"))
            SaveBundlerModel(    (string(input_filename) + surfix + ".txt").c_str(),
                                camera_data, point_data, measurements, ptidx, camidx);
    }

    /////////////////////////////////////////////////////////////////////////////////////////
    ParallelBA::DeviceT device = ParallelBA::PBA_CUDA_DEVICE_DEFAULT;
    if(strstr(driver_argument, "--float"))          device = ParallelBA::PBA_CPU_FLOAT;
    else if(strstr(driver_argument, "--double"))    device = ParallelBA::PBA_CPU_DOUBLE;

    /////////////////////////////////////////////////////////////////////
    ParallelBA pba(device);          //You should reusing the same object for all new data

    /////////////////////////////////////////////////////////
    //Parameters can be changed before every call of RunBundleAdjustment
    //But do not change them from another thread when it is running BA.
    pba.ParseParam(argc, argv);      //indirect parameter tuning from commandline

    //           equivalent to pba.GetInternalConfig()->__fixed_focallength = true;

    //check src/pba/ConfigBA.h for more details on the parameter system
    //           equivalent to pba.GetInternalConfig()->__use_radial_distortion = *;
    pba.EnableRadialDistortion(ParallelBA::PBA_NO_DISTORTION); // if you want to enable radial distortion
    //distortion_type = ParallelBA::PBA_MEASUREMENT_DISTORTION;
    //int oo = ParallelBA::PBA_MEASUREMENT_DISTORTION;
    pba.GetInternalConfig()->__lm_max_iteration = 100;
    pba.GetInternalConfig()->__cg_max_iteration = 30;
   // pba.GetInternalConfig()->__
    //pba.GetInternalConfig()->__reset_initial_distortion = false;
    //pba.SetFixedIntrinsics(true); //if your focal lengths are calibrated.
    //
    pba.SetFocalLengthFixed(true);//SetFixedIntrinsics(true);
    ////////////////////////////////////
    //Tweaks before bundle adjustment
    //1. For each camera, you can call CameraT::SetConstantCamera() to keep it unchanged.
    //pba.SetNextBundleMode(ParallelBA::BUNDLE_FULL); //chose a truncated mode?
    pba.SetNextBundleMode(ParallelBA::BUNDLE_ONLY_MOTION); //chose a truncated mode?

    ////////////////////////////////////////////////////////////////
    pba.SetCameraData(camera_data.size(),  &camera_data[0]);                        //set camera parameters
    pba.SetPointData(point_data.size(), &point_data[0]);                            //set 3D point data
    pba.SetProjection(measurements.size(), &measurements[0], &ptidx[0], &camidx[0]);//set the projections

    vector<int> cmask;
    if(strstr(driver_argument, "--common"))
    {
        cmask.resize(camera_data.size(), 0);
        pba.SetFocalMask(&cmask[0]);
    }
    //WARNING: measumrents must be stored in correct order
    //all measutments (in different images) for a single 3D point must be stored continously,
    //Currently, there is no order verification internally.
    //Basically, ptidx should be non-decreasing

    //////////////////////////////////////////////////////
    //pba.SetTimeBudget(10);      //use at most 10 seconds?
    pba.RunBundleAdjustment();    //run bundle adjustment, and camera_data/point_data will be modified


    //Write the optimized system to file
    const char*  outpath = pba.GetInternalConfig()->GetOutputParam();
    SaveModelFile(outpath, camera_data, point_data, measurements, ptidx, camidx, photo_names, point_color);
    //It is easy to visualize the camera/points yourself,
    return 0;
}



//count number of 2D measurements
int PBundleAdjuster::Count2DMeasurements(const vector<CloudPoint>& pointcloud) {
    int K = 0;
    for (unsigned int i=0; i<pointcloud.size(); i++) {
        for (unsigned int ii=0; ii<pointcloud[i].imgpt_for_img.size(); ii++) {
            if (pointcloud[i].imgpt_for_img.find(ii) !=  pointcloud[i].imgpt_for_img.end()) {
                K ++;
            }
        }
    }
    return K;
}

void convertCamera(cv::Mat K,
                   cv::Matx34d Pout,
                   std::vector<CameraT>& camera_data){

    //for(std::map<int, cv::Matx34d>::iterator i = Pmats.begin(); i != Pmats.end(); ++i){

        cv::Matx34d P = Pout;

        double* m = new double[9];
        double t[3];

        m[0] = P(0,0);
        m[1] = P(0,1);
        m[2] = P(0,2);
        t[0] = P(0,3);

        m[3] = P(1,0);
        m[4] = P(1,1);
        m[5] = P(1,2);
        t[1] = P(1,3);

        m[6] = P(2,0);
        m[7] = P(2,1);
        m[8] = P(2,2);
        t[2] = P(2,3);

        CameraT camt;
        camt.SetTranslation(t);
        camt.SetMatrixRotation(m);
      //  double llll = distortion_coef.at<double>(0,1);//data[1];

        //camt.f = camera_matrix.at<double>(0,0);

        //const float focal_mm = 4.8;
        cv::Mat ck = K;
        camt.f = ck.at<double>(0,0);//camera_matrix.at<double>(0,0);//1300 * 5.0 / focal_mm;
        //camt.radial = 0.0;//*/- 0.45 / focal_mm / focal_mm;// (double) ((double) distortion_coef.data[1]) * (camera_matrix.at<double>(0,0) * camera_matrix.at<double>(0,0) + camera_matrix.at<double>(1,1) * camera_matrix.at<double>(1,1));// (float) distortion_coef.data[1];//camera_matrix.at<double>(0,1);//1.13350556e-08;       // <----------------------------------------ATENCAO
        camt.radial = 0.0;//-distortion_coef.at<double>(0,0) / pow(4.8, 2.0);
        //camt.radial = distortion_coef.data[1];
        //if((*i).first == 1)
        //    camt.constant_camera = 1.0f;                 //<------------------------ Manter sempre a mesma posição?
        //else{
        //if(to_pose == (*i).first)
        //    camt.constant_camera = 1.0f;                 //<------------------------ Manter sempre a mesma posição?
        //else
            camt.constant_camera = 0.0f;
        camt.distortion_type = 0.0;                 //<------------------------ Mudar conforme o tipo de distorção
        camera_data.push_back(camt);
    //}

}

Point2D DistortPointR1(const Point2D& pt, double k1) {
        if (k1 == 0)
                return pt;
        const double t2 = pt.y*pt.y;
        const double t3 = t2*t2*t2;
        const double t4 = pt.x*pt.x;
        const double t7 = k1*(t2+t4);
        if (k1 > 0) {
                const double t8 = 1.0/t7;
                const double t10 = t3/(t7*t7);
                const double t14 = sqrt(t10*(0.25+t8/27.0));
                const double t15 = t2*t8*pt.y*0.5;
                const double t17 = pow(t14+t15,1.0/3.0);
                const double t18 = t17-t2*t8/(t17*3.0);
                return Point2D(t18*pt.x/pt.y, t18);
        } else {
                const double t9 = t3/(t7*t7*4.0);
                const double t11 = t3/(t7*t7*t7*27.0);
                const std::complex<double> t12 = t9+t11;
                const std::complex<double> t13 = sqrt(t12);
                const double t14 = t2/t7;
                const double t15 = t14*pt.y*0.5;
                const std::complex<double> t16 = t13+t15;
                const std::complex<double> t17 = pow(t16,1.0/3.0);
                const std::complex<double> t18 = (t17+t14 / (t17*3.0))*std::complex<double>(0.0,sqrt(3.0));
                const std::complex<double> t19 = -0.5*(t17+t18)+t14/(t17*6.0);
                return Point2D(t19.real()*pt.x/pt.y, t19.real());
        }
}

void convertCloud(std::vector<cv::Point2f> p2D,
                  std::vector<cv::Point3f> p3D,
                  cv::Mat K,
                  //cv::Matx34d Pout,
                  vector<int>& camidx,
                  vector<int>& ptidx,
                  vector<Point2D>& measurements,
                  vector<Point3D> &point_data){

    for(int i = 0; i < p3D.size(); i++){
        //CloudPoint cp = pointcloud[i];

        Point3D pt3d;
        //pt3d.reserved = 1000;
        pt3d.SetPoint(p3D[i].x, p3D[i].y, p3D[i].z);
        point_data.push_back(pt3d);

        //for(int f = 0; f < images_info.size(); f++){
        //for(std::map<int,int>::iterator it = cp.imgpt_for_img.begin(); it != cp.imgpt_for_img.end(); it++){
            //ir ao imgpt_to_img e fazer a relação entre o measurements--> ptidx --> pointdata
            //No cameradata, colocar a relação entre o ptidx e a camara correspondente
            //std::vector<int> rel = cp.imgpt_for_img;


           // int pos = it->second;//cp.imgpt_for_img[it->first];
            //if(pos > -1){
            cv::Mat camera_matrix = K;//images_info[it->first]->getK();

                cv::Matx33f kc;
                kc(0,0) = 1.0;
                kc(0,1) = 0.0;
                kc(0,2) = camera_matrix.at<double>(0,2) / camera_matrix.at<double>(0,0) ;

                kc(1,0) = 0.0;
                kc(1,1) = camera_matrix.at<double>(1,1) / camera_matrix.at<double>(0,0);
                kc(1,2) = camera_matrix.at<double>(1,2) / camera_matrix.at<double>(0,0);

                kc(2,0) = 0.0; kc(2,1) = 0.0; kc(2,2) = 1.0;

                cv::Matx33f kcinv = kc.inv();

                cv::Matx31f old_meas;
                old_meas(0,0) = p2D[i].x - camera_matrix.at<double>(0,2);
                old_meas(1,0) = p2D[i].y - camera_matrix.at<double>(1,2);
                old_meas(2,0) = 1.0;

                cv::Matx31f new_meas;

                new_meas(0,0) = kcinv(0,0) * old_meas(0,0) + kcinv(0,1) * old_meas(1,0) + kcinv(0,2) * old_meas(2,0);
                new_meas(1,0) = kcinv(1,0) * old_meas(0,0) + kcinv(1,1) * old_meas(1,0) + kcinv(1,2) * old_meas(2,0);
                new_meas(2,0) = kcinv(2,0) * old_meas(0,0) + kcinv(2,1) * old_meas(1,0) + kcinv(2,2) * old_meas(2,0);

                //Point2D cr;
                //cr.SetPoint2D(imgpts[f][pos].pt.x - camera_matrix.at<double>(0,2), imgpts[f][pos].pt.y - camera_matrix.at<double>(1,2));
                //float r = - distortion_coef.at<double>(0,1) / 4.8 / 4.8;
                //float r2 = r * (cr.x * cr.x + cr.y * cr.y);//*/
                Point2D cr;
                cr.SetPoint2D(new_meas(0,0) ,new_meas(1,0));

                Point2D meas;
                //meas = DistortPointR1(cr, -distortion_coef.at<double>(0,0) / pow(4.8,2.0)   );
                meas.SetPoint2D(new_meas(0,0) ,new_meas(1,0) );

                measurements.push_back(meas);

                ptidx.push_back(i);
                camidx.push_back(0);//idxcamera2idx.find(it->first)->second);
                                //camidx.push_back(idxcamera2idx.find(f)->second);
            //}
         //}
    }
}

void PBundleAdjuster::adjustBundle(std::vector<cv::Point2f> p2D,
                                   std::vector<cv::Point3f> p3D,
                                   cv::Mat Kmat,
                                   cv::Matx34d &Pout){

    int N = 1, M = p3D.size(), K = p2D.size();

    cout << "N (cams) = " << N << " M (points) = " << M << " K (measurements) = " << K << endl;
    /********************************************************************************/
    /*	Use PBA for sparse bundle adjustment									*/
    /********************************************************************************/

    char* arr[] = {" ","file.nvm","--double"};
    int k = 3;

    vector<CameraT> camera_data;
    vector<Point3D> point_data;
    vector<Point2D> measurements;
    vector<int> camidx;
    vector<int> ptidx;

    //map<int ,cv::Matx34d> before = Pmats;
    //map<int ,cv::Matx34d> after;

    /*cv::Matx34d kkk = Matx34d::zeros();
    Pmats[0] = kkk;//*/

    //int counter = 0;
    /*for( std::map<int, cv::Matx34d>::iterator iter = Pmats.begin();iter != Pmats.end(); iter++, counter++){
        int val = iter->first;
        idxcamera2idx[val] = counter;
    }*/

    convertCamera(Kmat,Pout, camera_data);//, distortion_coef, img_to_pose);
    convertCloud(p2D,p3D,Kmat,camidx,ptidx,measurements,point_data);//*/


    //Execute Parallel Bundle Adjustment
    pba(k, arr, camera_data, point_data, measurements, camidx, ptidx);

   // std::cout << names[0] << std::endl;

    double* transl = new double[3];
    double* rotat = new double[9];

    //std::map<int, cv::Matx34d>::iterator iter = Pmats.begin();

    //for(int i = 0; i < camera_data.size(); i++){
        camera_data[0].GetTranslation(transl);
        camera_data[0].GetMatrixRotation(rotat);

        double focal = camera_data[0].f;
        double radial = camera_data[0].radial;

        //cam_matrix.at<double>(0,0) = focal;
        //cam_matrix.at<double>(1,1) = focal;

        cv::Mat cmt = Kmat;//images_info[iter->first]->getK();
        cmt.at<double>(0,0) = focal;
        cmt.at<double>(1,1) = focal;
        Kmat = cmt;

        cv::Matx34d P;
        P(0,0) = rotat[0];
        P(0,1) = rotat[1];
        P(0,2) = rotat[2];
        P(0,3) = transl[0];

        P(1,0) = rotat[3];
        P(1,1) = rotat[4];
        P(1,2) = rotat[5];
        P(1,3) = transl[1];

        P(2,0) = rotat[6];
        P(2,1) = rotat[7];
        P(2,2) = rotat[8];
        P(2,3) = transl[2];

        Pout = P;

    delete[] transl;
    delete[] rotat;
}


