#pragma once

#include "../Common.h"
#include "../FMatchers/SiftGPUMatcher.h"
#include "../IO/ConfigFile.h"
#include "../Geocoding/GeoOperations.h"
#include "../Database/PrioriGeoDB.h"
#include "../IO/IOPrioriFeats.h"
#include "PBundleAdjuster.h"
#include "../IO/Statistics.h"


struct PrioriModel{
    std::string model_path;
    std::string model_seed;
    std::string model_comp;
    std::string model_location;
    bool has_gps;

    int n_pts_seed;
    int n_views_seed;
    int n_pts_comp;
    int n_views_comp;

    PrioriModel():
        model_path(""),
        model_seed(""),
        model_comp(""),
        model_location(""),
        has_gps(false),
        n_pts_seed(0),
        n_views_seed(0),
        n_pts_comp(0),
        n_views_comp(0){}

    PrioriModel(std::string model_p, std::string model_s, std::string model_c, std::string model_l, int n_p_seed,int n_p_comp,int n_v_seed, int n_v_comp, bool gps):
        model_path(model_p),
        model_seed(model_s),
        model_comp(model_c),
        model_location(model_l),
        has_gps(gps),
        n_pts_seed(n_p_seed),
        n_views_seed(n_v_seed),
        n_pts_comp(n_p_comp),
        n_views_comp(n_v_comp){}
};

class PrioriGeoLocalizor {
protected:
    cv::Ptr<AbstractFeatureMatcher> feature_matcher;

    //Pose Estimation variables
    int max_search;
    double ratio_seed;
    double ratio_comp;
    double thrs_w;

    std::string priori_root;

    PrioriGeoDB geodb;

    PBundleAdjuster pba;

    std::vector<PrioriModel> model_list;

    //Seed cloud stuff
    std::vector<cv::Point3f> seed_cloud_points;
    std::vector<std::pair<int,float> >  seed_visibility_indexes;
    std::vector<std::map<int, int> >  seed_visibility_matrix;
    std::vector<int> seed_start_indexes;
    std::vector<cv::Matx44d > seed_gps_axis;
    ann_1_1_char::ANNkd_tree *seed_tree;

    //Comp cloud stuff
    std::vector<cv::Point3f> comp_cloud_points;
    std::vector<std::pair<int, float> > comp_visibility_indexes;
    std::vector<std::map<int, int> > comp_visibility_matrix;
    std::vector<unsigned char> comp_descriptors;                   //Convert to ANNpointer to save 128 for each's on querying comp
    std::vector<int> comp_start_indexes;


public:
    PrioriGeoLocalizor(std::string imgs_path, bool preemp,ConfigOptions options){
        feature_matcher = new SiftGPUMatcher(imgs_path, preemp, 200, 10, 2);
        max_search = options.priorifeats.max_search;
        ratio_comp = options.priorifeats.ratio_comp;
        ratio_seed = options.priorifeats.ratio_seed;
        thrs_w = options.priorifeats.thrs_w;
        priori_root = std::string(options.priori_root);

        model_list = std::vector<PrioriModel>();

        geodb = PrioriGeoDB(priori_root + "geodatabase.db");
    }

    void InitializeSiftGPU(PrioriOptions po);

    void LoadGlobalCloud();
    bool InitializeDB();
    bool UnloadSiftGPU();

    int InferModelIDSeed(int index);
    int InferModelIDComp(int index);

    bool FindPose(std::vector<cv::Point2f> &max_2d,
                  std::vector<cv::Point3f> &max_3d,  cv::Mat K , cv::Matx34d &Pout);
    bool LocateImg(std::string imgs_path,
                   std::string img_name,
                   StatPriori &loc_statistics);


    bool CoocurrenceSamplingSeed(std::vector<cv::KeyPoint> query_points,
                                 std::map<int, cv::DMatch> matches,
                                 std::vector<cv::Point2f> &max2D,
                                 std::vector<cv::Point3f> &max3D);

    bool CoocurrenceSamplingComp(std::vector<cv::KeyPoint> query_points,
                                 std::map<int, std::pair<int, cv::DMatch> > matches,
                                 std::vector<cv::Point2f> &max2D,
                                 std::vector<cv::Point3f> &max3D);


    bool QueryPhotograph(std::vector<cv::KeyPoint> query_keys,
                         std::vector<float> query_descs,
                         cv::Mat K,
                         StatPriori &loc_statistics);/*
                         cv::Matx34d &Pout,
                         GPSCoords &gps,
                         std::string &direction,
                         int &idx);*/

    bool MatchGlobalComp(std::vector<cv::KeyPoint> query_keys,
                         std::vector<float> query_descs,
                         std::vector<std::pair<int, float> > seed_visibility_indexes_copy,
                         std::map<int, std::map<int, cv::DMatch> > matches_by_model,
                         cv::Mat K, StatPriori &loc_statistics,
                         cv::Matx34d &Pout,
                         int &posed_idx);

    bool MatchGlobalSeed(std::vector<cv::KeyPoint> query_points, std::vector<float> query_descs,
                         std::vector<std::pair<int, float> > &seed_visibility_indexes_copy,
                         std::map<int, std::map<int, cv::DMatch> > &matches_by_model, cv::Mat K,
                         StatPriori &loc_statistics, cv::Matx34d &Pout,
                         int &posed_idx);

};

