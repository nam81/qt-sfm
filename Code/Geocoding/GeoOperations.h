#pragma once
#include "GeoOperations.h"
//#include <opencv2/highgui/highgui.hpp>
//#include <opencv2/imgproc/imgproc.hpp>
//#include <opencv2/video/tracking.hpp>
#include <eigen3/Eigen/Eigen>
//#include <iostream>
#include "../ImageV.h"


//Compute matrix G
bool Compute3DtoGPSMat(std::vector<ImageV*> images_info,
                        std::map<int, cv::Matx34d> pmats,
                        cv::Matx44d &G);

//Compute model to GPS coordinates
bool ComputeModelToGPS(std::vector<cv::Point3d> points, std::vector<GPSCoords > gpss, cv::Matx44d &G);

//Compute GPS coordinates
void ModelToGPS(cv::Matx34d P, cv::Matx44d G, GPSCoords &gps_coords, std::string &dir);

//Compute deviantion error
double CompareGroundTruth(GPSCoords estimation, GPSCoords truth);
