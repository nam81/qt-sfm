#include "GeoOperations.h"
#include "opencv2/calib3d/calib3d.hpp"
#include <math.h>
#include <fstream>

#define _USE_MATH_DEFINES

#define TO_RAD M_PI / 180.0
#define EARTH_RADIUS 6371000.0 //meteers
#define GPS_FRACTION 1000000.0

#define NORTH "North"
#define NEAST "North East"
#define NWEST "North West"
#define SOUTH "South"
#define SEAST "South East"
#define SWEST "South West"
#define WEST  "West"
#define EAST  "East"
#define UNKNOWN "UNKNOWN"

#define M_PI_30 M_PI / 6.0
#define M_PI_60 M_PI / 3.0
#define M_PI_90 M_PI / 2.0
#define M_PI_270 3.0 * M_PI / 2.0

bool Compute3DtoGPSMat(std::vector<ImageV*> images_info,
                        std::map<int, cv::Matx34d> pmats,
                        cv::Matx44d &G){

    std::vector<cv::Point3d> positions;//(images_info.size());
    std::vector<GPSCoords> gps_coords;//(images_info.size());

    for(std::map<int,cv::Matx34d>::iterator iter = pmats.begin(); iter != pmats.end(); iter++){
        cv::Matx33d R;
        cv::Matx31d T;
        R(0,0) = pmats[iter->first](0,0);
        R(0,1) = pmats[iter->first](0,1);
        R(0,2) = pmats[iter->first](0,2);

        R(1,0) = pmats[iter->first](1,0);
        R(1,1) = pmats[iter->first](1,1);
        R(1,2) = pmats[iter->first](1,2);

        R(2,0) = pmats[iter->first](2,0);
        R(2,1) = pmats[iter->first](2,1);
        R(2,2) = pmats[iter->first](2,2);

        T(0,0) = pmats[iter->first](0,3);
        T(1,0) = pmats[iter->first](1,3);
        T(2,0) = pmats[iter->first](2,3);
        cv::Matx31d C = (- R.t()) * T;

        cv::Point3d pt3d(C(0,0), C(1,0), C(2,0));

        positions.push_back(pt3d);
        gps_coords.push_back(images_info[iter->first]->getGPSArray());
    }

    return ComputeModelToGPS(positions,gps_coords,G);

    //ComputeGeoAxis2(positions,gps_coords,gps_axis);
}

//Compute transformation matrix G (model<->GPS)
bool ComputeModelToGPS(std::vector<cv::Point3d> points,std::vector<GPSCoords > gpss, cv::Matx44d &G){

    int non_estimated = 0;
    std::vector<cv::Point3d> left,right;
    for(int i = 0; i < points.size(); i++){
        if(gpss[i].latitude != -200){
            left.push_back(points[i]);
            right.push_back(cv::Point3d(gpss[i].latitude * GPS_FRACTION,gpss[i].longitude * GPS_FRACTION, gpss[i].altitude));
        }else{
            non_estimated++;
        }

    }

    if(non_estimated == points.size()){
        G = cv::Matx44d(1.0, 1.0, 1.0,1.0,
                        1.0, 1.0, 1.0,1.0,
                        1.0, 1.0, 1.0,1.0,
                        1.0, 1.0, 1.0,1.0);

        return false;
    }

    //Compute affine 3D transform to estimate the transformation matrix between model<->GPS
    cv::Mat M(3,4,CV_64F);// = cv::Mat_::zeros(3,3); //= cv::getAffineTransform(pos1,gps1);
    std::vector<uchar> inliers;

    cv::estimateAffine3D(left,right,M,inliers);


    G = cv::Matx44d(M.at<double>(0,0), M.at<double>(0,1), M.at<double>(0,2), M.at<double>(0,3),
                    M.at<double>(1,0), M.at<double>(1,1), M.at<double>(1,2), M.at<double>(1,3),
                    M.at<double>(2,0), M.at<double>(2,1), M.at<double>(2,2), M.at<double>(2,3),
                                  0.0,               0.0,               0.0,              1.0);


    return true;
}

//Compute direction given the center position and the position * direction_vector
std::string ComputeDirection(GPSCoords gps1, GPSCoords gps2){

    gps2.latitude = gps2.latitude - gps1.latitude;
    gps2.longitude = gps2.longitude - gps1.longitude;
    gps2.altitude = gps2.altitude - gps1.altitude;

    double x = pow(gps2.longitude, 2.0);
    double y = pow(gps2.latitude, 2.0);

    double norm_dir = sqrt(x + y);

    x = acos(gps2.longitude / norm_dir);
    y = asin(gps2.latitude / norm_dir);

    bool not_zero = true;
    double round = M_PI_90;
    //int quandrant = 0;
    //bool negativity_x = false;
    //bool negativity_y = false;

    int quadrant = 1;


    if(gps2.longitude < 0 && gps2.latitude < 0){
        //negativity_x = true;
        //negativity_y = true;
        quadrant = 3;
    }else{
        if(gps2.longitude < 0 && gps2.latitude > 0)
            quadrant = 2;
        else
            if(gps2.longitude > 0 && gps2.latitude < 0)
                quadrant = 4;
    }
    /*while(not_zero){
        if(x - round > 0.0){
            round += round;
            //quandrant++;
        }
        not_zero = false;
    }*/

    /*if(!negativity){
        if(x - round < 0)
            quandrant = 0;
        else
            quandrant = 1;
    }else
    {
        if(x - round < 0)
            quandrant = 3;
        else
            quandrant = 2;
    }*/

    double lll = M_PI_30;


    switch(quadrant){

    case 1 :
        if(x < M_PI_30)
            return NORTH;
        if(x > M_PI_30 && x < M_PI_60)
            return NWEST;
        if(x > M_PI_60)
            return WEST;
        break;
    case 2 :
        if(x < M_PI_30 + M_PI_90)
            return WEST;
        if(x > M_PI_30 + M_PI_90 && x < M_PI_60 + M_PI_90)
            return SWEST;
        if(x > M_PI_60 + M_PI_90)
            return SOUTH;
        break;
    case 4 :
        if(x < M_PI_30 )
            return NORTH;
        if(x > M_PI_30  && x < M_PI_60 )
            return NEAST;
        if(x > M_PI_60 )
            return EAST;
        break;
    case 3 :
        if(x < M_PI_30 + M_PI_90)
            return EAST;
        if(x > M_PI_30 + M_PI_90 && x < M_PI_60 + M_PI_90)
            return SEAST;
        if(x > M_PI_60 + M_PI_90)
            return SOUTH;
        break;
    default :
        return UNKNOWN;
    }

    //Should not reach here
    return "";

}

//Compute GPS coordinates
void ModelToGPS(cv::Matx34d P, cv::Matx44d G, GPSCoords &gps_coords, std::string& dir){

    cv::Matx33d R;
    cv::Matx31d T;
    R(0,0) = P(0,0);
    R(0,1) = P(0,1);
    R(0,2) = P(0,2);

    R(1,0) = P(1,0);
    R(1,1) = P(1,1);
    R(1,2) = P(1,2);

    R(2,0) = P(2,0);
    R(2,1) = P(2,1);
    R(2,2) = P(2,2);

    T(0,0) = P(0,3);
    T(1,0) = P(1,3);
    T(2,0) = P(2,3);
    cv::Matx31d C = (- R.t()) * T;
    cv::Matx41d C2(C(0,0),
                   C(1,0),
                   C(2,0),
                   1.0);

    cv::Matx41d gps =  G * C2;//C2 * G.inv() ;
    gps_coords.latitude = (gps(0,0) / gps(3,0)) / GPS_FRACTION;
    gps_coords.longitude = (gps(1,0) / gps(3,0)) / GPS_FRACTION;
    gps_coords.altitude = (gps(2,0) / gps(3,0));


    cv::Matx31d DForward = cv::Matx31d(R(2,0),
                                       R(2,1),
                                       R(2,2));

    cv::Matx31d TIP = T + DForward * 3.0;

    cv::Matx31d C3 = (- R.t()) * TIP;

    //cv::Matx31d C3 = C + DForward * 30.0;
    C2 = cv::Matx41d(C3(0,0),
                     C3(1,0),
                     C3(2,0),
                     1.0);


    cv::Matx41d gps_tip = G * C2;
    GPSCoords gps_coords2;
    gps_coords2.latitude = (gps_tip(0,0) / gps_tip(3,0)) / GPS_FRACTION;
    gps_coords2.longitude = (gps_tip(1,0) / gps_tip(3,0)) / GPS_FRACTION;
    gps_coords2.altitude = (gps_tip(2,0) / gps_tip(3,0));

    dir = ComputeDirection(gps_coords, gps_coords2);
}

//Compute deviation from the ground truth
double CompareGroundTruth(GPSCoords estimation, GPSCoords truth){

    double dLat = (truth.latitude - estimation.latitude) * TO_RAD;
    double dLon = (truth.longitude - estimation.longitude) * TO_RAD;
    double lat1 = estimation.latitude * TO_RAD;
    double lat2 = truth.latitude * TO_RAD;

    double a = sin(dLat/2.0) * sin(dLat/2.0) +
               sin(dLon/2.0) * sin(dLon/2.0) * cos(lat1) * cos(lat2);

    double c = 2.0 * atan2(sqrt(a), sqrt(1.0-a));

    return EARTH_RADIUS * c;
}


