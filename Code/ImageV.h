#pragma once

#include <opencv2/features2d/features2d.hpp>
//#include "../Geocoding/GeoOperations.h"

struct GPSCoords{
    double latitude;
    double longitude;
    double altitude;

    GPSCoords():
        latitude(-200.0),
        longitude(-200.0),
        altitude(-200.0){}

    GPSCoords(double lat, double lon, double alt):
        latitude(lat),
        longitude(lon),
        altitude(alt){}
};

class ImageV{

private:
    std::vector<cv::KeyPoint> keypoints;
    std::vector<float> descriptors;
    //std::map<int,int> projected_points;
    cv::Mat K;
    //cv::Mat Kinv;
    GPSCoords gps_coordinates;

public:
    ImageV(){
        keypoints = std::vector<cv::KeyPoint>();
        descriptors = std::vector<float>();
        //projected_points = std::map<int,int>();
        K = cv::Mat();
        //Kinv = cv::Mat();
        gps_coordinates = GPSCoords();
    }
    ~ImageV(){
        delete &keypoints;
        //delete &K;
        //delete &Kinv;
        delete &descriptors;
        delete &gps_coordinates;
    }


    //Gets
    std::vector<cv::KeyPoint> getKeypoints(){return keypoints;}
    std::vector<float> getDescriptors(){return descriptors;}
    //std::map<int,int> getProjPoints(){return projected_points;}
    cv::Mat getK(){return K;}
    //cv::Mat getKinv(){return Kinv;}
    GPSCoords getGPSArray(){return gps_coordinates;}
    double getLatitude(){return gps_coordinates.latitude;}
    double getLongitude(){return gps_coordinates.longitude;}
    double getAltitude(){return gps_coordinates.altitude;}


    cv::KeyPoint getPt(int i){ return keypoints[i];}
    float getDes(int i){ return descriptors[i];}

    /*int getPtProj(int i){
        std::map<int,int>::iterator iter = projected_points.find(i);
        if(iter != projected_points.end())
            return (*iter).second;
        else
            return -1;
    }*/

    int getKeysLen(){return keypoints.size();}

    //Sets
    void setKeypoints(std::vector<cv::KeyPoint> kp){ keypoints = kp;}
    void setDescriptors(std::vector<float> ds){descriptors = ds;}
    //void setProjPoints(std::map<int,int> pp){projected_points = pp;}
    void setK(cv::Mat k){ K = k;}
    void setGPS(GPSCoords geo){ gps_coordinates = geo;}
    void setLatitude(double x){ gps_coordinates.latitude = x;}
    void setLongitude(double y){ gps_coordinates.longitude = y;}
    void setAltitude(double z){ gps_coordinates.altitude = z;}

    //void setKinv(cv::Mat ki){ Kinv = ki;}

    void setPt(int i, cv::KeyPoint kp){keypoints[i] = kp;}
    void setDes(int i, float v){descriptors[i] = v;}

    /*int setPtProj(int pos, int cloud_pos){
        if(projected_points.find(pos) == projected_points.end()){
            projected_points.insert(std::make_pair(pos,cloud_pos));
        }else
            return -1;
    }*/

    /*int erasePtProj(int p){
        if(projected_points.find(p) != projected_points.end()){
            projected_points.erase(projected_points.find(p));
            return 0;
        }
        else
            return -1;
    }*/
};
