#pragma once
#include <iostream>
#include <map>

struct PrioriOptions{

    int Kc;
    int Ks;
    int max_seed_points;
    int max_comp_points;

    float ratio_seed;
    float ratio_comp;
    int max_search;
    float thrs_w;

    //Siftgpu
    int use_cuda;
    char* fo;
    char* tc_type;
    char* tc;
    char* ori_n;

    PrioriOptions():
        Kc(100),
        Ks(5),
        max_seed_points(2000),
        max_comp_points(0),
        ratio_seed(0.3),
        ratio_comp(0.9),
        max_search(5000),
        thrs_w(5.0),
        use_cuda(0),
        fo("0"),
        tc_type("-tc1"),
        tc("2280"),
        ori_n("1"){}


};

struct SynthOptions{

    float view_width;
    float view_height;
    float view_focal;
    float view_fov;
    float view_min_focal;
    float view_max_focal;
    float view_lean_angle;
    int min_per_view;
    float extend_boundary;

    float strafe_pos;

    int max_feats_match;

    //Siftgpu
    int use_cuda;
    char* fo;
    char* tc_type;
    char* tc;
    char* ori_n;


    SynthOptions():
        view_width(1200.0),
        view_height(875.0),
        view_focal(1000.0),
        view_fov(60.0),
        view_min_focal(0.1),
        view_max_focal(3.0),
        view_lean_angle(1.0),
        min_per_view(150),
        extend_boundary(5.0),
        strafe_pos(1.0),
        max_feats_match(2368),
        use_cuda(0),
        fo("0"),
        tc_type("-tc1"),
        tc("1280"),
        ori_n("1"){}


};


struct ConfigOptions {
    PrioriOptions priorifeats;
    SynthOptions synthfeats;

    char* synth_root;
    char* priori_root;

    char* vocab_learn_path;
    char* vocab_data_path;
    int vocab_levels;
    int vocab_branches;
    int vocab_type;
    int top_documents;

    ConfigOptions():
        priorifeats(),
        synthfeats(),
        synth_root("../SynthOutput/"),
        priori_root("../PrioriOutput/"),
        vocab_learn_path("voclearn.db"),
        vocab_data_path("vocdata.db"),
        vocab_levels(3),
        vocab_branches(50),
        vocab_type(1),
        top_documents(10){}
};


bool ReadConfigFile1(const char* file_path, ConfigOptions &config);
