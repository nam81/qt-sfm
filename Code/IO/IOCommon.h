#pragma once

#include "../Common.h"
#include "../FMatchers/SiftGPUMatcher.h"
#include "../../3rdparty/VocabTree2/lib/ann_1.1_char/include/ANN/ANN.h"


//=============================================== IMGAES =========================================================

bool ReadImageList(const char* list_path, std::vector<std::string>& img_names);

int WriteImageList(std::string path,std::string file_name, bool pose_estimation);

int WriteImageList(std::string file_name, std::vector<std::string> imgnames);

//------------------------------------------------ MATCHES -----------------------------------------------------------
//ReaadSiftVSFM
bool ReadSiftVSFM(const char* sift_path, std::vector<cv::KeyPoint>& keys, std::vector<float>& descs);

//Read matches from .mat
bool ReadMatches(int idx_i, int idx_j, std::string img1, std::string img2, int FC1, int FC2, int &NM, std::vector<cv::DMatch>& matches);

//Write matches to .mat
void WriteMatches(std::string img1, std::string img2, int fc1, int fc2, int num_match, std::vector<cv::DMatch>& dmatches);

//------------------------------------------------ FEATURES -----------------------------------------------------------
//Read features from .sift to SiftGPU
bool ReadSIFTA(const char* sift_path, std::vector<float> pLoc, std::vector<unsigned int> pDes);

//Read features from .sift to opencv sift
bool ReadSiftA(const char* sift_path, std::vector<cv::KeyPoint>& keys, std::vector<float> &descs);
bool ReadSiftB(const char* sift_path, std::vector<cv::KeyPoint>& keys, std::vector<float>& descs);


//Write features from opencv sift to .sift
//TODO: saveSIFT has a bug on saving the pixel positions
void SaveSIFTA(const char* sift_path, std::vector<cv::KeyPoint> keys, std::vector<float> descs);
void SaveSIFTB(const char* sift_path, std::vector<cv::KeyPoint> keys, std::vector<float> descs);

//-------------------------------------------- Converters ------------------------------------------------------------
bool AsciiToBin(const char* sift_path);

//------------------------------------------ Output Files  -----------------------------------------------------------
bool ReadVizOutput(const char* output_path,
                   std::map<std::string, std::vector<int> > &posed_list,
                   std::vector<std::string> &posed_names,
                   std::vector<cv::Matx34d> &posed_P);

//---------------------------------------------------- NVM ---------------------------------------------------------------

//Read model from NVM
//Save method based on the SaveNVM function from Multicore Bundle Adjustment
void SaveToNVM(const char* file_name,
               std::vector<std::string> names,
               std::map<int ,cv::Matx34d> Pmats,
               cv::Mat& camera_matrix,
               std::vector<CloudPoint>& pointcloud,
               std::vector<cv::Vec3b>& pointcloudRGB);

//Saves Cloud for prioritized features
/*void SaveToNVM(const char* file_name,
               std::vector<ImageV *> imgs_info,
               std::vector<CloudPoint> pointcloud);
*/
//Load Cloud for prioritized features
/*bool LoadFromNVM(const char* file_name,
                 std::vector<std::vector<cv::Point2d> > &keypoints,
                 std::vector<std::vector<float> > &descriptors,
                 std::vector<cv::Point3d> &pointcloud);
*/
//Read model from NVM
/*
bool LoadFromNVM(const char* file_name,
                 std::vector<std::string>& imgnames,
                 std::map<int ,cv::Matx34d>& Pmats,
                 cv::Mat& K,
                 std::vector<CloudPoint>& pointcloud,
                 std::vector<cv::Vec3b>& pointcloudRGB,
                 std::map<int,int>& vws,
                 std::vector<ImageV*> &img_info);*/

//Read mode from NVM (VisualSFM style)
bool LoadFromNVM(const char* file_name,
                 std::vector<std::string>& imgnames,
                 std::map<int ,cv::Matx34d>& Pmats,
                 std::vector<cv::Mat> &K,
                 std::vector<CloudPoint> &pointcloud,
                 std::vector<cv::Vec3b>& pointcloudRGB, int type);

//For simple visualization
bool LoadFromNVM(const char* file_name,
                 std::map<int ,cv::Matx34d>& Pmats,
                 std::vector<cv::Point3d>& pointcloud,
                 std::vector<cv::Vec3b>& pointcloudRGB, int type);

