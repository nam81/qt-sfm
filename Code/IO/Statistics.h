#pragma once

#include <vector>
#include <string>
#include "../Geocoding/GeoOperations.h"
#include <ctime>

class StatSynth{

private:
    bool success;
    std::string photo_name, direction, location, model_name;
    int doc_attempt;
    GPSCoords gps_coords;
    cv::Matx34d Pmat;
    clock_t time;

#ifdef GEO_DEBUG
    clock_t t_k_matrix, t_extraction, t_vocab, t_match, t_pnp, t_gps;
#endif

public:

    StatSynth(){
        success = false;
        photo_name = std::string();
        doc_attempt = -1;
        gps_coords = GPSCoords();
        location = std::string();
        direction = std::string();
        model_name = std::string();
        Pmat = cv::Matx34d();
#ifdef GEO_DEBUG
        t_k_matrix = 0;
        t_extraction = 0;
        t_vocab = 0;
        t_match = 0;
        t_pnp = 0;
        t_gps = 0;
#endif
    }

    StatSynth(std::string p_name, clock_t t){
        success = false;
        photo_name = p_name;
        time = t;
        doc_attempt = -1;
        gps_coords = GPSCoords();
        location = std::string();
        direction = std::string();
        model_name = std::string();
        Pmat = cv::Matx34d();        
#ifdef GEO_DEBUG
        t_k_matrix = 0;
        t_extraction = 0;
        t_vocab = 0;
        t_match = 0;
        t_pnp = 0;
        t_gps = 0;
#endif
    }

    StatSynth(std::string p_name, clock_t t, int d_attempt, GPSCoords g_coords, std::string loc, std::string dir, std::string m_name, cv::Matx34d P){
        success = true;
        photo_name = p_name;
        time = t;
        doc_attempt = d_attempt;
        gps_coords = g_coords;
        location = loc;
        direction = dir;
        model_name = m_name;
        Pmat = P;
#ifdef GEO_DEBUG
        t_k_matrix = 0;
        t_extraction = 0;
        t_vocab = 0;
        t_match = 0;
        t_pnp = 0;
        t_gps = 0;
#endif
    }

    ~StatSynth(){}

    //Gets
    bool getSuccess(){return success;}
    std::string getName(){return photo_name;}
    int getDoc(){return doc_attempt;}
    GPSCoords getGPS(){return gps_coords;}
    std::string getLocation(){return location;}
    std::string getDirection(){return direction;}
    std::string getModelName(){return model_name;}
    cv::Matx34d getPmat(){return Pmat;}

    clock_t getTime(){return time;}

    void setTTime(clock_t t){time = t;}

#ifdef GEO_DEBUG
    clock_t getTK(){return t_k_matrix;}
    clock_t getTExt(){return t_extraction;}
    clock_t getTVoc(){return t_vocab;}
    clock_t getTMatch(){return t_match;}
    clock_t getTPnP(){return t_pnp;}
    clock_t getTGPS(){return t_gps;}


    //Sets

    void setTK(clock_t tk){t_k_matrix = tk;}
    void setTExt(clock_t te){t_extraction = te;}
    void setTVoc(clock_t tv){t_vocab = tv;}
    void setTMatch(clock_t tm){t_match = tm;}
    void setTPnP(clock_t tp){t_pnp = tp;}
    void setTGPS(clock_t tg){t_gps = tg;}

    void setTimes(clock_t tk,clock_t te,clock_t tv,clock_t tm,clock_t tp,clock_t tg){
        t_k_matrix = tk;
        t_extraction = te;
        t_vocab = tv;
        t_match = tm;
        t_pnp = tp;
        t_gps = tg;
    }
#endif

};

class StatisticsSynth{

private:
    std::vector<StatSynth> statistics;
    int siftgpu;

public:
    StatisticsSynth(){
       statistics = std::vector<StatSynth>();
       siftgpu = 0;

    }

    StatisticsSynth(int sift_mode){
       statistics = std::vector<StatSynth>();
       siftgpu = sift_mode;
    }

    //Inserts
    //void insertInfo(Stat stats);
    void insertInfo(StatSynth stats);

    //Sets
    void setSiftType(int type){
        siftgpu = type;
    }


    //ToFiles
    bool toFileStatistics(std::string file_path);

    bool toFileViz(std::string output_path);

};


//=================================== Statistics Prioritized FEatures ===============================

class StatPriori{

private:
    bool success;
    std::string photo_name, direction, location, model_name;
    GPSCoords gps_coords;
    cv::Matx34d Pmat;

    clock_t time;
#ifdef GEO_DEBUG
    clock_t t_k_matrix, t_extraction, t_ann, t_cooc, t_p3p, t_pba, t_gps;
#endif

public:
    StatPriori(){
        success = false;
        photo_name = std::string();
        gps_coords = GPSCoords();
        location = std::string();
        direction = std::string();
        model_name = std::string();
        Pmat = cv::Matx34d();
#ifdef GEO_DEBUG
        t_k_matrix = 0;
        t_extraction = 0;
        t_ann = 0;
        t_cooc = 0;
        t_p3p = 0;
        t_pba = 0;
        t_gps = 0;
#endif
    }

    StatPriori(std::string p_name, clock_t t){
        success = false;
        photo_name = p_name;
        time = t;
        gps_coords = GPSCoords();
        location = std::string();
        direction = std::string();
        model_name = std::string();
        Pmat = cv::Matx34d();
#ifdef GEO_DEBUG
        t_k_matrix = 0;
        t_extraction = 0;
        t_ann = 0;
        t_cooc = 0;
        t_p3p = 0;
        t_pba = 0;
        t_gps = 0;
#endif
    }

    StatPriori(std::string p_name, clock_t t, GPSCoords g_coords, std::string loc, std::string dir, std::string m_name, cv::Matx34d P){
        success = true;
        photo_name = p_name;
        time = t;
        gps_coords = g_coords;
        location = loc;
        direction = dir;
        model_name = m_name;
        Pmat = P;
#ifdef GEO_DEBUG
        t_k_matrix = 0;
        t_extraction = 0;
        t_ann = 0;
        t_cooc = 0;
        t_p3p = 0;
        t_pba = 0;
        t_gps = 0;
#endif
    }

    //Gets
    bool getSuccess(){return success;}
    std::string getName(){return photo_name;}
    GPSCoords getGPS(){return gps_coords;}
    std::string getLocation(){return location;}
    std::string getDirection(){return direction;}
    std::string getModelName(){return model_name;}
    cv::Matx34d getPmat(){return Pmat;}

    clock_t getTime(){return time;}

    //Sets
    void setSuccess(bool s){success=s;}
    void setName(std::string n){photo_name=n;}
    void setGPS(GPSCoords g){ gps_coords = g;}
    void setLocation(std::string l){ location = l;}
    void setDirection(std::string d){ direction = d;}
    void setModelName(std::string m){ model_name = m;}
    void setPmat(cv::Matx34d p){ Pmat = p;}
    void setTTime(clock_t t){time = t;}


#ifdef GEO_DEBUG
    clock_t getTK(){return t_k_matrix;}
    clock_t getTExt(){return t_extraction;}
    clock_t getTANN(){return t_ann;}
    clock_t getTCooc(){return t_cooc;}
    clock_t getTP3P(){return t_p3p;}
    clock_t getTPBA(){return t_pba;}
    clock_t getTGPS(){return t_gps;}

    void setTK(clock_t tk){t_k_matrix = tk;}
    void setTExt(clock_t te){t_extraction = te;}
    void setTANN(clock_t ta){t_ann = ta;}
    void setTCooc(clock_t tc){t_cooc = tc;}
    void setTP3P(clock_t tp){t_p3p = tp;}
    void setTPBA(clock_t tb){t_pba = tb;}
    void setTGPS(clock_t tg){t_gps = tg;}

    void setTimes(clock_t tk,clock_t te,clock_t ta,clock_t tc,clock_t tp,clock_t tb, clock_t tg){
        t_k_matrix = tk;
        t_extraction = te;
        t_ann = ta;
        t_cooc = tc;
        t_p3p = tp;
        t_pba = tb;
        t_gps = tg;
    }
#endif
};

class StatisticsPriori{

private:
    std::vector<StatPriori> statistics;
    int siftgpu;

public:
    StatisticsPriori(){
       statistics = std::vector<StatPriori>();
       siftgpu = 0;
    }

    StatisticsPriori(int sift_mode){
       statistics = std::vector<StatPriori>();
       siftgpu = sift_mode;
    }

    //Inserts
    //void insertInfo(Stat stats);
    void insertInfo(StatPriori stats);


    //Gets

    //Sets
    void setSiftType(int type){
        siftgpu = type;
    }


    //ToFiles
    bool toFileStatistics(std::string file_path);
    bool toFileStatisticsS(std::string imgs_path);


    bool toFileViz(std::string output_path);

};
