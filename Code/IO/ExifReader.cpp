#include "ExifReader.h"
#include "../Geocoding/GeoOperations.h"
#include <fstream>

#define INCHES2MM 1.0 / 25.4
#define CM2MM 10.0


bool ComputeFocalXY(std::string img_path, cv::Mat &K){
    Exiv2::LogMsg::setLevel(Exiv2::LogMsg::error);

    try{
        Exiv2::Image::AutoPtr image = Exiv2::ImageFactory::open(img_path);
        assert(image.get() != 0);
        image->readMetadata();

        Exiv2::ExifData &exifData = image->exifData();
        if (exifData.empty()) {
            std::string error(img_path);
            error += ": No Exif data found in the file";
            throw Exiv2::Error(1, error);

            return false;
        }

        Exiv2::ExifKey f = Exiv2::ExifKey("Exif.Photo.FocalLength");
        Exiv2::ExifKey fx = Exiv2::ExifKey("Exif.Photo.FocalPlaneXResolution");
        Exiv2::ExifKey fy = Exiv2::ExifKey("Exif.Photo.FocalPlaneYResolution");
        Exiv2::ExifKey w = Exiv2::ExifKey("Exif.Photo.PixelXDimension");
        Exiv2::ExifKey h = Exiv2::ExifKey("Exif.Photo.PixelYDimension");
        Exiv2::ExifKey u = Exiv2::ExifKey("Exif.Photo.FocalPlaneResolutionUnit");


        Exiv2::ExifData::iterator pos_f = exifData.findKey(f);
        if(pos_f == exifData.end())
            return false;

        Exiv2::ExifData::iterator pos_fx = exifData.findKey(fx);
        if(pos_fx == exifData.end())
            return false;
        Exiv2::ExifData::iterator pos_fy = exifData.findKey(fy);
        if(pos_fy == exifData.end())
            return false;
        Exiv2::ExifData::iterator pos_w = exifData.findKey(w);
        if(pos_w == exifData.end())
            return false;
        Exiv2::ExifData::iterator pos_h = exifData.findKey(h);
        if(pos_h == exifData.end())
            return false;
        Exiv2::ExifData::iterator pos_u = exifData.findKey(u);
        if(pos_u == exifData.end())
            return false;

        float focal_length, focalx,focaly, height, width;
        focal_length = pos_f->value().toFloat();
        focalx = pos_fx->value().toFloat();
        focaly = pos_fy->value().toFloat();
        width = pos_w->value().toFloat();
        height = pos_h->value().toFloat();

        int unit_type = pos_u->value().toLong();
        double unit_convertion = 0.0;

        if(unit_type == 1)
            unit_convertion = 1.0;
        if(unit_type == 2)
            unit_convertion = INCHES2MM;
        if(unit_type == 3)
            unit_convertion = CM2MM;

        /*if(width > 1300){
            double resize = 1300.0 / width;

            K.at<double>(0,0) = resize * unit_convertion * focalx * focal_length;//(1300.0 * focalx / width ) /focal_length;
            K.at<double>(1,1) = resize * unit_convertion * focaly * focal_length;//(1300.0 * focaly / width ) /focal_length;
            K.at<double>(0,2) = 1300.0 / 2.0;
            K.at<double>(1,2) = 975.0 / 2.0;
        }else{*/



            K.at<double>(0,0) = unit_convertion * focalx * focal_length;//(focalx) /focal_length;
            K.at<double>(1,1) = unit_convertion * focaly * focal_length;//(focaly) /focal_length;
            K.at<double>(0,2) = width / 2.0;
            K.at<double>(1,2) = height / 2.0;

        //}
    }catch (Exiv2::AnyError& e) {
#ifdef GEO_DEBUG
        std::cerr << "[Exiv2] Couldn't extract geotags from " << img_path << ". --> " << e << "'\n";
#endif
        return false;
        //exit(0);
    }/*catch (const std::exception& e1) {
        // ...
        std::cout << "[Exiv2] Couldn't extract geotags from " << img_path << ". --> " << "'\n";

    }catch (const int& e2) {
        // ...
        std::cout << "[Exiv2] Couldn't extract geotags from " << img_path << ". --> " << "'\n";

    }catch (const std::string& e3) {
        // ...
        std::cout << "[Exiv2] Couldn't extract geotags from " << img_path << ". --> " << "'\n";

    }*/

    //std::cout << K << std::endl;
}

bool ComputeFocal35mm(std::string img_path, cv::Mat &K){
    Exiv2::LogMsg::setLevel(Exiv2::LogMsg::error);

    try{
        Exiv2::Image::AutoPtr image = Exiv2::ImageFactory::open(img_path);
        assert(image.get() != 0);
        image->readMetadata();

        Exiv2::ExifData &exifData = image->exifData();
        if (exifData.empty()) {
            std::string error(img_path);
            error += ": No Exif data found in the file";
            throw Exiv2::Error(1, error);

            return false;
        }

        Exiv2::ExifKey f = Exiv2::ExifKey("Exif.Photo.FocalLength");
        Exiv2::ExifKey w = Exiv2::ExifKey("Exif.Photo.PixelXDimension");
        Exiv2::ExifKey mm35 = Exiv2::ExifKey("Exif.Photo.FocalLengthIn35mmFilm");
        Exiv2::ExifKey h = Exiv2::ExifKey("Exif.Photo.PixelYDimension");


        Exiv2::ExifData::iterator pos_f = exifData.findKey(f);
        if(pos_f == exifData.end())
            return false;

        Exiv2::ExifData::iterator pos_w = exifData.findKey(w);
        if(pos_w == exifData.end())
            return false;

        Exiv2::ExifData::iterator pos_h = exifData.findKey(h);
        if(pos_h == exifData.end())
            return false;

        Exiv2::ExifData::iterator pos_35 = exifData.findKey(mm35);
        if(pos_35 == exifData.end())
            return false;




        float focal_length, focal35, width, height;
        focal_length = pos_f->value().toFloat();
        width = pos_w->value().toFloat();
        height = pos_h->value().toFloat();
        focal35 = pos_35->value().toFloat();

        //if(width > 1300){
          //  double resize = 1300.0 / width;

            K.at<double>(0,0) = width * focal35 / 35.0;//focal_length / focal35;//(1300.0 * focalx / width ) /focal_length;
            K.at<double>(1,1) = width * focal35 / 35.0;//focal_length / focal35;//(1300.0 * focaly / width ) /focal_length;
            K.at<double>(0,2) = width / 2.0;
            K.at<double>(1,2) = height / 2.0;
        //}else{
            /*/
            K.at<double>(0,0) = width * focal_length / focal35;//(focalx) /focal_length;
            K.at<double>(1,1) = width * focal_length / focal35;//(focaly) /focal_length;
          //  K.at<double>(0,2) = width / 2.0;
          //  K.at<double>(1,2) = height / 2.0;
            K.at<double>(0,2) = 1300.0 / 2.0;
            K.at<double>(1,2) = 975.0 / 2.0;
//*/
        //}
    }catch (Exiv2::AnyError& e) {
#ifdef GEO_DEBUG
        std::cerr << "[Exiv2] Couldn't extract geotags from " << img_path << ". --> " << e << "'\n";
#endif
        return false;
        //exit(0);
    }

}

bool ComputeDefaultFocal(std::string img_path, cv::Mat &K){
    Exiv2::LogMsg::setLevel(Exiv2::LogMsg::error);

    try{
        Exiv2::Image::AutoPtr image = Exiv2::ImageFactory::open(img_path);
        assert(image.get() != 0);
        image->readMetadata();

        Exiv2::ExifData &exifData = image->exifData();
        if (exifData.empty()) {
            std::string error(img_path);
            error += ": No Exif data found in the file";
            throw Exiv2::Error(1, error);

            return false;
        }

        Exiv2::ExifKey w = Exiv2::ExifKey("Exif.Photo.PixelXDimension");
        Exiv2::ExifKey h = Exiv2::ExifKey("Exif.Photo.PixelYDimension");

        Exiv2::ExifData::iterator pos_w = exifData.findKey(w);
        if(pos_w == exifData.end())
            return false;

        Exiv2::ExifData::iterator pos_h = exifData.findKey(h);
        if(pos_h == exifData.end())
            return false;

        float width, height;
        width = pos_w->value().toFloat();
        height = pos_h->value().toFloat();

        //if(width > 1300){
          //  double resize = 1300.0 / width;
        //std::cout << "   [Exif] Using default camera focal length..." << std::endl;

            K.at<double>(0,0) = width * 1.2;//focal35 / 35.0;//focal_length / focal35;//(1300.0 * focalx / width ) /focal_length;
            K.at<double>(1,1) = width * 1.2;//focal35 / 35.0;//focal_length / focal35;//(1300.0 * focaly / width ) /focal_length;
            K.at<double>(0,2) = width / 2.0;
            K.at<double>(1,2) = height / 2.0;
        //}else{
            /*/
            K.at<double>(0,0) = width * focal_length / focal35;//(focalx) /focal_length;
            K.at<double>(1,1) = width * focal_length / focal35;//(focaly) /focal_length;
          //  K.at<double>(0,2) = width / 2.0;
          //  K.at<double>(1,2) = height / 2.0;
            K.at<double>(0,2) = 1300.0 / 2.0;
            K.at<double>(1,2) = 975.0 / 2.0;
//*/
        //}
    }catch (Exiv2::AnyError& e) {
#ifdef GEO_DEBUG
        std::cerr << "[Exiv2] Couldn't extract geotags from " << img_path << ". --> " << e << "'\n";
#endif
        return false;
        //exit(0);
    }


}

bool ExtractKmatrix(std::string img_path, cv::Mat &K){

    K = cv::Mat_<double>::zeros(3,3);
    K.at<double>(2,2) = 1.0;

    //Try with focalx, focaly, focalunit
    if(ComputeFocalXY(img_path, K))
        return true;
//*/
    //Try with 35mm
    if(ComputeFocal35mm(img_path, K))
        return true;

    if(ComputeDefaultFocal(img_path,K))
        return false;

    //Default focallength
    K.at<double>(0,0) = 420.0 * 3.7 / 3.2;
    K.at<double>(1,1) = 320.0 * 3.7 / 3.2;
    K.at<double>(0,2) = 420.0 / 2.0;
    K.at<double>(1,2) = 320.0 / 2.0;

    return false;
}

double convertDMStoDD(double days, double minutes, double seconds, bool negativity){
    double degrees = days + minutes / 60.0 + seconds / 3600.0;

    if(negativity)
        return -degrees;
    else
        return degrees;
}

bool ExtractGeotags(std::string name, GPSCoords &geotags_known){
    Exiv2::LogMsg::setLevel(Exiv2::LogMsg::error);

    try{
        Exiv2::Image::AutoPtr image = Exiv2::ImageFactory::open(name);
        assert(image.get() != 0);
        image->readMetadata();

        Exiv2::ExifData &exifData = image->exifData();
        if (exifData.empty()) {
            std::string error(name);
            error += ": No Exif data found in the file";
            throw Exiv2::Error(1, error);

            return false;
        }

        Exiv2::ExifKey key_lat = Exiv2::ExifKey("Exif.GPSInfo.GPSLatitude");
        Exiv2::ExifKey key_lat_ref = Exiv2::ExifKey("Exif.GPSInfo.GPSLatitudeRef");
        Exiv2::ExifKey key_lon = Exiv2::ExifKey("Exif.GPSInfo.GPSLongitude");
        Exiv2::ExifKey key_lon_ref = Exiv2::ExifKey("Exif.GPSInfo.GPSLongitudeRef");
        Exiv2::ExifKey key_alt = Exiv2::ExifKey("Exif.GPSInfo.GPSAltitude");


        Exiv2::ExifData::iterator pos_lat = exifData.findKey(key_lat);
        if(pos_lat == exifData.end())
            return false;

        Exiv2::ExifData::iterator pos_lat_ref = exifData.findKey(key_lat_ref);
        if(pos_lat_ref == exifData.end())
            return false;

        Exiv2::ExifData::iterator pos_lon = exifData.findKey(key_lon);
        if(pos_lon == exifData.end())
            return false;

        Exiv2::ExifData::iterator pos_lon_ref = exifData.findKey(key_lon_ref);
        if(pos_lon_ref == exifData.end())
            return false;

        Exiv2::ExifData::iterator pos_alt = exifData.findKey(key_alt);
        if(pos_alt == exifData.end())
            return false;


        bool negativity;

        std::string lat_side = pos_lat_ref->value().toString();
        if(lat_side.compare("S") == 0)
            negativity = true;
        else
            negativity = false;

        geotags_known.latitude = convertDMStoDD(pos_lat->value().toFloat(0),pos_lat->value().toFloat(1),pos_lat->value().toFloat(2), negativity);

        std::string lon_side = pos_lon_ref->value().toString();
        if(lon_side.compare("W") == 0)
            negativity = true;
        else
            negativity = false;

        geotags_known.longitude = convertDMStoDD(pos_lon->value().toFloat(0), pos_lon->value().toFloat(1), pos_lon->value().toFloat(2), negativity);

        geotags_known.altitude = pos_alt->value().toFloat();

#ifdef GEO_DEBUG
        std::cout << "[" << name << "]" << std::endl;
        std::cout << "      Latitude:  " << geotags_known.latitude << std::endl;
        std::cout << "      Longitude: " << geotags_known.longitude << std::endl;
        std::cout << "      Altitude:  " << geotags_known.altitude << std::endl;
#endif
    }catch (Exiv2::AnyError& e) {
#ifdef GEO_DEBUG
        std::cerr << "[Exiv2] Couldn't extract geotags from " << name << ". --> " << e << "'\n";
#endif
        return false;

    }

}


bool SaveToOutput(std::string filename,std::string imgs_path, std::vector<std::string> posed_names, std::vector<GPSCoords > gps_coords, std::vector<std::string> posed_locations, std::vector<std::string> posed_directions){

    std::ofstream os(filename.c_str());
    if(!os.is_open())return false;

    int n_imgs = int(posed_names.size());

    os << n_imgs << std::endl;

    for(int i = 0; i < n_imgs; i++){
        GPSCoords gps_given;

        bool existent_gps = ExtractGeotags(imgs_path + posed_names[i],gps_given);


        os << "[" << posed_names[i] << "]" << std::endl;

        os << "   Location: " << posed_locations[i] << std::endl;
        os << "   Direction: " << posed_directions[i] << std::endl;

        if(gps_coords[i].latitude == -200){
            os << "   Lat: " << "-" << "   " << "-" << " " << std::endl;
            os << "   Lon: " << "-" << "   " << "-" << " " << std::endl;
            os << "   Alt: " << "-" << "   " << "-" << " " << std::endl;
            os << "Error: " << "-" << "   " << std::endl << std::endl;

        }else if(existent_gps){

            double absolute_error = CompareGroundTruth(gps_coords[i],gps_given);

            os << "   Lat: " << std::setprecision(10) << gps_given.latitude << "   " << std::setprecision(10) << gps_coords[i].latitude << std::endl;
            os << "   Lon: " << std::setprecision(10) << gps_given.longitude << "   " << std::setprecision(10) << gps_coords[i].longitude << std::endl;
            os << "   Alt: " << std::setprecision(10) << gps_given.altitude << "   " << std::setprecision(10) << gps_coords[i].altitude << std::endl;
            os << "Error: " << std::setprecision(2) << absolute_error << " metters" << std::endl << std::endl;
        }else{
            os << "   Lat: " << "-" << "   " << std::setprecision(10) << gps_coords[i].latitude << std::endl;
            os << "   Lon: " << "-" << "   " << std::setprecision(10) << gps_coords[i].longitude << std::endl;
            os << "   Alt: " << "-" << "   " << std::setprecision(10) << gps_coords[i].altitude << std::endl;
            os << "Error: " << "-" << std::endl << std::endl;
        }
    }

    os.close();

    return true;
}


bool ReadCoordsFile(std::string filename, std::map<std::string, GPSCoords> &coordinates){

    std::ifstream is (filename.c_str());
    if(!is.is_open())return false;

    int n_imgs;

    is >> n_imgs;

    for(int i = 0; i < n_imgs; i++){
        std::string imgname;
        GPSCoords ccs;

        is >> imgname >> ccs.latitude >> ccs.longitude >> ccs.altitude;

        coordinates.insert(std::make_pair(imgname, ccs));

    }

    is.close();

    return true;
}
