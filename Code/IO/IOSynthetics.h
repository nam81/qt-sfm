#pragma once

//#include <opencv2/core/core.hpp>
#include <opencv2/features2d/features2d.hpp>

//===================================================== Tmp Files   ===============================================
void SaveTMP(const char* sift_path,std::vector<cv::Point3f> cps, std::vector<cv::KeyPoint> kps, std::vector<float> dcs);

bool ReadTMP(const char* sift_path, std::vector<cv::Point3f> &cps, std::vector<cv::KeyPoint>& keys, std::vector<float>& descs);

//=========================================== 3D Documents ===========================================================
//3D document reader
bool Read3DDoc(const char* sift_path, std::vector<cv::KeyPoint>& keys, std::vector<float>& descs, /*cv::Matx34d &P, cv::Mat &K,*/ std::vector<cv::Point3f> &cps,  cv::Matx44d &G);

//3D document descriptor reader for vocabulary trees
bool Read3DDoc(const char* sift_path, std::vector<float> &scales, std::vector<unsigned char>& descs, int &num_keys);

//3D documents writer
void Save3DDoc(const char* sift_path, std::vector<cv::Point3f> cps, std::vector<cv::KeyPoint> kps, std::vector<float> dcs, cv::Matx44d G);
