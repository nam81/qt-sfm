#pragma once

#include "../Common.h"
#include "../../3rdparty/VocabTree2/lib/ann_1.1_char/include/ANN/ANN.h"


struct PrioriPtSeed{
    int idx_to_compressed;
    cv::Point3d p3d;
    //int n_cams;
    //std::vector<int> cameras_idx;
    int magnitude;
    std::vector<float> descriptor;

    PrioriPtSeed():
        idx_to_compressed(-1),
        p3d(cv::Point3d()),
        //n_cams(-1),
        //cameras_idx(std::vector<int>()),
        magnitude(-1),
        descriptor(std::vector<float>()){}


    PrioriPtSeed(int idx, cv::Point3d p, int viz_idx, std::vector<float> descs):
        idx_to_compressed(idx),
        p3d(p),
        //n_cams(n),
        //cameras_idx(cams),
        magnitude(viz_idx),
        descriptor(descs){}
};



bool ReadCloudList(std::string file_name, std::vector<std::string> &list_s, std::vector<std::string> &list_c);
void UpdateCloudList(std::string file_name, std::string clouds, std::string cloudc);

//=========================================== Priori Features ========================================================

void SavePrioriCloud(const char *file_name, std::string belonging_model, std::vector<ImageV*> img_info, std::vector<CloudPoint> pointcloud, std::map<int,int> compressed_points, cv::Matx44d gps_coords);


bool LoadPrioriCloud(const char* file_name,
                     std::vector<cv::Point3f> &seed_cloud_points,
                     std::vector<unsigned char> &seed_descriptors,
                     std::vector<std::pair<int,float > > &seed_visibility_indexes,
                     std::vector<std::map<int, int> > &seed_visibility_matrix,
                     std::vector<cv::Matx44d> &seed_gps_axis,
                     int &last_point_idx, int &last_camera_idx);

bool LoadPrioriCloud(const char* file_name,
                     std::string &belonging_model,
                     std::vector<cv::Point3f> &cloud_points,
                     ann_1_1_char::ANNpointArray &descriptors,
                     std::vector<std::pair<int, float> > &visibility_indexes,
                     std::vector<std::map<int, int> > &visibility_matrix,
                     cv::Matx44d &gps_axis);

bool LoadPrioriCloud(const char* file_name,
                     std::string &belonging_model,
                     std::vector<std::pair<int, float> > visibility_indexes_before,
                     std::vector<std::pair<int, float> > &visibility_indexes_after,
                     std::vector<std::vector<int> > &visibility_matrix,
                     std::vector<cv::Point3f> &cloud_points,
                     std::vector<float> &descriptors,
                     cv::Matx44d &gps_axis);

bool LoadPrioriCloud(const char* file_name,
                     std::string &belonging_model,
                     int start_idx,
                     std::vector<std::pair<int,float> > visibility_indexes_before,
                     std::vector<std::pair<int,float> > &visibility_indexes_after,
                     std::vector<std::map<int, int> > &visibility_matrix,
                     std::vector<cv::Point3f> &cloud_points,
                     std::vector<float> &descriptors,
                     cv::Matx44d &gps_axis);

void SavePrioriCloudB(const char *file_name,
                      std::vector<ImageV*> img_info,
                      std::vector<CloudPoint> pointcloud,
                      std::map<int,int> compressed_points,
                      cv::Matx44d gps_coords);

bool LoadPrioriCloudB(const char* file_name, std::map<int, int> &indexes_to_compressed,
                     std::vector<std::pair<int, float> > &visibility_indexes,
                     std::vector<std::vector<int> > &visibility_matrix,
                     std::vector<cv::Point3f> &cloud_points,
                     ann_1_1_char::ANNpointArray &descriptors,
                     cv::Matx44d &gps_axis);

bool LoadPrioriCloudB(const char* file_name,
                      std::map<int, int> indexes_to_compressed,
                      std::vector<std::pair<int, float> > visibility_indexes_before,
                      std::vector<std::pair<int, float> > &visibility_indexes_after,
                      std::vector<std::vector<int> > &visibility_matrix,
                      std::vector<cv::Point3f> &cloud_points,
                      std::vector<float> &descriptors,
                      cv::Matx44d &gps_axis);
