#include "Statistics.h"
#include "ExifReader.h"

#include <fstream>

void clockToTime(clock_t c, int& hour, int &min, int &sec, int &msec){
    double time = (double) (c) / CLOCKS_PER_SEC;

    hour = time / 3600.0;

    min = fmod(time / 60.0, 60.0);

    sec = fmod(time, 60.0);

    msec = (fmod(time, 60.0) - floor(fmod(time, 60.0))) * 1000.0;
}


void displayTime(std::ofstream &out, int &hour, int &min, int &sec, int &msec){

    if(hour > 0)
        out << hour << " hours ";

    if(min > 0)
        out << min << " mins ";

    if(sec > 0)
        out << sec << " secs ";

    out << msec << " ms" << std::endl;
}



void StatisticsSynth::insertInfo(StatSynth stats){
    statistics.push_back(stats);
}

void StatisticsPriori::insertInfo(StatPriori stats){
    statistics.push_back(stats);
}

bool StatisticsSynth::toFileStatistics(std::string imgs_path){

    std::vector<double> errors;
    std::vector<GPSCoords> ground_truth;

    std::ofstream os((imgs_path + "output_statistics.txt").c_str());
    if(!os.is_open())return false;

    int n_imgs = int(statistics.size());

    os << "----- Geolocalization Output " << std::endl;
    os << " Mode: ";

    os << "Synthetic Views";

    os << std::endl;

    os << " SiftGPU: ";

    if(siftgpu == 1)
        os << "CUDA";
    else
        os << "OpenGL";

    os << std::endl << std::endl;

    int n_localized = 0;
    clock_t total_time;
    clock_t mean_per_photograph;

#ifdef GEO_DEBUG
    clock_t mean_tk, mean_ext, mean_voc, mean_match, mean_pnp, mean_gps;
#endif

    bool existent_gps;
    GPSCoords gps_given;
    double mean_error = 0.0;
    double low_b_err = 1000000.0, high_b_err = -1000000.0;

    if(statistics.size() != 0){
        total_time = statistics[0].getTime();
#ifdef GEO_DEBUG
        mean_tk = statistics[0].getTK();
        mean_ext = statistics[0].getTExt();
        mean_voc = statistics[0].getTVoc();
        mean_match = statistics[0].getTMatch();
        mean_pnp = statistics[0].getTPnP();
        mean_gps = statistics[0].getTGPS();
#endif


        if(ExtractGeotags(imgs_path + statistics[0].getName(),gps_given) && statistics[0].getSuccess()){

            double er = CompareGroundTruth(statistics[0].getGPS(), gps_given);
            mean_error += er;
            errors.push_back(er);
            ground_truth.push_back(gps_given);

            //if(low_b_err > er)
                low_b_err = er;
            //if(high_b_err < er)
                high_b_err = er;

        }else{
            errors.push_back(-1.0);
            ground_truth.push_back(gps_given);
        }


        if(statistics[0].getSuccess()){
            n_localized++;
        }
    }else{
        return false;
    }

    for(int i = 1; i < n_imgs; i++){
        if(ExtractGeotags(imgs_path + statistics[i].getName(),gps_given) && statistics[i].getSuccess()){
            double er = CompareGroundTruth(statistics[i].getGPS(), gps_given);
            mean_error += er;

            errors.push_back(er);
            ground_truth.push_back(gps_given);

            if(low_b_err > er)
                low_b_err = er;
            if(high_b_err < er)
                high_b_err = er;
        }else{
            errors.push_back(-1.0);
            ground_truth.push_back(gps_given);
        }

        if(statistics[i].getSuccess()){
            n_localized++;
        }
        total_time += statistics[i].getTime();
 #ifdef GEO_DEBUG
        mean_tk += statistics[i].getTK();
        mean_ext += statistics[i].getTExt();
        mean_voc += statistics[i].getTVoc();
        mean_match += statistics[i].getTMatch();
        mean_pnp += statistics[i].getTPnP();
        mean_gps += statistics[i].getTGPS();
#endif


    }

    mean_error = mean_error / n_localized;

    mean_per_photograph = total_time / n_imgs;
#ifdef GEO_DEBUG
    mean_tk = mean_tk / n_imgs;
    mean_ext = mean_ext / n_imgs;
    mean_voc = mean_voc / n_imgs;
    mean_match = mean_match / n_imgs;
    mean_pnp = mean_pnp/ n_imgs;
    mean_gps = mean_gps / n_imgs;
#endif

    os << "----- General Info" << std::endl;
    os << " Total Photographs: " << n_imgs << std::endl;
    os << " Total Geocoded: " << n_localized << std::endl;

    double rate = (double) n_localized / (double) n_imgs * 100.0;
    os << " Geocoded Rate: " << std::setprecision(3) << rate << "%" <<  std::endl;

    os << " Mean GeoLoc Error: " << mean_error << " meters" << std::endl;
    os << " GeoLoc Error Ranges: " << low_b_err << " to " << high_b_err << " meters" << std::endl;

    int hours = 0, mins = 0, secs = 0, msecs = 0;

    clockToTime(total_time, hours,mins,secs, msecs);
    os << " Total Time: ";
    displayTime(os, hours,mins,secs, msecs);

    clockToTime(mean_per_photograph, hours,mins,secs, msecs);
    os << " Mean Time Per Photo: ";
    displayTime(os, hours,mins,secs,msecs);


#ifdef GEO_DEBUG
    os << " [Mean Time Detailed]" << std::endl;
    os << "    K Matrix: ";
    clockToTime(mean_tk, hours,mins,secs, msecs);
    displayTime(os, hours,mins,secs,msecs);

    os << "    Feature Extraction: ";
    clockToTime(mean_ext, hours,mins,secs,msecs);
    displayTime(os,hours,mins,secs,msecs);

    os << "    Top Vocab Docs: ";
    clockToTime(mean_voc, hours,mins,secs,msecs);
    displayTime(os,hours,mins,secs,msecs);

    os << "    Feature Matching: ";
    clockToTime(mean_match, hours,mins,secs,msecs);
    displayTime(os,hours,mins,secs,msecs);

    os << "    PnP Ransac: ";
    clockToTime(mean_pnp, hours,mins,secs,msecs);
    displayTime(os,hours,mins,secs,msecs);

    os << "    GPS Convertion: ";
    clockToTime(mean_gps, hours,mins,secs,msecs);
    displayTime(os,hours,mins,secs,msecs);

    os << std::endl;

#endif

    os << std::endl;

    os << "----- Individual Info" << std::endl;

    for(int i = 0; i < n_imgs; i++){
        //GPSCoords gps_given;
        std::string photo_name = statistics[i].getName();

        os << "[" << photo_name << "]" << std::endl;

        os << "   [Time Info]" << std::endl;

        clockToTime(statistics[i].getTime(), hours,mins,secs, msecs);
        os << "      Time: ";
        displayTime(os, hours,mins,secs,msecs);

#ifdef GEO_DEBUG
        os << "      [Detailed Time]" << std::endl;
        os << "         K Matrix: ";
        clockToTime(statistics[i].getTK(), hours,mins,secs,msecs);
        displayTime(os,hours,mins,secs,msecs);

        os << "         Feature Extraction: ";
        clockToTime(statistics[i].getTExt(), hours,mins,secs,msecs);
        displayTime(os,hours,mins,secs,msecs);

        os << "         Top Vocab Docs: ";
        clockToTime(statistics[i].getTVoc(), hours,mins,secs,msecs);
        displayTime(os,hours,mins,secs,msecs);

        os << "         Feature Matching: ";
        clockToTime(statistics[i].getTMatch(), hours,mins,secs,msecs);
        displayTime(os,hours,mins,secs,msecs);

        os << "         PnP Ransac: ";
        clockToTime(statistics[i].getTPnP(), hours,mins,secs,msecs);
        displayTime(os,hours,mins,secs,msecs);

        os << "         GPS Convertion: ";
        clockToTime(statistics[i].getTGPS(), hours,mins,secs,msecs);
        displayTime(os,hours,mins,secs,msecs);
#endif

        if(statistics[i].getSuccess()){

            os << "      Posed on: " << statistics[i].getDoc() << " attempt" << std::endl;

            bool existent_gps = true;
            if(errors[i] < 0)
                existent_gps = false;

//            = ExtractGeotags(imgs_path + photo_name,gps_given);

            os << "   [GPS Info]" << std::endl;
            os << "      Location: " << statistics[i].getLocation() << std::endl;
            os << "      Direction: " << statistics[i].getDirection() << std::endl;

            GPSCoords gps_coords = statistics[i].getGPS();

            if(gps_coords.latitude == -200){
                os << "      Lat: " << "-" << "   " << "-" << " " << std::endl;
                os << "      Lon: " << "-" << "   " << "-" << " " << std::endl;
                os << "      Alt: " << "-" << "   " << "-" << " " << std::endl;
                os << "   Error: " << "-" << "   " << std::endl << std::endl;

            }else if(existent_gps){

                //double absolute_error = CompareGroundTruth(gps_coords,gps_given);

                os << "      Lat: " << std::setprecision(10) << ground_truth[i].latitude << "   " << std::setprecision(10) << gps_coords.latitude << std::endl;
                os << "      Lon: " << std::setprecision(10) << ground_truth[i].longitude << "   " << std::setprecision(10) << gps_coords.longitude << std::endl;
                os << "      Alt: " << std::setprecision(10) << ground_truth[i].altitude << "   " << std::setprecision(10) << gps_coords.altitude << std::endl;
                os << "   Error: " << std::setprecision(2) << errors[i] << " meters" << std::endl << std::endl;
            }else{
                os << "      Lat: " << "-" << "   " << std::setprecision(10) << gps_coords.latitude << std::endl;
                os << "      Lon: " << "-" << "   " << std::setprecision(10) << gps_coords.longitude << std::endl;
                os << "      Alt: " << "-" << "   " << std::setprecision(10) << gps_coords.altitude << std::endl;
                os << "   Error: " << "-" << std::endl << std::endl;
            }
        }else{
           os << "      Not geolocalized..." << std::endl <<std::endl;
        }
    }

    os.close();

    return true;
}

bool StatisticsSynth::toFileViz(std::string output_path){

    std::map<std::string, std::vector<int> > posed_list;

    for(int i = 0; i < statistics.size(); i++){
        if(statistics[i].getSuccess())
            posed_list[statistics[i].getModelName()].push_back(i);
    }

    int n_models = posed_list.size();
    if(n_models <=0) return false;
    std::ofstream out((output_path + "output_viz.txt").c_str());
    out.flags(std::ios::fixed);

    out << n_models << std::endl;

    for(std::map<std::string, std::vector<int> >::iterator model_iter = posed_list.begin(); model_iter != posed_list.end(); model_iter++){

        out << model_iter->first << " " << model_iter->second.size() << std::endl;
        for(int i = 0; i < model_iter->second.size(); i++){
            cv::Matx34d P = statistics[model_iter->second[i]].getPmat();
            out << statistics[model_iter->second[i]].getName() << " " << P(0,0) << " " << P(0,1) << " " << P(0,2) << " " << P(0,3)
                                                               << " " << P(1,0) << " " << P(1,1) << " " << P(1,2) << " " << P(1,3)
                                                               << " " << P(2,0) << " " << P(2,1) << " " << P(2,2) << " " << P(2,3)
                                                               << std::endl;
        }
        out<<std::endl;
    }

    out.close();


}


bool StatisticsPriori::toFileStatistics(std::string imgs_path){

    std::vector<double> errors;
    std::vector<GPSCoords> ground_truth;

    std::ofstream os((imgs_path + "output_statistics.txt").c_str());
    if(!os.is_open())return false;

    int n_imgs = int(statistics.size());

    os << "----- Geolocalization Output " << std::endl;
    os << " Mode: ";

    os << "Prioritized Features";

    os << std::endl;

    os << " SiftGPU: ";

    if(siftgpu == 1)
        os << "CUDA";
    else
        os << "OpenGL";

    os << std::endl << std::endl;

    int n_localized = 0;
    clock_t total_time; // = clock_t();
    clock_t mean_per_photograph; // = clock_t();

#ifdef GEO_DEBUG
    clock_t mean_tk, mean_ext, mean_ann, mean_cooc, mean_p3p, mean_pba, mean_gps;
#endif

    bool existent_gps;
    GPSCoords gps_given;
    double mean_error = 0.0;
    double low_b_err = 1000000.0, high_b_err = -1000000.0;

    if(statistics.size() != 0){
        total_time = statistics[0].getTime();
        //mean_per_photograph = statistics[0].getTime();

#ifdef GEO_DEBUG
       mean_tk = statistics[0].getTK();
       mean_ext = statistics[0].getTExt();
       mean_ann = statistics[0].getTANN();
       mean_cooc = statistics[0].getTCooc();
       mean_p3p = statistics[0].getTP3P();
       mean_pba = statistics[0].getTPBA();
       mean_gps = statistics[0].getTGPS();
#endif

       if(ExtractGeotags(imgs_path + statistics[0].getName(),gps_given) && statistics[0].getSuccess()){

           double er = CompareGroundTruth(statistics[0].getGPS(), gps_given);
           mean_error += er;
           errors.push_back(er);
           ground_truth.push_back(gps_given);

           //if(low_b_err > er)
               low_b_err = er;
           //if(high_b_err < er)
               high_b_err = er;

       }else{
           errors.push_back(-1.0);
           ground_truth.push_back(gps_given);
       }


        if(statistics[0].getSuccess()){
            n_localized++;
        }
    }else{
        return false;
    }

    for(int i = 1; i < n_imgs; i++){
        if(ExtractGeotags(imgs_path + statistics[i].getName(),gps_given) && statistics[i].getSuccess()){
            double er = CompareGroundTruth(statistics[i].getGPS(), gps_given);
            mean_error += er;

            errors.push_back(er);
            ground_truth.push_back(gps_given);

            if(low_b_err > er)
                low_b_err = er;
            if(high_b_err < er)
                high_b_err = er;
        }else{
            errors.push_back(-1.0);
            ground_truth.push_back(gps_given);
        }

        if(statistics[i].getSuccess()){
            n_localized++;
        }

#ifdef GEO_DEBUG
       mean_tk += statistics[i].getTK();
       mean_ext += statistics[i].getTExt();
       mean_ann += statistics[i].getTANN();
       mean_cooc += statistics[i].getTCooc();
       mean_p3p += statistics[i].getTP3P();
       mean_pba += statistics[i].getTPBA();
       mean_gps += statistics[i].getTGPS();
#endif

        total_time += statistics[i].getTime();
    }

    mean_error = mean_error / n_localized;

    mean_per_photograph = total_time / n_imgs;
#ifdef GEO_DEBUG
    mean_tk = mean_tk / n_imgs;
    mean_ext = mean_ext / n_imgs;
    mean_ann = mean_ann / n_imgs;
    mean_cooc = mean_cooc / n_imgs;
    mean_p3p = mean_p3p / n_imgs;
    mean_pba = mean_pba / n_imgs;
    mean_gps = mean_gps / n_imgs;
#endif
    os << "----- General Info" << std::endl;
    os << " Total Photographs: " << n_imgs << std::endl;
    os << " Total Geocoded: " << n_localized << std::endl;

    double rate = (double) n_localized / (double) n_imgs * 100.0;
    os << " Geocoded Rate: " << std::setprecision(3) << rate << "%" <<  std::endl;

    os << " Mean GeoLoc Error: " << mean_error << " meters" << std::endl;
    os << " GeoLoc Error Ranges: " << low_b_err << " to " << high_b_err << " meters" << std::endl;

    int hours = 0, mins = 0, secs = 0, msecs = 0;

    clockToTime(total_time, hours,mins,secs, msecs);
    os << " Total Time: ";
    displayTime(os, hours,mins,secs, msecs);

    clockToTime(mean_per_photograph, hours,mins,secs, msecs);
    os << " Mean Time Per Photo: ";
    displayTime(os, hours,mins,secs,msecs);

#ifdef GEO_DEBUG
    os << " [Mean Time Detailed]" << std::endl;
    os << "    K Matrix: ";
    clockToTime(mean_tk, hours,mins,secs, msecs);
    displayTime(os, hours,mins,secs,msecs);

    os << "    Feature Extraction: ";
    clockToTime(mean_ext, hours,mins,secs,msecs);
    displayTime(os,hours,mins,secs,msecs);

    os << "    ANN: ";
    clockToTime(mean_ann, hours,mins,secs,msecs);
    displayTime(os,hours,mins,secs,msecs);

    os << "    Coocurrence Sampling: ";
    clockToTime(mean_cooc, hours,mins,secs,msecs);
    displayTime(os,hours,mins,secs,msecs);

    os << "    P3P Ransac: ";
    clockToTime(mean_p3p, hours,mins,secs,msecs);
    displayTime(os,hours,mins,secs,msecs);


    os << "    P. Bundle Adjust: ";
    clockToTime(mean_pba, hours,mins,secs,msecs);
    displayTime(os,hours,mins,secs,msecs);


    os << "    GPS Convertion: ";
    clockToTime(mean_gps, hours,mins,secs,msecs);
    displayTime(os,hours,mins,secs,msecs);

#endif

    os << std::endl;

    os << "----- Individual Info" << std::endl;

    for(int i = 0; i < n_imgs; i++){
        GPSCoords gps_given;
        std::string photo_name = statistics[i].getName();

        os << "[" << photo_name << "]" << std::endl;

        os << "   [Time Info]" << std::endl;

        clockToTime(statistics[i].getTime(), hours,mins,secs, msecs);
        os << "      Time: ";
        displayTime(os, hours,mins,secs,msecs);

#ifdef GEO_DEBUG
        os << "      [Detailed Time]" << std::endl;
        os << "         K Matrix: ";
        clockToTime(statistics[i].getTK(), hours,mins,secs,msecs);
        displayTime(os,hours,mins,secs,msecs);

        os << "         Feature Extraction: ";
        clockToTime(statistics[i].getTExt(), hours,mins,secs,msecs);
        displayTime(os,hours,mins,secs,msecs);

        os << "         ANN: ";
        clockToTime(statistics[i].getTANN(), hours,mins,secs,msecs);
        displayTime(os,hours,mins,secs,msecs);

        os << "         Coocurrence Sampling: ";
        clockToTime(statistics[i].getTCooc(), hours,mins,secs,msecs);
        displayTime(os,hours,mins,secs,msecs);

        os << "         P3P: ";
        clockToTime(statistics[i].getTP3P(), hours,mins,secs,msecs);
        displayTime(os,hours,mins,secs,msecs);

        os << "         P. Bundle Adjust: ";
        clockToTime(statistics[i].getTPBA(), hours,mins,secs,msecs);
        displayTime(os,hours,mins,secs,msecs);

        os << "         GPS Convertion: ";
        clockToTime(statistics[i].getTGPS(), hours,mins,secs,msecs);
        displayTime(os,hours,mins,secs,msecs);
#endif


        if(statistics[i].getSuccess()){
            //bool existent_gps = ExtractGeotags(imgs_path + photo_name,gps_given);

            bool existent_gps = true;
            if(errors[i] < 0)
                existent_gps = false;

            os << "   [GPS Info]" << std::endl;
            os << "      Location: " << statistics[i].getLocation() << std::endl;
            os << "      Direction: " << statistics[i].getDirection() << std::endl;

            GPSCoords gps_coords = statistics[i].getGPS();

            if(gps_coords.latitude == -200){
                os << "      Lat: " << "-" << "   " << "-" << " " << std::endl;
                os << "      Lon: " << "-" << "   " << "-" << " " << std::endl;
                os << "      Alt: " << "-" << "   " << "-" << " " << std::endl;
                os << "   Error: " << "-" << "   " << std::endl << std::endl;

            }else if(existent_gps){

                //double absolute_error = CompareGroundTruth(gps_coords,gps_given);

                os << "      Lat: " << std::setprecision(10) << ground_truth[i].latitude << "   " << std::setprecision(10) << gps_coords.latitude << std::endl;
                os << "      Lon: " << std::setprecision(10) << ground_truth[i].longitude << "   " << std::setprecision(10) << gps_coords.longitude << std::endl;
                os << "      Alt: " << std::setprecision(10) << ground_truth[i].altitude << "   " << std::setprecision(10) << gps_coords.altitude << std::endl;
                os << "   Error: " << std::setprecision(2) << errors[i] << " meters" << std::endl << std::endl;
            }else{
                os << "      Lat: " << "-" << "   " << std::setprecision(10) << gps_coords.latitude << std::endl;
                os << "      Lon: " << "-" << "   " << std::setprecision(10) << gps_coords.longitude << std::endl;
                os << "      Alt: " << "-" << "   " << std::setprecision(10) << gps_coords.altitude << std::endl;
                os << "   Error: " << "-" << std::endl << std::endl;
            }
        }else{
           os << "      Not geolocalized..." << std::endl <<std::endl;
        }
    }

    os.close();

    return true;
}

bool StatisticsPriori::toFileViz(std::string output_path){

    std::map<std::string, std::vector<int> > posed_list;

    for(int i = 0; i < statistics.size(); i++){
        if(statistics[i].getSuccess())
            posed_list[statistics[i].getModelName()].push_back(i);
    }

    int n_models = posed_list.size();
    if(n_models <=0) return false;
    std::ofstream out((output_path + "output_viz.txt").c_str());
    out.flags(std::ios::fixed);

    out << n_models << std::endl;

    for(std::map<std::string, std::vector<int> >::iterator model_iter = posed_list.begin(); model_iter != posed_list.end(); model_iter++){

        out << model_iter->first << " " << model_iter->second.size() << std::endl;
        for(int i = 0; i < model_iter->second.size(); i++){
            cv::Matx34d P = statistics[model_iter->second[i]].getPmat();
            out << statistics[model_iter->second[i]].getName() << " " << P(0,0) << " " << P(0,1) << " " << P(0,2) << " " << P(0,3)
                                                               << " " << P(1,0) << " " << P(1,1) << " " << P(1,2) << " " << P(1,3)
                                                               << " " << P(2,0) << " " << P(2,1) << " " << P(2,2) << " " << P(2,3)
                                                               << std::endl;
        }
        out<<std::endl;
    }

    out.close();


}
