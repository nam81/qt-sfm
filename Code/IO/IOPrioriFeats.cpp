#include "IOPrioriFeats.h"
//#include "../../Common.h"
#include <fstream>


bool ReadCloudList(std::string file_name, std::vector<std::string> &list_s, std::vector<std::string> &list_c){
    std::ifstream is (file_name.c_str());
    if(!is.is_open())return false;

    int n_clouds;

    is >> n_clouds;

    if(n_clouds < 0){
        std::cerr << "Unspecified number of clouds." << std::endl;
    }

    for(int i = 0; i < n_clouds; i++){
        std::string cloud_s, cloud_c;

        is >> cloud_s;
        is >> cloud_c;

        list_s.push_back(cloud_s);
        list_c.push_back(cloud_c);
    }

    is.close();

    //std::sort(img_names.begin(), img_names.end());

    return true;
}

void UpdateCloudList(std::string file_name, std::string clouds, std::string cloudc){


    std::vector<std::string> list_s;
    std::vector<std::string> list_c;

    ReadCloudList(file_name,list_s, list_c);

    std::ofstream out(file_name.c_str());
    out << (int) (list_s.size()) + 1 << "\n";

    for(int i = 0; i < list_s.size(); i++){
        out << list_s[i] << std::endl;
        out << list_c[i] << std::endl;
    }

    out << clouds << std::endl;
    out << cloudc << std::endl;

    out.close();

    return ;


}


//===================================================== Priori Features =================================================================================

void SavePrioriCloud(const char *file_name,
                     std::string belonging_model,
                     std::vector<ImageV*> img_info,
                     std::vector<CloudPoint> pointcloud,
                     std::map<int,int> compressed_points,
                     cv::Matx44d gps_axis){

    std::cout << "   Saving model to " << file_name << "...\n";
    std::ofstream out(file_name);

    out << "Priori_Seed\n";

    int imgs_size = int(img_info.size());
    int n_points = int(compressed_points.size());

    out << belonging_model << std::endl;

    out <<  imgs_size << ' ' << n_points << '\n';
    out << gps_axis(0,0) << ' ' << gps_axis(0,1) << ' ' << gps_axis(0,2) << ' ' << gps_axis(0,3) << std::endl;
    out << gps_axis(1,0) << ' ' << gps_axis(1,1) << ' ' << gps_axis(1,2) << ' ' << gps_axis(1,3) <<std::endl;
    out << gps_axis(2,0) << ' ' << gps_axis(2,1) << ' ' << gps_axis(2,2) << ' ' << gps_axis(2,3) <<std::endl;
    out << gps_axis(3,0) << ' ' << gps_axis(3,1) << ' ' << gps_axis(3,2) << ' ' << gps_axis(3,3) <<std::endl;

    for(std::map<int,int>::iterator iter = compressed_points.begin(); iter != compressed_points.end(); iter++){
        CloudPoint cp = pointcloud[iter->first];
        out << iter->first << ' ' << cp.pt.x << ' ' << cp.pt.y << ' ' << cp.pt.z << ' ';

        int cams_size = cp.imgpt_for_img.size();

        out << cams_size << ' ';

        //float highest_scale = 0.0;
        //int pt_pos, cam_pos;

        std::vector<float> descs_mean(128, 0.0);

        for(std::map<int,int>::iterator it = cp.imgpt_for_img.begin(); it != cp.imgpt_for_img.end(); it++){
            out << it->first << ' ';

            //ImageV* imgv = img_info[it->first];
            /*if(highest_scale < imgv->getPt(it->second).size){
                highest_scale = imgv->getPt(it->second).size;
                cam_pos = it->first;
                pt_pos = it->second;
            }*/

            for(int d = 0; d < 128; d++){
                descs_mean[d] += img_info[it->first]->getDes(it->second * 128 + d);
            }
        }

        //cv::Point2f pt = cv::Point2f(0,0);//img_info[cam_pos]->getPt(pt_pos).pt;

        //out << pt.x << ' ' << pt.y << '\n';

        for(int i = 0; i < 128; i++){
            float d = descs_mean[i] / (float) cams_size;
            out << static_cast<unsigned int>(floor(d * 512.0f + 0.5f)) << ' ';
        }


        out << '\n';
    }

    out.close();

    return;

}


bool LoadPrioriCloud(const char* file_name,
                     std::vector<cv::Point3f> &seed_cloud_points,
                     std::vector<unsigned char> &seed_descriptors,
                     std::vector<std::pair<int,float > > &seed_visibility_indexes,
                     std::vector<std::map<int,int > > &seed_visibility_matrix,
                     std::vector<cv::Matx44d> &seed_gps_axis,
                     int &last_point_idx,
                     int &last_camera_idx){


    if(file_name == NULL)return false;
    std::ifstream in(file_name);
#ifdef GEO_DEBUG
    std::cout << "Loading cameras/points: " << file_name <<"\n" ;
#endif
    if(!in.is_open()) return false;
    std::string belonging_model;
    in >> belonging_model;

    std::string header;
    in >> header; //file header

    int npoints = 0, ncams = 0;

    //////////////////////////////////////
    in >> ncams;
    in >> npoints; if(npoints <= 0) return false;
    cv::Matx44d gps_axis;
    in >> gps_axis(0,0) >> gps_axis(0,1) >> gps_axis(0,2) >> gps_axis(0,3);
    in >> gps_axis(1,0) >> gps_axis(1,1) >> gps_axis(1,2) >> gps_axis(1,3);
    in >> gps_axis(2,0) >> gps_axis(2,1) >> gps_axis(2,2) >> gps_axis(2,3);
    in >> gps_axis(3,0) >> gps_axis(3,1) >> gps_axis(3,2) >> gps_axis(3,3);

    seed_gps_axis.push_back(gps_axis);

    //keys = std::vector<cv::KeyPoint>(npoints);
    //descriptors = ann_1_1_char::annAllocPts(npoints, 128);
    //seed_visibility_matrix.resize(npoints , std::vector<int>( ncams , 0 ) );
    //visibility_indexes = std::vector<std::pair<int,int> >(npoint,0);
    //cloud_points = std::vector<cv::Point3f>(npoints);

    //read image projections and 3D points.
    //point_data.resize(npoint);
    int i;
    for(i = 0; i < npoints; ++i)
    {

        cv::Point3f p3d;
        int id;

        //float pt[3];
        int cc[3];//, npj;
        in  >> id >> p3d.x >> p3d.y >> p3d.z;

        //indexes_to_compressed.insert(std::make_pair(i,id));


        //cloud_points[i] = p3d;
        seed_cloud_points[last_point_idx + i] = p3d;

        int n_cameras = -1;
        //Total cameras associated to this point
        in >> n_cameras;

        //visibility_indexes.push_back(std::make_pair(id, 0.0));
        seed_visibility_indexes[last_point_idx + i]  = std::make_pair(id, 0.0);
        //cv::Point2f p2d;

        //seed_visibility_matrix.push_back(std::map<int,int>());
        for(int j = 0; j < n_cameras; j++){

            int cam_id;

            in >> cam_id;

            seed_visibility_matrix[last_point_idx + i][last_camera_idx + cam_id] = cam_id;
            seed_visibility_indexes[last_point_idx + i].second = seed_visibility_indexes[last_point_idx + i].second + 1.0;
        }

        //in >> F.x >> p2d.y;

        //keys[i] = cv::KeyPoint(p2d,0);

        for(int d = 0; d < 128; d++)
        {
            unsigned int udesc;
            in >> udesc;
            float f = static_cast<float>(udesc) / 512.0;
            seed_descriptors[(last_point_idx + i) * 128 + d] = static_cast<unsigned char>(floor(f * 512.0 + 0.5));// / 512.0;
        }

    }
    last_point_idx+= i;

    last_camera_idx +=ncams;
    ///////////////////////////////////////////////////////////////////////////////
    std::cout << seed_cloud_points.size() << " 3D points;\n " << last_camera_idx << " cameras\n";

    return true;




}





bool LoadPrioriCloud(const char* file_name,
                     std::string &belonging_model,
                     std::vector<cv::Point3f> &cloud_points,
                     ann_1_1_char::ANNpointArray &descriptors,
                     std::vector<std::pair<int,float> > &visibility_indexes,
                     std::vector<std::vector<int> > &visibility_matrix,
                     //std::vector<cv::KeyPoint> &keys,
                     cv::Matx44d &gps_axis){

    if(file_name == NULL)return false;
    std::ifstream in(file_name);
#ifdef GEO_DEBUG
    std::cout << "Loading cameras/points: " << file_name <<"\n" ;
#endif
    if(!in.is_open()) return false;

    in >> belonging_model;

    std::string header;
    in >> header; //file header

    int npoints = 0, ncams = 0;

    //////////////////////////////////////
    in >> ncams;
    in >> npoints; if(npoints <= 0) return false;
    in >> gps_axis(0,0) >> gps_axis(0,1) >> gps_axis(0,2) >> gps_axis(0,3);
    in >> gps_axis(1,0) >> gps_axis(1,1) >> gps_axis(1,2) >> gps_axis(1,3);
    in >> gps_axis(2,0) >> gps_axis(2,1) >> gps_axis(2,2) >> gps_axis(2,3);
    in >> gps_axis(3,0) >> gps_axis(3,1) >> gps_axis(3,2) >> gps_axis(3,3);

    //keys = std::vector<cv::KeyPoint>(npoints);
    descriptors = ann_1_1_char::annAllocPts(npoints, 128);
    visibility_matrix.resize(npoints , std::vector<int>( ncams , 0 ) );
    //visibility_indexes = std::vector<std::pair<int,int> >(npoint,0);
    cloud_points = std::vector<cv::Point3f>(npoints);

    //read image projections and 3D points.
    //point_data.resize(npoint);
    for(int i = 0; i < npoints; ++i)
    {

        cv::Point3f p3d;
        int id;

        //float pt[3];
        int cc[3];//, npj;
        in  >> id >> p3d.x >> p3d.y >> p3d.z;

        //indexes_to_compressed.insert(std::make_pair(i,id));


        cloud_points[i] = p3d;

        int n_cameras = -1;
        //Total cameras associated to this point
        in >> n_cameras;

        visibility_indexes.push_back(std::make_pair(id, 0.0));

        //cv::Point2f p2d;


        for(int j = 0; j < n_cameras; j++){

            int cam_id;

            in >> cam_id;

            visibility_matrix[i][cam_id] = 1;
            visibility_indexes[i].second = visibility_indexes[i].second +1.0;
        }

        //in >> F.x >> p2d.y;

        //keys[i] = cv::KeyPoint(p2d,0);

        for(int d = 0; d < 128; d++)
        {
            unsigned int udesc;
            in >> udesc;
            float f = static_cast<float>(udesc) / 512.0;
            descriptors[i][d] = static_cast<unsigned char>(floor(f * 512.0 + 0.5));// / 512.0;
        }


    }
    ///////////////////////////////////////////////////////////////////////////////
    std::cout << cloud_points.size() << " 3D points;\n " << cloud_points.size() << " projections\n";

    return true;




}

bool LoadPrioriCloud(const char* file_name,
                     std::string &belonging_model,
                     //std::map<int,cv::DMatch> &matches,
                     //std::map<int,int> indexes_to_compressed,
                     std::vector<std::pair<int,float> > visibility_indexes_before,
                     std::vector<std::pair<int,float> > &visibility_indexes_after,
                     std::vector<std::vector<int> > &visibility_matrix,
                     std::vector<cv::Point3f> &cloud_points,
                     //std::vector<cv::KeyPoint> &keys,
                     std::vector<float> &descriptors,
                     cv::Matx44d &gps_axis){

    if(file_name == NULL)return false;
    std::ifstream in(file_name);
#ifdef GEO_DEBUG
    std::cout << "Loading cameras/points: " << file_name <<"\n" ;
#endif
    if(!in.is_open()) return false;

    in >> belonging_model;

    std::string header;
    in >> header; //file header


    int npoints = 0, ncams = 0;


    //////////////////////////////////////
    in >> ncams;
    in >> npoints; if(npoints <= 0) return false;
    in >> gps_axis(0,0) >> gps_axis(0,1) >> gps_axis(0,2) >> gps_axis(0,3);
    in >> gps_axis(1,0) >> gps_axis(1,1) >> gps_axis(1,2) >> gps_axis(1,3);
    in >> gps_axis(2,0) >> gps_axis(2,1) >> gps_axis(2,2) >> gps_axis(2,3);
    in >> gps_axis(3,0) >> gps_axis(3,1) >> gps_axis(3,2) >> gps_axis(3,3);

    //keys = std::vector<cv::KeyPoint>(npoints);
    descriptors = std::vector<float>(npoints * 128);
    visibility_matrix.resize(npoints , std::vector<int>( ncams , 0 ) );
    //visibility_indexes = std::vector<std::pair<int,int> >(npoint,0);
    cloud_points = std::vector<cv::Point3f>(npoints);

    //read image projections and 3D points.
    //point_data.resize(npoint);
    //std::map<int,cv::DMatch> new_matches;
    int idx = 0;
    for(int i = 0; i < npoints; ++i)
    {

        cv::Point3f p3d;
        int id;

        //float pt[3];
        int cc[3];//, npj;
        double x3d, y3d, z3d;
        in  >> id >> x3d >> y3d >> z3d;

        /*if(indexes_to_compressed.find(id) != indexes_to_compressed.end()){
            if(matches.find(id) != matches.end()){
                cv::DMatch dm = matches.find(id)->second;
                dm.trainIdx = i;
                new_matches[i] = dm;
            }

        }//*/

        p3d = cv::Point3f(x3d,y3d,z3d);

        //indexes_to_compressed.insert(std::make_pair(id,i));


        cloud_points[i] = p3d;

        int n_cameras = -1;
        //Total cameras associated to this point
        in >> n_cameras;

        bool point_found = false;
       // if(indexes_to_compressed.find(id) != indexes_to_compressed.end()){

        if(id == visibility_indexes_before[idx].first){
            visibility_indexes_after.push_back(std::make_pair(id, visibility_indexes_before[idx++].second));
            point_found = true;
        }else{
            visibility_indexes_after.push_back(std::make_pair(id, 0.0));

        }//*/

        //cv::Point2f p2d;


        for(int j = 0; j < n_cameras; j++){

            int cam_id;

            in >> cam_id;

            visibility_matrix[i][cam_id] = 1;
            if(!point_found)
                visibility_indexes_after[i].second = visibility_indexes_after[i].second +1;
        }

        //in >> p2d.x >> p2d.y;

        //keys[i] = cv::KeyPoint(p2d,0);

        for(int d = 0; d < 128; d++)
        {
            unsigned int udesc;
            in >> udesc;
            float f = static_cast<float>(udesc) / 512.0;
            descriptors[i* 128 + d] = f;// / 512.0;
        }


    }

    //matches = new_matches;

    ///////////////////////////////////////////////////////////////////////////////
    std::cout << cloud_points.size() << " 3D points;\n " << cloud_points.size() << " projections\n";

    return true;


}


bool LoadPrioriCloud(const char* file_name,
                     std::string &belonging_model,
int start_idx,                     //std::map<int,int> indexes_to_compressed,
                     std::vector<std::pair<int,float> > visibility_indexes_before,
                     std::vector<std::pair<int,float> > &visibility_indexes_after,
                     std::vector<std::map<int,int> > &visibility_matrix,
                     std::vector<cv::Point3f> &cloud_points,
                     //std::vector<cv::KeyPoint> &keys,
                     std::vector<float> &descriptors,
                     cv::Matx44d &gps_axis){

    if(file_name == NULL)return false;
    std::ifstream in(file_name);
#ifdef GEO_DEBUG
    std::cout << "Loading cameras/points: " << file_name <<"\n" ;
#endif
    if(!in.is_open()) return false;

    in >> belonging_model;

    std::string header;
    in >> header; //file header


    int npoints = 0, ncams = 0;


    //////////////////////////////////////
    in >> ncams;
    in >> npoints; if(npoints <= 0) return false;
    in >> gps_axis(0,0) >> gps_axis(0,1) >> gps_axis(0,2) >> gps_axis(0,3);
    in >> gps_axis(1,0) >> gps_axis(1,1) >> gps_axis(1,2) >> gps_axis(1,3);
    in >> gps_axis(2,0) >> gps_axis(2,1) >> gps_axis(2,2) >> gps_axis(2,3);
    in >> gps_axis(3,0) >> gps_axis(3,1) >> gps_axis(3,2) >> gps_axis(3,3);

    //keys = std::vector<cv::KeyPoint>(npoints);
    descriptors = std::vector<float>(npoints * 128);
    visibility_matrix = std::vector<std::map<int,int> >(npoints);
    //visibility_indexes = std::vector<std::pair<int,int> >(npoint,0);
    cloud_points = std::vector<cv::Point3f>(npoints);

    //read image projections and 3D points.
    //point_data.resize(npoint);
    int idx = 0;
    for(int i = 0; i < npoints; ++i)
    {

        cv::Point3f p3d;
        int id;

        //float pt[3];
        int cc[3];//, npj;
        double x3d, y3d, z3d;
        in  >> id >> x3d >> y3d >> z3d;

        /*if(indexes_to_compressed.find(id) != indexes_to_compressed.end()){
            if(matches.find(id) != matches.end()){
                cv::DMatch dm = matches.find(id)->second;
                dm.trainIdx = i;
                new_matches[i] = dm;
            }

        }//*/

        p3d = cv::Point3f(x3d,y3d,z3d);

        //indexes_to_compressed.insert(std::make_pair(id,i));


        cloud_points[i] = p3d;

        int n_cameras = -1;
        //Total cameras associated to this point
        in >> n_cameras;

        bool point_found = false;
       // if(indexes_to_compressed.find(id) != indexes_to_compressed.end()){

        if(id == visibility_indexes_before[idx + start_idx].first){
            visibility_indexes_after.push_back(std::make_pair(id, visibility_indexes_before[start_idx + idx++].second));
            point_found = true;
        }else{
            visibility_indexes_after.push_back(std::make_pair(id, 0.0));

        }//*/

        //cv::Point2f p2d;


        for(int j = 0; j < n_cameras; j++){

            int cam_id;

            in >> cam_id;

            visibility_matrix[i][cam_id] = cam_id;
            if(!point_found)
                visibility_indexes_after[i].second = visibility_indexes_after[i].second +1;
        }

        //in >> p2d.x >> p2d.y;

        //keys[i] = cv::KeyPoint(p2d,0);

        for(int d = 0; d < 128; d++)
        {
            unsigned int udesc;
            in >> udesc;
            float f = static_cast<float>(udesc) / 512.0;
            descriptors[i* 128 + d] = f;// / 512.0;
        }


    }

    //matches = new_matches;

    ///////////////////////////////////////////////////////////////////////////////
    std::cout << cloud_points.size() << " 3D points;\n " << cloud_points.size() << " projections\n";

    return true;


}


//=================================================== BINARY FUNCTIONS ===========================================================
void SavePrioriCloudB(const char *file_name,
                     std::vector<ImageV*> img_info,
                     std::vector<CloudPoint> pointcloud,
                     std::map<int,int> compressed_points,
                     cv::Matx44d gps_axis){


    int n_points = int(compressed_points.size());
    if(n_points <=0) return;

    int n_cams = int(img_info.size());

    std::ofstream out(file_name, std::ios::out | std::ios::binary);
    out.flags(std::ios::fixed);

    out.write(reinterpret_cast<const char *>(&n_cams), sizeof(int));
    out.write(reinterpret_cast<const char *>(&n_points), sizeof(int));

    out.write(reinterpret_cast<const char *>(&gps_axis(0,0)), sizeof(double));
    out.write(reinterpret_cast<const char *>(&gps_axis(0,1)), sizeof(double));
    out.write(reinterpret_cast<const char *>(&gps_axis(0,2)), sizeof(double));
    out.write(reinterpret_cast<const char *>(&gps_axis(0,3)), sizeof(double));

    out.write(reinterpret_cast<const char *>(&gps_axis(1,0)), sizeof(double));
    out.write(reinterpret_cast<const char *>(&gps_axis(1,1)), sizeof(double));
    out.write(reinterpret_cast<const char *>(&gps_axis(1,2)), sizeof(double));
    out.write(reinterpret_cast<const char *>(&gps_axis(1,3)), sizeof(double));

    out.write(reinterpret_cast<const char *>(&gps_axis(2,0)), sizeof(double));
    out.write(reinterpret_cast<const char *>(&gps_axis(2,1)), sizeof(double));
    out.write(reinterpret_cast<const char *>(&gps_axis(2,2)), sizeof(double));
    out.write(reinterpret_cast<const char *>(&gps_axis(2,3)), sizeof(double));

    out.write(reinterpret_cast<const char *>(&gps_axis(3,0)), sizeof(double));
    out.write(reinterpret_cast<const char *>(&gps_axis(3,1)), sizeof(double));
    out.write(reinterpret_cast<const char *>(&gps_axis(3,2)), sizeof(double));
    out.write(reinterpret_cast<const char *>(&gps_axis(3,3)), sizeof(double));


    //out.write(reinterpret_cast<const char *>(&K.at<double>(0,0)), sizeof(double));
    //out.write(reinterpret_cast<const char *>(&K.at<double>(0,2)), sizeof(double));
    //out.write(reinterpret_cast<const char *>(&K.at<double>(1,2)), sizeof(double));

    //out.write(reinterpret_cast<const char *>(&P(0,0)), sizeof(double));
    //out.write(reinterpret_cast<const char *>(&P(1,0)), sizeof(double));
    //out.write(reinterpret_cast<const char *>(&P(2,0)), sizeof(double));

    //out.write(reinterpret_cast<const char *>(&P(0,1)), sizeof(double));
    //out.write(reinterpret_cast<const char *>(&P(1,1)), sizeof(double));
    //out.write(reinterpret_cast<const char *>(&P(2,1)), sizeof(double));

    //out.write(reinterpret_cast<const char *>(&P(0,2)), sizeof(double));
    //out.write(reinterpret_cast<const char *>(&P(1,2)), sizeof(double));
    //out.write(reinterpret_cast<const char *>(&P(2,2)), sizeof(double));

    //out.write(reinterpret_cast<const char *>(&P(0,3)), sizeof(double));
    //out.write(reinterpret_cast<const char *>(&P(1,3)), sizeof(double));
    //out.write(reinterpret_cast<const char *>(&P(2,3)), sizeof(double));

    for(std::map<int,int>::iterator iter = compressed_points.begin(); iter != compressed_points.end(); iter++){
        CloudPoint cp = pointcloud[iter->first];

        out.write(reinterpret_cast<const char *>(&iter->second), sizeof(int));

        out.write(reinterpret_cast<const char *>(&cp.pt.x), sizeof(double));
        out.write(reinterpret_cast<const char *>(&cp.pt.y), sizeof(double));
        out.write(reinterpret_cast<const char *>(&cp.pt.z), sizeof(double));

        int cams_size = int(cp.imgpt_for_img.size());

        out.write(reinterpret_cast<const char *>(&cams_size), sizeof(int));

        std::vector<float> descs_mean(128, 0.0);

        for(std::map<int,int>::iterator it = cp.imgpt_for_img.begin(); it != cp.imgpt_for_img.end(); it++){
            out.write(reinterpret_cast<const char *>(&it->first), sizeof(int));

            for(int d = 0; d < 128; d++){
                descs_mean[d] += img_info[it->first]->getDes(it->second * 128 + d);
            }
        }

        for(int i = 0; i < 128; i++){
            float d = descs_mean[i] / (float) cams_size;

            unsigned int desc = static_cast<unsigned int>(floor(d * 512.0f + 0.5f));

            out.write(reinterpret_cast<const char *>(&desc), sizeof(unsigned int));
        }
    }

    out.close();
}

bool LoadPrioriCloudB(const char* file_name,
                     std::map<int,int > &indexes_to_compressed,
                     std::vector<std::pair<int,float> > &visibility_indexes,
                     std::vector<std::vector<int> > &visibility_matrix,
                     std::vector<cv::Point3f> &cloud_points,
                     //std::vector<cv::KeyPoint> &keys,
                     ann_1_1_char::ANNpointArray &descriptors,
                     cv::Matx44d &gps_axis){

    if(file_name == NULL)return false;
    std::ifstream in(file_name, std::ios::in | std::ios::binary);
#ifdef GEO_DEBUG
    std::cout << "Loading cameras/points: " << file_name <<"\n" ;
#endif
    if(!in.is_open()) return false;

    int npoints = 0, ncams = 0;

    //////////////////////////////////////
    in.read((char*)&ncams,sizeof(int));
    in.read((char*)&npoints,sizeof(int));

    if(npoints <= 0) return false;

    in.read((char*)&gps_axis(0,0),sizeof(double));
    in.read((char*)&gps_axis(0,1),sizeof(double));
    in.read((char*)&gps_axis(0,2),sizeof(double));
    in.read((char*)&gps_axis(0,3),sizeof(double));

    in.read((char*)&gps_axis(1,0),sizeof(double));
    in.read((char*)&gps_axis(1,1),sizeof(double));
    in.read((char*)&gps_axis(1,2),sizeof(double));
    in.read((char*)&gps_axis(1,3),sizeof(double));

    in.read((char*)&gps_axis(2,0),sizeof(double));
    in.read((char*)&gps_axis(2,1),sizeof(double));
    in.read((char*)&gps_axis(2,2),sizeof(double));
    in.read((char*)&gps_axis(2,3),sizeof(double));

    in.read((char*)&gps_axis(3,0),sizeof(double));
    in.read((char*)&gps_axis(3,1),sizeof(double));
    in.read((char*)&gps_axis(3,2),sizeof(double));
    in.read((char*)&gps_axis(3,3),sizeof(double));

    //keys = std::vector<cv::KeyPoint>(npoints);
    descriptors = ann_1_1_char::annAllocPts(npoints, 128);
    visibility_matrix.resize(npoints , std::vector<int>( ncams , 0 ) );
    //visibility_indexes = std::vector<std::pair<int,int> >(npoint,0);
    cloud_points = std::vector<cv::Point3f>(npoints);

    //read image projections and 3D points.
    //point_data.resize(npoint);
    for(int i = 0; i < npoints; ++i)
    {

        cv::Point3f p3d;
        int id;

        //float pt[3];
        int cc[3];//, npj;
        in.read((char*)&id,sizeof(int));
        in.read((char*)&p3d.x,sizeof(double));
        in.read((char*)&p3d.y,sizeof(double));
        in.read((char*)&p3d.z,sizeof(double));

        indexes_to_compressed.insert(std::make_pair(i,id));

        cloud_points[i] = p3d;

        int n_cameras = -1;
        //Total cameras associated to this point
        in.read((char*)&n_cameras,sizeof(int));

        visibility_indexes.push_back(std::make_pair(id, 0.0));

        //cv::Point2f p2d;


        for(int j = 0; j < n_cameras; j++){

            int cam_id;

            in.read((char*)&cam_id,sizeof(int));

            visibility_matrix[i][cam_id] = 1;
            visibility_indexes[i].second = visibility_indexes[i].second +1.0;
        }

        //in >> F.x >> p2d.y;

        //keys[i] = cv::KeyPoint(p2d,0);

        for(int d = 0; d < 128; d++)
        {
            unsigned int value;
            in.read((char*) &value, sizeof(unsigned int));
            float f = static_cast<float>(value) / 512.0;
            descriptors[i][d] = static_cast<unsigned char>(floor(f * 512.0 + 0.5));// / 512.0;
        }
    }
    ///////////////////////////////////////////////////////////////////////////////
    std::cout << cloud_points.size() << " 3D points;\n " << cloud_points.size() << " projections\n";

    return true;
}

bool LoadPrioriCloudB(const char* file_name,
                     //std::map<int,cv::DMatch> &matches,
                     std::map<int,int> indexes_to_compressed,
                     std::vector<std::pair<int,float> > visibility_indexes_before,
                     std::vector<std::pair<int,float> > &visibility_indexes_after,
                     std::vector<std::vector<int> > &visibility_matrix,
                     std::vector<cv::Point3f> &cloud_points,
                     //std::vector<cv::KeyPoint> &keys,
                     std::vector<float> &descriptors,
                     cv::Matx44d &gps_axis){

    if(file_name == NULL)return false;
    std::ifstream in(file_name, std::ios::in | std::ios::binary);

#ifdef GEO_DEBUG
    std::cout << "Loading cameras/points: " << file_name <<"\n" ;
#endif
    if(!in.is_open()) return false;

    int npoints = 0, ncams = 0;

    //////////////////////////////////////
    in.read((char*)&ncams,sizeof(int));
    in.read((char*)&npoints,sizeof(int));

    if(npoints <= 0) return false;

    in.read((char*)&gps_axis(0,0),sizeof(double));
    in.read((char*)&gps_axis(0,1),sizeof(double));
    in.read((char*)&gps_axis(0,2),sizeof(double));
    in.read((char*)&gps_axis(0,3),sizeof(double));

    in.read((char*)&gps_axis(1,0),sizeof(double));
    in.read((char*)&gps_axis(1,1),sizeof(double));
    in.read((char*)&gps_axis(1,2),sizeof(double));
    in.read((char*)&gps_axis(1,3),sizeof(double));

    in.read((char*)&gps_axis(2,0),sizeof(double));
    in.read((char*)&gps_axis(2,1),sizeof(double));
    in.read((char*)&gps_axis(2,2),sizeof(double));
    in.read((char*)&gps_axis(2,3),sizeof(double));

    in.read((char*)&gps_axis(3,0),sizeof(double));
    in.read((char*)&gps_axis(3,1),sizeof(double));
    in.read((char*)&gps_axis(3,2),sizeof(double));
    in.read((char*)&gps_axis(3,3),sizeof(double));


    //keys = std::vector<cv::KeyPoint>(npoints);
    descriptors = std::vector<float>(npoints * 128);
    visibility_matrix.resize(npoints , std::vector<int>( ncams , 0 ) );
    //visibility_indexes = std::vector<std::pair<int,int> >(npoint,0);
    cloud_points = std::vector<cv::Point3f>(npoints);

    //read image projections and 3D points.
    //point_data.resize(npoint);
    //std::map<int,cv::DMatch> new_matches;
    int idx = 0;
    for(int i = 0; i < npoints; ++i)
    {

        cv::Point3f p3d;
        int id;

        //float pt[3];
        int cc[3];//, npj;
        in.read((char*)&id,sizeof(int));
        in.read((char*)&p3d.x,sizeof(double));
        in.read((char*)&p3d.y,sizeof(double));
        in.read((char*)&p3d.z,sizeof(double));

        /*if(indexes_to_compressed.find(id) != indexes_to_compressed.end()){
            if(matches.find(id) != matches.end()){
                cv::DMatch dm = matches.find(id)->second;
                dm.trainIdx = i;
                new_matches[i] = dm;
            }

        }//*/

        //p3d = cv::Point3f(x3d,y3d,z3d);

        //indexes_to_compressed.insert(std::make_pair(id,i));

        cloud_points[i] = p3d;

        int n_cameras = -1;
        //Total cameras associated to this point
        in.read((char*)&n_cameras,sizeof(int));

        bool point_found = false;
        if(indexes_to_compressed.find(id) != indexes_to_compressed.end()){
            visibility_indexes_after.push_back(std::make_pair(id, visibility_indexes_before[idx++].second));
            point_found = true;
        }else{
            visibility_indexes_after.push_back(std::make_pair(id, 0.0));

        }//*/

        //cv::Point2f p2d;


        for(int j = 0; j < n_cameras; j++){

            int cam_id;

            in.read((char*)&cam_id,sizeof(int));

            visibility_matrix[i][cam_id] = 1;
            if(!point_found)
                visibility_indexes_after[i].second = visibility_indexes_after[i].second +1;
        }

        //in >> p2d.x >> p2d.y;

        //keys[i] = cv::KeyPoint(p2d,0);

        for(int d = 0; d < 128; d++)
        {
            unsigned int value;
            in.read((char*) &value, sizeof(unsigned int));
            descriptors[i* 128 + d] =  static_cast<float>(value) / 512.0f;

        }


    }

    //matches = new_matches;

    ///////////////////////////////////////////////////////////////////////////////
    std::cout << cloud_points.size() << " 3D points;\n " << cloud_points.size() << " projections\n";

    return true;


}

