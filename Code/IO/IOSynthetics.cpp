#include "../IO/IOSynthetics.h"


//#include "../../Common.h"
#include <fstream>
//===================================================== Tmp Files   ===============================================

void SaveTMP(const char* sift_path,std::vector<cv::Point3f> cps, std::vector<cv::KeyPoint> kps, std::vector<float> dcs){
    int n_keys = kps.size();
    int s_desc = 128;
    if(n_keys <=0) return;

    std::ofstream out(sift_path, std::ios::out | std::ios::binary);
    out.flags(std::ios::fixed);

    out.write(reinterpret_cast<const char *>(&n_keys), sizeof(int));
    out.write(reinterpret_cast<const char *>(&s_desc), sizeof(int));


    for(int i = 0; i < n_keys; i++)
    {
        float x = kps[i].pt.x,
                y = kps[i].pt.y,
                s = kps[i].size,
                o = kps[i].angle,
                x3d = cps[i].x,
                y3d = cps[i].y,
                z3d = cps[i].z;


        out.write(reinterpret_cast<const char *>(&y), sizeof(float));
        out.write(reinterpret_cast<const char *>(&x), sizeof(float));
        out.write(reinterpret_cast<const char *>(&s), sizeof(float));
        out.write(reinterpret_cast<const char *>(&o), sizeof(float));
        out.write(reinterpret_cast<const char *>(&x3d), sizeof(float));
        out.write(reinterpret_cast<const char *>(&y3d), sizeof(float));
        out.write(reinterpret_cast<const char *>(&z3d), sizeof(float));
    }

    for(int i = 0; i < n_keys; i++){
        for(int k = 0; k < 128; k ++)
        {
            unsigned int desc = static_cast<unsigned int>(floor(0.5f +  512.0f*dcs[i * 128 +k]));

            out.write(reinterpret_cast<const char *>(&desc), sizeof(unsigned int));
        }
    }

    out.close();
}

bool ReadTMP(const char* sift_path, std::vector<cv::Point3f> &cps, std::vector<cv::KeyPoint>& keys, std::vector<float>& descs){
    int npt, nd;

    std::ifstream is(sift_path, std::ios::in | std::ios::binary);

    if(!is.is_open())return false;

    is.read((char*)&npt,sizeof(int));
    is.read((char*)&nd, sizeof(int));


    for(int i = 0; i < npt; i++){
        float x,y,s,o,x3d,y3d,z3d;

        is.read((char*)&y,sizeof(float));
        is.read((char*)&x,sizeof(float));
        is.read((char*)&s,sizeof(float));
        is.read((char*)&o,sizeof(float));
        is.read((char*)&x3d,sizeof(float));
        is.read((char*)&y3d,sizeof(float));
        is.read((char*)&z3d,sizeof(float));
        keys.push_back(cv::KeyPoint(cv::Point2f(x,y),s,-o));
        cps.push_back(cv::Point3f(x3d,y3d,z3d));
    }

    for(int i = 0; i < npt * 128; i++ ){
        unsigned int value;
        is.read((char*) &value, sizeof(unsigned int));
        descs.push_back(static_cast<float>(value) / 512.0f);
    }

    is.close();
    return true;
}

//===================================================== 3D Documents ====================================================================================

//Read 3D Doc (Binary)
bool Read3DDoc(const char* sift_path, std::vector<cv::KeyPoint>& keys, std::vector<float>& descs, /*cv::Matx34d &P, cv::Mat &K,*/ std::vector<cv::Point3f> &cps, cv::Matx44d &G){
    int npt, nd;

    std::ifstream is(sift_path, std::ios::in | std::ios::binary);

    if(!is.is_open())return false;

    is.read((char*)&npt,sizeof(int));
    is.read((char*)&nd, sizeof(int));

    //K = cv::Mat_<double>::zeros(3,3);

    //double f,cx,cy;

    //for(int i = 0; i < 15; i++)
    //    is.read((char*)(&f), sizeof(double));

    is.read((char*)(&G(0,0)), sizeof(double));
    is.read((char*)(&G(0,1)), sizeof(double));
    is.read((char*)(&G(0,2)), sizeof(double));
    is.read((char*)(&G(0,3)), sizeof(double));

    is.read((char*)(&G(1,0)), sizeof(double));
    is.read((char*)(&G(1,1)), sizeof(double));
    is.read((char*)(&G(1,2)), sizeof(double));
    is.read((char*)(&G(1,3)), sizeof(double));


    is.read((char*)(&G(2,0)), sizeof(double));
    is.read((char*)(&G(2,1)), sizeof(double));
    is.read((char*)(&G(2,2)), sizeof(double));
    is.read((char*)(&G(2,3)), sizeof(double));

    is.read((char*)(&G(3,0)), sizeof(double));
    is.read((char*)(&G(3,1)), sizeof(double));
    is.read((char*)(&G(3,2)), sizeof(double));
    is.read((char*)(&G(3,3)), sizeof(double));

    /*is.read((char*)(&f), sizeof(double));
    is.read((char*)(&cx), sizeof(double));
    is.read((char*)(&cy), sizeof(double));

    K.at<double>(0,0) = f;
    K.at<double>(1,1) = f;
    K.at<double>(0,2) = cx;
    K.at<double>(1,2) = cy;
    K.at<double>(2,2) = 1.0;

    is.read((char*)(&P(0,0)), sizeof(double));
    is.read((char*)(&P(1,0)), sizeof(double));
    is.read((char*)(&P(2,0)), sizeof(double));

    is.read((char*)(&P(0,1)), sizeof(double));
    is.read((char*)(&P(1,1)), sizeof(double));
    is.read((char*)(&P(2,1)), sizeof(double));

    is.read((char*)(&P(0,2)), sizeof(double));
    is.read((char*)(&P(1,2)), sizeof(double));
    is.read((char*)(&P(2,2)), sizeof(double));

    is.read((char*)(&P(0,3)), sizeof(double));
    is.read((char*)(&P(1,3)), sizeof(double));
    is.read((char*)(&P(2,3)), sizeof(double));*/


    for(int i = 0; i < npt; i++){
        float x,y,s,o,x3d,y3d,z3d;

        is.read((char*)&y,sizeof(float));
        is.read((char*)&x,sizeof(float));
        is.read((char*)&s,sizeof(float));
        is.read((char*)&o,sizeof(float));
        is.read((char*)&x3d,sizeof(float));
        is.read((char*)&y3d,sizeof(float));
        is.read((char*)&z3d,sizeof(float));
        keys.push_back(cv::KeyPoint(cv::Point2f(x,y),s,-o));
        cps.push_back(cv::Point3f(x3d,y3d,z3d));
    }

    for(int i = 0; i < npt * 128; i++ ){
        unsigned int value;
        is.read((char*) &value, sizeof(unsigned int));
        descs.push_back(static_cast<float>(value) / 512.0f);
    }

    is.close();
    return true;
}

bool Read3DDoc(const char* sift_path, std::vector<float> &scales, std::vector<unsigned char>& descs, int &num_keys){
    int npt, nd;

    std::ifstream is(sift_path, std::ios::in | std::ios::binary);

    if(!is.is_open())return false;

    is.read((char*)&npt,sizeof(int));
    is.read((char*)&nd, sizeof(int));

    num_keys = npt;
    /* Filter keys */


    //K = cv::Mat_<double>::zeros(3,3);

    //double f,cx,cy;

    //for(int i = 0; i < 15; i++)
    //    is.read((char*)(&f), sizeof(double));

    double skip_double;

    is.read((char*)(&skip_double), sizeof(double));
    is.read((char*)(&skip_double), sizeof(double));
    is.read((char*)(&skip_double), sizeof(double));
    is.read((char*)(&skip_double), sizeof(double));

    is.read((char*)(&skip_double), sizeof(double));
    is.read((char*)(&skip_double), sizeof(double));
    is.read((char*)(&skip_double), sizeof(double));
    is.read((char*)(&skip_double), sizeof(double));


    is.read((char*)(&skip_double), sizeof(double));
    is.read((char*)(&skip_double), sizeof(double));
    is.read((char*)(&skip_double), sizeof(double));
    is.read((char*)(&skip_double), sizeof(double));

    is.read((char*)(&skip_double), sizeof(double));
    is.read((char*)(&skip_double), sizeof(double));
    is.read((char*)(&skip_double), sizeof(double));
    is.read((char*)(&skip_double), sizeof(double));

    /*is.read((char*)(&f), sizeof(double));
    is.read((char*)(&cx), sizeof(double));
    is.read((char*)(&cy), sizeof(double));

    K.at<double>(0,0) = f;
    K.at<double>(1,1) = f;
    K.at<double>(0,2) = cx;
    K.at<double>(1,2) = cy;
    K.at<double>(2,2) = 1.0;

    is.read((char*)(&P(0,0)), sizeof(double));
    is.read((char*)(&P(1,0)), sizeof(double));
    is.read((char*)(&P(2,0)), sizeof(double));

    is.read((char*)(&P(0,1)), sizeof(double));
    is.read((char*)(&P(1,1)), sizeof(double));
    is.read((char*)(&P(2,1)), sizeof(double));

    is.read((char*)(&P(0,2)), sizeof(double));
    is.read((char*)(&P(1,2)), sizeof(double));
    is.read((char*)(&P(2,2)), sizeof(double));

    is.read((char*)(&P(0,3)), sizeof(double));
    is.read((char*)(&P(1,3)), sizeof(double));
    is.read((char*)(&P(2,3)), sizeof(double));*/


    for(int i = 0; i < npt; i++){
        float x,y,s,o,x3d,y3d,z3d;

        is.read((char*)&y,sizeof(float));
        is.read((char*)&x,sizeof(float));
        is.read((char*)&s,sizeof(float));
        is.read((char*)&o,sizeof(float));
        is.read((char*)&x3d,sizeof(float));
        is.read((char*)&y3d,sizeof(float));
        is.read((char*)&z3d,sizeof(float));

        scales.push_back(s);
    }

    for(int i = 0; i < npt * 128; i++ ){
        unsigned int value;
        is.read((char*) &value, sizeof(unsigned int));
        descs.push_back(static_cast<unsigned char>(value));// / 512.0f;
        //descs.push_back(static_cast<unsigned char>(floor(desc_float * 512.0f + 0.5f)));
    }

    is.close();
    return true;
}

//Save 3D Doc (Binary)
void Save3DDoc(const char* sift_path, std::vector<cv::Point3f> cps, std::vector<cv::KeyPoint> kps, std::vector<float> dcs, cv::Matx44d G){
    int n_keys = kps.size();
    int s_desc = 128;
    if(n_keys <=0) return;

    std::ofstream out(sift_path, std::ios::out | std::ios::binary);
    out.flags(std::ios::fixed);

    out.write(reinterpret_cast<const char *>(&n_keys), sizeof(int));
    out.write(reinterpret_cast<const char *>(&s_desc), sizeof(int));

    out.write(reinterpret_cast<const char *>(&G(0,0)), sizeof(double));
    out.write(reinterpret_cast<const char *>(&G(0,1)), sizeof(double));
    out.write(reinterpret_cast<const char *>(&G(0,2)), sizeof(double));
    out.write(reinterpret_cast<const char *>(&G(0,3)), sizeof(double));


    out.write(reinterpret_cast<const char *>(&G(1,0)), sizeof(double));
    out.write(reinterpret_cast<const char *>(&G(1,1)), sizeof(double));
    out.write(reinterpret_cast<const char *>(&G(1,2)), sizeof(double));
    out.write(reinterpret_cast<const char *>(&G(1,3)), sizeof(double));


    out.write(reinterpret_cast<const char *>(&G(2,0)), sizeof(double));
    out.write(reinterpret_cast<const char *>(&G(2,1)), sizeof(double));
    out.write(reinterpret_cast<const char *>(&G(2,2)), sizeof(double));
    out.write(reinterpret_cast<const char *>(&G(2,3)), sizeof(double));

    out.write(reinterpret_cast<const char *>(&G(3,0)), sizeof(double));
    out.write(reinterpret_cast<const char *>(&G(3,1)), sizeof(double));
    out.write(reinterpret_cast<const char *>(&G(3,2)), sizeof(double));
    out.write(reinterpret_cast<const char *>(&G(3,3)), sizeof(double));


    for(int i = 0; i < n_keys; i++)
    {
        float x = kps[i].pt.x,
                y = kps[i].pt.y,
                s = kps[i].size,
                o = kps[i].angle,
                x3d = cps[i].x,
                y3d = cps[i].y,
                z3d = cps[i].z;

        out.write(reinterpret_cast<const char *>(&y), sizeof(float));
        out.write(reinterpret_cast<const char *>(&x), sizeof(float));
        out.write(reinterpret_cast<const char *>(&s), sizeof(float));
        out.write(reinterpret_cast<const char *>(&o), sizeof(float));
        out.write(reinterpret_cast<const char *>(&x3d), sizeof(float));
        out.write(reinterpret_cast<const char *>(&y3d), sizeof(float));
        out.write(reinterpret_cast<const char *>(&z3d), sizeof(float));
    }

    for(int i = 0; i < n_keys; i++){
        for(int k = 0; k < 128; k ++)
        {
            unsigned int desc = static_cast<unsigned int>(floor(0.5f +  512.0f*dcs[i * 128 +k]));

            out.write(reinterpret_cast<const char *>(&desc), sizeof(unsigned int));
        }
    }

    out.close();


}
