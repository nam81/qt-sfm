#include "ConfigFile.h"
#include <iostream>
#include <fstream>
#include <ios>

void fillOptions(std::map<std::string,int>  &available_options){

    available_options.insert(std::make_pair("Kc", 0));
    available_options.insert(std::make_pair("Ks", 1));
    available_options.insert(std::make_pair("max_seed_points", 2));
    available_options.insert(std::make_pair("max_comp_points", 3));
    available_options.insert(std::make_pair("ratio_seed", 4));
    available_options.insert(std::make_pair("ratio_comp", 5));
    available_options.insert(std::make_pair("max_search", 6));
    available_options.insert(std::make_pair("thrs_w", 7));
    available_options.insert(std::make_pair("siftgpu_priori",8));

    available_options.insert(std::make_pair("view_width", 50));
    available_options.insert(std::make_pair("view_height", 51));
    available_options.insert(std::make_pair("view_focal", 52));
    available_options.insert(std::make_pair("view_fov", 53));
    available_options.insert(std::make_pair("view_min_focal", 54));
    available_options.insert(std::make_pair("view_max_focal", 55));
    available_options.insert(std::make_pair("view_lean_angle", 56));
    available_options.insert(std::make_pair("min_per_view", 57));
    available_options.insert(std::make_pair("view_strafe", 58));
    available_options.insert(std::make_pair("max_feats_match", 60));
    available_options.insert(std::make_pair("siftgpu_synth",62));
    available_options.insert(std::make_pair("extend_boundary",64));

    available_options.insert(std::make_pair("top_documents", 130));
    available_options.insert(std::make_pair("vocab_branches",131));
    available_options.insert(std::make_pair("vocab_levels",132));
    available_options.insert(std::make_pair("vocab_learn",133));
    available_options.insert(std::make_pair("vocab_data",134));
    available_options.insert(std::make_pair("vocab_type",135));

    available_options.insert(std::make_pair("priori_root",150));
    available_options.insert(std::make_pair("synth_root",151));

}

bool ReadConfigFile1(const char* file_path, ConfigOptions &config){
    std::map<std::string, int> available_options;

    fillOptions(available_options);

    std::ifstream is (file_path);
    if(!is.is_open())return false;


    while(is.peek() != -1){

        //If commented line, skip line
        if((is >> std::ws).peek() == std::char_traits<char>::to_int_type('#')){
            //std::cout << "Commented line" << std::endl;
            std::string to_ign;
            std::getline(is,to_ign);
            continue;
        }

        std::string option;

        is >> option;

        //else parse and option
        std::map<std::string,int>::iterator iterator = available_options.find(option);

        if(iterator == available_options.end() && option.size() != 0){
#ifdef GEO_DEBUG
            std::cout << "Unknown option " << option << "...skipping." << std::endl;
#endif
            continue;
        }

        switch(iterator->second){
        case 0 :
            is >> config.priorifeats.Kc;
            break;
        case 1 :
            is >> config.priorifeats.Ks;
            break;
        case 2 :
            is >> config.priorifeats.max_seed_points;
            break;
        case 3 :
            is >> config.priorifeats.max_comp_points;
            break;
        case 4 :
            is >> config.priorifeats.ratio_seed;
            break;
        case 5 :
            is >> config.priorifeats.ratio_comp;
            break;
        case 6 :
            is >> config.priorifeats.max_search;
            break;
        case 7 :
            is >> config.priorifeats.thrs_w;
            break;
        case 8 :
            is >> config.priorifeats.use_cuda;
            config.priorifeats.fo = new char[1];
            is >> config.priorifeats.fo;
            config.priorifeats.tc_type = new char[4];
            is >> config.priorifeats.tc_type;
            config.priorifeats.tc = new char[10];
            is >> config.priorifeats.tc;
            config.priorifeats.ori_n = new char[1];
            is >> config.priorifeats.ori_n;
            break;

        case 50 :
            is >> config.synthfeats.view_width;
            break;
        case 51 :
            is >> config.synthfeats.view_height;
            break;
        case 52 :
            is >> config.synthfeats.view_focal;
            break;
        case 53 :
            is >> config.synthfeats.view_fov;
            break;
        case 54 :
            is >> config.synthfeats.view_min_focal;
            break;
        case 55 :
            is >> config.synthfeats.view_max_focal;
            break;
        case 56 :
            is >> config.synthfeats.view_lean_angle;
            break;
        case 57 :
            is >> config.synthfeats.min_per_view;
            break;
        case 58 :
            is >> config.synthfeats.strafe_pos;
            break;
        case 60 :
            is >> config.synthfeats.max_feats_match;
            break;
        case 62 :
            is >> config.synthfeats.use_cuda;
            config.synthfeats.fo = new char[1];
            is >> config.synthfeats.fo;
            config.synthfeats.tc_type = new char[4];
            is >> config.synthfeats.tc_type;
            config.synthfeats.tc = new char[10];
            is >> config.synthfeats.tc;
            config.synthfeats.ori_n = new char[1];
            is >> config.synthfeats.ori_n;
            break;

        case 64 :
            is >> config.synthfeats.extend_boundary;
            break;

        case 130 :
            is >> config.top_documents;
            break;
        case 131 :
            is >> config.vocab_branches;
            break;
        case 132 :
            is >> config.vocab_levels;
            break;
        case 133 :
            config.vocab_learn_path = new char[50];
            is >> config.vocab_learn_path;
            break;
        case 134 :
            config.vocab_data_path = new char[50];
            is >> config.vocab_data_path;
            break;
        case 135 :
            is >> config.vocab_type;
            break;


        case 150 :
            config.priori_root = new char[50];
            is >> config.priori_root;
            break;
        case 151 :
            config.synth_root = new char[50];
            is >> config.synth_root;
            break;

        }
    }//*/

    is.close();


    return true;
}
