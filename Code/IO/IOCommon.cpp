#include "IOCommon.h"
#include "../Common.h"
#include <string.h>
#include <iostream>
#include <vector>
#include <string>
#include <iomanip>
#include <algorithm>
#include <cstdlib>
#include <fstream>
#include <ios>


//================================================ IMAGES ============================================================


//Read List of images to process
bool ReadImageList(const char* list_path, std::vector<std::string>& img_names){

    std::ifstream is (list_path);
    if(!is.is_open())return false;

    std::string name;
    while(is >> name){
        img_names.push_back(name);
    }

    is.close();

    return true;
}


//Write the names of existing images into a file
int WriteImageList(std::string path,std::string file_name, bool pose_estimation){

    std::vector<std::string> imgs_names;
    open_imgs_dir(path.c_str(), imgs_names, pose_estimation);

    const char* list_path = (path + file_name).c_str();


    std::ofstream out(list_path);
    out << (int) imgs_names.size() << "\n";

    for(int i = 0; i < imgs_names.size(); i++){
        out << imgs_names[i] << "\n";
    }

    out.close();

    return imgs_names.size();
}

int WriteImageList(std::string file_name, std::vector<std::string> imgnames){

    std::vector<std::string> img_names_before;

    ReadImageList(file_name.c_str(), img_names_before);

    for(int i = 0; i < imgnames.size(); i++)
        img_names_before.push_back(imgnames[i]);

    const char* list_path = (file_name).c_str();


    std::ofstream out(list_path);

    out << (int) img_names_before.size() << "\n";

    for(int i = 0; i < img_names_before.size(); i++){
        out << img_names_before[i] << "\n";
    }

    out.close();

    return img_names_before.size();
}


//------------------------------------------------ FEATURES -----------------------------------------------------------
bool ReadSiftVSFM(const char* sift_path, std::vector<cv::KeyPoint>& keys, std::vector<float>& descs){
    //char* npt_c,* nd_c;
    int npt, nd, name, version, loc_dim;

    std::ifstream is(sift_path, std::ios::in | std::ios::binary);

    if(!is.is_open())return false;

    is.read((char*)&name,sizeof(int));
    is.read((char*)&version,sizeof(int));
    is.read((char*)&npt,sizeof(int));
    is.read((char*)&loc_dim,sizeof(int));
    is.read((char*)&nd, sizeof(int));


    for(int i = 0; i < npt; i++){
        float x,y,s,o, color;

        is.read((char*)&x,sizeof(float));
        is.read((char*)&y,sizeof(float));
        is.read((char*)&color,sizeof(float));
        is.read((char*)&s,sizeof(float));
        is.read((char*)&o,sizeof(float));
        keys.push_back(cv::KeyPoint(cv::Point2f(x,y),s,-o));

    }

    for(int i = 0; i < npt * 128; i++ ){
        unsigned char value;
        is.read((char*) &value, sizeof(unsigned char));
        descs.push_back(static_cast<float>(value) / 512.0f);
    }

    int end;
    is.read((char*)&end,sizeof(int));

    is.close();
    return true;
}


//Read SiftGPU style (ASCII)
bool ReadSIFTA(const char* sift_path, std::vector<float> pLoc, std::vector<unsigned int> pDes){
    int npt=0, nd=0;
    std::ifstream is(sift_path);
    if(!is.is_open())return 0;
    is>> npt >> nd;
    if (npt <= 0 || nd <= 0 )
    {
        is.close();
        return false;
    }else
    {

        int i, j;
        int locDim = 5;
        int desDim = 128;

        ///////////////////////////////////////
        //float * pLoc =  malloc(sizeof(float) * locDim * npt);//new LocationData( locDim +1, npt);//one more column for z
        //unsigned int *	pDes =  malloc(sizeof(unsigned int) * desDim * npt);//new DescriptorData(desDim, npt);
        pLoc = std::vector<float>(npt * locDim);
        pDes = std::vector<unsigned int>(npt * desDim);

        ////////////////////////////////////////////////
        for ( i = 0 ; i< npt; i+=5)
        {

            is>> pLoc[i+1]; //x
            is>> pLoc[i]; //
            //SIFT_LOCATION_NEW
            //pLoc[0] += 0.5f;
            //pLoc[1] += 0.5f;
            pLoc[i + 2] = 0;
            is>> pLoc[i + 3];
            is>> pLoc[i + 4];
            pLoc[i + 4] = - pLoc[i + 4];//flip the angle

            for ( j = 0 ; j< desDim; j++){
                unsigned int value;
                is >> value;
                pDes[(i / 5) * 128 + j] = value;
            }
        }

        is.close();
        return true;
    }
}

//Save SiftGPU style (ASCII)
void SaveSIFTA(const char* szFile, std::vector<SiftGPU::SiftKeypoint> loc, std::vector<float> desc){

    int n = loc.size();
    if(n <=0) return;
    std::ofstream out(szFile);
    out << n << " " << 128 <<"\n";
    //
    for(int i = 0; i < n; i++)
    {

        //LTYPE * loc = _locData->getpt(i);
        //unsigned char * des = _desData->getpt(i);
       //out << loc[i].y << " " << loc[i].x << " " << loc[i].s << " " << loc[i].o << endl;
        out << std::setprecision(2) << loc[i].y <<" "
            << std::setprecision(2) << loc[i].x <<" "
            << std::setprecision(3) << loc[i].s <<" "
            << std::setprecision(3) << loc[i].o << std::endl;

        for(int k = 0; k < 128; k ++)
        {
            out<< ((unsigned int)(floor(0.5f + 512.0f*desc[128 * i + k])))<<" ";
            if ( (k+1)%20 == 0 ) out<<std::endl;
        }
        out<<std::endl;
    }

    out.close();
}

//Read Opencv style (ASCII)
bool ReadSiftA(const char* sift_path, std::vector<cv::KeyPoint>& keys, std::vector<float>& descs){
    int npt=0, nd=0;
    std::ifstream is(sift_path);
    if(!is.is_open())return false;
    is>> npt >> nd;
    if (npt <= 0 || nd <= 0 )
    {
        is.close();
        return false;
    }else{
        int i, j;
        int desDim = 128;

        ///////////////////////////////////////
        //float * pLoc =  malloc(sizeof(float) * locDim * npt);//new LocationData( locDim +1, npt);//one more column for z
        //unsigned int *	pDes =  malloc(sizeof(unsigned int) * desDim * npt);//new DescriptorData(desDim, npt);
        keys = std::vector<cv::KeyPoint>(npt);
        descs = std::vector<float>(npt * nd);
        //LTYPE * pLoc = _locData->data();
        //DTYPE *	pDes = _desData->data();

        ////////////////////////////////////////////////
        for ( i = 0 ; i< npt; i++)
        {
            is>> keys[i].pt.y; //x
            is>> keys[i].pt.x; //

            is>> keys[i].size;
            is>> keys[i].angle;
            keys[i].angle = - keys[i].angle;//flip the angle

            for ( j = 0 ; j< desDim; j++)
            {
                unsigned int value;
                is >> value;
                descs[i  *128 + j] = static_cast<float>(value) / 512.0f;
            }
        }
    }

    is.close();
    return true;
}

//Read Opencv style (Binary)
bool ReadSiftB(const char* sift_path, std::vector<cv::KeyPoint>& keys, std::vector<float>& descs){
    char* npt_c,* nd_c;
    int npt, nd;

    std::ifstream is(sift_path, std::ios::in | std::ios::binary);

    if(!is.is_open())return false;

    is.read((char*)&npt,sizeof(int));
    is.read((char*)&nd, sizeof(int));


    for(int i = 0; i < npt; i++){
        float x,y,s,o;

        is.read((char*)&y,sizeof(float));
        is.read((char*)&x,sizeof(float));
        is.read((char*)&s,sizeof(float));
        is.read((char*)&o,sizeof(float));
        keys.push_back(cv::KeyPoint(cv::Point2f(x,y),s,-o));
    }

    for(int i = 0; i < npt * 128; i++ ){
        unsigned int value;
        is.read((char*) &value, sizeof(unsigned int));
        descs.push_back(static_cast<float>(value) / 512.0f);
    }

    is.close();
    return true;
}

//Save Opencv style (ASCII)
void SaveSIFTA(const char* sift_path, std::vector<cv::KeyPoint> keys, std::vector<float> descs){
    int n = keys.size();
    if(n <=0) return;
    std::ofstream out(sift_path);
    out.flags(std::ios::fixed);

    out << n << " " << 128 << "\n";
    //
    for(int i = 0; i < n; i++)
    {
        //LTYPE * loc = _locData->getpt(i);
        //unsigned char * des = _desData->getpt(i);
       //out << loc[i].y << " " << loc[i].x << " " << loc[i].s << " " << loc[i].o << endl;
        out << std::setprecision(2) << keys[i].pt.y <<" "
            << std::setprecision(2) << keys[i].pt.x <<" "
            << std::setprecision(3) << keys[i].size <<" "
            << std::setprecision(3) << keys[i].angle << std::endl;

        for(int k = 0; k < 128; k ++)
        {
            out<< static_cast<unsigned int>(floor(0.5f +  512.0f*descs[i * 128 +k]))<<" ";
            if ( (k+1)%20 == 0 ) out << std::endl;
        }
        out<<std::endl;
    }

    out.close();
}

//Save Opencv style (Binary)
void SaveSIFTB(const char* sift_path, std::vector<cv::KeyPoint> keys, std::vector<float> descs){
    int n = keys.size();
    int s_desc = 128;
    if(n <=0) return;

    std::ofstream out(sift_path, std::ios::out | std::ios::binary);
    out.flags(std::ios::fixed);

    out.write(reinterpret_cast<const char *>(&n), sizeof(n));
    out.write(reinterpret_cast<const char *>(&s_desc), sizeof(s_desc));

    for(int i = 0; i < n; i++)
    {
        float x = keys[i].pt.x,
                y = keys[i].pt.y,
                s = keys[i].size,
                o = keys[i].angle;

        out.write(reinterpret_cast<const char *>(&y), sizeof(y));
        out.write(reinterpret_cast<const char *>(&x), sizeof(x));
        out.write(reinterpret_cast<const char *>(&s), sizeof(s));
        out.write(reinterpret_cast<const char *>(&o), sizeof(o));
    }

    for(int i = 0; i < n; i++){
        for(int k = 0; k < 128; k ++)
        {
            unsigned int desc = static_cast<unsigned int>(floor(0.5f +  512.0f*descs[i * 128 +k]));

            out.write(reinterpret_cast<const char *>(&desc), sizeof(desc));
        }
    }

    out.close();

}


bool ReadVizOutput(const char* output_path,
                   std::map<std::string, std::vector<int> > &posed_list,
                   std::vector<std::string> &posed_names,
                   std::vector<cv::Matx34d> &posed_P){

    int n_models=0, nd=0;
    std::ifstream is(output_path);

    if(!is.is_open())return false;

    is >> n_models;

    int index = 0;
    for(int i = 0; i < n_models; i++){
        std::string model_path;
        int n_imgs;

        is >> model_path >> n_imgs;


        std::vector<int> back_indexing(n_imgs);

        for(int j = 0; j < n_imgs; j++){
            std::string img_name;
            cv::Matx34d P;

            is >> img_name >> P(0,0) >> P(0,1) >> P(0,2) >> P(0,3)
                           >> P(1,0) >> P(1,1) >> P(1,2) >> P(1,3)
                           >> P(2,0) >> P(2,1) >> P(2,2) >> P(2,3);

            back_indexing[j] = index;
            posed_names.push_back(img_name);
            posed_P.push_back(P);
            index++;
        }

        posed_list.insert(std::make_pair(model_path, back_indexing));

    }

    is.close();

    return true;
}

//---------------------------------------------- Converters -------------------------------------------------------

//ASCII --> Binary convertion
bool AsciiToBin(const char* sift_path){
    std::vector<cv::KeyPoint> kps;
    std::vector<float> descs;

    if(!ReadSiftA(sift_path,kps,descs))
        return false;

    SaveSIFTB(sift_path,kps,descs);

    return true;
}



//--------------------------------------------------- NVM -------------------------------------------------------------

//Read NVM (ours)
void SaveToNVM(const char* file_name,
               std::vector<std::string> names,
               std::map<int ,cv::Matx34d> Pmats,
               cv::Mat& camera_matrix,
               std::vector<CloudPoint>& pointcloud,
               std::vector<cv::Vec3b>& pointcloudRGB){

    //std::vector<CameraT> camera_data;

    std::cout << "   Saving model to " << file_name << "...\n";
    std::ofstream out(file_name);

    out << "NVM_V3_R9T\n" << Pmats.size() << '\n' << std::setprecision(12);
    //if(names.size() < Pmats.size()) names.resize(Pmats.size(),std::string("unknown"));
    //if(ptc.size() < 3 * point_data.size()) ptc.resize(point_data.size() * 3, 0);

    //for(size_t i = 0; i < camera_data.size(); ++i)
    //{
    int removed_index = 0;
    //for(int i = 0; i < names.size(); i++){
    for(std::map<int,cv::Matx34d>::iterator it_pmats = Pmats.begin(); it_pmats != Pmats.end(); it_pmats++){

        //if(Pmats.find(i) != Pmats.end()){

            //CameraT& cam = camera_data[i - removed_index];
            cv::Matx34d cam = (*it_pmats).second;//Pmats.find(i)->second;
            out << names[(*it_pmats).first] << ' ' << camera_matrix.at<double>(0,0) << ' ';
            out << camera_matrix.at<double>(0,2) << ' ';
            out << camera_matrix.at<double>(1,2) << ' ';

            for(int j  = 0; j < 3; ++j){
                out << cam(j,0) << ' ';
                out << cam(j,1) << ' ';
                out << cam(j,2) << ' ';
            }

            //Normalized measurement distortion??
            out << cam(0,3) << ' ' << cam(1,3) << ' ' << cam(2,3) << ' '
                            << 0 << ' ' << 0 << '\n';
        //}else{
        //    out << names[i] << ' ' << 0 << '\n';
        //    removed_index++;
        //}
    }
    out << pointcloud.size() << '\n';

    //for(size_t i = 0, j = 0; i < point_data.size(); ++i){
    for(int i = 0; i < pointcloud.size(); i++){
        CloudPoint cp = pointcloud[i];
        cv::Vec3b  color = pointcloudRGB[i];
        out << cp.pt.x << ' ' << cp.pt.y << ' ' << cp.pt.z << ' '
            << (int) color[0] << ' ' << (int) color[1] << ' ' << (int) color[2] << ' ';
        //int j = 0;
        //while(j < cp.imgpt_for_img.size()){
            //if(cp.imgpt_for_img[j] != -1){
        //        out << cp.imgpt_for_img[j] << ' ';
            //}
        //    j++;
        //}

        out << cp.imgpt_for_img.size() << ' ';

        for(std::map<int,int>::iterator it = cp.imgpt_for_img.begin(); it != cp.imgpt_for_img.end(); it++){
            out << it->first << ' ' << it->second << ' ';
        }

        out << '\n';
    }

    return;
}


//Read NVM (ours)
void SaveToNVM(const char* file_name,
               std::vector<ImageV*> imgs_info,
               std::vector<CloudPoint> pointcloud){

    //std::vector<CameraT> camera_data;

    std::cout << "   Saving model to " << file_name << "...\n";
    std::ofstream out(file_name);

    out << "NVM_VPF\n";

    out << pointcloud.size() << '\n';

    //for(size_t i = 0, j = 0; i < point_data.size(); ++i){
    for(int i = 0; i < pointcloud.size(); i++){
        CloudPoint cp = pointcloud[i];

        out << cp.pt.x << ' ' << cp.pt.y << ' ' << cp.pt.z << ' ';

        out << cp.imgpt_for_img.size() << '\n';

        for(std::map<int,int>::iterator it = cp.imgpt_for_img.begin(); it != cp.imgpt_for_img.end(); it++){
            ImageV *imgv = imgs_info[it->first];
            cv::KeyPoint kp = imgv->getPt(it->second);
            //std::vector<float> dc = imgv->getDes(it->second);

            out << kp.pt.x << ' ' << kp.pt.y << ' ';

            for(int j = 0; j < 128; j++){
                float dc = imgv->getDes(it->second * 128 + j);
                unsigned int desc = static_cast<unsigned int>(floor(0.5f +  512.0f*dc));

                out << desc << ' ';
            }
            out << '\n';
        }
        //out << '\n';
    }

    out.close();

    return;
}

bool LoadFromNVM(const char* file_name,
                 std::vector<std::vector<cv::Point2d> > &keypoints,
                 std::vector<std::vector<float> > &descriptors,
                 std::vector<cv::Point3d> &pointcloud){

    if(file_name == NULL)return false;
    std::ifstream in(file_name);

#ifdef GEO_DEBUG
    std::cout << "Loading cameras/points: " << file_name <<"\n" ;
#endif
    if(!in.is_open()) return false;

    std::string header;
    in >> header; //file header


    int ncam = 0, npoint = 0, nproj = 0;

    //////////////////////////////////////
    in >> npoint;   if(npoint <= 0) return false;
    keypoints = std::vector<std::vector<cv::Point2d> >(npoint);
    descriptors = std::vector<std::vector<float> >(npoint);
    pointcloud = std::vector<cv::Point3d>(npoint);

    //read image projections and 3D points.
    //point_data.resize(npoint);
    for(int i = 0; i < npoint; ++i)
    {

        cv::Point3d p3d;

        //float pt[3];
        int cc[3];//, npj;
        in  >> p3d.x >> p3d.y >> p3d.z;

        int n_cameras = -1;
        //Total cameras associated to this point
        in >> n_cameras;

        for(int j = 0; j < n_cameras; j++){

            cv::Point2d p2d;
            int cam_pos, feat_pos;
            in >> p2d.x;
            in >> p2d.y;

            keypoints[i].push_back(p2d);

            for(int d = 0; d < 128; d++)
            {
                unsigned int udesc;
                in >> udesc;
                descriptors[i].push_back(static_cast<float>(udesc) / 512.0);
            }
            //image_info[cam_pos]->setPtProj(feat_pos, i);
        }

        pointcloud[i] = p3d;

    }
    ///////////////////////////////////////////////////////////////////////////////
    std::cout << pointcloud.size() << " 3D points;\n ";// << nproj << " projections\n";

    return true;
}


//Load NVM (ours)
bool LoadFromNVM(const char* file_name,
                 std::vector<std::string>& imgnames,
                 std::map<int ,cv::Matx34d>& Pmats,
                 cv::Mat& K,
                 std::vector<CloudPoint>& pointcloud,
                 std::vector<cv::Vec3b>& pointcloudRGB,
                 std::map<int,int> &vws,
                 std::vector<ImageV*> &image_info){

    if(file_name == NULL)return false;
    std::ifstream in(file_name);

#ifdef GEO_DEBUG
    std::cout << "Loading cameras/points: " << file_name <<"\n" ;
#endif
    if(!in.is_open()) return false;

    K =  (cv::Mat_<double>(3,3) <<
          0.0, 0.0, 0.0,
          0.0, 0.0, 0.0,
          0.0, 0.0, 1.0);

    int rotation_parameter_num = 4;
    bool format_r9t = false;
    std::string token;
    if(in.peek() == 'N')
    {
        in >> token; //file header
        if(strstr(token.c_str(), "R9T"))
        {
            rotation_parameter_num = 9;    //rotation as 3x3 matrix
            format_r9t = true;
        }
    }

    int ncam = 0, npoint = 0, nproj = 0;
    // read # of cameras
    in >> ncam;  if(ncam <= 0) return false;

    //read the camera parameters
    std::map<int, cv::Matx34d> Pmats2;
    std::vector<std::string> proc_imgs;

    for(int i = 0; i < ncam; ++i)
    {
        double f,cx,cy, q[9], c[3], d[2];
        in >> token >> f ;
        //Se foi processada mas nao resultou na res
        if(f == 0){
            //camera_data[i].SetFocalLength(0);
            proc_imgs.push_back(token);
            continue;
        }
        in >> cx;
        in >> cy;

        K.at<double>(0,0) = f;
        K.at<double>(1,1) = f;
        K.at<double>(0,2) = cx;
        K.at<double>(1,2) = cy;

        for(int j = 0; j < 3; ++j){

            in >> Pmats2[i](j,0);
            in >> Pmats2[i](j,1);
            in >> Pmats2[i](j,2);
        }
        // . . . Measurement distortion | end of camera params
        in >> Pmats2[i](0,3) >> Pmats2[i](1,3) >> Pmats2[i](2,3) >> d[0] >> d[1];

        proc_imgs.push_back(token);
    }

    std::vector<bool> imgs_to_process;
    std::vector<int> redirect(proc_imgs.size(),-1);
    bool skip;
    for(int i = 0; i < imgnames.size(); i++){
        skip = false;
        for(int j = 0; j < proc_imgs.size(); j++){
            //std::vector<std::string> str1 = split(imgnames[i], '.');
            std::string str1 = split_on_limiter(imgnames[i],'.', false);
            //std::vector<std::string> str2 = split(proc_imgs[j], '.');
            std::string str2 = split_on_limiter(imgnames[i], '.', false);

            if(strcmp(str1.c_str(),str2.c_str()) == 0){
                if(Pmats2.find(j) != Pmats2.end()){
                    Pmats[i] = Pmats2.find(j)->second;
                    redirect[j] = i;
                    vws.insert(std::make_pair(i,1));
                }else{
                    vws.insert(std::make_pair(i,-1));
                }

                imgs_to_process.push_back(false);
                skip = true;
                continue;
            }
        }

        if(!skip)
            imgs_to_process.push_back(true);
    }

    //////////////////////////////////////
    in >> npoint;   if(npoint <= 0) return false;

    //read image projections and 3D points.
    //point_data.resize(npoint);
    for(int i = 0; i < npoint; ++i)
    {

        CloudPoint cp;
        cp.reprojection_error = 0;

        //float pt[3];
        int cc[3];//, npj;
        in  >> cp.pt.x >> cp.pt.y >> cp.pt.z
            >> cc[0] >> cc[1] >> cc[2];// >> npj;

        cv::Vec3b color(cc[0],cc[1],cc[2]);

        //cp.imgpt_for_img = std::map<int,int>();//std::vector<int>(imgs_to_process.size(), -1);

        int n_cameras = -1;
        //Total cameras associated to this point
        in >> n_cameras;

        for(int j = 0; j < n_cameras; j++){

            int cam_pos, feat_pos;
            in >> cam_pos;
            in >> feat_pos;

            cp.imgpt_for_img[cam_pos] = feat_pos;
            //image_info[cam_pos]->setPtProj(feat_pos, i);
        }

        pointcloud.push_back(cp);
        pointcloudRGB.push_back(color);

    }
    ///////////////////////////////////////////////////////////////////////////////
#ifdef GEO_DEBUG
    std::cout << ncam << " cameras; " << pointcloud.size() << " 3D points\n ";// << n_means << " projections\n";
#endif
    return true;
}


//Load NVM VisualSFM style
bool LoadFromNVM(const char* file_name,
                 std::vector<std::string>& imgnames,
                 std::map<int ,cv::Matx34d>& Pmats,
                 std::vector<cv::Mat> & Kmats,
                 std::vector<CloudPoint>& pointcloud,
                 std::vector<cv::Vec3b>& pointcloudRGB,
                 int type){

    if(file_name == NULL)return false;
    std::ifstream in(file_name);

#ifdef GEO_DEBUG
    std::cout << "Loading cameras/points: " << file_name <<"\n" ;
#endif
    if(!in.is_open()) return false;



    int ncam = 0, npoint = 0, nproj = 0;

    if(type == 0){
        int rotation_parameter_num = 4;
        bool format_r9t = false;
        std::string token;
        if(in.peek() == 'N')
        {
            in >> token; //file header
            if(strstr(token.c_str(), "R9T"))
            {
                rotation_parameter_num = 9;    //rotation as 3x3 matrix
                format_r9t = true;
            }
        }

        // read # of cameras
        in >> ncam;  if(ncam <= 0) return false;

        //read the camera parameters

        for(int i = 0; i < ncam; ++i)
        {
            double f,cx,cy, q[9], c[3], d[2];
            in >> token >> f ;
            //Se foi processada mas nao resultou na res
            /*if(f == 0){
                //camera_data[i].SetFocalLength(0);
                proc_imgs.push_back(token);
                continue;
            }//*/
            in >> cx;
            in >> cy;

            cv::Mat K =  (cv::Mat_<double>(3,3) <<
                          0.0, 0.0, 0.0,
                          0.0, 0.0, 0.0,
                          0.0, 0.0, 1.0);

            K.at<double>(0,0) = f;
            K.at<double>(1,1) = f;
            K.at<double>(0,2) = cx;
            K.at<double>(1,2) = cy;
            Kmats.push_back(K);

            for(int j = 0; j < 3; ++j){

                in >> Pmats[i](j,0);
                in >> Pmats[i](j,1);
                in >> Pmats[i](j,2);
            }
            // . . . Measurement distortion | end of camera params
            in >> Pmats[i](0,3) >> Pmats[i](1,3) >> Pmats[i](2,3) >> d[0] >> d[1];

            imgnames.push_back(token);
        }

    }else{
        int rotation_parameter_num = 4;
        bool format_r9t = false;
        std::string token;
        if(in.peek() == 'N')
        {
            in >> token;
        }

        // read # of cameras
        in >> ncam;  if(ncam <= 1) return false;

        //read the camera parameters
        for(int i = 0; i < ncam; ++i)
        {
            double f, c[3];
            in >> token;
            in >> f ;
            //TODO: cx, cy
            cv::Mat K =  (cv::Mat_<double>(3,3) <<
                          0.0, 0.0, 0.0,
                          0.0, 0.0, 0.0,
                          0.0, 0.0, 1.0);

            K.at<double>(0,0) = f;
            K.at<double>(1,1) = f;
            //K.at<double>(0,2) = 1300.0 / 2.0;
            //K.at<double>(1,2) = 975.0 / 2.0;
            Kmats.push_back(K);

            double w,x,y,z, radial;
            int line_e;
            cv::Matx31d C;

            in >> w;
            in >> x;
            in >> y;
            in >> z;

           // double norm = sqrt(pow(w,2.0) + pow(x,2.0) + pow(y,2.0) + pow(z,2.0));

            /*w /= norm;
            x /= norm;
            y /= norm;
            z /= norm;//*/

            cv::Matx33d R(pow(w,2.0) + pow(x,2.0) - pow(y,2.0) - pow(z,2.0),
                          2 * x * y - 2 * z * w,
                          2 * w * y + 2 * x * z,

                          2 * x * y + 2 * z * w,
                          pow(w,2.0) - pow(x,2.0) + pow(y,2.0) - pow(z,2.0),
                          2 * y * z - 2 * x * w,

                          2 * x * z - 2 * y * w,
                          2 * y * z + 2 * x * w,
                          pow(w,2.0) - pow(x,2.0) - pow(y,2.0) + pow(z,2.0)
                         );

            in >> c[0];
            in >> c[1];
            in >> c[2];

            C = cv::Matx31d(c[0],c[1],c[2]);

            cv::Matx31d T = - R * C;

            Pmats[i] = cv::Matx34d(R(0,0), R(0,1), R(0,2), T(0,0),
                                   R(1,0), R(1,1), R(1,2), T(1,0),
                                   R(2,0), R(2,1), R(2,2), T(2,0));

            in >> radial;

            in >> line_e;

            //proc_imgs.push_back(token);
            imgnames.push_back(token);
        }

    }

    //////////////////////////////////////

    in >> npoint;   if(npoint <= 0) return false;



    //read image projections and 3D points.
    //point_data.resize(npoint);
    for(int i = 0; i < npoint; ++i)
    {

        CloudPoint cp;
        cp.reprojection_error = 0;

        //float pt[3];
        int cc[3];//, npj;
        in  >> cp.pt.x >> cp.pt.y >> cp.pt.z
            >> cc[0] >> cc[1] >> cc[2];// >> npj;

        cv::Vec3b color(cc[0],cc[1],cc[2]);

        int n_cameras = -1;
        //Total cameras associated to this point
        in >> n_cameras;

        for(int j = 0; j < n_cameras; j++){

            int cam_pos, feat_pos;
            double x,y;
            in >> cam_pos;
            in >> feat_pos;
            if(type == 1){
                in >> x;
                in >> y;
            }
            cp.imgpt_for_img[cam_pos] = feat_pos;
            //image_info[cam_pos]->setPtProj(feat_pos, i);
        }

        pointcloud.push_back(cp);
        pointcloudRGB.push_back(color);

    }
    ///////////////////////////////////////////////////////////////////////////////
#ifdef GEO_DEBUG
    std::cout << ncam << " cameras; " << pointcloud.size() << " 3D points\n ";// << n_means << " projections\n";
#endif
    return true;
}





//Load NVM just for visualization
bool LoadFromNVM(const char* file_name,
                 std::map<int ,cv::Matx34d>& Pmats,
                 std::vector<cv::Point3d>& pointcloud,
                 std::vector<cv::Vec3b>& pointcloudRGB,
                 int type){

    //std::vector<CameraT> camera_data;


    if(file_name == NULL)return false;
    std::ifstream in(file_name);

#ifdef GEO_DEBUG
    std::cout << "Loading cameras/points: " << file_name <<"\n" ;
#endif
    if(!in.is_open()) return false;

    cv::Mat K =  (cv::Mat_<double>(3,3) <<
          0.0, 0.0, 0.0,
          0.0, 0.0, 0.0,
          0.0, 0.0, 1.0);

    int ncam = 0, npoint = 0, nproj = 0;
    std::map<int, cv::Matx34d> Pmats2;

    int rotation_parameter_num = 4;
    bool format_r9t = false;
    std::string token;
    if(type == 0){
        if(in.peek() == 'N')
        {
            in >> token; //file header
            if(strstr(token.c_str(), "R9T"))
            {
                rotation_parameter_num = 9;    //rotation as 3x3 matrix
                format_r9t = true;
            }
        }

        // read # of cameras
        in >> ncam;  if(ncam <= 0) return false;

        //read the camera parameters
        for(int i = 0; i < ncam; ++i)
        {
            double f, q[9], c[3], d[2],cx ,cy;
            in >> token >> f ;
            //Se foi processada mas nao resultou na res
            if(f == 0){
                //camera_data[i].SetFocalLength(0);
                continue;
            }

            in >> cx;
            in >> cy;

            K.at<double>(0,0) = f;
            K.at<double>(1,1) = f;
            K.at<double>(0,2) = cx;
            K.at<double>(1,2) = cy;

            for(int j = 0; j < 3; ++j){

                in >> Pmats2[i](j,0);
                in >> Pmats2[i](j,1);
                in >> Pmats2[i](j,2);
            }
            // . . . Measurement distortion | end of camera params
            in >> Pmats2[i](0,3) >> Pmats2[i](1,3) >> Pmats2[i](2,3) >> d[0] >> d[1];

        }
    }else{

        in >> token;
        // read # of cameras
        in >> ncam;  if(ncam <= 1) return false;

        //read the camera parameters
        for(int i = 0; i < ncam; ++i)
        {
            double f,cx,cy, q[9], c[3], d[2];
            in >> token;
            in >> f ;

            //in >> cx;
            //in >> cy;

            //TODO: cx, cy
            cv::Mat K =  (cv::Mat_<double>(3,3) <<
                          0.0, 0.0, 0.0,
                          0.0, 0.0, 0.0,
                          0.0, 0.0, 1.0);

            K.at<double>(0,0) = f;
            K.at<double>(1,1) = f;
            //K.at<double>(0,2) = 1300.0 / 2.0;
            //K.at<double>(1,2) = 975.0 / 2.0;

            double w,x,y,z, radial;
            int line_e;
            cv::Matx31d C;

            in >> w;
            in >> x;
            in >> y;
            in >> z;

            double norm = sqrt(pow(w,2.0) + pow(x,2.0) + pow(y,2.0) + pow(z,2.0));

            /*w /= norm;
            x /= norm;
            y /= norm;
            z /= norm;//*/

            cv::Matx33d R(pow(w,2.0) + pow(x,2.0) - pow(y,2.0) - pow(z,2.0),
                          2 * x * y - 2 * z * w,
                          2 * w * y + 2 * x * z,

                          2 * x * y + 2 * z * w,
                          pow(w,2.0) - pow(x,2.0) + pow(y,2.0) - pow(z,2.0),
                          2 * y * z - 2 * x * w,

                          2 * x * z - 2 * y * w,
                          2 * y * z + 2 * x * w,
                          pow(w,2.0) - pow(x,2.0) - pow(y,2.0) + pow(z,2.0)
                         );

            in >> c[0];
            in >> c[1];
            in >> c[2];

            C = cv::Matx31d(c[0],c[1],c[2]);

            cv::Matx31d T = - R * C;

            Pmats2[i] = cv::Matx34d(R(0,0), R(0,1), R(0,2), T(0,0),
                                   R(1,0), R(1,1), R(1,2), T(1,0),
                                   R(2,0), R(2,1), R(2,2), T(2,0));

            in >> radial;

            in >> line_e;

            //proc_imgs.push_back(token);
        }

    }
    Pmats = Pmats2;

    //////////////////////////////////////
    in >> npoint;
    if(npoint <= 0) return false;

    //read image projections and 3D points.
    //point_data.resize(npoint);
    int n_means = 0;
    for(int i = 0; i < npoint; ++i)
    {

        cv::Point3d cp;
        //float pt[3];
        int cc[3];//, npj;
        in  >> cp.x >> cp.y >> cp.z
            >> cc[0] >> cc[1] >> cc[2];// >> npj;
        int smth, n_cams;
        double x,y;
        in >> n_cams;
        for(int j = 0; j < n_cams; j++){
            //if(!imgs_to_process[j])
                in >> smth;
                in >> smth;
                if(type == 1)
                {
                    in >> x;
                    in >> y;
                }
        }

        n_means += n_cams;

        cv::Vec3b color(cc[0],cc[1],cc[2]);

        pointcloud.push_back(cp);
        pointcloudRGB.push_back(color);

    }
    ///////////////////////////////////////////////////////////////////////////////
#ifdef GEO_DEBUG
    std::cout << ncam << " cameras; " << pointcloud.size() << " 3D points " << n_means << " projections\n";
#endif
    return true;
}





