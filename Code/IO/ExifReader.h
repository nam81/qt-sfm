#include "exiv2/exiv2.hpp"
#include "../Common.h"


bool ExtractKmatrix(std::string img_path, cv::Mat &K);

bool ExtractGeotags(std::string name,
                    GPSCoords &geotags_known);

bool SaveToOutput(std::string filename,
                  std::string imgs_path,
                  std::vector<std::string> posed_names,
                  std::vector<GPSCoords > gps_coords,
                  std::vector<std::string> posed_locations,
                  std::vector<std::string> posed_directions);

bool ReadCoordsFile(std::string filename,
                    std::map<std::string, GPSCoords> &coordinates);
