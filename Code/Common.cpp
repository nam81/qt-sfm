#include "Common.h"
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>

#ifndef WIN32
#include <dirent.h>
#endif

using namespace std;
using namespace cv;

/*
std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems) {
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}
std::vector<std::string> split(const std::string &s, char delim) {
    std::vector<std::string> elems;
    split(s, delim, elems);
    return elems;
}*/


std::vector<cv::DMatch> FlipMatches(const std::vector<cv::DMatch>& matches) {
    std::vector<cv::DMatch> flip;
    for(int i=0;i<matches.size();i++) {
        flip.push_back(matches[i]);
        swap(flip.back().queryIdx,flip.back().trainIdx);
    }
    return flip;
}

std::vector<cv::Point3d> CloudPointsToPoints(const std::vector<CloudPoint> cpts) {
    std::vector<cv::Point3d> out;
    for (unsigned int i=0; i<cpts.size(); i++) {
        out.push_back(cpts[i].pt);
    }
    return out;
}

void GetAlignedPointsFromMatch(const std::vector<cv::KeyPoint>& imgpts1,
                               const std::vector<cv::KeyPoint>& imgpts2,
                               const std::vector<cv::DMatch>& matches,
                               std::vector<cv::KeyPoint>& pt_set1,
                               std::vector<cv::KeyPoint>& pt_set2){
    for (unsigned int i=0; i<matches.size(); i++) {
        //cout << "matches[i].queryIdx " << matches[i].queryIdx << " matches[i].trainIdx " << matches[i].trainIdx << endl;
        pt_set1.push_back(imgpts1[matches[i].queryIdx]);
        pt_set2.push_back(imgpts2[matches[i].trainIdx]);
    }
}

void KeyPointsToPoints(const vector<KeyPoint>& kps, vector<Point2f>& ps) {
    ps.clear();
    for (unsigned int i=0; i<kps.size(); i++) ps.push_back(kps[i].pt);
}

void PointsToKeyPoints(const vector<Point2f>& ps, vector<KeyPoint>& kps) {
    kps.clear();
    for (unsigned int i=0; i<ps.size(); i++) kps.push_back(KeyPoint(ps[i],1.0f));
}

#define intrpmnmx(val,min,max) (max==min ? 0.0 : ((val)-min)/(max-min))


bool hasEnding (std::string const &fullString, std::string const &ending)
{
    if (fullString.length() >= ending.length()) {
        return (0 == fullString.compare (fullString.length() - ending.length(), ending.length(), ending));
    } else {
        return false;
    }
}

bool hasEndingLower (string const &fullString_, string const &_ending)
{
    string fullstring = fullString_, ending = _ending;
    transform(fullString_.begin(),fullString_.end(),fullstring.begin(),::tolower); // to lower
    return hasEnding(fullstring,ending);
}

void imshow_250x250(const string& name_, const Mat& patch) {
    Mat bigpatch; cv::resize(patch,bigpatch,Size(250,250));
    imshow(name_,bigpatch);
}

int BasifyFilename(const char *filename, char *base)
{
    strcpy(base, filename);
    base[strlen(base) - 4] = 0;

    return 0;
}


std::string split_on_limiter(std::string to_split, char limiter, bool side){
    std::string splitted;
    int lastindex = to_split.find_last_of(limiter);

    if(!side){
        splitted = to_split.substr(0,lastindex + 1);
    }else{
        splitted = to_split.substr(lastindex + 1, to_split.size());

    }

    return splitted;
}

void open_imgs_dir(const char* dir_name, std::vector<std::string>& images_read, bool synthetic){

    if (dir_name == NULL) {
        return;
    }

    string dir_name_ = string(dir_name);
    vector<string> files_;

    DIR *dp;
    struct dirent *ep;
    dp = opendir (dir_name);

    if (dp != NULL)
    {
        while (ep = readdir (dp)) {
            if (ep->d_name[0] != '.')
                files_.push_back(ep->d_name);
        }

        (void) closedir (dp);
        std::sort(files_.begin(), files_.end());
    }
    else {
        cerr << ("Couldn't open the directory");
        return;
    }

    for (unsigned int i=0; i<files_.size(); i++) {
        if(synthetic == false){
            if (files_[i][0] == '.' || !(hasEndingLower(files_[i],"jpg")||hasEndingLower(files_[i],"png"))) {
                continue;
            }

            images_read.push_back(files_[i]);
        }else{
            if (files_[i][0] == '.' || !(hasEndingLower(files_[i],"sift"))) {
                continue;
            }

            images_read.push_back(files_[i]);
        }
    }
}
