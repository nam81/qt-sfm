#pragma once
#include "AbstractFeatureMatcher.h"
#include "../../3rdparty/siftgpu/src/SiftGPU/SiftGPU.h"

class SiftGPUMatcher  : public AbstractFeatureMatcher{

    SiftGPU *sift;
    SiftMatchGPU *matcher;// = new SiftMatchGPU(4096);
    void * hsiftgpu;

    //Preemptive Matching Stuff
    bool use_preemptive;
    int preemptive_h;
    int preemptive_th;
    std::string path;


public:
    SiftGPUMatcher(std::string p,
                   bool _use_preemptive,
                   int _preemptive_h,
                   int _preemptive_th,
                   int option);

    void ExtractFeatures(std::string imgnames, std::vector<cv::KeyPoint> &imgpts, std::vector<float> &imgdesc);
    bool MatchFeatures(std::vector<float> desc1, int size1, std::vector<float> desc2, int size2, int max_sift, std::vector<cv::DMatch>* matches);


    void SetCuda(char* graphics_id);
    void SetDA();
    void SetMaxD(char* maxd);
    void SetOptions(char *f_octave, char* tc, char *max_features, char *n_orientations);
    void CreateExtractorContext();
    void CreateMatcherContext();

    void VerifyExtractorContext();
    void VerifyMatcherContext();
    void UnloadSift();




};

