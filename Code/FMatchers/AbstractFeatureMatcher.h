#pragma once

#include "../Common.h"

//-------- Abstract class for every feature matcher --------//
class AbstractFeatureMatcher {

public:
    AbstractFeatureMatcher(){}
    virtual void ExtractFeatures(std::string imgnames, std::vector<cv::KeyPoint> &imgpts, std::vector<float> &imgdesc) = 0;
    virtual bool MatchFeatures(std::vector<float> desc1, int size1, std::vector<float> desc2, int size2,int max_sift, std::vector<cv::DMatch>* matches) = 0;
    virtual void CreateMatcherContext() = 0;
    virtual void CreateExtractorContext() = 0;
    virtual void VerifyExtractorContext() = 0;
    virtual void VerifyMatcherContext() = 0;
    virtual void UnloadSift() = 0;
    virtual void SetOptions(char *f_octave, char* tc, char *max_features, char *n_orientations) = 0;
    virtual void SetCuda(char* graphics_id) = 0;


};
