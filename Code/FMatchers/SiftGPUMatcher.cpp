#include <stdlib.h>
#include <string>
#include <vector>
#include <iostream>
#include <math.h>
//#include "../Readers/FeaturePoints.h"
//#include "../Readers/MatchFile.h"
#include "SiftGPUMatcher.h"
#include "../../3rdparty/siftgpu/src/SiftGPU/SiftGPU.h"
#include <opencv2/video/video.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/cvflann/flann.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <set>
#include <cstdlib>


using namespace std;
using namespace cv;



//#define SIFTGPU_CUDA


////////////////////////////////////////////////////////////////////////////
#if !defined(SIFTGPU_STATIC) && !defined(SIFTGPU_DLL_RUNTIME)
// SIFTGPU_STATIC comes from compiler
#define SIFTGPU_DLL_RUNTIME
// Load at runtime if the above macro defined
// comment the macro above to use static linking
#endif

//#define DEBUG_SIFTGPU  //define this to use the debug version in windows

#ifdef SIFTGPU_DLL_RUNTIME
#include <dlfcn.h>
#define FREE_MYLIB dlclose
#define GET_MYPROC dlsym
#endif

//Convert SiftKeypoint to cv::KeyPoint
void convert_keypoints(std::vector<std::vector<cv::KeyPoint> >& imgpts, std::vector<vector<SiftGPU::SiftKeypoint> >& keys_l){
    for(int i = 0; i < (int) keys_l.size(); i++){
        std::vector<cv::KeyPoint> keys;

        vector<SiftGPU::SiftKeypoint> keys_sift;
        keys_sift = keys_l[i];

        for(int j = 0; j < keys_sift.size(); j++){
            cv::KeyPoint k;
            SiftGPU::SiftKeypoint l = keys_sift[j];

            k.angle = abs(l.o);
            k.size  = l.s;
            cv::Point po;
            po.x = l.x;
            po.y = l.y;
            k.pt = po;

            keys.push_back(k);
        }
        imgpts[i] = keys;
    }

}

//Execute SiftGPU extractor
SiftGPUMatcher::SiftGPUMatcher(std::string p,
                               bool _use_preemptive,
                               int _preemptive_h,
                               int _preemptive_th,
                               int option) :
    AbstractFeatureMatcher(),
    path(p),
    use_preemptive(_use_preemptive),
    preemptive_h(_preemptive_h),
    preemptive_th(_preemptive_th)
{

#ifdef SIFTGPU_DLL_RUNTIME

    hsiftgpu = dlopen("libsiftgpu.so", RTLD_LAZY);


    if(hsiftgpu == NULL){
        std::cout << "ERROR: libsiftgpu.so didnt load correctly." << std::endl;
        exit(0);
    }
    SiftGPU* (*pCreateNewSiftGPU)(int) = NULL;
    SiftMatchGPU* (*pCreateNewSiftMatchGPU)(int) = NULL;
    pCreateNewSiftGPU = (SiftGPU* (*) (int)) GET_MYPROC(hsiftgpu, "CreateNewSiftGPU");
    pCreateNewSiftMatchGPU = (SiftMatchGPU* (*)(int)) GET_MYPROC(hsiftgpu, "CreateNewSiftMatchGPU");
    sift = pCreateNewSiftGPU(1);
    matcher = pCreateNewSiftMatchGPU(4096);

        //process parameters
        //The following parameters are default in V340
        //-m,       up to 2 orientations for each feature (change to single orientation by using -m 1)
        //-s        enable subpixel subscale (disable by using -s 0)



        //-fo -1    staring from -1 octave
        //-v 1      only print out # feature and overall time
        //-loweo    add a (.5, .5) offset
        //-tc <num> set a soft limit to number of detected features

        //NEW:  parameters for  GPU-selection
        //1. CUDA.                   Use parameter "-cuda", "[device_id]"
        //2. OpenGL.				 Use "-Display", "display_name" to select monitor/GPU (XLIB/GLUT)
        //   		                 on windows the display name would be something like \\.\DISPLAY4

        //////////////////////////////////////////////////////////////////////////////////////
        //You use CUDA for nVidia graphic cards by specifying
        //-cuda   : cuda implementation (fastest for smaller images)
        //          CUDA-implementation allows you to create multiple instances for multiple threads
        //          Checkout src\TestWin\MultiThreadSIFT
        /////////////////////////////////////////////////////////////////////////////////////

        //////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////Two Important Parameters///////////////////////////
        // First, texture reallocation happens when image size increases, and too many
        // reallocation may lead to allocatoin failure.  You should be careful when using
        // siftgpu on a set of images with VARYING imag sizes. It is recommended that you
        // preset the allocation size to the largest width and largest height by using function
        // AllocationPyramid or prameter '-p' (e.g. "-p", "1024x768").

        // Second, there is a parameter you may not be aware of: the allowed maximum working
        // dimension. All the SIFT octaves that needs a larger texture size will be skipped.
        // The default prameter is 2560 for the unpacked implementation and 3200 for the packed.
        // Those two default parameter is tuned to for 768MB of graphic memory. You should adjust
        // it for your own GPU memory. You can also use this to keep/skip the small featuers.
        // To change this, call function SetMaxDimension or use parameter "-maxd".
        //
        // NEW: by default SiftGPU will try to fit the cap of GPU memory, and reduce the working
        // dimension so as to not allocate too much. This feature can be disabled by -nomc
        //////////////////////////////////////////////////////////////////////////////////////



        ///////////////////////////////////////////////////////////////////////
        //Only the following parameters can be changed after initialization (by calling ParseParam).
        //-dw, -ofix, -ofix-not, -fo, -unn, -maxd, -b
        //to change other parameters at runtime, you need to first unload the dynamically loaded libaray
        //reload the libarary, then create a new siftgpu instance


        //Create a context for computation, and SiftGPU will be initialized automatically
        //The same context can be used by SiftMatchGPU

#endif
}


void SiftGPUMatcher::SetCuda(char* graphics_id){

    char* args[] = {"-cuda", graphics_id};
    sift->ParseParam(2, args);


}

void SiftGPUMatcher::SetDA(){
    char * args[] = {"-da"};

    sift->ParseParam(1, args);
}
/*void SiftGPUMatcher::SetMaxD(char* maxd){
    char * args[] = {"-maxd", maxd};

    //sift->ParseParam(2, args);

    char * argv[] = {"-cuda", "1",
                         "-fo", "0",
                         "-tc2",
                         "1280",
                         "-m",
                         "2",
                         "-v", "0",
                         "-loweo"
                         ,"-maxd",
                         "2048"
                        };
    int argc = sizeof(argv)/sizeof(char*);

    sift->ParseParam(argc, argv);

}*/


void SiftGPUMatcher::SetOptions(char* f_octave, char* tc, char* max_features, char* n_orientations){
    char * args[] = {
        "-fo", f_octave,
        tc, max_features,
        "-m", n_orientations,
        "-v", "1",
        "-loweo"
    };

    int argc = sizeof(args)/sizeof(char*);

    sift->ParseParam(argc, args);
}


//=================================================== Extractors ==============================================================

void showFeatures(std::string imgpath, std::vector<cv::KeyPoint> kps){

    Mat image = imread(imgpath.c_str());

    std::cout << "[SIFT] Showing " << kps.size() << " features" << std::endl;

    Mat outputImage;
    Scalar keypointColor = Scalar(255, 0, 0);     // Blue keypoints.
    drawKeypoints(image, kps, outputImage, keypointColor, DrawMatchesFlags::DEFAULT);
    namedWindow("Output");
    imshow("Output", outputImage);
    cv::waitKey(0);
}


void SiftGPUMatcher::ExtractFeatures(std::string imgnames, std::vector<cv::KeyPoint> &imgpts, std::vector<float> &imgdesc){

    sift->VerifyContextGL();

    if(sift->RunSIFT((path + imgnames).c_str())) {

        //get feature count
        int size = sift->GetFeatureNum();

        //allocate memory
        std::vector<SiftGPU::SiftKeypoint> k(size);

        std::vector<float> d(128*size);

        //reading back feature vectors is faster than writing files
        //if you dont need keys or descriptors, just put NULLs here
        sift->GetFeatureVector(&k[0], &d[0]);

        imgdesc = std::vector<float>(size * 128);
        imgpts = std::vector<cv::KeyPoint>(size);

        for(int i = 0; i < k.size(); i++){
            cv::KeyPoint kp;

            kp.pt.x = k[i].x;
            kp.pt.y = k[i].y;
            kp.size = k[i].s;
            kp.angle = - k[i].o;

            imgpts[i] = kp;
            for(int j = 0; j < 128; j++){
                imgdesc[i * 128 + j] = d[i * 128 + j];
            }

        }


    }else{
        std::cerr << "Something went wrong" << std::endl;
    }
}

void SiftGPUMatcher::CreateExtractorContext(){
    sift->CreateContextGL();
}

void SiftGPUMatcher::VerifyExtractorContext(){
    sift->VerifyContextGL();
}

void SiftGPUMatcher::CreateMatcherContext(){
    //matcher->SetLanguage(SiftMatchGPU::SIFTMATCH_CUDA); // +i for the (i+1)-th device

    matcher->CreateContextGL();
}

void SiftGPUMatcher::VerifyMatcherContext(){
    matcher->VerifyContextGL();
}

void SiftGPUMatcher::UnloadSift(){
#ifdef SIFTGPU_DLL_RUNTIME
    delete sift;
    delete matcher;
    //FREE_MYLIB(hsiftgpu);
#endif//*/
}


//========================================================== Matchers =======================================================================

//Execute SiftGPU Matcher<
bool SiftGPUMatcher::MatchFeatures(std::vector<float> desc1, int size1, std::vector<float> desc2, int size2,int max_sift, std::vector<cv::DMatch>* matches){

    //**********************GPU SIFT MATCHING*********************************
    //**************************select shader language*************************
    //SiftMatchGPU will use the same shader lanaguage as SiftGPU by default
    //Before initialization, you can choose between glsl, and CUDA(if compiled).

    //matcher->SetLanguage(SiftMatchGPU::SIFTMATCH_GLSL); // +i for the (i+1)-th device

    //Verify current OpenGL Context and initialize the Matcher;
    //If you don't have an OpenGL Context, call matcher->CreateContextGL instead;
    matcher->VerifyContextGL(); //must call once

    //Set descriptors to match, the first argument must be either 0 or 1
    //if you want to use more than 4096 or less than 4096
    //call matcher->SetMaxSift() to change the limit before calling setdescriptor

    if(size1 == desc1.size()) size1 /= 128;
    if(size2 == desc2.size()) size2 /= 128;

    int num_match = -1;
    int preemptive_size = preemptive_h * 128;

    //match and get result.
    int (*aux_match_buf)[2] = new int[preemptive_size][2];
    int (*match_buf)[2]     = new int[size1][2];


        if(use_preemptive//*/
                ){

            std::vector<float> desc_aux_i(preemptive_size);
            std::vector<float> desc_aux_j(preemptive_size);

            for(int i = 0; i < preemptive_size; i++){
                desc_aux_i[i] = desc1[i];
                desc_aux_j[i] = desc2[i];
            }

            matcher->SetDescriptors(0, preemptive_h, &desc_aux_i[0]); //image 1
            matcher->SetDescriptors(1, preemptive_h, &desc_aux_j[0]); //image 2*/


            num_match = matcher->GetSiftMatch(preemptive_h, aux_match_buf, 0.7,0.8);
        }else{
            num_match = preemptive_th;
        }

        if(num_match >= preemptive_th){
            //matcher->VerifyContextGL(); //must call once

            matcher->SetMaxSift(max_sift);
            matcher->SetDescriptors(0, size1, &desc1[0]); //image 1
            matcher->SetDescriptors(1, size2, &desc2[0]); //image 2


            //use the default thresholds. Check the declaration in SiftGPU.h
            num_match = matcher->GetSiftMatch(size1, match_buf, 0.7, 0.8);
            //std::cout << "Found " << num_match << "matches" << std::endl;

            //write_matches(pic_name1[0], pic_name2[0], sizes[idx_i], sizes[idx_j], num_match, match_buf);

        }else{
            //std::cout << "Pair [" << idx_i << ", " << idx_j << "]" << " didn't pass the preemptive test [" << num_match << "]" << std::endl;
            match_buf = aux_match_buf;
            return false;
        }




    for(int i = 0; i < num_match; i++){
        DMatch dmatch;

        dmatch.distance = acos(match_buf[i][0] * match_buf[i][1]);
        dmatch.imgIdx   = 0;
        dmatch.queryIdx = match_buf[i][0];
        dmatch.trainIdx = match_buf[i][1];
        matches->push_back(dmatch);
    }



    //delete match_buf, aux_match_buf;

    return true;
/*    delete matcher;

#ifdef SIFTGPU_DLL_RUNTIME
    FREE_MYLIB(hsiftgpu);
#endif//*/

}

