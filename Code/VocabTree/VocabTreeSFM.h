#pragma once
#include <vector>
#include <string>
#include <sstream>
#include "../../3rdparty/VocabTree2/VocabLib/VocabTree.h"
#include <map>
//#include "../Common.h"
#include "../../3rdparty/VocabTree2/VocabLib/keys2.h"
#include "../../3rdparty/VocabTree2/lib/imagelib/defines.h"
#include "../../3rdparty/VocabTree2/lib/imagelib/qsort.h"
#include <iostream>
//#include <ios>
#include <fstream>
#include <string>

class VocabTreeSFM{

protected:
    VocabTree database_tree; //Vocabulary Tree by Noah Snavely

    std::string vocab_learn_path; //Vocabulary path
    std::string vocab_db_path;    //Vocabulary Tree path

    bool use_tfidf;               //Use tfidf
    bool normalize;               //Normalize database

    DistanceType distance_type;   //Distance used in scoring

    int n_db_images;              //Number of database images

public:
    VocabTreeSFM(){}
    VocabTreeSFM(std::string v_r, std::string v_l, std::string v_db){
        database_tree = VocabTree();
        vocab_learn_path = (v_r + v_l);
        vocab_db_path = (v_r + v_db);
        normalize = false;
        use_tfidf = true;
        distance_type = DistanceMin;
    }

    //Builds a Vocabulary with the given photograph sift files
    bool BuildVocabTree(std::string path, std::vector<std::string> imgs_names, int depth, int bf, int restarts);

    //Builds or updates the existing vocabulary tree with the given 3D documents
    int BuildVocabDB(/*std::string path,*/ std::vector<std::string> imgs_names, std::vector<int>& voc_indexes);

    //Deletes Synthetic Views from the Vocabulary Tree
    int DeleteVocabImgs(std::vector<int> imgs_indexes);

    //Query Vocabulary Tree and retrieve the top best documents
    int QueryVocTree(/*std::string query_in,*/std::vector<float>  keys,int num_nbrs , std::vector<std::pair<int, double> >& matches_inference);

    //Loads an existing vocabulary tree
    bool LoadVocabTree(int sift_db_size);

    //Removes all inserted views from an existing vocabulary tree
    int ResetVocabTree();

    //Print scores retrieved from a query
    void PrintScores(std::vector<std::pair<int,double> > scores, int top_documents);

};
