/*
 * Copyright 2011-2012 Noah Snavely, Cornell University
 * (snavely@cs.cornell.edu).  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:

 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 *    copyright notice, this list of conditions and the following
 *    disclaimer in the documentation and/or other materials provided
 *    with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY NOAH SNAVELY ''AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL NOAH SNAVELY OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 * DAMAGE.
 *
 * The views and conclusions contained in the software and
 * documentation are those of the authors and should not be
 * interpreted as representing official policies, either expressed or
 * implied, of Cornell University.
 *
 */


/*
 * Some of the used function on Noah Snavely's code were
 * readapted to work with this code.
 */

#include <cstdio>
#include <time.h>
#include "VocabTreeSFM.h"
#include "../IO/IOSynthetics.h"

#define MAX_ARRAY_SIZE 8388608 // 2 ** 23

void VocabTreeSFM::PrintScores(std::vector<std::pair<int,double> > scores, int top_documents){

    int max = std::min(int(scores.size()), top_documents);
    for(int i = 0; i < max; i++){
        std::cout << " " << scores[i].first << " with score " << scores[i].second << std::endl;
    }
}

//Reads a 3D document file
unsigned char* ReadAndFilterKeys(const char *keyfile, int dim,
                                               double min_feature_scale,
                                               int max_keys, int &num_keys_out)
{

    /* Filter keys */
    std::vector<unsigned char> keys_char_before;// = new unsigned char[num_keys * dim];
    std::vector<float> scales;
    int num_keys;
    Read3DDoc(keyfile, scales, keys_char_before, num_keys);

    if (num_keys == 0) {
        num_keys_out = 0;
        return NULL;
    }

    unsigned char* keys_char = new unsigned char[num_keys * 128];


    int num_keys_filtered = 0;
    if (min_feature_scale == 0.0 && max_keys == 0) {
        for (int j = 0; j < num_keys * dim; j++) {
            keys_char[j] = keys_char_before[j];
        }
        num_keys_filtered = num_keys;
    } else {
        for (int j = 0; j < num_keys; j++) {
            if (scales[j] < min_feature_scale)
                continue;

            for (int k = 0; k < dim; k++) {
                keys_char[num_keys_filtered * dim + k] =
                    (unsigned char) keys_char_before[j * dim + k];
            }

            num_keys_filtered++;

            if (max_keys > 0 && num_keys_filtered >= max_keys)
                break;
        }
    }

    num_keys_out = num_keys_filtered;

    return keys_char;
}


//-----------------------------------------------------------------------------------------------------------
//Builds a Vocabulary with the given photograph sift files
bool VocabTreeSFM::BuildVocabTree(std::string path, std::vector<std::string> imgs_names, int depth, int bf, int restarts){

#ifdef GEO_DEBUG
    printf("Building tree with depth: %d, branching factor: %d, "
           "and restarts: %d\n", depth, bf, restarts);
#endif

    std::vector<std::string> key_files;// = imgs_names;
    for(int i = 0; i < imgs_names.size(); i++){
        //std::vector<std::string> pic_name = split(imgs_names[i], '.');

        key_files.push_back(path + imgs_names[i] /*+ ".sift"*/);
    }


    int num_files = (int) key_files.size();
    unsigned long total_keys = 0;
    for (int i = 0; i < num_files; i++) {
        int num_keys = GetNumberOfKeys(key_files[i].c_str());
        total_keys += num_keys;
    }
#ifdef GEO_DEBUG
    printf("Total number of keys: %lu\n", total_keys);
    fflush(stdout);
#endif
    int dim = 128;
    unsigned long long len = (unsigned long long) total_keys * dim;

    int num_arrays =
            len / MAX_ARRAY_SIZE + ((len % MAX_ARRAY_SIZE) == 0 ? 0 : 1);

    unsigned char **vs = new unsigned char *[num_arrays];
#ifdef GEO_DEBUG
    printf("Allocating %llu bytes in total, in %d arrays\n", len, num_arrays);
#endif
    unsigned long long total = 0;
    for (int i = 0; i < num_arrays; i++) {
        unsigned long long remainder = len - total;
        unsigned long len_curr = MIN(remainder, MAX_ARRAY_SIZE);
#ifdef GEO_DEBUG
        printf("Allocating array of size %lu\n", len_curr);
        fflush(stdout);
#endif
        vs[i] = new unsigned char[len_curr];

        remainder -= len_curr;
    }

    /* Create the array of pointers */
#ifdef GEO_DEBUG
    printf("Allocating pointer array of size %lu\n", 4 * total_keys);
    fflush(stdout);
#endif

    unsigned char **vp = new unsigned char *[total_keys];

    unsigned long off = 0;
    unsigned long curr_key = 0;
    int curr_array = 0;
    for (int i = 0; i < num_files; i++) {    
#ifdef GEO_DEBUG
        printf("  Reading keyfile %s\n", key_files[i].c_str());
        fflush(stdout);
#endif
        short int *keys;
        int num_keys = 0;

        keypt_t *info = NULL;
        num_keys = ReadKeyFile(key_files[i].c_str(), &keys);

        if (num_keys > 0) {
            for (int j = 0; j < num_keys; j++) {
                for (int k = 0; k < dim; k++) {
                    vs[curr_array][off + k] = keys[j * dim + k];
                }

                vp[curr_key] = vs[curr_array] + off;
                curr_key++;
                off += dim;
                if (off == MAX_ARRAY_SIZE) {
                    off = 0;
                    curr_array++;
                }
            }


            delete [] keys;

            if (info != NULL)
                delete [] info;
        }
    }

    VocabTree tree;
    tree.Build(total_keys, dim, depth, bf, restarts, vp);
    tree.Write(vocab_learn_path.c_str());

    return true;
}


//Builds or updates the existing vocabulary tree with the given 3D documents
int VocabTreeSFM::BuildVocabDB(/*std::string path,*/ std::vector<std::string> imgs_names, std::vector<int> &voc_indexes)
{
    //    printf("Usage: %s <list.in> <tree.in> <tree.out> [use_tfidf:1] "
    //           "[normalize:1] [start_id:0] [distance_type:1]\n",
    //           argv[0]);

    double min_feature_scale = 1.4;
    int start_id;

    switch (distance_type) {
    case DistanceDot:
#ifdef GEO_DEBUG
        printf("[VocabMatch] Using distance Dot\n");
#endif
        break;
    case DistanceMin:
#ifdef GEO_DEBUG
        printf("[VocabMatch] Using distance Min\n");
#endif
        break;
    default:
#ifdef GEO_DEBUG
            printf("[VocabMatch] Using no known distance!\n");
#endif
        break;
    }

        std::vector<std::string> key_files;
        for(int i = 0; i < imgs_names.size(); i++){
            //std::vector<std::string> pic_name = split(imgs_names[i], '.');

            key_files.push_back((/*path +*/ imgs_names[i]).c_str());
        }        

        int found_vocdb = database_tree.Read(vocab_db_path.c_str(), false);

        if(found_vocdb == 0){
            start_id = database_tree.GetMaxDatabaseImageIndex() + 1;
        }else{
            start_id = 0;
            database_tree.Read(vocab_learn_path.c_str(),true);
        }

#if 1
        database_tree.Flatten();
#endif

        database_tree.m_distance_type = distance_type;
        database_tree.SetInteriorNodeWeight(0.0);


        const int dim = 128;

        int num_imgs_to_insert = (int) key_files.size();
        unsigned long count = 0;

        /* Initialize leaf weights to 1.0 */
        database_tree.SetConstantLeafWeights();
        if(found_vocdb != 0){
            database_tree.ClearDatabase();
        }

        for (int i = 0; i < num_imgs_to_insert; i++) {
            int num_keys = 0;
            unsigned char *keys = ReadAndFilterKeys(key_files[i].c_str(),
                                                    dim, min_feature_scale,
                                                    0, num_keys);
#ifdef GEO_DEBUG
            printf("[VocabBuildDB] Adding vector %d (%d keys)\n",
                   start_id + i, num_keys);
#endif
            database_tree.AddImageToDatabase(start_id + i, num_keys, keys);

            voc_indexes.push_back(start_id + i);

            count += num_keys;

            if (num_keys > 0)
                delete [] keys;
        }

#ifdef GEO_DEBUG
        printf("[VocabBuildDB] Pushed %lu features\n", count);
        fflush(stdout);
#endif

        if (use_tfidf)
            database_tree.ComputeTFIDFWeights(0);

        if (normalize)
            database_tree.NormalizeDatabase(start_id, num_imgs_to_insert);

#ifdef GEO_DEBUG
        printf("[VocabBuildDB] Writing tree...\n");
#endif
        database_tree.Write(vocab_db_path.c_str());

        return 0;
}


//Deletes Synthetic Views from the Vocabulary Tree
int VocabTreeSFM::DeleteVocabImgs(std::vector<int> imgs_indexes){

    switch (distance_type) {
    case DistanceDot:
#ifdef GEO_DEBUG
        printf("[VocabMatch] Using distance Dot\n");
#endif
        break;
    case DistanceMin:
#ifdef GEO_DEBUG
        printf("[VocabMatch] Using distance Min\n");
#endif
        break;
    default:
#ifdef GEO_DEBUG
            printf("[VocabMatch] Using no known distance!\n");
#endif
        break;
    }


        //printf("[VocabBuildDB] Reading tree %s...\n", tree_in);
        //fflush(stdout);

        int found_vocdb = database_tree.Read(vocab_db_path.c_str(), false);

        if(found_vocdb == 0){
            //start_id = tree.GetMaxDatabaseImageIndex() + 1;
        }else{
            std::cout << "[ERROR] Cannot find voctree file" << std::endl;
            exit(0);
        }

#if 1
        database_tree.Flatten();
#endif

        database_tree.m_distance_type = distance_type;
        database_tree.SetInteriorNodeWeight(0.0);

        for(int i = 0; i < imgs_indexes.size(); i++)
            database_tree.RemoveVocImg((unsigned int) imgs_indexes[i]);


        /* Initialize leaf weights to 1.0 */
        database_tree.SetConstantLeafWeights();

        if (use_tfidf)
            database_tree.ComputeTFIDFWeights(0);//num_imgs_to_insert);

        //if (normalize)
        //    tree.NormalizeDatabase(start_id, num_imgs_to_insert);

#ifdef GEO_DEBUG
        printf("[VocabBuildDB] Writing tree...\n");
#endif
        database_tree.Write(vocab_db_path.c_str());

        return 0;

}


//Loads an existing vocabulary tree
bool VocabTreeSFM::LoadVocabTree(int sift_db_size){

    //DistanceType distance_type = DistanceMin;
    //bool normalize = true;

#ifdef GEO_DEBUG
    printf("[VocabMatch] Using tree %s\n", vocab_db_path.c_str());
#endif

    switch (distance_type) {
    case DistanceDot:
#ifdef GEO_DEBUG
        printf("[VocabMatch] Using distance Dot\n");
#endif
        break;
    case DistanceMin:
#ifdef GEO_DEBUG
        printf("[VocabMatch] Using distance Min\n");
#endif
        break;
    default:
#ifdef GEO_DEBUG
        printf("[VocabMatch] Using no known distance!\n");
#endif
        break;
    }

    /* Read the tree */
#ifdef GEO_DEBUG
    printf("[VocabMatch] Reading tree...\n");
    fflush(stdout);
#endif
    clock_t start = clock();
    //VocabTree tree;
    if(database_tree.Read(vocab_db_path.c_str(), false) == -1){
        std::cerr << "[Voctree] Error opening voctree --> " << vocab_db_path << std::endl;
        exit(0);
    }

    clock_t end = clock();
#ifdef GEO_DEBUG
    printf("[VocabMatch] Read tree in %0.3fs\n",
           (double) (end - start) / CLOCKS_PER_SEC);
#endif

#if 1
    database_tree.Flatten();
#endif

    database_tree.SetDistanceType(distance_type);
    database_tree.SetInteriorNodeWeight(0, 0.0);

    n_db_images = sift_db_size;

    return true;

}

//Query Vocabulary Tree and retrieve the top best documents
int VocabTreeSFM::QueryVocTree(std::vector<float> keys_i, int num_nbrs, std::vector<std::pair<int,double> >& matches_inference){

    int num_db_images = n_db_images;

#ifdef GEO_DEBUG
    printf("[VocabMatch] Read %d database images\n", num_db_images);
    /* Now score each query keyfile */
    printf("[VocabMatch] Scoring %d query images...\n", 1);//num_query_images);
    fflush(stdout);
#endif


#if 0
    FILE *f_html = fopen(output_html, "w");
    PrintHTMLHeader(f_html, num_nbrs);
#endif

    //float *scores = new float[num_db_images];
    std::map<unsigned int,float> scores;
    double *scores_d = new double[num_db_images];
    int *perm = new int[num_db_images];

    //for (int i = 0; i < num_query_images; i++) {
    clock_t start = clock();

    /* Clear scores */
    //for (int j = 0; j < num_db_images; j++)
    //    scores[j] = 0.0;

    unsigned char *keys = new unsigned char[keys_i.size()];

    for(int k_c = 0; k_c < keys_i.size(); k_c++ ){
        keys[k_c] = (unsigned char) static_cast<unsigned int>(floor(0.5f + 512.0f*keys_i[k_c]));
    }
    int num_keys = keys_i.size() / 128;

    //keys = ReadKeys(query_files[i].c_str(), dim, num_keys);
    clock_t end;
    clock_t start_score = clock();
    double mag = database_tree.ScoreQueryKeys(num_keys, normalize, keys, scores);
    clock_t end_score = end = clock();
#ifdef GEO_DEBUG
    printf("[VocabMatch] Scored image in %0.3fs "
           "( %0.3fs total, num_keys = %d, mag = %0.3f )\n",
           //query_in.c_str(),//query_files[i].c_str(),
           (double) (end_score - start_score) / CLOCKS_PER_SEC,
           (double) (end - start) / CLOCKS_PER_SEC, num_keys, mag);
#endif
    /* Find the top scores */
    double mean_scores =0.0;

    std::map<unsigned int, float>::iterator iter = scores.begin();
    std::vector<int> back_indexing;
    int j;
    for (j = 0; j < num_db_images; j++) {

        if(iter != scores.end()){
            back_indexing.push_back(iter->first);
            scores_d[j] = (double) scores[iter->first];
            mean_scores += scores_d[j];
            iter++;
        }
        else{
            scores_d[j] = 0.0;
        }
    }



    mean_scores /= num_db_images;

    qsort_descending();
    qsort_perm(num_db_images, scores_d, perm);

    int top = MIN(num_nbrs, num_db_images);
    top = MIN(top, (int) scores.size());

    for (int j = 0; j < top; j++) {
        // if (perm[j] == index_i)
        //     continue;
        //fprintf(f_match, "%d %d %0.4f\n", 0, perm[j], scores_d[j]);
        //if(scores[j] > mean_scores)
        double sec = scores_d[j];
        matches_inference.push_back(std::make_pair(back_indexing[perm[j]], sec));//[i].push_back(perm[j]);
        //fprintf(f_match, "%d %d %0.4f\n", i, perm[j], mag - scores_d[j]);
    }

#if 0
    PrintHTMLRow(f_html, query_files[i], scores_d,
                 perm, num_nbrs, db_files);
#endif

    delete [] keys;

#if 0
    PrintHTMLFooter(f_html);
    fclose(f_html);
#endif

    //delete [] scores;
    delete [] scores_d;
    delete [] perm;

    return 0;
}

//Removes all inserted views from an existing vocabulary tree
int VocabTreeSFM::ResetVocabTree(){
    int start_id;

    int found_vocdb = database_tree.Read(vocab_db_path.c_str(), false);

    if(found_vocdb == 0){
        start_id = database_tree.GetMaxDatabaseImageIndex() + 1;
    }else{
        std::cout << "[ERROR] Cannot find voctree file" << std::endl;
        exit(0);
    }
#if 1
    database_tree.Flatten();
#endif

    database_tree.m_distance_type = distance_type;
    database_tree.SetInteriorNodeWeight(0.0);


    /* Initialize leaf weights to 1.0 */
    database_tree.SetConstantLeafWeights();

    for(int i = 0; i < start_id; i++)
        database_tree.RemoveVocImg((unsigned int) i);

#ifdef GEO_DEBUG
    printf("[VocabBuildDB] Writing tree...\n");
#endif
    database_tree.Write(vocab_db_path.c_str());

    return 0;
}
