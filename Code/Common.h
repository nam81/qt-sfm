#pragma once

//#pragma warning(disable: 4244 18 4996 4800)

//#include <opencv2/core/core.hpp>
//#include <opencv2/features2d/features2d.hpp>
//#include <vector>
//#include <iostream>
//#include <list>
//#include <set>
//#include "3rdparty/siftgpu/src/SiftGPU/SiftGPU.h"
#include "ImageV.h"

struct CloudPoint {
    cv::Point3d pt;
    std::map<int,int> imgpt_for_img;
    double reprojection_error;
};

struct TwoVGeo{
    cv::Mat F;
    cv::Mat_<double> E;
    cv::Mat H;

    int n_inliers;
    int n_matches;
};


struct CameraV {
    cv::Matx34d P;

    cv::Mat K;
    cv::Mat_<double> Kinv;
    //cv::Mat K_32f;

    cv::Mat distortion_coeff;
    cv::Mat distcoeff_32f;

    std::string name;

    int id;

    //-1 to didnt get 3D space geom
    // 0 needs to be processed
    // 1 was already processed
    int reconstructed;

};

std::vector<cv::DMatch> FlipMatches(const std::vector<cv::DMatch>& matches);
void KeyPointsToPoints(const std::vector<cv::KeyPoint>& kps, std::vector<cv::Point2f>& ps);
void PointsToKeyPoints(const std::vector<cv::Point2f>& ps, std::vector<cv::KeyPoint>& kps);

std::vector<cv::Point3d> CloudPointsToPoints(const std::vector<CloudPoint> cpts);

void GetAlignedPointsFromMatch(const std::vector<cv::KeyPoint>& imgpts1,
                               const std::vector<cv::KeyPoint>& imgpts2,
                               const std::vector<cv::DMatch>& matches,
                               std::vector<cv::KeyPoint>& pt_set1,
                               std::vector<cv::KeyPoint>& pt_set2);

void drawArrows(cv::Mat& frame, const std::vector<cv::Point2f>& prevPts, const std::vector<cv::Point2f>& nextPts, const std::vector<uchar>& status, const std::vector<float>& verror, const cv::Scalar& line_color = cv::Scalar(0, 0, 255));

#ifdef USE_PROFILING
#define CV_PROFILE(msg,code)	{\
    std::cout << msg << " ";\
    double __time_in_ticks = (double)cv::getTickCount();\
    { code }\
    std::cout << "DONE " << ((double)cv::getTickCount() - __time_in_ticks)/cv::getTickFrequency() << "s" << std::endl;\
}
#else
#define CV_PROFILE(msg,code) code
#endif

int BasifyFilename(const char *filename, char *base);
void open_imgs_dir(const char* dir_name,std::vector<std::string>& images_names, bool synthetic);
void imshow_250x250(const std::string& name_, const cv::Mat& patch);

std::string split_on_limiter(std::string to_split, char limiter, bool side);

/*std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems);

std::vector<std::string> split(const std::string &s, char delim);
*/
class compare_1 { // simple comparison function
   public:
      bool operator()(const double x,const double y) { return (y-x)<0; } // returns x>y
};

