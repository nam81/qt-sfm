/*
 * Class taken from the SFMToyLib implementation.
 * Code located at: https://github.com/royshil/SfM-Toy-Library
 *
 */


#pragma once

#include <opencv2/calib3d/calib3d.hpp>
#include "../Common.h"

/**
 From "Triangulation", Hartley, R.I. and Sturm, P., Computer vision and image understanding, 1997
 */
cv::Mat_<double> LinearLSTriangulation(cv::Point3d u,		//homogenous image point (u,v,1)
                                       cv::Matx34d P,		//camera 1 matrix
                                       cv::Point3d u1,		//homogenous image point in 2nd camera
                                       cv::Matx34d P1		//camera 2 matrix
                                       );

#define EPSILON 0.0001
/**
 From "Triangulation", Hartley, R.I. and Sturm, P., Computer vision and image understanding, 1997
 */
cv::Mat_<double> IterativeLinearLSTriangulation(cv::Point3d u,	//homogenous image point (u,v,1)
                                                cv::Matx34d P,  //camera 1 matrix
                                                cv::Point3d u1,	//homogenous image point in 2nd camera
                                                cv::Matx34d P1	//camera 2 matrix
                                                );

double TriangulatePoints(const std::vector<cv::KeyPoint>& pt_set1,
                         const std::vector<cv::KeyPoint>& pt_set2,
                         const cv::Mat& K,
                         const cv::Mat& Kinv,
                         const cv::Mat& distcoeff,
                         const cv::Matx34d& P,
                         const cv::Matx34d& P1,
                         std::vector<CloudPoint>& pointcloud);

bool TestTriangulation(const std::vector<CloudPoint>& pcloud, const cv::Matx34d& P, std::vector<uchar>& status);

