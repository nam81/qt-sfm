#pragma once

#include "../Common.h"

#undef __SFM__DEBUG__

bool CheckCoherentRotation(cv::Mat_<double>& R);

cv::Mat FindFundamentalMat(const std::vector<cv::KeyPoint>& imgpts1,
                           const std::vector<cv::KeyPoint>& imgpts2,
                           std::vector<cv::DMatch>& matches,
                           int option);

cv::Mat FindHomography(std::vector<cv::KeyPoint> imgpts_i,
                       std::vector<cv::KeyPoint> imgpts_j,
                       std::vector<cv::DMatch> &matches_ij,
                       int &inliers,
                       int option);


bool FindPoseEstimation(cv::Mat_<double>& rvec,
                        cv::Mat_<double>& t,
                        cv::Mat_<double>& R,
                        cv::Mat K,
                        cv::Mat& distortion_coeff,
                        std::vector<cv::Point3f> ppcloud,
                        std::vector<cv::Point2f> imgPoints);

bool FindPoseEstimationPriori(cv::Mat_<double>& t,
                        cv::Mat_<double>& R,
                        cv::Mat K,
                        cv::Mat& distortion_coeff,
                        std::vector<cv::Point3f> &ppcloud,
                        std::vector<cv::Point2f> &imgPoints);

bool FindCameraMatrices(TwoVGeo tvg,
                        const cv::Mat& K,
                        const cv::Mat& Kinv,
                        const cv::Mat& distcoeff,
                        const std::vector<cv::KeyPoint>& imgpts1,
                        const std::vector<cv::KeyPoint>& imgpts2,
                        cv::Matx34d& P,
                        cv::Matx34d& P1,
                        std::vector<cv::DMatch>& matches,
                        std::vector<CloudPoint>& outCloud);

cv::Mat RefineFundamentalMat(const std::vector<cv::KeyPoint>& imgpts1,
                       const std::vector<cv::KeyPoint>& imgpts2,
                       std::vector<cv::KeyPoint>& imgpts1_good,
                       std::vector<cv::KeyPoint>& imgpts2_good,
                       std::vector<cv::DMatch>& matches);
