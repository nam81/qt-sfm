#include "FindCameraMatrices.h"
#include "../Projections/Triangulation.h"
#include <opencv2/core/core.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <vector>
#include <iostream>

#include <opencv2/calib3d/calib3d.hpp>

using namespace cv;
using namespace std;

//#include <Eigen/Eigen>
#define DECOMPOSE_SVD
//#define USE_EIGEN


#ifdef USE_EIGEN
#include <Eigen/Eigen>
#endif


#ifndef CV_PCA_DATA_AS_ROW
#define CV_PCA_DATA_AS_ROW 0
#endif

//========================================= Essential Matrix Stuff ========================================

void DecomposeEssentialUsingHorn90(double _E[9], double _R1[9], double _R2[9], double _t1[3], double _t2[3]) {
    //from : http://people.csail.mit.edu/bkph/articles/Essential.pdf
#ifdef USE_EIGEN
    using namespace Eigen;

    Matrix3d E = Map<Matrix<double,3,3,RowMajor> >(_E);
    Matrix3d EEt = E * E.transpose();
    Vector3d e0e1 = E.col(0).cross(E.col(1)),e1e2 = E.col(1).cross(E.col(2)),e2e0 = E.col(2).cross(E.col(0));
    Vector3d b1,b2;

#if 1
    //Method 1
    Matrix3d bbt = 0.5 * EEt.trace() * Matrix3d::Identity() - EEt; //Horn90 (12)
    Vector3d bbt_diag = bbt.diagonal();
    if (bbt_diag(0) > bbt_diag(1) && bbt_diag(0) > bbt_diag(2)) {
        b1 = bbt.row(0) / sqrt(bbt_diag(0));
        b2 = -b1;
    } else if (bbt_diag(1) > bbt_diag(0) && bbt_diag(1) > bbt_diag(2)) {
        b1 = bbt.row(1) / sqrt(bbt_diag(1));
        b2 = -b1;
    } else {
        b1 = bbt.row(2) / sqrt(bbt_diag(2));
        b2 = -b1;
    }
#else
    //Method 2
    if (e0e1.norm() > e1e2.norm() && e0e1.norm() > e2e0.norm()) {
        b1 = e0e1.normalized() * sqrt(0.5 * EEt.trace()); //Horn90 (18)
        b2 = -b1;
    } else if (e1e2.norm() > e0e1.norm() && e1e2.norm() > e2e0.norm()) {
        b1 = e1e2.normalized() * sqrt(0.5 * EEt.trace()); //Horn90 (18)
        b2 = -b1;
    } else {
        b1 = e2e0.normalized() * sqrt(0.5 * EEt.trace()); //Horn90 (18)
        b2 = -b1;
    }
#endif

    //Horn90 (19)
    Matrix3d cofactors; cofactors.col(0) = e1e2; cofactors.col(1) = e2e0; cofactors.col(2) = e0e1;
    cofactors.transposeInPlace();

    //B = [b]_x , see Horn90 (6) and http://en.wikipedia.org/wiki/Cross_product#Conversion_to_matrix_multiplication
    Matrix3d B1; B1 <<	0,-b1(2),b1(1),
                        b1(2),0,-b1(0),
                        -b1(1),b1(0),0;
    Matrix3d B2; B2 <<	0,-b2(2),b2(1),
                        b2(2),0,-b2(0),
                        -b2(1),b2(0),0;

    Map<Matrix<double,3,3,RowMajor> > R1(_R1),R2(_R2);

    //Horn90 (24)
    R1 = (cofactors.transpose() - B1*E) / b1.dot(b1);
    R2 = (cofactors.transpose() - B2*E) / b2.dot(b2);
    Map<Vector3d> t1(_t1),t2(_t2);
    t1 = b1; t2 = b2;

    cout << "Horn90 provided " << endl << R1 << endl << "and" << endl << R2 << endl;
#endif
}

bool CheckCoherentRotation(cv::Mat_<double>& R) {
    //std::cout << "R; " << R << std::endl;
    //double s = cv::norm(cv::abs(R),cv::Mat_<double>::eye(3,3),cv::NORM_L1);
    //std::cout << "Distance from I: " << s << std::endl;
    //if (s > 2.3) { // norm of R from I is large -> probably bad rotation
    //	std::cout << "rotation is probably not coherent.." << std::endl;
    //	return false;	//skip triangulation
    //}
    //Eigen::Map<Eigen::Matrix<double,3,3,Eigen::RowMajor> > eR(R[0]);
    //if(eR(2,0) < -0.9)
    //{
    //	cout << "rotate 180deg (PI rad) on Y" << endl;

    //	cout << "before" << endl << eR << endl;
    //	Eigen::AngleAxisd aad(-M_PI/2.0,Eigen::Vector3d::UnitY());
    //	eR *= aad.toRotationMatrix();
    //	cout << "after" << endl << eR << endl;
    //}
    //if(eR(0,0) < -0.9) {
    //	cout << "flip right vector" << endl;
    //	eR.row(0) = -eR.row(0);
    //}

    if(fabsf(determinant(R))-1.0 > 1e-07) {
        cerr << "det(R) != +-1.0, this is not a rotation matrix" << endl;
        return false;
    }

    return true;
}


void TakeSVDOfE(Mat_<double>& E, Mat& svd_u, Mat& svd_vt, Mat& svd_w) {
#if 1
    //Using OpenCV's SVD
    SVD svd(E);//,SVD::MODIFY_A);
    svd_u = svd.u;
    svd_vt = svd.vt;
    svd_w = svd.w;
#else
    //Using Eigen's SVD
    cout << "Eigen3 SVD..\n";
    Eigen::Matrix3f  e = Eigen::Map<Eigen::Matrix<double,3,3,Eigen::RowMajor> >((double*)E.data).cast<float>();
    Eigen::JacobiSVD<Eigen::MatrixXf> svd(e, Eigen::ComputeThinU | Eigen::ComputeThinV);
    Eigen::MatrixXf Esvd_u = svd.matrixU();
    Eigen::MatrixXf Esvd_v = svd.matrixV();
    svd_u = (Mat_<double>(3,3) << Esvd_u(0,0), Esvd_u(0,1), Esvd_u(0,2),
                          Esvd_u(1,0), Esvd_u(1,1), Esvd_u(1,2),
                          Esvd_u(2,0), Esvd_u(2,1), Esvd_u(2,2));
    Mat_<double> svd_v = (Mat_<double>(3,3) << Esvd_v(0,0), Esvd_v(0,1), Esvd_v(0,2),
                          Esvd_v(1,0), Esvd_v(1,1), Esvd_v(1,2),
                          Esvd_v(2,0), Esvd_v(2,1), Esvd_v(2,2));
    svd_vt = svd_v.t();
    svd_w = (Mat_<double>(1,3) << svd.singularValues()[0] , svd.singularValues()[1] , svd.singularValues()[2]);
#endif

    cout << "----------------------- SVD ------------------------\n";
    cout << "U:\n"<<svd_u<<"\nW:\n"<<svd_w<<"\nVt:\n"<<svd_vt<<endl;
    cout << "----------------------------------------------------\n";
}

bool DecomposeEtoRandT(
    Mat_<double>& E,
    Mat_<double>& R1,
    Mat_<double>& R2,
    Mat_<double>& t1,
    Mat_<double>& t2)
{
#ifdef DECOMPOSE_SVD
    //Using HZ E decomposition
    Mat svd_u, svd_vt, svd_w;
    TakeSVDOfE(E,svd_u,svd_vt,svd_w);

    //check if first and second singular values are the same (as they should be)
    double singular_values_ratio = fabsf(svd_w.at<double>(0) / svd_w.at<double>(1));
    if(singular_values_ratio>1.0) singular_values_ratio = 1.0/singular_values_ratio; // flip ratio to keep it [0,1]
    if (singular_values_ratio < 0.7) {
        cout << "singular values are too far apart\n";
        return false;
    }

    Matx33d W(0.0,-1.0,0.0,	//HZ 9.13
              1.0,0.0,0.0,
              0.0,0.0,1.0);
    Matx33d Wt(0.0,1.0,0.0,
               -1.0,0.0,0.0,
               0.0,0.0,1.0);

    R1 = svd_u * Mat(W) * svd_vt; //HZ 9.19
    R2 = svd_u * Mat(Wt) * svd_vt; //HZ 9.19
    t1 = svd_u.col(2); //u3
    t2 = -svd_u.col(2); //u3
#else
    //Using Horn E decomposition
    DecomposeEssentialUsingHorn90(E[0],R1[0],R2[0],t1[0],t2[0]);
#endif
    return true;
}

//===============================================================================================================


Mat FindFundamentalMat(const vector<KeyPoint>& imgpts1,
                       const vector<KeyPoint>& imgpts2,
                       vector<DMatch>& matches,
                       int option){

    //Try to eliminate keypoints based on the fundamental matrix
    //(although this is not the proper way to do this)
    vector<uchar> status(imgpts1.size());

#ifdef __SFM__DEBUG__
    std::vector< DMatch > good_matches_;
    std::vector<KeyPoint> keypoints_1, keypoints_2;
#endif
    //	undistortPoints(imgpts1, imgpts1, cam_matrix, distortion_coeff);
    //	undistortPoints(imgpts2, imgpts2, cam_matrix, distortion_coeff);
    //

    vector<KeyPoint> imgpts1_good;
    vector<KeyPoint> imgpts2_good;

    vector<KeyPoint> imgpts1_tmp ;
    vector<KeyPoint> imgpts2_tmp;

    if (matches.size() <= 0) {
        imgpts1_tmp = imgpts1;
        imgpts2_tmp = imgpts2;
    } else {
        GetAlignedPointsFromMatch(imgpts1, imgpts2, matches, imgpts1_tmp, imgpts2_tmp);
    }

    Mat F;
    {
        vector<Point2f> pts1,pts2;
        KeyPointsToPoints(imgpts1_tmp, pts1);
        KeyPointsToPoints(imgpts2_tmp, pts2);
#ifdef __SFM__DEBUG__
        cout << "pts1 " << pts1.size() << " (orig pts " << imgpts1_tmp.size() << ")" << endl;
        cout << "pts2 " << pts2.size() << " (orig pts " << imgpts2_tmp.size() << ")" << endl;
#endif
        double minVal,maxVal;
        cv::minMaxIdx(pts1,&minVal,&maxVal);
        if(option == 0)
            F = findFundamentalMat(pts1, pts2, FM_RANSAC, 0.006 * maxVal, 0.95, status); //threshold from [Snavely07 4.1]
        else
            F = findFundamentalMat(pts1, pts2, FM_LMEDS, 0.006 * maxVal, 0.95, status); //threshold from [Snavely07 4.1]

    }

    vector<DMatch> new_matches;
    //cout << "F keeping " << countNonZero(status) << " / " << status.size() << endl;
    for (unsigned int i=0; i<status.size(); i++) {
        if (status[i])
        {
            imgpts1_good.push_back(imgpts1_tmp[i]);
            imgpts2_good.push_back(imgpts2_tmp[i]);

            //new_matches.push_back(DMatch(matches[i].queryIdx,matches[i].trainIdx,matches[i].distance));
            new_matches.push_back(matches[i]);
#ifdef __SFM__DEBUG__
            good_matches_.push_back(DMatch(imgpts1_good.size()-1,imgpts1_good.size()-1,1.0));
            keypoints_1.push_back(imgpts1_tmp[i]);
            keypoints_2.push_back(imgpts2_tmp[i]);
#endif
        }
    }

    //cout << matches.size() << " matches before, " << new_matches.size() << " new matches after Fundamental Matrix\n";
    matches = new_matches; //keep only those points who survived the fundamental matrix

#if 0
    //-- Draw only "good" matches
#ifdef __SFM__DEBUG__
    if(!img_1.empty() && !img_2.empty()) {
        vector<Point2f> i_pts,j_pts;
        Mat img_orig_matches;
        { //draw original features in red
            vector<uchar> vstatus(imgpts1_tmp.size(),1);
            vector<float> verror(imgpts1_tmp.size(),1.0);
            img_1.copyTo(img_orig_matches);
            KeyPointsToPoints(imgpts1_tmp, i_pts);
            KeyPointsToPoints(imgpts2_tmp, j_pts);
            drawArrows(img_orig_matches, i_pts, j_pts, vstatus, verror, Scalar(0,0,255));
        }
        { //superimpose filtered features in green
            vector<uchar> vstatus(imgpts1_good.size(),1);
            vector<float> verror(imgpts1_good.size(),1.0);
            i_pts.resize(imgpts1_good.size());
            j_pts.resize(imgpts2_good.size());
            KeyPointsToPoints(imgpts1_good, i_pts);
            KeyPointsToPoints(imgpts2_good, j_pts);
            drawArrows(img_orig_matches, i_pts, j_pts, vstatus, verror, Scalar(0,255,0));
            imshow( "Filtered Matches", img_orig_matches );
        }
        int c = waitKey(0);
        if (c=='s') {
            imwrite("fundamental_mat_matches.png", img_orig_matches);
        }
        destroyWindow("Filtered Matches");
    }
#endif
#endif

    return F;
}

//Following Snavely07 4.2 - find how many inliers are in the Homography between 2 views
Mat FindHomography(std::vector<cv::KeyPoint> imgpts_i, std::vector<cv::KeyPoint> imgpts_j, std::vector<cv::DMatch> &matches_ij, int& inliers, int option){

    vector<cv::KeyPoint> ikpts,jkpts; vector<cv::Point2f> ipts,jpts;
    GetAlignedPointsFromMatch(imgpts_i,imgpts_j,matches_ij,ikpts,jkpts);
    KeyPointsToPoints(ikpts,ipts);
    KeyPointsToPoints(jkpts,jpts);

    double minVal,maxVal; cv::minMaxIdx(ipts,&minVal,&maxVal); // flatten point2d?? or it takes max of width and height

    vector<uchar> status;


    cv::Mat H;
    if(option == 0)
        H = cv::findHomography(ipts,jpts,status,CV_RANSAC, 0.004 * maxVal); //threshold from Snavely07
    else
        H = cv::findHomography(ipts,jpts,status,CV_LMEDS, 0.004 * maxVal); //threshold from Snavely07

    //Filter inliers based on homography
    std::vector<cv::DMatch> new_matches;
    for(int i = 0; i <  status.size(); i++){

        if(status[i] == 1){
            cv::DMatch dm;
            dm.trainIdx = matches_ij[i].trainIdx;
            dm.queryIdx = matches_ij[i].queryIdx;
            dm.imgIdx   = matches_ij[i].imgIdx;
            dm.distance = matches_ij[i].distance;

            new_matches.push_back(dm);
        }
    }

    //Set filtered inlier matches
    matches_ij = new_matches;

    inliers = cv::countNonZero(status);

    return H; //number of inliers

}



bool FindPoseEstimation(cv::Mat_<double>& rvec,
                        cv::Mat_<double>& t,
                        cv::Mat_<double>& R,
                        cv::Mat K,
                        cv::Mat& distortion_coeff,
                        std::vector<cv::Point3f> ppcloud,
                        std::vector<cv::Point2f> imgPoints){

    int limit = 10;

    if(ppcloud.size() < limit || imgPoints.size() < limit || ppcloud.size() != imgPoints.size()) {
        //something went wrong aligning 3D to 2D points..
        cerr << "couldn't find [enough] corresponding cloud points... (only " << ppcloud.size() << ")" <<endl;
        return false;
    }

    vector<int> inliers;

    //use CPU
    double minVal,maxVal; cv::minMaxIdx(imgPoints,&minVal,&maxVal);

    CV_PROFILE("solvePnPRansac",cv::solvePnPRansac(ppcloud,
                                                   imgPoints,
                                                   K,
                                                   distortion_coeff,
                                                   rvec,
                                                   t,
                                                   false,
                                                   1000,
                                                   0.006 * maxVal,
                                                   //3,
                                                   0.25 * (double)(imgPoints.size()),
                                                   inliers,
                                                   CV_EPNP);)/*/
    CV_PROFILE("solvePnP",cv::solvePnP(ppcloud, imgPoints, K, distortion_coeff, rvec, t, false, CV_EPNP);)//*/

    vector<cv::Point2f> projected3D;
    cv::projectPoints(ppcloud, rvec, t, K, distortion_coeff, projected3D);


    float val = 0.0;
    if(inliers.size()==0) { //get inliers
        //for(int i=0;i<projected3D.size();i++) {
        //    val += (norm(projected3D[i]-imgPoints[i]));
        //}
        //val /= projected3D.size();
        for(int i = 0; i < projected3D.size(); i++){

            if(norm(projected3D[i]-imgPoints[i]) < 6.0/*val + val / 10.0*/ )
                inliers.push_back(i);
        }

    }


    //std::cout << K << std::endl;
#if 0
    //display reprojected points and matches
    cv::Mat reprojected = imread("../Synthetics/P5161063.JPG");// imgs_orig[working_view].copyTo(reprojected);
    //cv::Mat reprojected = imread("../Images/IMG_4930.jpg");// imgs_orig[working_view].copyTo(reprojected);

    for(int ppt=0;ppt<imgPoints.size();ppt++) {
        cv::circle(reprojected, imgPoints[ppt], 2, cv::Scalar(255,0,0), CV_FILLED);
        cv::circle(reprojected, projected3D[ppt], 2, cv::Scalar(0,255,0), CV_FILLED);
    }
    for (int ppt=0; ppt<inliers.size(); ppt++) {
        cv::circle(reprojected, imgPoints[inliers[ppt]], 2, cv::Scalar(255,255,0), CV_FILLED);
    }

    for(int ppt=0;ppt<imgPoints.size();ppt++) {
        cv::line(reprojected,imgPoints[ppt],projected3D[ppt],cv::Scalar(0,0,255),1);
    }
    for (int ppt=0; ppt<inliers.size(); ppt++) {
        cv::line(reprojected,imgPoints[inliers[ppt]],projected3D[inliers[ppt]],cv::Scalar(0,0,255),1);
    }

    stringstream ss; ss << "inliers " << inliers.size() << " / " << projected3D.size();
    putText(reprojected, ss.str(), cv::Point(5,20), CV_FONT_HERSHEY_PLAIN, 1.0, cv::Scalar(0,255,255), 2);

    cv::imshow("__tmp", reprojected);
    cv::waitKey(0);
    cv::destroyWindow("__tmp");
#endif
    //cv::Rodrigues(rvec, R);
    //visualizerShowCamera(R,t,0,255,0,0.1);

    if(inliers.size() < limit//(double)(imgPoints.size()) / 5.0//*/
            ) {
#ifdef GEO_DEBUG
        cerr << "not enough inliers to consider a good pose ("<<inliers.size()<<"/"<<imgPoints.size()<<")"<< endl;
#endif
        return false;
    }

    if(cv::norm(t) > 200.0) {
        // this is bad...
        cerr << "estimated camera movement is too big, skip this camera\r\n";
        return false;
    }

    cv::Rodrigues(rvec, R);
    if(!CheckCoherentRotation(R)) {
        cerr << "rotation is incoherent. we should try a different base view..." << endl;
        return false;
    }

    //std::cout << "found t = " << t << "\nR = \n"<<R<<std::endl;
    return true;
}

bool FindPoseEstimationPriori(cv::Mat_<double>& t,
                              cv::Mat_<double>& R,
                              cv::Mat K,
                              cv::Mat& distortion_coeff,
                              std::vector<cv::Point3f> &ppcloud,
                              std::vector<cv::Point2f> &imgPoints){

    int limit = 12;
    cv::Mat_<double> rvec(0.0,0.0,0.0);

    if(ppcloud.size() < limit || imgPoints.size() < limit || ppcloud.size() != imgPoints.size()) {
        //something went wrong aligning 3D to 2D points..
        //cerr << "couldn't find [enough] corresponding cloud points... (only " << ppcloud.size() << ")" <<endl;
        return false;
    }

    vector<int> inliers;

    //use CPU
    double minVal,maxVal; cv::minMaxIdx(imgPoints,&minVal,&maxVal);

    CV_PROFILE("solvePnPRansac",cv::solvePnPRansac(ppcloud,
                                                   imgPoints,
                                                   K,
                                                   distortion_coeff,
                                                   rvec,
                                                   t,
                                                   false,
                                                   1000,
                                                   0.006 * maxVal,
                                                   //3,
                                                   0.25 * (double)(imgPoints.size()),
                                                   inliers,
                                                   CV_P3P);)/*/
    CV_PROFILE("solvePnP",cv::solvePnP(ppcloud, imgPoints, K, distortion_coeff, rvec, t, false, CV_EPNP);)//*/

    vector<cv::Point2f> projected3D;
    cv::projectPoints(ppcloud, rvec, t, K, distortion_coeff, projected3D);


    float val = 0.0;
    if(inliers.size()==0) { //get inliers
        //for(int i=0;i<projected3D.size();i++) {
        //    val += (norm(projected3D[i]-imgPoints[i]));
        //}
        //val /= projected3D.size();
        for(int i = 0; i < projected3D.size(); i++){

            if(norm(projected3D[i]-imgPoints[i]) < 6.0/*val + val / 10.0*/ )
                inliers.push_back(i);
        }

    }

    std::vector<cv::Point2f> inliers2D;
    std::vector<cv::Point3f> inliers3D;
    for(int i = 0; i < inliers.size();i++){
        inliers2D.push_back(imgPoints[inliers[i]]);
        inliers3D.push_back(ppcloud[inliers[i]]);
    }

    imgPoints = inliers2D;
    ppcloud = inliers3D;


    //std::cout << K << std::endl;
#if 0
    //display reprojected points and matches
    cv::Mat reprojected = imread("../Synthetics/IMG_4913.JPG");// imgs_orig[working_view].copyTo(reprojected);
    //cv::Mat reprojected = imread("../Images/IMG_4930.jpg");// imgs_orig[working_view].copyTo(reprojected);

    for(int ppt=0;ppt<imgPoints.size();ppt++) {
        cv::circle(reprojected, imgPoints[ppt], 2, cv::Scalar(255,0,0), CV_FILLED);
        cv::circle(reprojected, projected3D[ppt], 2, cv::Scalar(0,255,0), CV_FILLED);
    }
    for (int ppt=0; ppt<inliers.size(); ppt++) {
        cv::circle(reprojected, imgPoints[inliers[ppt]], 2, cv::Scalar(255,255,0), CV_FILLED);
    }

    for(int ppt=0;ppt<imgPoints.size();ppt++) {
        cv::line(reprojected,imgPoints[ppt],projected3D[ppt],cv::Scalar(0,0,255),1);
    }
    for (int ppt=0; ppt<inliers.size(); ppt++) {
        cv::line(reprojected,imgPoints[inliers[ppt]],projected3D[inliers[ppt]],cv::Scalar(0,0,255),1);
    }

    stringstream ss; ss << "inliers " << inliers.size() << " / " << projected3D.size();
    putText(reprojected, ss.str(), cv::Point(5,20), CV_FONT_HERSHEY_PLAIN, 1.0, cv::Scalar(0,255,255), 2);

    cv::imshow("__tmp", reprojected);
    cv::waitKey(0);
    cv::destroyWindow("__tmp");
#endif
    //cv::Rodrigues(rvec, R);
    //visualizerShowCamera(R,t,0,255,0,0.1);

    if(inliers.size() < limit) {
#ifdef GEO_DEBUG
        cerr << "not enough inliers to consider a good pose ("<<inliers.size()<<"/"<<imgPoints.size()<<")"<< endl;
#endif
        return false;
    }

    if(cv::norm(t) > 200.0) {
        // this is bad...
        cerr << "estimated camera movement is too big, skip this camera\r\n";
        return false;
    }

    cv::Rodrigues(rvec, R);
    if(!CheckCoherentRotation(R)) {
        cerr << "rotation is incoherent. we should try a different base view..." << endl;
        return false;
    }

    //std::cout << "found t = " << t << "\nR = \n"<<R<<std::endl;
    return true;
}



bool FindCameraMatrices(TwoVGeo tvg,
                        const Mat& K,
                        const Mat& Kinv,
                        const Mat& distcoeff,
                        const vector<KeyPoint>& imgpts1,
                        const vector<KeyPoint>& imgpts2,
                        Matx34d& P,
                        Matx34d& P1,
                        vector<DMatch>& matches,
                        vector<CloudPoint>& outCloud){
    //Find camera matrices
    {
        cout << "Find camera matrices...";
        double t = getTickCount();

        vector<KeyPoint> imgpts1_good;
        vector<KeyPoint> imgpts2_good;
        GetAlignedPointsFromMatch(imgpts1,imgpts2,matches,imgpts1_good,imgpts2_good);


        if(tvg.n_inliers < 100) { // || ((double)imgpts1_good.size() / (double)imgpts1.size()) < 0.25
#ifdef GEO_DEBUG
            cerr << "not enough inliers after F matrix" << endl;
#endif
            return false;
        }
        std::cout << tvg.F << std::endl;

        //Essential matrix: compute then extract cameras [R|t]
        //These K params have to be different to compute de E matrix from different cameras intrisic parameters!!!!!!!!!!
        tvg.E = K.t() * tvg.F * K; //according to HZ (9.12)

        //according to http://en.wikipedia.org/wiki/Essential_matrix#Properties_of_the_essential_matrix
        if(fabsf(determinant(tvg.E)) > 1e-07) {
            cout << "det(E) != 0 : " << determinant(tvg.E) << "\n";
            P1 = 0;
            return false;
        }

        //Allocate space for the Rotation, translation matrices
        Mat_<double> R1(3,3);
        Mat_<double> R2(3,3);
        Mat_<double> t1(1,3);
        Mat_<double> t2(1,3);

        //decompose E to P' , HZ (9.19)
        {
            if (!DecomposeEtoRandT(tvg.E,R1,R2,t1,t2)) return false;

            if(determinant(R1)+1.0 < 1e-09) {
                //according to http://en.wikipedia.org/wiki/Essential_matrix#Showing_that_it_is_valid
                cout << "det(R) == -1 ["<<determinant(R1)<<"]: flip E's sign" << endl;
                tvg.E = - tvg.E;
                DecomposeEtoRandT(tvg.E,R1,R2,t1,t2);
            }
            if (!CheckCoherentRotation(R1)) {
                cout << "resulting rotation is not coherent\n";
                P1 = 0;
                return false;
            }

            P1 = Matx34d(R1(0,0),	R1(0,1),	R1(0,2),	t1(0),
                         R1(1,0),	R1(1,1),	R1(1,2),	t1(1),
                         R1(2,0),	R1(2,1),	R1(2,2),	t1(2));
            cout << "Testing P1 " << endl << Mat(P1) << endl;

            vector<CloudPoint> pcloud,pcloud1; vector<KeyPoint> corresp;
            double reproj_error1 = TriangulatePoints(imgpts1_good, imgpts2_good, K, Kinv, distcoeff, P, P1, pcloud);
            double reproj_error2 = TriangulatePoints(imgpts2_good, imgpts1_good, K, Kinv, distcoeff, P1, P, pcloud1);
            vector<uchar> tmp_status;
            //check if pointa are triangulated --in front-- of cameras for all 4 ambiguations
            if (!TestTriangulation(pcloud,P1,tmp_status) || !TestTriangulation(pcloud1,P,tmp_status) || reproj_error1 > 100.0 || reproj_error2 > 100.0) {
                P1 = Matx34d(R1(0,0),	R1(0,1),	R1(0,2),	t2(0),
                             R1(1,0),	R1(1,1),	R1(1,2),	t2(1),
                             R1(2,0),	R1(2,1),	R1(2,2),	t2(2));
                cout << "Testing P1 "<< endl << Mat(P1) << endl;

                pcloud.clear(); pcloud1.clear(); corresp.clear();
                reproj_error1 = TriangulatePoints(imgpts1_good, imgpts2_good, K, Kinv, distcoeff, P, P1, pcloud);
                reproj_error2 = TriangulatePoints(imgpts2_good, imgpts1_good, K, Kinv, distcoeff, P1, P, pcloud1);

                if (!TestTriangulation(pcloud,P1,tmp_status) || !TestTriangulation(pcloud1,P,tmp_status) || reproj_error1 > 100.0 || reproj_error2 > 100.0) {
                    if (!CheckCoherentRotation(R2)) {
                        cout << "resulting rotation is not coherent\n";
                        P1 = 0;
                        return false;
                    }

                    P1 = Matx34d(R2(0,0),	R2(0,1),	R2(0,2),	t1(0),
                                 R2(1,0),	R2(1,1),	R2(1,2),	t1(1),
                                 R2(2,0),	R2(2,1),	R2(2,2),	t1(2));
                    cout << "Testing P1 "<< endl << Mat(P1) << endl;

                    pcloud.clear(); pcloud1.clear(); corresp.clear();
                    reproj_error1 = TriangulatePoints(imgpts1_good, imgpts2_good, K, Kinv, distcoeff, P, P1, pcloud);
                    reproj_error2 = TriangulatePoints(imgpts2_good, imgpts1_good, K, Kinv, distcoeff, P1, P, pcloud1);

                    if (!TestTriangulation(pcloud,P1,tmp_status) || !TestTriangulation(pcloud1,P,tmp_status) || reproj_error1 > 100.0 || reproj_error2 > 100.0) {
                        P1 = Matx34d(R2(0,0),	R2(0,1),	R2(0,2),	t2(0),
                                     R2(1,0),	R2(1,1),	R2(1,2),	t2(1),
                                     R2(2,0),	R2(2,1),	R2(2,2),	t2(2));
                        cout << "Testing P1 "<< endl << Mat(P1) << endl;

                        pcloud.clear(); pcloud1.clear(); corresp.clear();
                        reproj_error1 = TriangulatePoints(imgpts1_good, imgpts2_good, K, Kinv, distcoeff, P, P1, pcloud);
                        reproj_error2 = TriangulatePoints(imgpts2_good, imgpts1_good, K, Kinv, distcoeff, P1, P, pcloud1);

                        if (!TestTriangulation(pcloud,P1,tmp_status) || !TestTriangulation(pcloud1,P,tmp_status) || reproj_error1 > 100.0 || reproj_error2 > 100.0) {
                            cout << "Shit." << endl;
                            return false;
                        }
                    }
                }
            }
            for (unsigned int i=0; i<pcloud.size(); i++) {
                outCloud.push_back(pcloud[i]);
            }
        }

        t = ((double)getTickCount() - t)/getTickFrequency();
        cout << "Done. (" << t <<"s)"<< endl;
    }
    return true;
}


