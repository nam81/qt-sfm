#pragma once

#include <vector>

class Calibration
{
public:
    Calibration() {}

    int computeCalibrationParams(int argc, char* pathToChess);

};

