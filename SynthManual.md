### Synthetic Views

To build synthetic views based on a structure from motion model run:

```
	./GeocodeUI s build <path_to_cloud(string)> <location(string)>
```

If the photographs contained by the original model have the associated GPS coordinates, a transformation matrix is computed for the synthetics generated on that model. This matrix is then used to compute the GPS coordinates of images to geocode. The GPS coordinates can be injected to each model photograph (using the standard exif GPS tags), or by composing a file named 'coordinates.txt' with the following structure. This file needs to be located within the model folder.

```
[Number of Images]
[Image1] [Latitude] [Longitude] [Altitude]
[Image2] [Latitude] [Longitude] [Altitude]
[Image3] [Latitude] [Longitude] [Altitude]
    .
    .
    .
[ImageX] [Latitude] [Longitude] [Altitude]
```
Images which the GPS is unknown must be filled with -200 -200 -200. This is a naive identifier to indicate which photographs aren't geocoded. Further implemenation versions will not require this identifier.

Building synthetic views will create, 3D documents which are stored in the synthetic's database at the SyntheticOutput folder. After compressing enough models, all the computed synthetic views can be added to the Vocabulary Tree by running:

```
	./GeocodeUI s learn
```

Remember to add the computed synthetic views before trying to geocode any photograph, or the process won't work since the vocabulary tree doesn't contain the needed descriptors to identify the top matches.
If you by mistake added a model to the vocabulary tree, the following removes models from both database and vocabulary tree:

```
       ./GeocodeUI s rem <model_id>
```

To check model_id's, run:

```
      ./GeocodeUI s models
```

After feeding the vocabulary tree with the generated 3D documents, the prototype is ready to geocode new photographs. All the photographs to geocode need to be within the same folder and that folder needs to contain a .txt with the list of images to pose:

```
[Image_name1]
[Image_name2]
[Image_name3]
      .
      .
      .
[Image_nameX]
``` 

Then you simply need to run:

```
	./GeocodeUI s pose <path_imgs_list>
```

After the geocoding process is finished, it will be outputed two files: 'output_statistics.txt' containing overall information of geocoded images, and 'output_viz.txt' which can be used to visualize the geocoded images placed within the 3D models. To run the output visualization, run:

```
	./GeocodeUI g <path_to_outputviz>
```

The PCL visualizer will be opened and 3D models may be explored. To change to the next model just type a letter and press "Enter" within the terminal. To quit the visualizer press 'q'.

Currently this prototype is pose estimating images within 300-400ms on an Intel Core i5-4200U (1.6GHz) with nVidia 820m graphic card.
